package com.wisdom.system.entity.base;

import java.util.List;

/**
 * PageResult 分页实体
 */

public class PageResult<T> {
    private List<T> list;

    private Long total;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public PageResult() {
    }

    public PageResult(List list, Long total) {
        this.list = list;
        this.total = total;
    }
}
