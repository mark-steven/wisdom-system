package com.wisdom.system.entity.job;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Table(name="JOB_LOG")
public class JobLog extends BaseEntity {

    @Column(name="FK_JOB_LOG_CODE")
    @NotBlank(message="调度日志类型不能为空！")
    private String fkJobLogCode;

    public String getFkJobLogCode(){
        return this.fkJobLogCode;
    }

    public void setFkJobLogCode(String fkJobLogCode){
        this.fkJobLogCode=fkJobLogCode;
    }

    // 调度状态(1.执行前，2.执行完成时)
    @Column(name="STATE")
    private int state;

    public int getState(){
        return this.state;
    }

    public void setState(int state){
        this.state=state;
    }

    @Column(name="ADD_TIME")
    private Date addTime;

    public Date getAddTime(){
        return this.addTime;
    }

    public void setAddTime(Date addTime){
        this.addTime=addTime;
    }

    @Column(name="CONTENT")
    @NotBlank(message="调度内容不能为空！")
    private String content;

    public String getContent(){
        return this.content;
    }

    public void setContent(String content){
        this.content=content;
    }

    //调度日志类型
    @Transient
    private String jobLogType;

    public String getJobLogType() {
        return jobLogType;
    }

    public void setJobLogType(String jobLogType) {
        this.jobLogType = jobLogType;
    }
}



