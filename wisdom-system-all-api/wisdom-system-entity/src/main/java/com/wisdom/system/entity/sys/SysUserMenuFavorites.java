package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "SYS_USER_MENU_FAVORITES")
public class SysUserMenuFavorites extends BaseEntity {

    //用户id
    @Column(name = "FK_USER_ID")
    private String fkUserId;

    public String getFkUserId() {
        return fkUserId;
    }

    public void setFkUserId(String fkUserId) {
        this.fkUserId = fkUserId;
    }

    //菜单id
    @Column(name = "FK_MENU_ID")
    private String fkMenuId;

    public String getFkMenuId() {
        return fkMenuId;
    }

    public void setFkMenuId(String fkMenuId) {
        this.fkMenuId = fkMenuId;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    //显示所属系统
    @Column(name = "FK_SYSTEM")
    private String fkSystem;

    // 菜单所属系统
    @Transient
    private String fkSystemCode;

    public String getFkSystemCode() {
        return fkSystemCode;
    }

    public void setFkSystemCode(String fkSystemCode) {
        this.fkSystemCode = fkSystemCode;
    }

    @Transient
    private String fkMenuName;

    @Transient
    private String fkMenuUrl;

    public String getFkMenuName() {
        return fkMenuName;
    }

    public void setFkMenuName(String fkMenuName) {
        this.fkMenuName = fkMenuName;
    }

    public String getFkMenuUrl() {
        return fkMenuUrl;
    }

    public void setFkMenuUrl(String fkMenuUrl) {
        this.fkMenuUrl = fkMenuUrl;
    }

    public String getFkSystem() {
        return fkSystem;
    }

    public void setFkSystem(String fkSystem) {
        this.fkSystem = fkSystem;
    }
}
