package com.wisdom.system.entity.sys;

import java.util.List;

/**
 * 系统用户权限树表
 */
public class SysMenuTree {

    /**
     * 唯一标识ID
     */
    private String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * 父节点Id
     */
    private String pid;

    public String getPid() {
        return this.pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * 菜单名称
     */
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 显示图标
     */
    private String icon;

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 显示图标
     */
    private String hasIcon;

    public String getHasIcon() {
        return this.hasIcon;
    }

    public void setHasIcon(String hasIcon) {
        this.hasIcon = hasIcon;
    }

    /**
     * 是否默认展开
     */
    private boolean spread;

    public boolean getSpread() {
        return this.spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    /**
     * 菜单地址
     */
    private String href;

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 子菜单
     */
    private List<SysMenuTree> children;

    public List<SysMenuTree> getChildren() {
        return this.children;
    }

    public void setChildren(List<SysMenuTree> children) {
        this.children = children;
    }

    /**
     * 菜单所属系统
     */
    private String fkSystem;

    public String getFkSystem() {
        return fkSystem;
    }

    public void setFkSystem(String fkSystem) {
        this.fkSystem = fkSystem;
    }
}
