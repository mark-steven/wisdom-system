package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "SYS_DEPARTMENT")
public class SysDepartment extends BaseEntity {

    //部门编码
    @Column(name = "CODE")
    @Length(min = 0,max = 64,message = "编码不能超过64")
    @NotBlank(message = "部门编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //部门名称
    @Column(name = "NAME")
    @NotBlank(message = "部门名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //部门地址
    @Column(name = "ADDRESS")
    private String address;

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //部门详情
    @Column(name = "DESCR")
    private String descr;

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    //状态
    @Column(name = "STATE")
    private int state;

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

    //拼音首码
    @Column(name = "PY_CODE")
    private String pyCode;

    public String getPyCode() {
        return this.pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    //五笔码
    @Column(name = "WB_CODE")
    private String wbCode;

    public String getWbCode() {
        return this.wbCode;
    }

    public void setWbCode(String wbCode) {
        this.wbCode = wbCode;
    }

    //创建时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //创建人
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    //排序
    @Column(name="SORT")
    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    //公司编码
    @Column(name = "FK_DEPT_TYPE_CODE")
    private String FkDeptTypeCode;

    public String getFkDeptTypeCode() {
        return FkDeptTypeCode;
    }

    public void setFkDeptTypeCode(String fkDeptTypeCode) {
        FkDeptTypeCode = fkDeptTypeCode;
    }

    //公司名称
    @Transient
    private String deptTypeName;

    public String getDeptTypeName() {
        return deptTypeName;
    }

    public void setDeptTypeName(String deptTypeName) {
        this.deptTypeName = deptTypeName;
    }
}


