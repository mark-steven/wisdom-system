package com.wisdom.system.entity.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wisdom.system.entity.base.BaseEntity;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "SYS_USER")
public class SysUser extends BaseEntity {

    //用户姓名
    @Column(name = "NAME")
    @NotBlank(message = "姓名不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //登录账号
    @Column(name = "LOGIN_CODE")
    @NotBlank(message = "账号不能为空！")
    private String loginCode;

    public String getLoginCode() {
        return this.loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    //用户密码
    @Column(name = "PASSWORD")
    private String password;

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //用户邮箱
    @Email(message = "邮箱格式不正确")
    @Column(name = "EMAIL")
    private String email;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //手机号码
    @Column(name = "PHONE")
    private String phone;

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    //性别
    @Column(name = "SEX")
    @NotNull(message = "性别不能为空！")
    private Integer sex;

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    //用户状态
    @Column(name = "STATE")
    private int state;

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

    //部门编码
    @Column(name = "FK_DEPARTMENT_CODE")
    private String fkDepartmentCode;

    public String getFkDepartmentCode() {
        return fkDepartmentCode;
    }

    public void setFkDepartmentCode(String fkDepartmentCode) {
        this.fkDepartmentCode = fkDepartmentCode;
    }

    @Transient
    private String departmentName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    //拼音首码
    @Column(name = "PY_CODE")
    private String pyCode;

    public String getPyCode() {
        return this.pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    //五笔码
    @Column(name = "WB_CODE")
    private String wbCode;

    public String getWbCode() {
        return this.wbCode;
    }

    public void setWbCode(String wbCode) {
        this.wbCode = wbCode;
    }

    //注册时间
    @CreatedDate
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //添加用户人
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    //修改时间
    @Column(name = "MODIFY_TIME")
    private Date modifyTime;

    public Date getModifyTime() {
        return this.modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    //修改人
    @Column(name = "MODIFY_USER_ID")
    private String modifyUserId;

    public String getModifyUserId() {
        return this.modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    //岗位编码
    @Column(name = "FK_JOB_CODE")
    private String fkJobCode;

    //岗位名称
    @Transient
    private String jobName;

    public String getFkJobCode() {
        return fkJobCode;
    }

    public void setFkJobCode(String fkJobCode) {
        this.fkJobCode = fkJobCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
}


