package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "SYS_SYSTEM_LOG")
public class SysSystemLog extends BaseEntity {
    //IP地址
    @Column(name = "IP_ADDRESS")
    private String ipAddress;

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    //请求路由
    @Column(name = "REQUEST_ROUTING")
    private String requestRouting;

    public String getRequestRouting() {
        return this.requestRouting;
    }

    public void setRequestRouting(String requestRouting) {
        this.requestRouting = requestRouting;
    }

    //参数
    @Column(name = "PARAM")
    private String param;

    public String getParam() {
        return this.param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    //请求类型编码
    @Column(name = "REQUEST_TYPE_CODE")
    private String requestTypeCode;

    public String getRequestTypeCode() {
        return this.requestTypeCode;
    }

    public void setRequestTypeCode(String requestTypeCode) {
        this.requestTypeCode = requestTypeCode;
    }

    //请求类型名称
    @Column(name = "REQUEST_TYPE_NAME")
    private String requestTypeName;

    public String getRequestTypeName() {
        return this.requestTypeName;
    }

    public void setRequestTypeName(String requestTypeName) {
        this.requestTypeName = requestTypeName;
    }

    //请求时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}



