package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "SYS_ROLE_MENU_AUTH")
public class SysRoleMenuAuth extends BaseEntity {

    //角色Id
    @Column(name = "FK_ROLE_ID")
    private String fkRoleId;

    public String getFkRoleId() {
        return fkRoleId;
    }

    public void setFkRoleId(String fkRoleId) {
        this.fkRoleId = fkRoleId;
    }

    //菜单Id
    @Column(name = "FK_MENU_ID")
    private String fkMenuId;

    public String getFkMenuId() {
        return fkMenuId;
    }

    public void setFkMenuId(String fkMenuId) {
        this.fkMenuId = fkMenuId;
    }

    //角色名称
    @Transient
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    //菜单名称
    @Transient
    private String menuName;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    //菜单状态
    @Transient
    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    //菜单url
    @Transient
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    //排序
    @Transient
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    //父节点ID
    @Transient
    private String pid;

    public String getPid() {
        return this.pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Transient
    private String menuSystemName;

    public String getMenuSystemName() {
        return menuSystemName;
    }

    public void setMenuSystemName(String menuSystemName) {
        this.menuSystemName = menuSystemName;
    }

}
