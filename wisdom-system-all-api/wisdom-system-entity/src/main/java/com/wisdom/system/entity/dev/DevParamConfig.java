package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "DEV_PARAM_CONFIG")
public class DevParamConfig extends BaseEntity {

    //参数KEY
    @Column(name = "PARAM_KEY")
    @NotBlank(message = "参数KEY不能为空！")
    private String paramKey;

    public String getParamKey() {
        return this.paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    //分类编码
    @Column(name = "FK_PARAM_CATEGORY_CODE")
    @NotBlank(message = "参数分类编码不能为空！")
    private String fkParamCategoryCode;

    public String getFkParamCategoryCode() {
        return this.fkParamCategoryCode;
    }

    public void setFkParamCategoryCode(String fkParamCategoryCode) {
        this.fkParamCategoryCode = fkParamCategoryCode;
    }

    //分类名称
    @Transient
    private String paramCategoryName;

    public String getParamCategoryName() {
        return paramCategoryName;
    }

    public void setParamCategoryName(String paramCategoryName) {
        this.paramCategoryName = paramCategoryName;
    }

    //分组编码
    @Column(name = "FK_PARAM_GROUP_CODE")
    @NotBlank(message = "参数分组编码不能为空！")
    private String fkParamGroupCode;

    public String getFkParamGroupCode() {
        return this.fkParamGroupCode;
    }

    public void setFkParamGroupCode(String fkParamGroupCode) {
        this.fkParamGroupCode = fkParamGroupCode;
    }

    //分组名称
    @Transient
    private String paramGroupName;

    public String getParamGroupName() {
        return paramGroupName;
    }

    public void setParamGroupName(String paramGroupName) {
        this.paramGroupName = paramGroupName;
    }

    //标签名称
    @Column(name = "TAG_NAME")
    @NotBlank(message = "标签名称不能为空！")
    private String tagName;

    public String getTagName() {
        return this.tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    //数据类型
    @Column(name = "TYPE")
    @NotBlank(message = "控件类型不能为空！")
    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //系统参数规则
    @Column(name = "RULE")
    private String rule;

    public String getRule() {
        return this.rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    // 值
    @Transient
    private String paramValue;

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

}


