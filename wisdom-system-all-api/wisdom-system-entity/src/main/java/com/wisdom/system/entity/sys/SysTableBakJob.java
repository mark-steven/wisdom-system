package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Table(name = "SYS_TABLE_BAK_JOB")
public class SysTableBakJob extends BaseEntity {

    // 名称
    @Column(name = "NAME")
    @NotBlank(message = "名称不能为空！")
    private String name;

    // 编码
    @Column(name = "CODE")
    @Length(min = 0, max = 64, message = "编码不能超过64")
    @NotBlank(message = "编码不能为空！")
    private String code;

    // 表名称
    @Column(name = "TABLE_NAME")
    @NotBlank(message = "备份表不能为空！")
    private String tableName;

    // 表空间
    @Column(name = "TABLE_SCHEMA")
    private String tableSchema;

    // 条件
    @Column(name = "CONDITIONS")
    @NotBlank(message = "条件不能为空！")
    private String conditions;

    // 状态，1可用，2停用
    @Column(name = "STATE")
    private int state;

    // 是否清除原表查询数据
    @Column(name = "IS_CLEAR_TABLE_DATA")
    private int isClearTableData;

    // 是否清空备份表数据
    @Column(name = "IS_CLEAR_BAK_DATA")
    private int isClearBakData;

    // 是否已执行数据
    @Column(name = "IS_EXECUTED")
    private int isExecuted;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public void setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getIsClearTableData() {
        return isClearTableData;
    }

    public void setIsClearTableData(int isClearTableData) {
        this.isClearTableData = isClearTableData;
    }

    public int getIsClearBakData() {
        return isClearBakData;
    }

    public void setIsClearBakData(int isClearBakData) {
        this.isClearBakData = isClearBakData;
    }

    public int getIsExecuted() {
        return isExecuted;
    }

    public void setIsExecuted(int isExecuted) {
        this.isExecuted = isExecuted;
    }
}
