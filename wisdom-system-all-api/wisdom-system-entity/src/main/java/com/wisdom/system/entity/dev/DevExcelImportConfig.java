package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Table(name = "DEV_EXCEL_IMPORT_CONFIG")
public class DevExcelImportConfig extends BaseEntity {

    //导入业务模块名称
    @Length(min = 0, max = 64, message = "编码不能超过64")
    @Column(name = "CODE")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //导入业务模块名称
    @Column(name = "NAME")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //表名
    @Column(name = "TABLE_NAME")
    @NotBlank(message = "表名不能为空！")
    private String tableName;

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    //开始行数
    @Column(name = "START_NUM")
    private int startNum;

    public int getStartNum() {
        return this.startNum;
    }

    public void setStartNum(int startNum) {
        this.startNum = startNum;
    }

    //结束行数
    @Column(name = "WITHOUT_END_NUM")
    private int withoutEndNum;

    public int getWithoutEndNum() {
        return this.withoutEndNum;
    }

    public void setWithoutEndNum(int withoutEndNum) {
        this.withoutEndNum = withoutEndNum;
    }

    //描述
    @Column(name = "DESCR")
    private String descr;

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Transient
    private List<DevExcelImportRule> devExcelImportRuleList;

    public List<DevExcelImportRule> getDevExcelImportRuleList() {
        return devExcelImportRuleList;
    }

    public void setDevExcelImportRuleList(List<DevExcelImportRule> devExcelImportRuleList) {
        this.devExcelImportRuleList = devExcelImportRuleList;
    }

}



