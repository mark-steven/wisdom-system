package com.wisdom.system.entity.job;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "JOB_TASK")
public class JobTask extends BaseEntity {

    //任务编码
    @Column(name = "CODE")
    @Length(min = 0,max = 64,message = "编码不能超过64")
    @NotBlank(message = "任务编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //任务名称
    @Column(name = "NAME")
    @NotBlank(message = "任务名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //调用类名称
    @Column(name = "CLAZZ_NAME")
    @NotBlank(message = "触发类不能为空！")
    private String clazzName;

    public String getClazzName() {
        return this.clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    //CRON规则编码
    @Column(name = "FK_CRON_CODE")
    @NotBlank(message = "频率(Cron)不能为空！")
    private String fkCronCode;

    public String getFkCronCode() {
        return this.fkCronCode;
    }

    public void setFkCronCode(String fkCronCode) {
        this.fkCronCode = fkCronCode;
    }

    //cron 表达式
    @Transient
    private String fkCronExpression;

    public String getFkCronExpression() {
        return fkCronExpression;
    }

    public void setFkCronExpression(String fkCronExpression) {
        this.fkCronExpression = fkCronExpression;
    }

    //状态：0停止；1启用
    @Column(name = "STATE")
    private int state;

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

    //说明
    @Column(name = "DESCR")
    private String descr;

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    //添加时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //添加用户
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    //cron 名称
    @Transient
    private String fkCronName;

    public String getFkCronName() {
        return fkCronName;
    }

    public void setFkCronName(String fkCronName) {
        this.fkCronName = fkCronName;
    }

}



