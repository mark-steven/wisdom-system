package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SYS_DICT_CATEGORY")
public class SysDictCategory extends BaseEntity implements Serializable {

    //字典编码
    @Column(name = "CODE")
    @Length(min = 0,max = 64,message = "编码不能超过64")
    @NotBlank(message = "编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //字典名称
    @Column(name = "NAME")
    @NotBlank(message = "名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //关联项展示方式：0 列表方式；1 树层级展示
    @Column(name = "ITEM_SHOW_TYPE")
    private int itemShowType;

    public int getItemShowType() {
        return this.itemShowType;
    }

    public void setItemShowType(int itemShowType) {
        this.itemShowType = itemShowType;
    }

    //创建人
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    //创建时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        if (this.addTime == null) {
            this.addTime = new Date();
        }
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //修改时间
    @Column(name = "MODIFY_TIME")
    private Date modifyTime;

    public Date getModifyTime() {
        return this.modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    //修改人
    @Column(name = "MODIFY_USER_ID")
    private String modifyUserId;

    public String getModifyUserId() {
        return this.modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

}


