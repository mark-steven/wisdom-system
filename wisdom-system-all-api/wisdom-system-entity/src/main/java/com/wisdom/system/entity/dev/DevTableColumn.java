package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

public class DevTableColumn extends BaseEntity {

    //字段名
    private String name;

    //备注
    private String comment;

    //类型
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
