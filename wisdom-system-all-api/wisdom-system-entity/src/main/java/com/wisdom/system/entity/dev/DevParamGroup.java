package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "DEV_PARAM_GROUP")
public class DevParamGroup extends BaseEntity {

    //分组编码
    @Column(name = "CODE")
    @Length(min = 0,max = 64,message = "编码不能超过64")
    @NotBlank(message = "分组编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //所属分类编码
    @Column(name = "FK_PARAM_CATEGORY_CODE")
    @NotBlank(message = "所属分类编码不能为空！")
    private String fkParamCategoryCode;

    public String getFkParamCategoryCode() {
        return this.fkParamCategoryCode;
    }

    public void setFkParamCategoryCode(String fkParamCategoryCode) {
        this.fkParamCategoryCode = fkParamCategoryCode;
    }

    //所属分类名称
    @Transient
    private String paramCategoryName;

    public String getParamCategoryName() {
        return paramCategoryName;
    }

    public void setParamCategoryName(String paramCategoryName) {
        this.paramCategoryName = paramCategoryName;
    }

    //分组名称
    @Column(name = "NAME")
    @NotBlank(message = "分组名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

}


