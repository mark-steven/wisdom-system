package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "SYS_DICT_ITEM")
public class SysDictItem extends BaseEntity {

    //字典项编码
    @Column(name = "CODE")
    @Length(min = 0, max = 64, message = "编码不能超过64")
    @NotBlank(message = "项编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //字典项名称
    @Column(name = "NAME")
    @NotBlank(message = "字典项名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //字典项值
    @Column(name = "ITEM_VALUE")
    @NotBlank(message = "字典项值不能为空！")
    private String itemValue;

    public String getItemValue() {
        return this.itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    //拼音码
    @Column(name = "PY_CODE")
    private String pyCode;

    public String getPyCode() {
        return this.pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    //五笔码
    @Column(name = "WB_CODE")
    private String wbCode;

    public String getWbCode() {
        return this.wbCode;
    }

    public void setWbCode(String wbCode) {
        this.wbCode = wbCode;
    }

    //父节点ID
    @Column(name = "PID")
    private String pid;

    public String getPid() {
        return this.pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    //字典分类编码
    @Column(name = "FK_DICT_CATEGORY_CODE")
    private String fkDictCategoryCode;

    public String getFkDictCategoryCode() {
        return this.fkDictCategoryCode;
    }

    public void setFkDictCategoryCode(String fkDictCategoryCode) {
        this.fkDictCategoryCode = fkDictCategoryCode;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    //创建人
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    //创建时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //修改时间
    @Column(name = "MODIFY_TIME")
    private Date modifyTime;

    public Date getModifyTime() {
        return this.modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    //修改人
    @Column(name = "MODIFY_USER_ID")
    private String modifyUserId;

    public String getModifyUserId() {
        return this.modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    @Transient
    private String pname;

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

}


