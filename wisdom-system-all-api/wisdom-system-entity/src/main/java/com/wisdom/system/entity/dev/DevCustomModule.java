package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Table(name = "DEV_CUSTOM_MODULE")
public class DevCustomModule extends BaseEntity {

    /**
     * 系统模块编码
     */
    @Column(name = "CODE")
    @NotBlank(message = "模块编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 系统模块名称
     */
    @Column(name = "NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "CONFIG_CONTENT")
    private String configContent;
    public String getConfigContent() {
        return configContent;
    }

    public void setConfigContent(String configContent) {
        this.configContent = configContent;
    }

    /**
     * 描述
     */
    @Column(name = "DESCR")
    private String descr;

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * 排序
     */
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    /**
     * 拼音首码
     */
    @Column(name = "PY_CODE")
    private String pyCode;

    public String getPyCode() {
        return this.pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    /**
     * 五笔码
     */
    @Column(name = "WB_CODE")
    private String wbCode;

    public String getWbCode() {
        return this.wbCode;
    }

    public void setWbCode(String wbCode) {
        this.wbCode = wbCode;
    }

    /**
     * 注册时间
     */
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 添加用户人
     */
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    /**
     * 部门编码
     */
    @Column(name = "FK_DEPT_CODE")
    private String fkDeptCode;

    public String getFkDeptCode() {
        return fkDeptCode;
    }

    public void setFkDeptCode(String fkDeptCode) {
        this.fkDeptCode = fkDeptCode;
    }

    /**
     * 部门名称
     */
    @Transient
    private String fkDeptName;

    public String getFkDeptName() {
        return fkDeptName;
    }

    public void setFkDeptName(String fkDeptName) {
        this.fkDeptName = fkDeptName;
    }


}



