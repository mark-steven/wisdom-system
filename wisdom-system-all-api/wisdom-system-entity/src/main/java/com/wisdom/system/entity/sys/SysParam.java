package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Table(name = "SYS_PARAM")
public class SysParam extends BaseEntity {

    //参数KEY
    @Column(name = "PARAM_KEY")
    @NotBlank(message = "参数KEY不能为空！")
    private String paramKey;

    public String getParamKey() {
        return this.paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    //参数值
    @Column(name = "PARAM_VALUE")
    @NotBlank(message = "参数值不能为空！")
    private String paramValue;

    public String getParamValue() {
        return this.paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

}


