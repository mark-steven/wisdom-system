package com.wisdom.system.entity.sys;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Table(name = "SYS_MENU")
public class SysMenu extends BaseEntity {

    //名称
    @Column(name = "NAME")
    @NotBlank(message = "菜单名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //菜单路径
    @Column(name = "URL")
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    //父节点ID
    @Column(name = "PID")
    private String pid;

    public String getPid() {
        return this.pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Column(name = "STATE")
    private int state;

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @Transient
    private String pname;

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

    //修改时间
    @Column(name = "MODIFY_TIME")
    private Date modifyTime;

    public Date getModifyTime() {
        return this.modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    //修改人
    @Column(name = "MODIFY_USER_ID")
    private String modifyUserId;

    public String getModifyUserId() {
        return this.modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    //所属系统
    @Column(name = "FK_SYSTEM")
    @NotBlank(message = "所属系统不能为空！")
    private String fkSystem;

    public String getFkSystem() {
        return this.fkSystem;
    }

    public void setFkSystem(String fkSystem) {
        this.fkSystem = fkSystem;
    }

    // 所属系统名称
    @Transient
    private String fkSystemName;

    public String getFkSystemName() {
        return this.fkSystemName;
    }

    public void setFkSystemName(String fkSystemName) {
        this.fkSystemName = fkSystemName;
    }
    
}


