package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "DEV_PARAM_CATEGORY")
public class DevParamCategory extends BaseEntity {

    //分类编码
    @Column(name = "CODE")
    @Length(min = 0, max = 64, message = "编码不能超过64")
    @NotBlank(message = "分类编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //分类名称
    @Column(name = "NAME")
    @NotBlank(message = "分类名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

}


