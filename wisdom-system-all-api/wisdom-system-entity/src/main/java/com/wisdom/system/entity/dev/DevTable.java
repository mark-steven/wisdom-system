package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

public class DevTable extends BaseEntity {

    //表名
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //备注
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
