package com.wisdom.system.entity.job;

import com.wisdom.system.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "JOB_CRON")
public class JobCron extends BaseEntity {

    //编码
    @Column(name = "CODE")
    @Length(min = 0, max = 100, message = "编码不能超过100")
    @NotBlank(message = "编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //名称
    @Column(name = "NAME")
    @Length(min = 0, max = 100, message = "名称不能超过100")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //cron表达式
    @Column(name = "CRON_EXPRESSION")
    @Length(min = 0, max = 100, message = "cron表达式不能超过100")
    @NotBlank(message = "cron表达式不能为空！")
    private String cronExpression;

    public String getCronExpression() {
        return this.cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    //配置还原
    @Column(name = "CONFIG_RESTORE")
    private String configRestore;

    public String getConfigRestore() {
        return this.configRestore;
    }

    public void setConfigRestore(String configRestore) {
        this.configRestore = configRestore;
    }

    //添加时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //添加用户
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

}



