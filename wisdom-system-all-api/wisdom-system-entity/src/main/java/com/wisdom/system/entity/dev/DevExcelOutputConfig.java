package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 报表配置实体
 */
@Table(name = "DEV_EXCEL_OUTPUT_CONFIG")
public class DevExcelOutputConfig extends BaseEntity {

    //excel编码
    @Column(name = "CODE")
    @NotBlank(message = "excel编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //excel名称
    @Column(name = "NAME")
    @NotBlank(message = "excel名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //查询语句配置
    @Column(name = "SQL_CONFIG_CONTENT")
    private String sqlConfigContent;

    public String getSqlConfigContent() {
        return this.sqlConfigContent;
    }

    public void setSqlConfigContent(String sqlConfigContent) {
        this.sqlConfigContent = sqlConfigContent;
    }

    //标题名
    @Column(name = "TITLE")
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    //脚注
    @Column(name = "SHEET")
    private String sheet;

    public String getSheet() {
        return this.sheet;
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    //描述
    @Column(name = "DESCR")
    private String descr;

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    //拼音首码
    @Column(name = "PY_CODE")
    private String pyCode;

    public String getPyCode() {
        return this.pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    //五笔码
    @Column(name = "WB_CODE")
    private String wbCode;

    public String getWbCode() {
        return this.wbCode;
    }

    public void setWbCode(String wbCode) {
        this.wbCode = wbCode;
    }

    //创建时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //创建人
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }

}


