package com.wisdom.system.entity.per;

import com.wisdom.system.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 考勤管理
 *
 * @author ghh
 * 20点50分
 */
@Table(name = "ATT_MANAGEMENT")
public class Attmanagement extends BaseEntity {

    //考勤号码
    @Column(name = "ATTENDANCE_NUM")
    private String attendanceNum;

    //姓名
    @Column(name = "NAME")
    private String name;

    //日期
    @Column(name = "DATE")
    private String date;

    //星期
    @Column(name = "WEEK")
    private String week;

    //部门
    @Column(name = "DEPT")
    private String dept;

    //签到时间
    @Column(name = "SIGN_IN_TIME")
    private String signInTime;

    //签退时间
    @Column(name = "SIGN_BACK_TIME")
    private String SignBackTime;

    public String getAttendanceNum() {
        return attendanceNum;
    }

    public void setAttendanceNum(String attendanceNum) {
        this.attendanceNum = attendanceNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getSignInTime() {
        return signInTime;
    }

    public void setSignInTime(String signInTime) {
        this.signInTime = signInTime;
    }

    public String getSignBackTime() {
        return SignBackTime;
    }

    public void setSignBackTime(String signBackTime) {
        SignBackTime = signBackTime;
    }
}
