package com.wisdom.system.entity.base;

/**
 * PageRequest 分页请求条件实体
 */
public class PageRequest {

    //当前页数
    private Integer pageNum;

    //每页条数
    private Integer pageSize;

    //排序字段
    private String orderField;

    //升序降序 asc|desc
    private String orderBy;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
