package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "DEV_EXCEL_IMPORT_RULE")
public class DevExcelImportRule extends BaseEntity {

    //关联配置ID
    @Column(name = "FK_EXCEL_IMPORT_CONFIG_ID")
    @NotBlank(message = "关联配置ID不能为空！")
    private String fkExcelImportConfigId;

    public String getFkExcelImportConfigId() {
        return this.fkExcelImportConfigId;
    }

    public void setFkExcelImportConfigId(String fkExcelImportConfigId) {
        this.fkExcelImportConfigId = fkExcelImportConfigId;
    }

    //表列名
    @Column(name = "COL_NAME")
    @NotBlank(message = "表列名不能为空！")
    private String colName;

    public String getColName() {
        return this.colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    //表列说明
    @Column(name = "COL_DESCR")
    private String colDescr;

    public String getColDescr() {
        return this.colDescr;
    }

    public void setColDescr(String colDescr) {
        this.colDescr = colDescr;
    }

    //是否可为空
    @Column(name = "IS_NULLABLE")
    private String isNullable;

    public String getIsNullable() {
        return isNullable;
    }

    public void setIsNullable(String isNullable) {
        this.isNullable = isNullable;
    }

    //数据类型
    @Column(name = "COL_DATA_TYPE")
    private String colDataType;

    public String getColDataType() {
        return this.colDataType;
    }

    public void setColDataType(String colDataType) {
        this.colDataType = colDataType;
    }

    //对应excle的列索引
    @Column(name = "EXCEL_COL_INDEX")
    private Integer excelColIndex;

    public Integer getExcelColIndex() {
        return this.excelColIndex;
    }

    public void setExcelColIndex(Integer excelColIndex) {
        this.excelColIndex = excelColIndex;
    }

    //excel列值验证规则
    @Column(name = "EXCEL_COL_VALUE_VERIFY")
    private String excelColValueVerify;

    public String getExcelColValueVerify() {
        return this.excelColValueVerify;
    }

    public void setExcelColValueVerify(String excelColValueVerify) {
        this.excelColValueVerify = excelColValueVerify;
    }

    //excel列值转换规则
    @Column(name = "EXCEL_COL_VALUE_CONVERT")
    private String excelColValueConvert;

    public String getExcelColValueConvert() {
        return this.excelColValueConvert;
    }

    public void setExcelColValueConvert(String excelColValueConvert) {
        this.excelColValueConvert = excelColValueConvert;
    }

    @Column(name = "SORT")
    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}



