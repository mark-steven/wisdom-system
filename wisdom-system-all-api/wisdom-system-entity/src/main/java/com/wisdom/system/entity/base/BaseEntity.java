package com.wisdom.system.entity.base;

import com.wisdom.system.common.util.StringUtil;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.UUID;

/**
 * BaseEntity 用于主表实体的基类
 */
public class BaseEntity {

    @Id
    @Column(name = "ID")
    private String id;

    public String getId() {
        if (StringUtil.isEmpty(this.id)) {
            this.id =  UUID.randomUUID().toString();
        }
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
