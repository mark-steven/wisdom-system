package com.wisdom.system.entity.dev;

import com.wisdom.system.entity.base.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "DEV_REPORT_CONFIG")
public class DevReportConfig extends BaseEntity {

    //报表编码
    @Column(name = "CODE")
    @NotBlank(message = "报表编码不能为空！")
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //报表名称
    @Column(name = "NAME")
    @NotBlank(message = "报表名称不能为空！")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //报表界面
    @Column(name = "PAGE_CONFIG_CONTENT")
    private String pageConfigContent;

    public String getPageConfigContent() {
        return this.pageConfigContent;
    }

    public void setPageConfigContent(String pageConfigContent) {
        this.pageConfigContent = pageConfigContent;
    }

    //报表查询语句
    @Column(name = "SQL_CONFIG_CONTENT")
    private String sqlConfigContent;

    public String getSqlConfigContent() {
        return sqlConfigContent;
    }

    public void setSqlConfigContent(String sqlConfigContent) {
        this.sqlConfigContent = sqlConfigContent;
    }

    //描述
    @Column(name = "DESCR")
    private String descr;

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    //排序
    @Column(name = "SORT")
    private int sort;

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    //拼音首码
    @Column(name = "PY_CODE")
    private String pyCode;

    public String getPyCode() {
        return this.pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    //五笔码
    @Column(name = "WB_CODE")
    private String wbCode;

    public String getWbCode() {
        return this.wbCode;
    }

    public void setWbCode(String wbCode) {
        this.wbCode = wbCode;
    }

    //创建时间
    @Column(name = "ADD_TIME")
    private Date addTime;

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    //创建人
    @Column(name = "ADD_USER_ID")
    private String addUserId;

    public String getAddUserId() {
        return this.addUserId;
    }

    public void setAddUserId(String addUserId) {
        this.addUserId = addUserId;
    }


}


