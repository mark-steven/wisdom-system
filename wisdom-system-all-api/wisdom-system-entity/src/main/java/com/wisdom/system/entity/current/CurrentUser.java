package com.wisdom.system.entity.current;

import java.util.List;

/*系统当前登录用户信息*/
public class CurrentUser {

    //用户ID
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String fkDeptTypeCode;

    public String getFkDeptTypeCode() {
        return fkDeptTypeCode;
    }

    public void setFkDeptTypeCode(String fkDeptTypeCode) {
        this.fkDeptTypeCode = fkDeptTypeCode;
    }

    //用户登录账号
    private String loginCode;

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    //用户密码
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //用户真实姓名
    private String realName;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    //用户性别
    private int sex;

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    //所属部门ID
    private String departmentId;

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    //所属部门编码
    private String departmentCode;

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    //所属部门
    private String departmentName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /*角色列表*/
    private List<CurrentRole> roleList;

    public List<CurrentRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<CurrentRole> roleList) {
        this.roleList = roleList;
    }

    /*账号状态*/
    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    /**
     * 邮箱
     */
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 手机号码
     */
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String fkDeptTypeName;

    public String getFkDeptTypeName() {
        return fkDeptTypeName;
    }

    public void setFkDeptTypeName(String fkDeptTypeName) {
        this.fkDeptTypeName = fkDeptTypeName;
    }
}
