package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysDepartment;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;

public interface SysDepartmentMapper extends Mapper<SysDepartment> {

    int updateSysDepartmentState(HashMap<String, Object> hashMap);

    List<SysDepartment> getSysDepartmentList(@Param("state") Integer state,@Param("fkDeptTypeCode") String fkDeptTypeCode, @Param("keyword") String keyword);

}
