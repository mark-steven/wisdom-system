package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysRoleMenuAuth;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色菜单 Mapper 接口
 *
 * @Date 2019/3/12 11:09
 * author: GuoHonghui
 */
public interface SysRoleMenuAuthMapper extends MyBatisMapper<SysRoleMenuAuth> {
    /**
     * 根据用户角色ID获取授权菜单列表
     *
     * @param fkRoleId
     * @param keyword
     * @return
     */
    List<SysRoleMenuAuth> getSysRoleMenuAuthList(@Param("fkRoleId") String fkRoleId, @Param("keyword") String keyword);
}
