package com.wisdom.system.dao.dev;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DevExcelImportDataMapper {

    int importData(@Param("tableName") String tableName, @Param("columnMap") Map<String, String> columnMap, @Param("valueList") List<Map<String, Object>> valueList);
}
