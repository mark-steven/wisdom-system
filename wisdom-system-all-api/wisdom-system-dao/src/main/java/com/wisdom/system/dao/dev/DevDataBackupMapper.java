package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevTableColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DevDataBackupMapper {

    int dataBackup(@Param("fromTable") String fromTable,
                   @Param("toTable") String toTable,
                   @Param("timeColumn") String timeColumn,
                   @Param("startTime") String startTime,
                   @Param("endTime") String endTime,
                   @Param("columns") List<DevTableColumn> columns);

    int fromTableNeedBackupData(@Param("fromTable") String fromTable,
                                @Param("timeColumn") String timeColumn,
                                @Param("startTime") String startTime,
                                @Param("endTime") String endTime);

    int deleteFromTableData(@Param("fromTable") String fromTable,
                            @Param("timeColumn") String timeColumn,
                            @Param("startTime") String startTime,
                            @Param("endTime") String endTime);
}
