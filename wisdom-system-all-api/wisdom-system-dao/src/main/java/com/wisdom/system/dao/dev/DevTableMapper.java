package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevTable;
import com.wisdom.system.entity.dev.DevTableColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DevTableMapper {

    //获取可以导入Excel的表信息列表
    List<DevTable> getDevTableList(@Param("tableSchema") String tableSchema, @Param("keyword") String keyword);

    //获取表字段名和类型
    List<DevTableColumn> getDevTableColumns(@Param("tableSchema") String tableSchema, @Param("keyword") String keyword);
}
