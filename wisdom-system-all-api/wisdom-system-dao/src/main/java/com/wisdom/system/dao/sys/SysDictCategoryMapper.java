package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysDictCategory;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;

public interface SysDictCategoryMapper extends MyBatisMapper<SysDictCategory> {
}
