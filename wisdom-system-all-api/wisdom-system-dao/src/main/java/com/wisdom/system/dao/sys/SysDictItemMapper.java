package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysDictItem;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;

public interface SysDictItemMapper extends MyBatisMapper<SysDictItem> {

    SysDictItem getSysDictItemInfo(String id);

 }
