package com.wisdom.system.dao.common;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CommonMapper {

    int isFieldValueExist(Map<String, Object> map);

    int getCustomSQLCount(@Param("sql") String sql);

    List<Map<String, Object>> getCustomSQLResultOfPage(@Param("sql") String sql, @Param("fromSize") Integer fromSize, @Param("pageSize") Integer pageSize);

    List<Map<String, Object>> getCustomSQLResult(@Param("sql") String sql);
}
