package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysUser;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户 Mapper 接口
 *
 * @Date 2019/3/7 10:50
 * author: GuoHonghui
 */
public interface SysUserMapper extends MyBatisMapper<SysUser> {

    /**
     * 更新用户状态
     *
     * @param hashMap
     * @return
     */
    int updateSysUserState(HashMap<String, Object> hashMap);

    /**
     * 根据主键id获取用户详情
     *
     * @param id
     * @return
     */
    SysUser getSysUserInfo(@Param("id") String id);

    /**
     * 修改用户密码
     *
     * @param map
     * @return
     */
    int updatePassword(Map<String,Object> map);

    /**
     * 根据关键字keyword获取用户列表
     *
     * @param keyword
     * @return
     */
    List<SysUser> getSysUserList(@Param("fkDepartmentCode") String fkDepartmentCode,@Param("keyword") String keyword);

    /**
     * 修改用户信息
     *
     * @param map
     * @return
     */
    int updateUserinfo(Map<String, Object> map);
}
