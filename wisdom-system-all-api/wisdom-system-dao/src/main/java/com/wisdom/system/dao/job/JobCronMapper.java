package com.wisdom.system.dao.job;

import com.wisdom.system.entity.job.JobCron;
import tk.mybatis.mapper.common.Mapper;

public interface JobCronMapper extends Mapper<JobCron> {
}

