package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevExcelOutputConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * 【报表配置】数据访问层接口类
 * Generate by CodeSmith
 * 2019年09月24日 04:03
 */
public interface DevExcelOutputConfigMapper extends Mapper<DevExcelOutputConfig> {
}

