package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysParam;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

public interface SysParamMapper extends MyBatisMapper<SysParam> {

    int deleteSysParamByGroupCode(@Param("paramGroupCode") String paramGroupCode);
}
