package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevExcelImportConfig;
import tk.mybatis.mapper.common.Mapper;

public interface DevExcelImportConfigMapper extends Mapper<DevExcelImportConfig> {
}

