package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevParamConfig;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DevParamConfigMapper extends Mapper<DevParamConfig> {

    List<DevParamConfig> getDevParamConfigList(@Param("fkParamCategoryCode") String fkParamCategoryCode, @Param("fkParamGroupCode") String fkParamGroupCode, @Param("keyword") String keyword);

    DevParamConfig getDevParamConfigInfo(@Param("id") String id);
}
