package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevExcelImportRule;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DevExcelImportRuleMapper extends MyBatisMapper<DevExcelImportRule> {

    //根据表名获取列信息
    List<DevExcelImportRule> getTableColumnList(@Param("tableName") String tableName, @Param("tableSchema") String tableSchema);

    //判断表名是否存在
    int tableCount(@Param("tableName") String tableName, @Param("tableSchema") String tableSchema);
}

