package com.wisdom.system.dao.job;

import com.wisdom.system.entity.job.JobLog;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JobLogMapper extends MyBatisMapper<JobLog> {

    List<JobLog> getJobLogList(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("fkJobLogCode") String fkJobLogCode,@Param("keyword") String keyword);
}

