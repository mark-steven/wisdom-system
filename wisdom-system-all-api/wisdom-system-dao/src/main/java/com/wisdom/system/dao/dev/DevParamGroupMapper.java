package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevParamGroup;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DevParamGroupMapper extends Mapper<DevParamGroup> {

    List<DevParamGroup> getDevParamGroupList(@Param("fkParamCategoryCode") String fkParamCategoryCode, @Param("keyword") String keyword);

}
