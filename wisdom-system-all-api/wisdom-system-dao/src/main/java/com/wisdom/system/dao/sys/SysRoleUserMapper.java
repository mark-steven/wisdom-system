package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysRoleUser;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户角色 Mapper 接口
 *
 * @Date 2019/3/8 14:43
 * author: GuoHonghui
 */
public interface SysRoleUserMapper extends MyBatisMapper<SysRoleUser> {

    /**
     * 根据用户角色ID获取用户列表
     *
     * @param fkRoleId
     * @param keyword
     * @return
     */
    List<SysRoleUser> getSysRoleUserList(@Param("fkRoleId") String fkRoleId, @Param("keyword") String keyword);
}
