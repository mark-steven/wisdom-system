package com.wisdom.system.dao.sys;

import org.apache.ibatis.annotations.Param;

public interface SysCurrentUser {

    //获取当前登录用户信息
    SysCurrentUser getSysCurrentUser(@Param("userId") String userId);

}