package com.wisdom.system.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MIMapper {

    int isFieldValueExist(Map<String, Object> map);

    int getCustomSQLCount(@Param("sql") String sql);

    List<Map<String, Object>> getCustomSQLResult(@Param("sql") String sql, @Param("fromSize") Integer fromSize, @Param("pageSize") Integer pageSize);
}
