package com.wisdom.system.dao.job;

import com.wisdom.system.entity.job.JobTask;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;

public interface JobTaskMapper extends Mapper<JobTask> {

    List<JobTask> getJobTaskList(@Param("keyword") String keyword);

    JobTask getJobTaskInfo(@Param("id") String id);

    /**
     * 更新任务状态
     *
     * @param hashMap
     * @return
     */
    int updateJobTaskState(HashMap<String, Object> hashMap);

    /**
     * 获取初始化启动的项目
     *
     * @return
     */
    List<JobTask> getJobTaskStartList();

}

