package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysTree;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysTreeMapper {

    //获取部门树
    List<SysTree> getSysDeptTree(@Param("keyword") String keyword, @Param("state") Integer state,@Param("code") String code,@Param("fkDeptTypeCode")String fkDeptTypeCode);

}
