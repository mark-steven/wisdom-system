package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysMenu;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface SysMenuMapper extends MyBatisMapper<SysMenu> {

    SysMenu getSysMenuInfo(@Param("id") String id);

    //修改用户状态
    int updateSysMenuState(HashMap<String, Object> hashMap);

    /**
     * 获取角色菜单列表
     *
     * @param
     * @return
     */
    List<SysMenu> getUserSysMenuList(@Param("fkUserId") String fkUserId, @Param("fkSystem") String fkSystem, @Param("state") String state);

    List<SysMenu> getSysMenuList(@Param("keyword") String keyword, @Param("fkSystem") String fkSystem);


}
