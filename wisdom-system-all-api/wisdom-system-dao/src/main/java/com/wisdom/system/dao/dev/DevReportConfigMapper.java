package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevReportConfig;
import tk.mybatis.mapper.common.Mapper;

public interface DevReportConfigMapper extends Mapper<DevReportConfig> {
}