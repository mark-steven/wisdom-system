package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevParamCategory;
import tk.mybatis.mapper.common.Mapper;

public interface DevParamCategoryMapper extends Mapper<DevParamCategory> {
}
