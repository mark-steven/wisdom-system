package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysUserMenuFavorites;
import com.wisdom.system.common.mybatis.tk.MyBatisMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户常用菜单设置 Mapper 接口
 *
 * @Date 2019/3/15 9:03
 * author: GuoHonghui
 */
public interface SysUserMenuFavoritesMapper extends MyBatisMapper<SysUserMenuFavorites> {

    /**
     * 获取用户常用菜单设置列表
     *
     * @param userId
     * @return
     */
    List<SysUserMenuFavorites> getSysUserMenuFavoritesList(@Param("userId") String userId, @Param("fkSystem") String fkSystem);

    int deleteSysUserMenuFavoritesList(@Param("fkUserId") String fkUserId, @Param("fkSystem") String fkSystem);
}
