package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysSystemLog;
import tk.mybatis.mapper.common.Mapper;

public interface SysSystemLogMapper extends Mapper<SysSystemLog> {
}

