package com.wisdom.system.dao.sys;

import com.wisdom.system.entity.sys.SysTableBakJob;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface SysTableBakJobMapper extends Mapper<SysTableBakJob> {

    //删除备份表
    int dropTableBak(@Param("tableBakName") String tableBakName);

    //创建备份表
    int createTableBak(@Param("tableName") String tableName, @Param("tableBakName") String tableBakName);

    //查询数据库
    List<Map> showDatabase();

    //获取对应数据库对应的表
    List<Map> getTableList(@Param("tableSchema") String tableSchema);

    //获取备份开启的数据列表
    List<SysTableBakJob> getSysTableBakJobExecuteList();

    //删除表数据
    int clearTableData(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName, @Param("conditions") String conditions);

    //删除备份表与源表重复插入数据预防主键冲突
    int clearRepeatTableBakData(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName, @Param("tableBakName") String tableBakName, @Param("conditions") String conditions);

    //备份表数据插入
    int insertTableBakList(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName,
                           @Param("bakTableSchema") String bakTableSchema, @Param("bakTableName") String bakTableName,
                           @Param("conditions") String conditions);

    //备份任务是否开启
    int updateSysTableBakJobState(@Param("id") String id, @Param("state") int state);

}
