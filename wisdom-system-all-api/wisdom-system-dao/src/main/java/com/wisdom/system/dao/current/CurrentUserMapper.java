package com.wisdom.system.dao.current;

import com.wisdom.system.entity.current.CurrentUser;

public interface CurrentUserMapper {

    //根据登录账号获取用户信息
    CurrentUser getCurrentUser(String loginCode);
}
