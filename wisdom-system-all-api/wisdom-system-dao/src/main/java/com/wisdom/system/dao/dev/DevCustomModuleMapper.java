package com.wisdom.system.dao.dev;

import com.wisdom.system.entity.dev.DevCustomModule;
import tk.mybatis.mapper.common.Mapper;

public interface DevCustomModuleMapper extends Mapper<DevCustomModule> {

}

