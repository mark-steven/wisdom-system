package com.wisdom.system.dao.current;

import com.wisdom.system.entity.current.CurrentRole;

import java.util.List;

public interface CurrentRoleMapper {

    //获取当前用户的角色
    List<CurrentRole> getCurrentRoleList(String fkUserId);
}
