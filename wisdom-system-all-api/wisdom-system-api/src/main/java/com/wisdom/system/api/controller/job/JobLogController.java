package com.wisdom.system.api.controller.job;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.job.JobLog;
import com.wisdom.system.service.job.JobLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api/job/log")
@Api(tags={"JobLogController"},description="服务接口")
public class JobLogController {

    @Autowired
    private JobLogService jobLogService;
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定", notes = "根据主键id删除指定")
    @SerializedField(moduleName = "删除")
    public int delJobLogInfo(@PathVariable(value = "id") String id) {
        return jobLogService.delJobLogInfo(id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除列表信息", notes = "根据ids批量删除列表信息")
    @SerializedField(moduleName = "批量删除")
    public int delJobLogList(@RequestBody String ids) {
        return jobLogService.delJobLogList(ids);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @SerializedField(moduleName = "获取列表")
    public List<JobLog> getJobLogList(@RequestParam(value = "startTime", required = false) String startTime,
                                      @RequestParam(value = "endTime", required = false) String endTime,
                                      @RequestParam(value = "fkJobLogCode", required = false) String fkJobLogCode,
                                      @RequestParam(value = "keyword", required = false) String keyword) {
        return jobLogService.getJobLogList(startTime,endTime,fkJobLogCode,keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页列表", notes = "获取分页列表")
    @SerializedField(moduleName = "获取列表")
    public PageResult<JobLog> getJobLogPageList(@RequestParam(value = "startTime", required = false) String startTime,
                                                @RequestParam(value = "endTime", required = false) String endTime,
                                                @RequestParam(value = "fkJobLogCode", required = false) String fkJobLogCode,
                                                @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return jobLogService.getJobLogPageList(startTime,endTime,fkJobLogCode,keyword, pageRequest);
    }
}


