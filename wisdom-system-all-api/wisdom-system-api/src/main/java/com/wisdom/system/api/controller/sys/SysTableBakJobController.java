package com.wisdom.system.api.controller.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysTableBakJob;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.sys.SysTableBakJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/sys/table/bak")
@Api(tags = {"SysTableBakJobController"}, description = "表数据备份保存接口")
public class SysTableBakJobController {
    @Autowired
    private SysTableBakJobService sysTableBakJobService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增记录信息", notes = "新增记录信息")
    @SerializedField(moduleName = "新增")
    public int addSysTableBakJobInfo(@RequestBody @Validated SysTableBakJob sysTableBakJob) {
        return sysTableBakJobService.addSysTableBakJobInfo(sysTableBakJob);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改记录信息", notes = "修改记录信息")
    @SerializedField(moduleName = "修改")
    public int updateSysTableBakJobInfo(@RequestBody SysTableBakJob sysTableBakJob) {
        return sysTableBakJobService.updateSysTableBakJobInfo(sysTableBakJob);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定", notes = "根据主键id删除指定")
    @SerializedField(moduleName = "删除")
    public int delSysTableBakJobInfo(@PathVariable(value = "id") String id) {
        return sysTableBakJobService.delSysTableBakJobInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除列表信息", notes = "根据ids批量删除列表信息")
    @SerializedField(moduleName = "批量删除")
    public int delSysTableBakJobList(@RequestBody String ids) {
        return sysTableBakJobService.delSysTableBakJobList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取详情", notes = "根据主键id获取详情")
    @SerializedField(moduleName = "获取")
    public SysTableBakJob getSysTableBakJobInfo(@PathVariable(value = "id") String id) {
        return sysTableBakJobService.getSysTableBakJobInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @SerializedField(moduleName = "获取列表")
    public List<SysTableBakJob> getSysTableBakJobList(@RequestParam(value = "state", required = false) Integer state,
                                                      @RequestParam(value = "keyword", required = false) String keyword) {
        return sysTableBakJobService.getSysTableBakJobList(state, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页列表", notes = "获取分页列表")
    @SerializedField(moduleName = "获取列表")
    public PageResult<SysTableBakJob> getSysTableBakJobPageList(@RequestParam(value = "state", required = false) Integer state,
                                                                @RequestParam(value = "keyword", required = false) String keyword,
                                                                PageRequest pageRequest) {
        return sysTableBakJobService.getSysTableBakJobPageList(state, keyword, pageRequest);
    }

    @RequestMapping(value = "/schema/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取数据库列表", notes = "获取数据库列表")
    @SerializedField(moduleName = "获取数据库列表")
    public List<Map> showDatabase() {
        return sysTableBakJobService.showDatabase();
    }

    @RequestMapping(value = "/tab/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取表列表", notes = "获取表列表")
    @SerializedField(moduleName = "获取列表")
    public List<Map> getTableList() {
        return sysTableBakJobService.getTableList();
    }

    @RequestMapping(value = "execute", method = RequestMethod.POST)
    @ApiOperation(value = "执行备份", notes = "执行备份")
    @SerializedField(moduleName = "执行备份")
    public int executeTableBak(@RequestBody SysTableBakJob sysTableBakJob) {
        return sysTableBakJobService.executeTableBak(sysTableBakJob);
    }

    @RequestMapping(value = "/{id}/{state}", method = RequestMethod.PUT)
    @ApiOperation(value = "更新备份表状态", notes = "更新备份表状态")
    @SerializedField(moduleName = "更新备份表状态")
    public int updateSysTableBakJobState(@PathVariable("id") String id, @PathVariable("state") int state) {
        return sysTableBakJobService.updateSysTableBakJobState(id, state);
    }
}
