package com.wisdom.system.api.controller.dev;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.dev.DevCustomModule;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.common.util.PinyinUtil;
import com.wisdom.system.service.dev.DevCustomModuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "api/dev/custom/module")
@Api(tags = {"DevCustomModuleController"}, description = "服务接口")
public class DevCustomModuleController {

    @Autowired
    private DevCustomModuleService devCustomModuleService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增记录信息", notes = "新增记录信息")
    @SerializedField(moduleName = "新增")
    public int addDevCustomModuleInfo(@RequestBody @Validated DevCustomModule devCustomModule) {
        CurrentUser currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();
        devCustomModule.setAddUserId(userId);
        devCustomModule.setAddTime(new Date());
        devCustomModule.setPyCode(PinyinUtil.ToFirstChar(devCustomModule.getName()));
        devCustomModule.setWbCode(PinyinUtil.toWBCode(devCustomModule.getName()));
        return devCustomModuleService.addDevCustomModuleInfo(devCustomModule);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改记录信息", notes = "修改记录信息")
    @SerializedField(moduleName = "修改")
    public int updateDevCustomModuleInfo(@RequestBody DevCustomModule devCustomModule) {
        return devCustomModuleService.updateDevCustomModuleInfo(devCustomModule);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定", notes = "根据主键id删除指定")
    @SerializedField(moduleName = "删除")
    public int delDevCustomModuleInfo(@PathVariable(value = "id") String id) {
        return devCustomModuleService.delDevCustomModuleInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除列表信息", notes = "根据ids批量删除列表信息")
    @SerializedField(moduleName = "批量删除")
    public int delDevCustomModuleList(@RequestBody String ids) {
        return devCustomModuleService.delDevCustomModuleList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取详情", notes = "根据主键id获取详情")
    @SerializedField(moduleName = "获取")
    public DevCustomModule getDevCustomModuleInfo(@PathVariable(value = "id") String id) {
        return devCustomModuleService.getDevCustomModuleInfo(id);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @ApiOperation(value = "根据code获取详情", notes = "根据code获取详情")
    @SerializedField(moduleName = "获取")
    public DevCustomModule getDevCustomModuleByCode(@PathVariable(value = "code") String code) {
        CurrentUser currentUser = userService.getCurrentUser();
        String departmentCode = currentUser.getDepartmentCode();
        return devCustomModuleService.getDevCustomModuleByCode(departmentCode, code);
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @SerializedField(moduleName = "获取列表")
    public List<DevCustomModule> getDevCustomModuleList(@RequestParam(value = "fkDeptCode", required = false) String fkDeptCode,
                                                        @RequestParam(value = "keyword", required = false) String keyword) {
        return devCustomModuleService.getDevCustomModuleList(fkDeptCode, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页列表", notes = "获取分页列表")
    @SerializedField(moduleName = "获取列表")
    public PageResult<DevCustomModule> getDevCustomModulePageList(@RequestParam(value = "fkDeptCode", required = false) String fkDeptCode,
                                                                  @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devCustomModuleService.getDevCustomModulePageList(fkDeptCode, keyword, pageRequest);
    }


}


