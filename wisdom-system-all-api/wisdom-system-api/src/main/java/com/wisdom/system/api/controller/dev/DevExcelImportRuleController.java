package com.wisdom.system.api.controller.dev;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevExcelImportRule;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.dev.DevExcelImportRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/dev/excel/import/rule")
@Api(tags = {"DevExcelImportRuleController"}, description = "EXCEL导入规则服务接口")
public class DevExcelImportRuleController {

    @Autowired
    private DevExcelImportRuleService devExcelImportRuleService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增EXCEL导入规则记录信息", notes = "新增EXCEL导入规则记录信息")
    @SerializedField(moduleName = "新增EXCEL导入规则")
    public int addDevExcelImportRuleInfo(@RequestBody @Validated DevExcelImportRule devExcelImportRule) {
        return devExcelImportRuleService.addDevExcelImportRuleInfo(devExcelImportRule);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改EXCEL导入规则记录信息", notes = "修改EXCEL导入规则记录信息")
    @SerializedField(moduleName = "修改EXCEL导入规则")
    public int updateDevExcelImportRuleInfo(@RequestBody @Validated DevExcelImportRule devExcelImportRule) {
        return devExcelImportRuleService.updateDevExcelImportRuleInfo(devExcelImportRule);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定EXCEL导入规则", notes = "根据主键id删除指定EXCEL导入规则")
    @SerializedField(moduleName = "删除EXCEL导入规则")
    public int delDevExcelImportRuleInfo(@PathVariable(value = "id") String id) {
        return devExcelImportRuleService.delDevExcelImportRuleInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除EXCEL导入规则列表信息", notes = "根据ids批量删除EXCEL导入规则列表信息")
    @SerializedField(moduleName = "批量删除EXCEL导入规则")
    public int delDevExcelImportRuleList(@RequestBody String ids) {
        return devExcelImportRuleService.delDevExcelImportRuleList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取EXCEL导入规则详情", notes = "根据主键id获取EXCEL导入规则详情")
    @SerializedField(moduleName = "获取EXCEL导入规则")
    public DevExcelImportRule getDevExcelImportRuleInfo(@PathVariable(value = "id") String id) {
        return devExcelImportRuleService.getDevExcelImportRuleInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取EXCEL导入规则列表", notes = "获取EXCEL导入规则列表")
    @SerializedField(moduleName = "获取EXCEL导入规则列表")
    public List<DevExcelImportRule> getDevExcelImportRuleList(@RequestParam(value = "keyword", required = false) String keyword) {
        return devExcelImportRuleService.getDevExcelImportRuleList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页EXCEL导入规则列表", notes = "获取分页EXCEL导入规则列表")
    @SerializedField(moduleName = "获取EXCEL导入规则列表")
    public PageResult<DevExcelImportRule> getDevExcelImportRulePageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devExcelImportRuleService.getDevExcelImportRulePageList(keyword, pageRequest);
    }

    @RequestMapping(value = "/columns/{tableName}", method = RequestMethod.GET)
    @ApiOperation(value = "根据表名获取列信息", notes = "根据表名获取列信息")
    @SerializedField(moduleName = "根据表名获取列信息")
    public PageResult<DevExcelImportRule> getTableColumnList(@PathVariable("tableName") String tableName, PageRequest pageRequest) {
        return devExcelImportRuleService.getTableColumnList(tableName, pageRequest);
    }
}


