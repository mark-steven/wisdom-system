package com.wisdom.system.api.controller.job;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.job.JobCron;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.job.JobCronService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/job/cron")
@Api(tags = {"JobCronController"}, description = "CRON规则服务接口")
public class JobCronController {

    @Autowired
    private JobCronService jobCronService;
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增CRON规则记录信息", notes = "新增CRON规则记录信息")
    @SerializedField(moduleName = "新增CRON规则")
    public int addJobCronInfo(@RequestBody @Validated JobCron jobCron) {
        CurrentUser currentUser = userService.getCurrentUser();
        jobCron.setAddUserId(currentUser.getUserId());
        return jobCronService.addJobCronInfo(jobCron);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改CRON规则记录信息", notes = "修改CRON规则记录信息")
    @SerializedField(moduleName = "修改CRON规则")
    public int updateJobCronInfo(@RequestBody @Validated JobCron jobCron) {
        return jobCronService.updateJobCronInfo(jobCron);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定CRON规则", notes = "根据主键id删除指定CRON规则")
    @SerializedField(moduleName = "删除CRON规则")
    public int delJobCronInfo(@PathVariable(value = "id") String id) {
        return jobCronService.delJobCronInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除CRON规则列表信息", notes = "根据ids批量删除CRON规则列表信息")
    @SerializedField(moduleName = "批量删除CRON规则")
    public int delJobCronList(@RequestBody String ids) {
        return jobCronService.delJobCronList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取CRON规则详情", notes = "根据主键id获取CRON规则详情")
    @SerializedField(moduleName = "获取CRON规则")
    public JobCron getJobCronInfo(@PathVariable(value = "id") String id) {
        return jobCronService.getJobCronInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取CRON规则列表", notes = "获取CRON规则列表")
    @SerializedField(moduleName = "获取CRON规则列表")
    public List<JobCron> getJobCronList(@RequestParam(value = "keyword", required = false) String keyword) {
        return jobCronService.getJobCronList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页CRON规则列表", notes = "获取分页CRON规则列表")
    @SerializedField(moduleName = "获取CRON规则列表")
    public PageResult<JobCron> getJobCronPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return jobCronService.getJobCronPageList(keyword, pageRequest);
    }
}


