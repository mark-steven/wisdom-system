package com.wisdom.system.api.controller.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.pojo.SysUserInfo;
import com.wisdom.system.entity.sys.SysUser;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.common.util.PinyinUtil;
import com.wisdom.system.service.sys.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户信息服务接口
 *
 * @Date 2019/3/7 10:55
 * author: GuoHonghui
 */
@RestController
@RequestMapping(value = "api/sys/user")
@Api(tags = {"04 SysUserController"}, description = "用户信息服务接口")
public class SysUserController {

    @Autowired
    private SysUserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增用户信息", notes = "新增用户信息")
    @SerializedField(moduleName = "新增用户")
    public int addSysUserInfo(@RequestBody @Validated SysUser sysUser) {
        sysUser.setPyCode(PinyinUtil.ToFirstChar(sysUser.getName()));
        sysUser.setWbCode(PinyinUtil.toWBCode(sysUser.getName()));
        return userService.addSysUserInfo(sysUser);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改用户信息", notes = "修改用户信息")
    @SerializedField(moduleName = "修改用户")
    public int updateSysUserInfo(@RequestBody @Validated SysUser sysUser) {
        return userService.updateSysUserInfo(sysUser);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定用户", notes = "根据主键id删除指定用户")
    @SerializedField(moduleName = "删除用户")
    public int delSysUserInfo(@PathVariable(value = "id") String id) {
        return userService.delSysUserInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除用户列表信息", notes = "根据ids批量删除用户列表信息")
    @SerializedField(moduleName = "批量删除用户")
    public int delSysUserList(@RequestBody String ids) {
        return userService.delSysUserList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取用户详情", notes = "根据主键id获取用户详情")
    @SerializedField(moduleName = "获取用户")
    public SysUser getSysUserInfo(@PathVariable(value = "id") String id) {
        return userService.getSysUserInfo(id);
    }

    @RequestMapping(value = "department/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户列表", notes = "获取用户列表")
    @SerializedField(moduleName = "获取用户列表", excludes = {"salt", "password"})
    public List<SysUser> getSysUserListByDepartmentCode(@RequestParam(value = "fkDepartmentCode", required = false) String fkDepartmentCode, @RequestParam(value = "keyword", required = false) String keyword) {
        return userService.getSysUserListByDepartmentCode(fkDepartmentCode, keyword);
    }

    @RequestMapping(value = "department/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页用户列表", notes = "获取分页用户列表")
    @SerializedField(moduleName = "获取用户列表")
    public PageResult<SysUser> getSysUserDepartmentCodePageList(@RequestParam(value = "fkDepartmentCode", required = false) String fkDepartmentCode, @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return userService.getSysUserPageListByDepartmentCode(fkDepartmentCode, keyword, pageRequest);
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户列表", notes = "获取用户列表")
    @SerializedField(moduleName = "获取用户列表", excludes = {"salt", "password"})
    public List<SysUser> getSysUserList(@RequestParam(value = "keyword", required = false) String keyword) {
        return userService.getSysUserList(keyword);
    }


    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页用户列表", notes = "获取分页用户列表")
    @SerializedField(moduleName = "获取用户列表")
    public PageResult<SysUser> getSysUserPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return userService.getSysUserPageList(keyword, pageRequest);
    }


    @RequestMapping(value = "/{id}/{state}", method = RequestMethod.PUT)
    @ApiOperation(value = "更新用户状态", notes = "更新用户状态")
    @SerializedField(moduleName = "更新用户状态")
    public int updateSysUserState(@PathVariable("id") String id, @PathVariable("state") int state) {
        return userService.updateSysUserState(id, state);
    }

    @RequestMapping(value = "/password", method = RequestMethod.POST)
    @ApiOperation(value = "更新用户密码", notes = "更新用户密码")
    @SerializedField(moduleName = "更新用户密码")
    public int updatePassword(SysUserInfo sysUserInfo) {
        return userService.updateSysUserPassword(sysUserInfo);
    }

    @RequestMapping(value = "/userinfo", method = RequestMethod.POST)
    @ApiOperation(value = "更新用户信息", notes = "更新用户信息")
    @SerializedField(moduleName = "更新用户信息")
    public int updateUserinfo(@RequestBody SysUserInfo sysUserInfo) {
        return userService.updateSysUserInfo(sysUserInfo);
    }

}
