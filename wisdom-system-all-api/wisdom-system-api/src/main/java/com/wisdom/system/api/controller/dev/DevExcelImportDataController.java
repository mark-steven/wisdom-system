package com.wisdom.system.api.controller.dev;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.dev.DevExcelImportDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;

@RestController
@RequestMapping(value = "api/dev/excel/import/data")
@Api(tags = {"DevExcelDataImportController"}, description = "EXCEL数据导入")
public class DevExcelImportDataController {

    @Autowired
    private DevExcelImportDataService devExcelImportDataService;

    @ApiOperation(value = "根据业务模块编码导入Excel数据", notes = "根据业务模块编码导入Excel数据")
    @SerializedField(moduleName = "导入数据")
    @RequestMapping(value = "{code}", method = RequestMethod.POST)
    public int dataImport(@PathVariable("code") String code, @RequestParam(value = "file") MultipartFile file) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = (FileInputStream) file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return devExcelImportDataService.importData(code,fileInputStream);
    }

}
