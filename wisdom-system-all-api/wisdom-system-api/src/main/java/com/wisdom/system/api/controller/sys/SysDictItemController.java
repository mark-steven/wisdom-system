package com.wisdom.system.api.controller.sys;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.sys.SysDictItem;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.common.util.PinyinUtil;
import com.wisdom.system.service.sys.SysDictItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/sys/dict/item")
@Api(tags = {"SysDictItemController"}, description = "系统字典项服务接口")
public class SysDictItemController {

    @Autowired
    private SysDictItemService sysDictItemService;
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增系统字典项记录信息", notes = "新增系统字典项记录信息")
    @SerializedField(moduleName = "新增系统字典项")
    public int addSysDictItemInfo(@RequestBody @Validated SysDictItem sysDictItem) {
        CurrentUser currentUser = userService.getCurrentUser();
        sysDictItem.setAddUserId(currentUser.getUserId());
        sysDictItem.setPyCode(PinyinUtil.ToFirstChar(sysDictItem.getName()));
        sysDictItem.setWbCode(PinyinUtil.toWBCode(sysDictItem.getName()));
        return sysDictItemService.addSysDictItemInfo(sysDictItem);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改系统字典项记录信息", notes = "修改系统字典项记录信息")
    @SerializedField(moduleName = "修改信息")
    public int updateSysDictItemInfo(@RequestBody @Validated SysDictItem sysDictItem) {
        return sysDictItemService.updateSysDictItemInfo(sysDictItem);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定系统字典项", notes = "根据主键id删除指定系统字典项")
    @SerializedField(moduleName = "删除系统字典项")
    public int delSysDictItemInfo(@PathVariable(value = "id") String id) {
        return sysDictItemService.delSysDictItemInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除系统字典项列表信息", notes = "根据ids批量删除系统字典项列表信息")
    @SerializedField(moduleName = "批量删除系统字典项")
    public int delSysDictItemList(@RequestBody String ids) {
        return sysDictItemService.delSysDictItemList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取系统字典项详情", notes = "根据主键id获取系统字典项详情")
    @SerializedField(moduleName = "获取系统字典项")
    public SysDictItem getSysDictItemInfo(@PathVariable("id") String id) {
        return sysDictItemService.getSysDictItemInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取系统字典项列表", notes = "获取系统字典项列表")
    @SerializedField(moduleName = "获取系统字典项列表")
    public List<SysDictItem> getSysDictItemList(@RequestParam(value = "fkDictCategoryCode", required = false) String fkDictCategoryCode, @RequestParam(value = "keyword", required = false) String keyword) {
        return sysDictItemService.getSysDictItemList(fkDictCategoryCode, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页系统字典项列表", notes = "获取分页系统字典项列表")
    @SerializedField(moduleName = "获取系统字典项列表")
    public PageResult<SysDictItem> getSysDictItemPageList(@RequestParam(value = "fkDictCategoryCode", required = false) String fkDictCategoryCode, @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return sysDictItemService.getSysDictItemPageList(fkDictCategoryCode, keyword, pageRequest);
    }
}

