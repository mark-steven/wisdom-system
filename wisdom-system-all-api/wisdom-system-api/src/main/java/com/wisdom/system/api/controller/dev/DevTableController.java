package com.wisdom.system.api.controller.dev;

import com.wisdom.system.entity.dev.DevTable;
import com.wisdom.system.entity.dev.DevTableColumn;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.dev.DevTableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/dev/table")
@Api(tags = {"DevTableController"}, description = "获取表信息")
public class DevTableController {

    @Autowired
    DevTableService devTableService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取基础表列表", notes = "获取基础表列表")
    @SerializedField(moduleName = "获取基础表列表")
    public List<DevTable> getDevTableList(@RequestParam(value = "keyword", required = false) String keyword) {
        return devTableService.getDevTableList(keyword);
    }

    @RequestMapping(value = "/columns/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取基础表列表", notes = "获取基础表列表")
    @SerializedField(moduleName = "获取基础表列表")
    public List<DevTableColumn> getDevTableColumnsList(@RequestParam(value = "keyword", required = false) String keyword) {
        return devTableService.getDevTableColumnsList(keyword);
    }
}
