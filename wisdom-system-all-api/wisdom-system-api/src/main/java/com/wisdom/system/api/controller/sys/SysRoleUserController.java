package com.wisdom.system.api.controller.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysRoleUser;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.sys.SysRoleUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户角色信息服务接口
 *
 * @Date 2019/3/8 14:44
 * author: GuoHonghui
 */
@RestController
@RequestMapping(value = "api/sys/role/user")
@Api(tags = {"05 SysUserRoleController"}, description = "用户角色信息服务接口")
public class SysRoleUserController {

    @Autowired
    private SysRoleUserService userRoleService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增用户角色信息", notes = "新增用户角色信息")
    @SerializedField(moduleName = "新增用户角色信息")
    public int addSysUserRoleInfo(@RequestBody @Validated SysRoleUser sysUserRole) {
        return userRoleService.addUserRoleInfo(sysUserRole);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改用户角色信息", notes = "修改用户角色信息")
    @SerializedField(moduleName = "修改用户角色信息")
    public int updateSysUserRoleInfo(@RequestBody @Validated SysRoleUser sysUserRole) {
        return userRoleService.updateSysUserRoleInfo(sysUserRole);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定用户信息", notes = "根据主键id删除指定用户角色")
    @SerializedField(moduleName = "删除用户角色")
    public int delSysUserRoleInfo(@PathVariable(value = "id") String id) {
        return userRoleService.delSysUserRoleInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除用户角色列表信息", notes = "根据ids批量删除用户角色列表信息")
    @SerializedField(moduleName = "批量删除用户角色")
    public int delSysUserRoleList(@RequestBody String ids) {
        return userRoleService.delSysUserRoleList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取用户角色详情", notes = "根据主键id获取用户角色详情")
    @SerializedField(moduleName = "获取用户角色")
    public SysRoleUser getSysUserRoleInfo(@PathVariable(value = "id") String id) {
        return userRoleService.getSysUserRoleInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户角色列表", notes = "获取用户角色列表")
    @SerializedField(moduleName = "获取用户角色列表")
    public List<SysRoleUser> getSysUserRoleList(@RequestParam(value = "fkRoleId", required = false) String fkRoleId, @RequestParam(value = "keyword", required = false) String keyword) {
        return userRoleService.getSysUserRoleList(fkRoleId, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页用户角色列表", notes = "获取分页用户角色列表")
    @SerializedField(moduleName = "获取用户角色列表")
    public PageResult<SysRoleUser> getSysUserRolePageList(String fkRoleId, String keyword, PageRequest pageRequest) {
        return userRoleService.getSysUserRolePageList(fkRoleId, keyword, pageRequest);
    }

    @RequestMapping(value = "/list/{fkRoleId}", method = RequestMethod.PUT)
    @ApiOperation(value = "批量更新角色信息", notes = "批量更新角色信息")
    @SerializedField(moduleName = "批量更新角色信息")
    public int updateSysUserRoleList(@PathVariable(value = "fkRoleId") String fkRoleId, @RequestBody List<SysRoleUser> sysUserRoleList) {
        return userRoleService.updateSysUserRoleList(fkRoleId, sysUserRoleList);
    }
}
