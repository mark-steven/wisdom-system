package com.wisdom.system.api.controller.sys;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.sys.SysDictCategory;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.sys.SysDictCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/sys/dict/category")
@Api(tags = {"01 SysDictCategoryController"}, description = "系统字典分类服务接口")
public class SysDictCategoryController {

    @Autowired
    private SysDictCategoryService sysDictCategoryService;
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增系统字典分类记录信息", notes = "新增系统字典分类记录信息")
    @SerializedField(moduleName = "新增系统字典分类")
    public int addSysDictCategoryInfo(@RequestBody @Validated SysDictCategory sysDictCategory) {
        CurrentUser currentUser = userService.getCurrentUser();
        sysDictCategory.setAddUserId(currentUser.getUserId());
        return sysDictCategoryService.addSysDictCategoryInfo(sysDictCategory);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改系统字典分类记录信息", notes = "修改系统字典分类记录信息")
    @SerializedField(moduleName = "修改系统字典分类")
    public int updateSysDictCategoryInfo(@RequestBody @Validated SysDictCategory sysDictCategory) {
        return sysDictCategoryService.updateSysDictCategoryInfo(sysDictCategory);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定系统字典分类", notes = "根据主键id删除指定系统字典分类")
    @SerializedField(moduleName = "删除系统字典分类")
    public int delSysDictCategoryInfo(@PathVariable(value = "id") String id) {
        return sysDictCategoryService.delSysDictCategoryInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除系统字典分类列表信息", notes = "根据ids批量删除系统字典分类列表信息")
    @SerializedField(moduleName = "批量删除系统字典分类")
    public int delSysDictCategoryList(@RequestBody String ids) {
        return sysDictCategoryService.delSysDictCategoryList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取系统字典分类详情", notes = "根据主键id获取系统字典分类详情")
    @SerializedField(moduleName = "获取系统字典分类")
    public SysDictCategory getSysDictCategoryInfo(@PathVariable(value = "id") String id) {
        return sysDictCategoryService.getSysDictCategoryInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取系统字典分类列表", notes = "获取系统字典分类列表")
    @SerializedField(moduleName = "获取系统字典分类列表")
    public List<SysDictCategory> getSysDictCategoryList(@RequestParam(value = "keyword", required = false) String keyword) {
        return sysDictCategoryService.getSysDictCategoryList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页系统字典分类列表", notes = "获取分页系统字典分类列表")
    @SerializedField(moduleName = "获取系统字典分类列表")
    public PageResult<SysDictCategory> getSysDictCategoryPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return sysDictCategoryService.getSysDictCategoryPageList(keyword, pageRequest);
    }
}

