package com.wisdom.system.api.core.current;

import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.service.current.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    CurrentUserService currentUserService;

    public CurrentUser getCurrentUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        String loginCode = userDetails.getUsername();//获取登录账号
        return currentUserService.getCurrentUser(loginCode);
    }
}
