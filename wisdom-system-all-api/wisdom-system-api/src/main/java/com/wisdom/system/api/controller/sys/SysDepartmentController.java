package com.wisdom.system.api.controller.sys;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.sys.SysDepartment;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.common.util.PinyinUtil;
import com.wisdom.system.service.sys.SysDepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/sys/department")
@Api(tags = {"SysDepartmentController"}, description = "部门服务接口")
public class SysDepartmentController {

    @Autowired
    private SysDepartmentService sysDepartmentService;
    
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增部门记录信息", notes = "新增部门记录信息")
    @SerializedField(moduleName = "新增部门")
    public int addSysDepartmentInfo(@RequestBody @Validated SysDepartment sysDepartment) {
        CurrentUser currentUser = userService.getCurrentUser();
        sysDepartment.setAddUserId(currentUser.getUserId());
        sysDepartment.setPyCode(PinyinUtil.ToFirstChar(sysDepartment.getName()));
        sysDepartment.setWbCode(PinyinUtil.toWBCode(sysDepartment.getName()));
        return sysDepartmentService.addSysDepartmentInfo(sysDepartment);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改部门记录信息", notes = "修改部门记录信息")
    @SerializedField(moduleName = "修改部门")
    public int updateSysDepartmentInfo(@RequestBody @Validated SysDepartment sysDepartment) {
        return sysDepartmentService.updateSysDepartmentInfo(sysDepartment);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定部门", notes = "根据主键id删除指定部门")
    @SerializedField(moduleName = "删除部门")
    public int delSysDepartmentInfo(@PathVariable(value = "id") String id) {
        return sysDepartmentService.delSysDepartmentInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除部门列表信息", notes = "根据ids批量删除部门列表信息")
    @SerializedField(moduleName = "批量删除部门")
    public int delSysDepartmentList(@RequestBody String ids) {
        return sysDepartmentService.delSysDepartmentList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取部门详情", notes = "根据主键id获取部门详情")
    @SerializedField(moduleName = "获取部门")
    public SysDepartment getSysDepartmentInfo(@PathVariable(value = "id") String id) {
        return sysDepartmentService.getSysDepartmentInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取部门列表", notes = "获取部门列表")
    @SerializedField(moduleName = "获取部门列表")
    public List<SysDepartment> getSysDepartmentList(@RequestParam(value = "state", required = false) Integer state, @RequestParam(value = "fkDeptTypeCode", required = false) String fkDeptTypeCode, @RequestParam(value = "keyword", required = false) String keyword) {
        return sysDepartmentService.getSysDepartmentList(state, fkDeptTypeCode, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页部门列表", notes = "获取分页部门列表")
    @SerializedField(moduleName = "获取部门列表")
    public PageResult<SysDepartment> getSysDepartmentPageList(@RequestParam(value = "state", required = false) Integer state, @RequestParam(value = "fkDeptTypeCode", required = false) String fkDeptTypeCode, @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return sysDepartmentService.getSysDepartmentPageList(state, fkDeptTypeCode, keyword, pageRequest);
    }

    @RequestMapping(value = "/{id}/{state}", method = RequestMethod.PUT)
    @ApiOperation(value = "更新部门状态", notes = "更新部门状态")
    @SerializedField(moduleName = "更新部门状态")
    public int updateSysDepartmentState(@PathVariable("id") String id, @PathVariable("state") int state) {
        return sysDepartmentService.updateSysDepartmentState(id, state);
    }

}

