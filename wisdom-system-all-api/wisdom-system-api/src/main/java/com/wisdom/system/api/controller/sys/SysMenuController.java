package com.wisdom.system.api.controller.sys;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.sys.SysMenu;
import com.wisdom.system.entity.sys.SysMenuTree;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.sys.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/sys/menu")
@Api(tags = {"03 SysMenuController"}, description = "系统菜单服务接口")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增菜单记录信息", notes = "新增菜单记录信息")
    @SerializedField(moduleName = "新增菜单")
    public int addSysMenuInfo(@RequestBody @Validated SysMenu sysMenu) {
        CurrentUser currentUser = userService.getCurrentUser();
        sysMenu.setAddUserId(currentUser.getUserId());
        return sysMenuService.addSysMenuInfo(sysMenu);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改菜单记录信息", notes = "修改菜单记录信息")
    @SerializedField(moduleName = "修改菜单")
    public int updateSysMenuInfo(@RequestBody @Validated SysMenu sysMenu) {
        return sysMenuService.updateSysMenuInfo(sysMenu);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定菜单", notes = "根据主键id删除指定菜单")
    @SerializedField(moduleName = "删除菜单")
    public int delSysMenuInfo(@PathVariable(value = "id") String id) {
        return sysMenuService.delSysMenuInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除菜单列表信息", notes = "根据ids批量删除菜单列表信息")
    @SerializedField(moduleName = "批量删除菜单")
    public int delSysMenuList(@RequestBody String ids) {
        return sysMenuService.delSysMenuList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取菜单详情", notes = "根据主键id获取菜单详情")
    @SerializedField(moduleName = "获取菜单")
    public SysMenu getSysMenuInfo(@PathVariable("id") String id) {
        return sysMenuService.getSysMenuInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取菜单列表", notes = "获取菜单列表")
    @SerializedField(moduleName = "获取菜单列表")
    public List<SysMenu> getSysMenuList(@RequestParam(value = "keyword", required = false) String keyword,
                                        @RequestParam(value = "fkSystem", required = false) String fkSystem) {
        return sysMenuService.getSysMenuList(keyword, fkSystem);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页菜单列表", notes = "获取分页菜单列表")
    @SerializedField(moduleName = "获取菜单列表")
    public PageResult<SysMenu> getSysMenuPageList(@RequestParam(value = "keyword", required = false) String keyword,
                                                  @RequestParam(value = "fkSystem", required = false) String fkSystem,
                                                  PageRequest pageRequest) {
        return sysMenuService.getSysMenuPageList(keyword, fkSystem, pageRequest);
    }

    @RequestMapping(value = "/{id}/{state}", method = RequestMethod.PUT)
    @ApiOperation(value = "更新菜单状态", notes = "更新菜单状态")
    @SerializedField(moduleName = "更新菜单状态")
    public int updateSysMenuState(@PathVariable("id") String id, @PathVariable("state") int state) {
        return sysMenuService.updateSysMenuState(id, state);
    }

    @RequestMapping(value = "/auth/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取登录人权限菜单分页列表", notes = "获取登录人权限菜单分页列表")
    @SerializedField(moduleName = "获取登录人权限菜单分页列表")
    public PageResult<SysMenu> getSysMenuAuthList(@RequestParam(value = "fkSystem", required = false) String fkSystem,
                                                  PageRequest pageRequest) {
        CurrentUser currentUser = userService.getCurrentUser();
        return sysMenuService.getUserSysMenuPageList(currentUser.getUserId(), fkSystem, "1", pageRequest);
    }

    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户权限菜单", notes = "获取用户权限菜单")
    @SerializedField(moduleName = "获取用户权限菜单")
    public List<SysMenuTree> getUserSysMenuTree(@RequestParam(value = "fkSystem", required = false) String fkSystem) {
        CurrentUser currentUser = userService.getCurrentUser();
        return sysMenuService.getUserSysMenuTree(currentUser.getUserId(), fkSystem);
    }
}