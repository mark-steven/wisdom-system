package com.wisdom.system.api.controller.per;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.per.Attmanagement;
import com.wisdom.system.entity.sys.SysDepartment;
import com.wisdom.system.service.per.AttmanagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/att/manage")
@Api(tags = {"AttmanagementController"}, description = "考勤服务接口")
public class AttmanagementController {

    @Autowired
    private AttmanagementService attmanagementService;

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除", notes = "根据主键id删除")
    @SerializedField(moduleName = "删除")
    public int delAttmanagementInfo(@PathVariable(value = "id") String id) {
        return attmanagementService.delAttmanagementInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除列表信息", notes = "根据ids批量删除列表信息")
    @SerializedField(moduleName = "批量删除")
    public int delAttmanagementList(@RequestBody String ids) {
        return attmanagementService.delAttmanagementList(ids);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取考勤列表", notes = "获取考勤列表")
    @SerializedField(moduleName = "获取考勤列表")
    public List<Attmanagement> getAttmanagementList(@RequestParam(value = "startTime", required = false) String startTime,
                                                    @RequestParam(value = "endTime", required = false) String endTime,
                                                    @RequestParam(value = "keyword", required = false) String keyword) {
        return attmanagementService.getAttmanagementList(startTime,endTime,keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页考勤列表", notes = "获取分页考勤列表")
    @SerializedField(moduleName = "获取考勤列表")
    public PageResult<Attmanagement> getAttmanagementPageList(@RequestParam(value = "startTime", required = false) String startTime,
                                                              @RequestParam(value = "endTime", required = false) String endTime,
                                                              @RequestParam(value = "keyword", required = false) String keyword,
                                                              PageRequest pageRequest) {
        //因为数据库的时间是YYYY/MM/DD,入参的格式是YYYY-MM-DD，所以做一层转换
        startTime = startTime.replace("-","/");
        endTime = endTime.replace("-","/");
        return attmanagementService.getAttmanagementPageList(startTime,endTime,keyword,pageRequest);
    }
}
