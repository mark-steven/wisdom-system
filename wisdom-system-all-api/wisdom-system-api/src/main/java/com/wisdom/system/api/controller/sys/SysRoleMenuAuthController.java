package com.wisdom.system.api.controller.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysRoleMenuAuth;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.sys.SysRoleMenuAuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 角色菜单信息接口
 *
 * @Date 2019/3/12 11:13
 * author: GuoHonghui
 */
@RestController
@RequestMapping(value = "api/sys/role/menu")
@Api(tags = {"06 SysRoleMenuController"}, description = "角色菜单信息接口")
public class SysRoleMenuAuthController {

    @Autowired
    private SysRoleMenuAuthService roleMenuAuthService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增角色菜单信息", notes = "新增角色菜单信息")
    @SerializedField(moduleName = "新增角色菜单信息")
    public int addSysRoleMenuAuthInfo(@RequestBody @Validated SysRoleMenuAuth sysRoleMenuAuth) {
        return roleMenuAuthService.addRoleMenuAuthInfo(sysRoleMenuAuth);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改角色菜单信息", notes = "修改角色菜单信息")
    @SerializedField(moduleName = "修改角色菜单信息")
    public int updateSysRoleMenuAuthInfo(@RequestBody @Validated SysRoleMenuAuth sysRoleMenuAuth) {
        return roleMenuAuthService.updateSysRoleMenuAuthInfo(sysRoleMenuAuth);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定角色菜单", notes = "根据主键id删除指定角色菜单")
    @SerializedField(moduleName = "删除角色菜单")
    public int delSysRoleMenuAuthInfo(@PathVariable(value = "id") String id) {
        return roleMenuAuthService.delSysRoleMenuAuthInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除角色菜单列表信息", notes = "根据ids批量删除角色菜单列表信息")
    @SerializedField(moduleName = "批量删除角色菜单")
    public int delSysRoleMenuAuthList(@RequestBody String ids) {
        return roleMenuAuthService.delSysRoleMenuAuthList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取角色详情", notes = "根据主键id获取角色详情")
    @SerializedField(moduleName = "获取角色菜单")
    public SysRoleMenuAuth getSysRoleMenuAuthInfo(@PathVariable(value = "id") String id) {
        return roleMenuAuthService.getSysRoleMenuAuthInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取列表", notes = "获取角色菜单列表")
    @SerializedField(moduleName = "获取角色菜单列表")
    public List<SysRoleMenuAuth> getSysRoleMenuAuthList(@RequestParam(value = "fkRoleId", required = false) String fkRoleId,
                                                        @RequestParam(value = "keyword", required = false) String keyword) {
        return roleMenuAuthService.getSysRoleMenuAuthList(fkRoleId, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页角色授权菜单列表", notes = "获取分页角色授权菜单列表")
    @SerializedField(moduleName = "获取分页角色授权菜单列表")
    public PageResult<SysRoleMenuAuth> getSysRoleMenuAuthPageList(@RequestParam(value = "fkRoleId", required = false) String fkRoleId,
                                                                  @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return roleMenuAuthService.getSysRoleMenuAuthPageList(fkRoleId, keyword, pageRequest);
    }

    @RequestMapping(value = "/list/{fkRoleId}", method = RequestMethod.PUT)
    @ApiOperation(value = "批量更新角色授权菜单信息", notes = "批量更新角色授权菜单信息")
    @SerializedField(moduleName = "批量更新角色授权菜单信息")
    public int updateSysRoleMenuAuthList(@PathVariable(value = "fkRoleId") String fkRoleId,
                                         @RequestBody List<SysRoleMenuAuth> sysRoleMenuAuthList) {
        return roleMenuAuthService.updateSysRoleMenuAuthList(fkRoleId, sysRoleMenuAuthList);
    }
}
