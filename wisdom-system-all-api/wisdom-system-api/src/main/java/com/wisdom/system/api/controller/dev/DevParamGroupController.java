package com.wisdom.system.api.controller.dev;

import com.wisdom.system.entity.dev.DevParamGroup;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.dev.DevParamGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/dev/param/group")
@Api(tags = {"DevParamGroupController"}, description = "系统参数分组表服务接口")
public class DevParamGroupController {

    @Autowired
    private DevParamGroupService devParamGroupService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增系统参数分组表记录信息", notes = "新增系统参数分组表记录信息")
    @SerializedField(moduleName = "新增系统参数分组表")
    public int addDevParamGroupInfo(@RequestBody @Validated DevParamGroup devParamGroup) {
        return devParamGroupService.addDevParamGroupInfo(devParamGroup);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改系统参数分组表记录信息", notes = "修改系统参数分组表记录信息")
    @SerializedField(moduleName = "修改系统参数分组表")
    public int updateDevParamGroupInfo(@RequestBody @Validated DevParamGroup devParamGroup) {
        return devParamGroupService.updateDevParamGroupInfo(devParamGroup);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定系统参数分组表", notes = "根据主键id删除指定系统参数分组表")
    @SerializedField(moduleName = "删除系统参数分组表")
    public int delDevParamGroupInfo(@PathVariable(value = "id") String id) {
        return devParamGroupService.delDevParamGroupInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除系统参数分组表列表信息", notes = "根据ids批量删除系统参数分组表列表信息")
    @SerializedField(moduleName = "批量删除系统参数分组表")
    public int delDevParamGroupList(@RequestBody String ids) {
        return devParamGroupService.delDevParamGroupList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取系统参数分组表详情", notes = "根据主键id获取系统参数分组表详情")
    @SerializedField(moduleName = "获取系统参数分组表")
    public DevParamGroup getDevParamGroupInfo(@PathVariable("id") String id) {
        return devParamGroupService.getDevParamGroupInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取系统参数分组表列表", notes = "获取系统参数分组表列表")
    @SerializedField(moduleName = "获取系统参数分组表列表")
    public List<DevParamGroup> getDevParamGroupList(@RequestParam(value = "fkParamCategoryCode", required = false) String fkParamCategoryCode, @RequestParam(value = "keyword", required = false) String keyword) {
        return devParamGroupService.getDevParamGroupList(fkParamCategoryCode, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页系统参数分组表列表", notes = "获取分页系统参数分组表列表")
    @SerializedField(moduleName = "获取系统参数分组表列表")
    public PageResult<DevParamGroup> getDevParamGroupPageList(@RequestParam(value = "fkParamCategoryCode", required = false) String fkParamCategoryCode, @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devParamGroupService.getDevParamGroupPageList(fkParamCategoryCode, keyword, pageRequest);
    }
}

