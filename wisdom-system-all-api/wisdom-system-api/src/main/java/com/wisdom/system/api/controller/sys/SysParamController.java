package com.wisdom.system.api.controller.sys;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.entity.out.ResultBody;
import com.wisdom.system.entity.sys.SysParam;
import com.wisdom.system.service.sys.SysParamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/sys/param")
@Api(tags = {"SysParamController"}, description = "系统参数表服务接口")
public class SysParamController {

    @Autowired
    private SysParamService sysParamService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "根据参数key值获取系统参数值", notes = "根据参数key值获取系统参数值")
    public ResultBody getSysParamValue(@RequestParam("paramKey") String paramKey) {
        String value = sysParamService.getSysParamValue(paramKey);
        ResultBody resultBody = new ResultBody(value);
        resultBody.setMsg("根据参数key值获取系统参数值成功！");
        return resultBody;
    }


    @RequestMapping(value = "/{paramKey}", method = RequestMethod.GET)
    @ApiOperation(value = "根据参数key值获取系统参数表详情", notes = "根据参数key值获取系统参数表详情")
    @SerializedField(moduleName = "根据参数key值获取系统参数信息")
    public SysParam getSysParamInfo(@PathVariable("paramKey") String paramKey) {
        return sysParamService.getSysParamInfo(paramKey);
    }

    @RequestMapping(value = "/list/{paramGroupCode}", method = RequestMethod.PUT)
    @ApiOperation(value = "批量更新参数信息", notes = "批量更新参数信息")
    @SerializedField(moduleName = "批量更新参数信息")
    public int updateSysParamList(@PathVariable(value = "paramGroupCode") String paramGroupCode, @RequestBody List<SysParam> sysParamList) {
        return sysParamService.updateSysParamList(paramGroupCode,sysParamList);
    }
}

