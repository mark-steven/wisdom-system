package com.wisdom.system.api.controller.sys;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.sys.SysUserMenuFavorites;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.sys.SysUserMenuFavoritesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户常用菜单设置接口
 *
 * @Date 2019/3/15 9:09
 * author: GuoHonghui
 */
@RestController
@RequestMapping(value = "api/sys/user/menu/favorites")
@Api(tags = {"11 SysUserMenuFavoritesController"}, description = "用户常用菜单设置接口")
public class SysUserMenuFavoritesController {

    @Autowired
    private SysUserMenuFavoritesService userMenuFavoritesService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增用户常用菜单设置", notes = "新增用户常用菜单设置")
    @SerializedField(moduleName = "新增用户常用菜单设置")
    public int addSysUserMenuFavoritesInfo(@RequestBody @Validated SysUserMenuFavorites sysUserMenuFavorites) {
        return userMenuFavoritesService.addUserMenuFavoritesInfo(sysUserMenuFavorites);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改用户常用菜单设置", notes = "修改用户常用菜单设置")
    @SerializedField(moduleName = "修改用户常用菜单设置")
    public int updateSysUserMenuFavoritesInfo(@RequestBody @Validated SysUserMenuFavorites sysUserMenuFavorites) {
        return userMenuFavoritesService.updateSysUserMenuFavoritesInfo(sysUserMenuFavorites);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定用户常用菜单设置", notes = "根据主键id删除指定用户常用菜单设置")
    @SerializedField(moduleName = "删除用户常用菜单设置")
    public int delSysUserMenuFavoritesInfo(@PathVariable(value = "id") String id) {
        return userMenuFavoritesService.delSysUserMenuFavoritesInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除用户常用菜单设置", notes = "根据ids批量删除用户常用菜单设置")
    @SerializedField(moduleName = "批量删除用户常用菜单设置")
    public int delSysUserMenuFavoritesList(@RequestBody String ids) {
        return userMenuFavoritesService.delSysUserMenuFavoritesList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取用户常用菜单设置", notes = "根据主键id获取用户常用菜单设置")
    @SerializedField(moduleName = "获取用户常用菜单设置")
    public SysUserMenuFavorites getSysUserMenuFavoritesInfo(@PathVariable(value = "id") String id) {
        return userMenuFavoritesService.getSysUserMenuFavoritesInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户常用菜单设置列表", notes = "获取用户常用菜单设置列表")
    @SerializedField(moduleName = "获取用户常用菜单设置列表")
    public List<SysUserMenuFavorites> getSysUserMenuFavoritesList(@RequestParam(value = "fkSystem", required = false) String fkSystem) {
        CurrentUser currentUser = userService.getCurrentUser();
        return userMenuFavoritesService.getSysUserMenuFavoritesList(currentUser.getUserId(), fkSystem);
    }

    @RequestMapping(value = "/list/{fkSystem}", method = RequestMethod.PUT)
    @ApiOperation(value = "批量更新用户常用菜单设置", notes = "批量更新用户常用菜单设置")
    @SerializedField(moduleName = "批量更新用户常用菜单设置")
    public int updateSysUserMenuFavoritesList(@PathVariable(value = "fkSystem") String fkSystem,
                                              @RequestBody List<SysUserMenuFavorites> sysUserMenuFavoritesList) {
        CurrentUser currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();
        int i = 1;
        for (SysUserMenuFavorites f : sysUserMenuFavoritesList) {
            f.setFkUserId(userId);
            f.setFkSystem(fkSystem);
            f.setSort(i);
            i++;
        }
        return userMenuFavoritesService.updateSysUserMenuFavoritesList(userId, sysUserMenuFavoritesList, fkSystem);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "批量新增用户常用菜单设置", notes = "批量新增用户常用菜单设置")
    @SerializedField(moduleName = "批量新增用户常用菜单设置")
    public int addSysUserMenuFavoritesList(@RequestBody @Validated List<SysUserMenuFavorites> sysUserMenuFavorites) {
        return userMenuFavoritesService.addSysUserMenuFavoritesList(sysUserMenuFavorites);
    }
}
