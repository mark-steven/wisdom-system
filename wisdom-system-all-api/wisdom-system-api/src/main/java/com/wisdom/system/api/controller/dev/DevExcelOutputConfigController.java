package com.wisdom.system.api.controller.dev;

import com.alibaba.fastjson.JSONObject;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.common.util.ExcelUtil;
import com.wisdom.system.common.util.PinyinUtil;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevExcelOutputConfig;
import com.wisdom.system.service.dev.DevExcelOutputConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="api/dev/excel/output")
@Api(tags={"DevExcelOutputConfigController"},description="Excel导出配置服务接口")
public class DevExcelOutputConfigController {

    @Autowired
    private DevExcelOutputConfigService devExcelOutputConfigService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value="新增Excel导出配置记录信息",notes="新增Excel导出配置记录信息")
    @SerializedField(moduleName = "新增Excel导出配置")
    public int addDevExcelOutputConfigInfo(@RequestBody @Validated DevExcelOutputConfig devExcelOutputConfig) {
        devExcelOutputConfig.setPyCode(PinyinUtil.ToFirstChar(devExcelOutputConfig.getName()));
        devExcelOutputConfig.setWbCode(PinyinUtil.toWBCode(devExcelOutputConfig.getName()));
        return devExcelOutputConfigService.addDevExcelOutputConfigInfo(devExcelOutputConfig);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改Excel导出配置记录信息", notes = "修改Excel导出配置记录信息")
    @SerializedField(moduleName = "修改Excel导出配置")
    public int updateDevExcelOutputConfigInfo(@RequestBody DevExcelOutputConfig devExcelOutputConfig) {
        return devExcelOutputConfigService.updateDevExcelOutputConfigInfo(devExcelOutputConfig);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定Excel导出配置", notes = "根据主键id删除指定Excel导出配置")
    @SerializedField(moduleName = "删除Excel导出配置")
    public int delDevExcelOutputConfigInfo(@PathVariable(value = "id") String id) {
        return devExcelOutputConfigService.delDevExcelOutputConfigInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除Excel导出配置列表信息", notes = "根据ids批量删除Excel导出配置列表信息")
    @SerializedField(moduleName = "批量删除Excel导出配置")
    public int delDevExcelOutputConfigList(@RequestBody String ids) {
        return devExcelOutputConfigService.delDevExcelOutputConfigList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取Excel导出配置详情", notes = "根据主键id获取Excel导出配置详情")
    @SerializedField(moduleName = "获取Excel导出配置")
    public DevExcelOutputConfig getDevExcelOutputConfigInfo(@PathVariable(value = "id") String id) {
        return devExcelOutputConfigService.getDevExcelOutputConfigInfo(id);
    }

    @RequestMapping(value = "code/{code}", method = RequestMethod.GET)
    @ApiOperation(value = "根据code获取Excel导出配置详情", notes = "根据code获取Excel导出配置详情")
    @SerializedField(moduleName = "获取Excel导出配置")
    public DevExcelOutputConfig getDevExcelOutputConfigByCode(@PathVariable(value = "code") String code) {
        return devExcelOutputConfigService.getDevExcelOutputConfigByCode(code);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取Excel导出配置列表", notes = "获取Excel导出配置列表")
    @SerializedField(moduleName = "获取Excel导出配置列表")
    public List<DevExcelOutputConfig> getDevExcelOutputConfigList(@RequestParam(value = "keyword", required = false) String keyword) {
       return devExcelOutputConfigService.getDevExcelOutputConfigList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取Excel导出配置分页列表", notes = "获取Excel导出配置分页列表")
    @SerializedField(moduleName = "获取Excel导出配置列表")
    public PageResult<DevExcelOutputConfig> getDevExcelOutputConfigPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devExcelOutputConfigService.getDevExcelOutputConfigPageList(keyword, pageRequest);
    }

    /**
     * 导出Excel
     *
     * @param response
     * @param code code
     * @param paramJson 请求占位符
     * @param sheetName 脚注
     * @param titleName 标题名称
     * @param ExcelName 导出报表名
     * @param pageRequest 页数页码
     * @throws IOException
     */
    @RequestMapping(value = "/exceldownload", method = RequestMethod.GET)
    @ApiOperation(value = "Excel导出", notes = "Excel导出")
    @SerializedField(moduleName = "Excel导出")
    public void excelDownload(HttpServletResponse response,
                              @RequestParam(value = "code") String code,
                              @RequestParam(value = "paramJson") String paramJson,
                              @RequestParam(value = "sheetName") String sheetName,
                              @RequestParam(value = "titleName") String titleName,
                              @RequestParam(value = "ExcelName") String ExcelName,
                              PageRequest pageRequest) throws IOException {
        String fileName = ExcelName;
        Map<String, Object> param = (Map<String, Object>) JSONObject.parse(paramJson);
        List<Map<String, Object>> list=devExcelOutputConfigService.getExcelOutputInfoPageList(code, param, pageRequest).getList();
        ExcelUtil.exportExcel(response, list, sheetName, fileName, 30,titleName);
    }
}


