package com.wisdom.system.api.controller.current;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.out.ResultBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/api/current")
@Api(tags = "MI Controller", description = "基础接口")
public class CurrentUserController {

    @Autowired
    UserService UserService;

    @Resource
    private ConsumerTokenServices consumerTokenServices;

    @RequestMapping(value = "/user/info", method = RequestMethod.GET)
    @ApiOperation(value = "获取当前登录用户的信息", notes = "获取当前登录用户的信息")
    public CurrentUser getCurrentUser() {
        return UserService.getCurrentUser();
    }

    /**
     * 退出系统
     *
     * @param accessToken
     * @return
     */
    @GetMapping("/logout")
    public ResultBody logout(@RequestParam("access_token") String accessToken) {
        String[] split = accessToken.split(",");
        String token = split[0];
        if (consumerTokenServices.revokeToken(token)) {
            ResultBody result = new ResultBody();
            result.setCode("0");
            result.setMsg("退出成功");
            return result;
        }
        ResultBody result = new ResultBody();
        result.setCode("-1");
        result.setMsg("退出失败");
        return result;
    }

}
