package com.wisdom.system.api.controller.sys;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysSystemLog;
import com.wisdom.system.service.sys.SysSystemLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api/sys/system/log")
@Api(tags={"SysSystemLogController"},description="系统日志表服务接口")
public class SysSystemLogController {

    @Autowired
    private SysSystemLogService sysSystemLogService;
    
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value="新增系统日志表记录信息",notes="新增系统日志表记录信息")
    @SerializedField(moduleName = "新增系统日志表")
    public int addSysSystemLogInfo(@RequestBody @Validated SysSystemLog sysSystemLog) {
        return sysSystemLogService.addSysSystemLogInfo(sysSystemLog);
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改系统日志表记录信息", notes = "修改系统日志表记录信息")
    @SerializedField(moduleName = "修改系统日志表")
    public int updateSysSystemLogInfo(@RequestBody SysSystemLog sysSystemLog) {
        return sysSystemLogService.updateSysSystemLogInfo(sysSystemLog);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定系统日志表", notes = "根据主键id删除指定系统日志表")
    @SerializedField(moduleName = "删除系统日志表")
    public int delSysSystemLogInfo(@PathVariable(value = "id") String id) {
        return sysSystemLogService.delSysSystemLogInfo(id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除系统日志表列表信息", notes = "根据ids批量删除系统日志表列表信息")
    @SerializedField(moduleName = "批量删除系统日志表")
    public int delSysSystemLogList(String ids) {
        return sysSystemLogService.delSysSystemLogList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取系统日志表详情", notes = "根据主键id获取系统日志表详情")
    @SerializedField(moduleName = "获取系统日志表")
    public SysSystemLog getSysSystemLogInfo(@PathVariable(value = "id") String id) {
        return sysSystemLogService.getSysSystemLogInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取系统日志表列表", notes = "获取系统日志表列表")
    @SerializedField(moduleName = "获取系统日志表列表")
    public List<SysSystemLog> getSysSystemLogList(@RequestParam(value = "keyword", required = false) String keyword) {
        return sysSystemLogService.getSysSystemLogList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页系统日志表列表", notes = "获取分页系统日志表列表")
    @SerializedField(moduleName = "获取系统日志表列表")
    public PageResult<SysSystemLog> getSysSystemLogPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return sysSystemLogService.getSysSystemLogPageList(keyword, pageRequest);
    }
}


