package com.wisdom.system.api.core.oauth.Thirdpartyauthorization;

import me.zhyd.oauth.config.AuthSource;

/**
 * @author guohonghui
 *
 * @version 1.0
 * 2020-02-03
 * @since 1.8
 */
public enum AuthCustomSource implements AuthSource {

    /**
     * 自己搭建的gitlab私服
     */
    MYGITLAB {
        /**
         * 授权的api
         *
         * @return url
         */
        @Override
        public String authorize() {
            return "http://gitlab.innodev.cn:9001/oauth/authorize";
        }

        /**
         * 获取accessToken的api
         *
         * @return url
         */
        @Override
        public String accessToken() {
            return "http://gitlab.innodev.cn:9001/oauth/token";
        }

        /**
         * 获取用户信息的api
         *
         * @return url
         */
        @Override
        public String userInfo() {
            return "http://gitlab.innodev.cn:9001/api/v4/user";
        }
    }
}
