package com.wisdom.system.api.controller.dev;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevExcelImportConfig;
import com.wisdom.system.service.dev.DevExcelImportConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api/dev/excel/import/config")
@Api(tags={"DevExcelImportConfigController"},description="EXCEL导入配置服务接口")
public class DevExcelImportConfigController {

    @Autowired
    private DevExcelImportConfigService devExcelImportConfigService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value="新增EXCEL导入配置记录信息",notes="新增EXCEL导入配置记录信息")
    @SerializedField(moduleName = "新增EXCEL导入配置")
    public int addDevExcelImportConfigInfo(@RequestBody @Validated DevExcelImportConfig devExcelImportConfig) {
        return devExcelImportConfigService.addDevExcelImportConfigInfo(devExcelImportConfig);
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改EXCEL导入配置记录信息", notes = "修改EXCEL导入配置记录信息")
    @SerializedField(moduleName = "修改EXCEL导入配置")
    public int updateDevExcelImportConfigInfo(@RequestBody @Validated DevExcelImportConfig devExcelImportConfig) {
        return devExcelImportConfigService.updateDevExcelImportConfigInfo(devExcelImportConfig);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定EXCEL导入配置", notes = "根据主键id删除指定EXCEL导入配置")
    @SerializedField(moduleName = "删除EXCEL导入配置")
    public int delDevExcelImportConfigInfo(@PathVariable(value = "id") String id) {
        return devExcelImportConfigService.delDevExcelImportConfigInfo(id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除EXCEL导入配置列表信息", notes = "根据ids批量删除EXCEL导入配置列表信息")
    @SerializedField(moduleName = "批量删除EXCEL导入配置")
    public int delDevExcelImportConfigList(@RequestBody String ids) {
        return devExcelImportConfigService.delDevExcelImportConfigList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取EXCEL导入配置详情", notes = "根据主键id获取EXCEL导入配置详情")
    @SerializedField(moduleName = "获取EXCEL导入配置")
    public DevExcelImportConfig getDevExcelImportConfigInfo(@PathVariable(value = "id") String id) {
        return devExcelImportConfigService.getDevExcelImportConfigInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取EXCEL导入配置列表", notes = "获取EXCEL导入配置列表")
    @SerializedField(moduleName = "获取EXCEL导入配置列表")
    public List<DevExcelImportConfig> getDevExcelImportConfigList(@RequestParam(value = "keyword", required = false) String keyword) {
        return devExcelImportConfigService.getDevExcelImportConfigList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页EXCEL导入配置列表", notes = "获取分页EXCEL导入配置列表")
    @SerializedField(moduleName = "获取EXCEL导入配置列表")
    public PageResult<DevExcelImportConfig> getDevExcelImportConfigPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devExcelImportConfigService.getDevExcelImportConfigPageList(keyword, pageRequest);
    }



}


