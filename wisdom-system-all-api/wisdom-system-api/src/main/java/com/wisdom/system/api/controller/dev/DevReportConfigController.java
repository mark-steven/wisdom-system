package com.wisdom.system.api.controller.dev;

import com.alibaba.fastjson.JSONObject;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.common.util.PinyinUtil;
import com.wisdom.system.common.util.StringUtil;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevReportConfig;
import com.wisdom.system.service.dev.DevReportConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/dev/report/config")
@Api(tags = {"DevReportConfigController"}, description = "服务接口")
public class DevReportConfigController {

    @Autowired
    private DevReportConfigService devReportConfigService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增记录信息", notes = "新增记录信息")
    @SerializedField(moduleName = "新增")
    public int addDevReportConfigInfo(@RequestBody @Validated DevReportConfig devReportConfig) {
        devReportConfig.setPyCode(PinyinUtil.ToFirstChar(devReportConfig.getName()));
        devReportConfig.setWbCode(PinyinUtil.toWBCode(devReportConfig.getName()));
        return devReportConfigService.addDevReportConfigInfo(devReportConfig);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改记录信息", notes = "修改记录信息")
    @SerializedField(moduleName = "修改")
    public int updateDevReportConfigInfo(@RequestBody DevReportConfig devReportConfig) {
        return devReportConfigService.updateDevReportConfigInfo(devReportConfig);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定", notes = "根据主键id删除指定")
    @SerializedField(moduleName = "删除")
    public int delDevReportConfigInfo(@PathVariable(value = "id") String id) {
        return devReportConfigService.delDevReportConfigInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除列表信息", notes = "根据ids批量删除列表信息")
    @SerializedField(moduleName = "批量删除")
    public int delDevReportConfigList(String ids) {
        return devReportConfigService.delDevReportConfigList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取详情", notes = "根据主键id获取详情")
    @SerializedField(moduleName = "获取")
    public DevReportConfig getDevReportConfigInfo(@PathVariable(value = "id") String id) {
        return devReportConfigService.getDevReportConfigInfo(id);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @ApiOperation(value = "根据报表编码获取详情", notes = "根据报表编码获取详情")
    @SerializedField(moduleName = "获取")
    public DevReportConfig getDevReportConfigInfoByCode(@PathVariable(value = "code") String code) {
        return devReportConfigService.getDevReportConfigInfoByCode(code);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @SerializedField(moduleName = "获取列表")
    public List<DevReportConfig> getDevReportConfigList(@RequestParam(value = "keyword", required = false) String keyword) {
        return devReportConfigService.getDevReportConfigList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页列表", notes = "获取分页列表")
    @SerializedField(moduleName = "获取列表")
    public PageResult<DevReportConfig> getDevReportConfigPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devReportConfigService.getDevReportConfigPageList(keyword, pageRequest);
    }

    @RequestMapping(value = "/apply", method = RequestMethod.GET)
    @ApiOperation(value = "报表应用", notes = "报表应用")
    @SerializedField(moduleName = "获取报表信息列表")
    @ResponseBody
    public PageResult<Map<String, Object>> getReportInfoList(@RequestParam(value = "code") String code,
                                                             @RequestParam(value = "paramJson", required = false) String paramJson,
                                                             PageRequest pageRequest) {
        Map<String, Object> param = null;
        if(StringUtil.isEmpty(paramJson)){
            param = new HashMap<>(2);
        }else{
            param = (Map<String, Object>) JSONObject.parse(paramJson);
        }
        return devReportConfigService.getReportInfoPageList(code, param, pageRequest);
    }
}

