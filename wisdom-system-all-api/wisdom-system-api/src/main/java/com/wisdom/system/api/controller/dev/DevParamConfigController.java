package com.wisdom.system.api.controller.dev;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevParamConfig;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.dev.DevParamConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/dev/param/config")
@Api(tags = {"DevParamConfigController"}, description = "系统参数配置表服务接口")
public class DevParamConfigController {

    @Autowired
    private DevParamConfigService devParamConfigService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增系统参数配置表记录信息", notes = "新增系统参数配置表记录信息")
    @SerializedField(moduleName = "新增系统参数配置表")
    public int addDevParamConfigInfo(@RequestBody @Validated DevParamConfig devParamConfig) {
        return devParamConfigService.addDevParamConfigInfo(devParamConfig);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改系统参数配置表记录信息", notes = "修改系统参数配置表记录信息")
    @SerializedField(moduleName = "修改系统参数配置表")
    public int updateDevParamConfigInfo(@RequestBody @Validated DevParamConfig devParamConfig) {
        return devParamConfigService.updateDevParamConfigInfo(devParamConfig);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定系统参数配置表", notes = "根据主键id删除指定系统参数配置表")
    @SerializedField(moduleName = "删除系统参数配置表")
    public int delDevParamConfigInfo(@PathVariable(value = "id") String id) {
        return devParamConfigService.delDevParamConfigInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除系统参数配置表列表信息", notes = "根据ids批量删除系统参数配置表列表信息")
    @SerializedField(moduleName = "批量删除系统参数配置表")
    public int delDevParamConfigList(@RequestBody String ids) {
        return devParamConfigService.delDevParamConfigList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取系统参数配置表详情", notes = "根据主键id获取系统参数配置表详情")
    @SerializedField(moduleName = "获取系统参数配置表")
    public DevParamConfig getDevParamConfigInfo(@PathVariable("id") String id) {
        return devParamConfigService.getDevParamConfigInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取系统参数配置表列表", notes = "获取系统参数配置表列表")
    @SerializedField(moduleName = "获取系统参数配置表列表")
    public List<DevParamConfig> getDevParamConfigList(@RequestParam(value = "fkParamCategoryCode", required = false) String fkParamCategoryCode, @RequestParam(value = "fkParamGroupCode", required = false) String fkParamGroupCode, @RequestParam(value = "keyword", required = false) String keyword) {
        return devParamConfigService.getDevParamConfigList(fkParamCategoryCode, fkParamGroupCode, keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页系统参数配置表列表", notes = "获取分页系统参数配置表列表")
    @SerializedField(moduleName = "获取系统参数配置表列表")
    public PageResult<DevParamConfig> getDevParamConfigPageList(@RequestParam(value = "fkParamCategoryCode", required = false) String fkParamCategoryCode, @RequestParam(value = "fkParamGroupCode", required = false) String fkParamGroupCode, @RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devParamConfigService.getDevParamConfigPageList(fkParamCategoryCode, fkParamGroupCode, keyword, pageRequest);
    }
}

