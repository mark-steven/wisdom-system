package com.wisdom.system.api.controller.sys;

import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.entity.sys.SysTree;
import com.wisdom.system.service.sys.SysTreeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/sys/tree")
public class SysTreeController {

    @Autowired
    private SysTreeService sysTreeService;

    @RequestMapping(value = "dept", method = RequestMethod.GET)
    @ApiOperation(value = "获取部门树", notes = "获取部门树")
    @SerializedField(moduleName = "获取部门树")
    public List<SysTree> getSysDepartmentTree(@RequestParam(value = "keyword", required = false) String keyword, @RequestParam(value = "state", required = false) Integer state, @RequestParam(value = "code", required = false) String code,@RequestParam(value = "fkDeptTypeCode", required = false) String fkDeptTypeCode) {
        return sysTreeService.getSysDeptTree(keyword, state,code,fkDeptTypeCode);
    }

}
