package com.wisdom.system.api.controller.dev;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevParamCategory;
import com.wisdom.system.common.annotation.SerializedField;
import com.wisdom.system.service.dev.DevParamCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api/dev/param/category")
@Api(tags={"DevParamCategoryController"},description="系统参数分类表服务接口")
public class DevParamCategoryController {

    @Autowired
    private DevParamCategoryService devParamCategoryService;
    
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value="新增系统参数分类表记录信息",notes="新增系统参数分类表记录信息")
    @SerializedField(moduleName = "新增系统参数分类表")
    public int addDevParamCategoryInfo(@RequestBody @Validated DevParamCategory sysParamCategory) {
        return devParamCategoryService.addDevParamCategoryInfo(sysParamCategory);
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改系统参数分类表记录信息", notes = "修改系统参数分类表记录信息")
    @SerializedField(moduleName = "修改系统参数分类表")
    public int updateDevParamCategoryInfo(@RequestBody @Validated DevParamCategory sysParamCategory) {
        return devParamCategoryService.updateDevParamCategoryInfo(sysParamCategory);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定系统参数分类表", notes = "根据主键id删除指定系统参数分类表")
    @SerializedField(moduleName = "删除系统参数分类表")
    public int delDevParamCategoryInfo(@PathVariable(value = "id") String id) {
        return devParamCategoryService.delDevParamCategoryInfo(id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除系统参数分类表列表信息", notes = "根据ids批量删除系统参数分类表列表信息")
    @SerializedField(moduleName = "批量删除系统参数分类表")
    public int delDevParamCategoryList(@RequestBody String ids) {
        return devParamCategoryService.delDevParamCategoryList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取系统参数分类表详情", notes = "根据主键id获取系统参数分类表详情")
    @SerializedField(moduleName = "获取系统参数分类表")
    public DevParamCategory getDevParamCategoryInfo(@PathVariable("id") String id) {
        return devParamCategoryService.getDevParamCategoryInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取系统参数分类表列表", notes = "获取系统参数分类表列表")
    @SerializedField(moduleName = "获取系统参数分类表列表")
    public List<DevParamCategory> getDevParamCategoryList(@RequestParam(value = "keyword", required = false) String keyword) {
        return devParamCategoryService.getDevParamCategoryList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页系统参数分类表列表", notes = "获取分页系统参数分类表列表")
    @SerializedField(moduleName = "获取系统参数分类表列表")
    public PageResult<DevParamCategory> getDevParamCategoryPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return devParamCategoryService.getDevParamCategoryPageList(keyword, pageRequest);
    }
}

