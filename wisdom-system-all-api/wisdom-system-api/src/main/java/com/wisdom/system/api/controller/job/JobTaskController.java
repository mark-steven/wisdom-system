package com.wisdom.system.api.controller.job;

import com.wisdom.system.api.core.current.UserService;
import com.wisdom.system.api.core.job.JobSchedule;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.current.CurrentUser;
import com.wisdom.system.entity.job.JobTask;
import com.wisdom.system.entity.out.ResultBody;
import com.wisdom.system.common.annotation.SerializedField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.wisdom.system.service.job.JobTaskService;

import java.util.List;

@RestController
@RequestMapping(value = "api/job/task")
@Api(tags = {"JobController"}, description = "调度")
public class JobTaskController {

    @Autowired
    private JobTaskService jobTaskService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "{id}/start", method = RequestMethod.GET)
    @ApiOperation(value = "启动调度", notes = "启动调度")
    public ResultBody startJob(@PathVariable("id") String id) {
        JobTask jobTask = jobTaskService.getJobTaskInfo(id);
        JobSchedule.startJob(jobTask);
        ResultBody resultBody = new ResultBody();
        int num = jobTaskService.updateJobTaskState(id, 1);
        resultBody.setCode("0");
        resultBody.setResult(num);
        resultBody.setMsg("启动任务成功！");
        return resultBody;
    }

    @RequestMapping(value = "{id}/stop", method = RequestMethod.GET)
    @ApiOperation(value = "停止调度", notes = "停止调度")
    public ResultBody stopJob(@PathVariable("id") String id) {
        JobTask jobTask = jobTaskService.getJobTaskInfo(id);
        int num = jobTaskService.updateJobTaskState(id, 0);
        JobSchedule.stopJob(jobTask);
        ResultBody resultBody = new ResultBody();
        resultBody.setCode("0");
        resultBody.setResult(num);
        resultBody.setMsg("停用任务成功！");
        return resultBody;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "新增调度任务记录信息", notes = "新增调度任务记录信息")
    @SerializedField(moduleName = "新增调度任务")
    public int addJobTaskInfo(@RequestBody @Validated JobTask jobTask) {
        CurrentUser currentUser = userService.getCurrentUser();
        jobTask.setAddUserId(currentUser.getUserId());
        return jobTaskService.addJobTaskInfo(jobTask);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "修改调度任务记录信息", notes = "修改调度任务记录信息")
    @SerializedField(moduleName = "修改调度任务")
    public int updateJobTaskInfo(@RequestBody @Validated JobTask jobTask) {
        return jobTaskService.updateJobTaskInfo(jobTask);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "根据主键id删除指定调度任务", notes = "根据主键id删除指定调度任务")
    @SerializedField(moduleName = "删除调度任务")
    public int delJobTaskInfo(@PathVariable(value = "id") String id) {
        return jobTaskService.delJobTaskInfo(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ids批量删除调度任务列表信息", notes = "根据ids批量删除调度任务列表信息")
    @SerializedField(moduleName = "批量删除调度任务")
    public int delJobTaskList(@RequestBody String ids) {
        return jobTaskService.delJobTaskList(ids);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据主键id获取调度任务详情", notes = "根据主键id获取调度任务详情")
    @SerializedField(moduleName = "获取调度任务")
    public JobTask getJobTaskInfo(@PathVariable(value = "id") String id) {
        return jobTaskService.getJobTaskInfo(id);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取调度任务列表", notes = "获取调度任务列表")
    @SerializedField(moduleName = "获取调度任务列表")
    public List<JobTask> getJobTaskList(@RequestParam(value = "keyword", required = false) String keyword) {
        return jobTaskService.getJobTaskList(keyword);
    }

    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取分页调度任务列表", notes = "获取分页调度任务列表")
    @SerializedField(moduleName = "获取调度任务列表")
    public PageResult<JobTask> getJobTaskPageList(@RequestParam(value = "keyword", required = false) String keyword, PageRequest pageRequest) {
        return jobTaskService.getJobTaskPageList(keyword, pageRequest);
    }
}
