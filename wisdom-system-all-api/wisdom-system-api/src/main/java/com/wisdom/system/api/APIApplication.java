package com.wisdom.system.api;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;


@EnableTransactionManagement(order = 10)
@MapperScan("com.wisdom.system.dao")
@EnableAuthorizationServer
@EnableAspectJAutoProxy(exposeProxy = true)
@SpringBootApplication
public class APIApplication {
    public static void main(String[] args) {
        SpringApplication.run(APIApplication.class, args);
        System.out.println("http://localhost:8080/swagger-ui.html");
    }
}
