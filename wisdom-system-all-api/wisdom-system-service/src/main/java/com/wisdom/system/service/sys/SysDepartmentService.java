package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysDepartment;

import java.util.List;

public interface SysDepartmentService {

    int addSysDepartmentInfo(SysDepartment sysDepartment);

    int updateSysDepartmentInfo(SysDepartment sysDepartment);

    int delSysDepartmentInfo(String id);

    int delSysDepartmentList(String ids);

    SysDepartment getSysDepartmentInfo(String id);

    List<SysDepartment> getSysDepartmentList(Integer state,String fkDeptTypeCode,String keyword);

    PageResult<SysDepartment> getSysDepartmentPageList(Integer state,String fkDeptTypeCode,String keyword, PageRequest pageRequest);

    int updateSysDepartmentState(String id, int state);

}

