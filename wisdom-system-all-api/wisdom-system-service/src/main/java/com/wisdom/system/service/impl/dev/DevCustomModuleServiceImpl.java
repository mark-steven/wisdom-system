package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.dev.DevCustomModuleMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import com.wisdom.system.entity.dev.DevCustomModule;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.common.util.StringUtil;
import com.wisdom.system.service.dev.DevCustomModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class DevCustomModuleServiceImpl implements DevCustomModuleService {

    @Autowired
    private DevCustomModuleMapper DevCustomModuleMapper;

    @Override
    @ValidationUnique(key = {"code"}, msg = {"系统模板参数编码已存在！"}, groupField = "fkDeptCode")
    public int addDevCustomModuleInfo(DevCustomModule devCustomModule) {
        return DevCustomModuleMapper.insert(devCustomModule);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"系统模板参数编码已存在！"}, groupField = "fkDeptCode", excludeSelf = true)
    public int updateDevCustomModuleInfo(DevCustomModule devCustomModule) {
        return DevCustomModuleMapper.updateByPrimaryKeySelective(devCustomModule);
    }

    @Override
    public int delDevCustomModuleInfo(String id) {
        return DevCustomModuleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delDevCustomModuleList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(DevCustomModule.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return DevCustomModuleMapper.deleteByExample(example);
    }

    @Override
    public DevCustomModule getDevCustomModuleInfo(String id) {
        return DevCustomModuleMapper.selectByPrimaryKey(id);
    }

    @Override
    public DevCustomModule getDevCustomModuleByCode(String departmentCode, String code) {
        Example example = new Example(DevCustomModule.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtil.isNotEmpty(departmentCode)) {
            criteria.andEqualTo("fkDeptCode", departmentCode);
        }
        criteria.andEqualTo("code", code);
        return DevCustomModuleMapper.selectOneByExample(example);
    }


    @Override
    public List<DevCustomModule> getDevCustomModuleList(String fkDeptCode, String keyword) {
        Example example = new Example(DevCustomModule.class);
        Example.Criteria criteria1 = example.createCriteria();
        criteria1.andEqualTo("fkDeptCode", fkDeptCode);
        Example.Criteria criteria2 = example.createCriteria();
        criteria2.orLike("name", SqlUtil.likeEscapeH(keyword))
                .orLike("code", SqlUtil.likeEscapeH(keyword))
                .orLike("pyCode", SqlUtil.likeEscapeH(keyword))
                .orLike("wbCode", SqlUtil.likeEscapeH(keyword));
        example.and(criteria2);
        example.orderBy("sort").asc();
        return DevCustomModuleMapper.selectByExample(example);
    }

    @Override
    public PageResult<DevCustomModule> getDevCustomModulePageList(String fkDeptCode, String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevCustomModuleList(fkDeptCode, keyword), page.getTotal());
    }


}




