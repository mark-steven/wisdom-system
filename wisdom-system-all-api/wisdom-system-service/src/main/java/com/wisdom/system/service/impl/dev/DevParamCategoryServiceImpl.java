package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.dev.DevParamCategoryMapper;
import com.wisdom.system.dao.dev.DevParamGroupMapper;
import com.wisdom.system.entity.dev.DevParamCategory;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevParamGroup;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.exception.LogicException;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevParamCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@Service
public class DevParamCategoryServiceImpl implements DevParamCategoryService {

    @Autowired
    private DevParamCategoryMapper devParamCategoryMapper;
    @Autowired
    private DevParamGroupMapper devParamGroupMapper;

    @Override
    @ValidationUnique(key = {"code"}, msg = {"分类编码已存在！"})
    public int addDevParamCategoryInfo(DevParamCategory sysParamCategory) {
            return devParamCategoryMapper.insert(sysParamCategory);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"分类编码已存在！"},excludeSelf = true)
    public int updateDevParamCategoryInfo(DevParamCategory sysParamCategory) {
        return devParamCategoryMapper.updateByPrimaryKeySelective(sysParamCategory);
    }

    @Override
    @Transactional
    public int delDevParamCategoryInfo(String id) {
        DevParamCategory devParamCategoryInfo = getDevParamCategoryInfo(id);
        Example example = new Example(DevParamGroup.class);
        example.createCriteria().andEqualTo("fkParamCategoryCode",devParamCategoryInfo.getCode());
        List<DevParamGroup> devParamGroups = devParamGroupMapper.selectByExample(example);
        if (devParamGroups.size() >= 1){
            throw LogicException.of("参数分类关联其他分类组，不允许删除！");
        }
        return devParamCategoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    @Transactional
    public int delDevParamCategoryList(String ids) {
        String[] idArray = ids.split(",");
        DevParamCategory devParamCategoryInfo = null;
        for (String id : idArray) {
            devParamCategoryInfo = getDevParamCategoryInfo(id);
        }
        Example example1 = new Example(DevParamGroup.class);
        example1.createCriteria().andEqualTo("fkParamCategoryCode",devParamCategoryInfo.getCode());
        List<DevParamGroup> devParamGroups = devParamGroupMapper.selectByExample(example1);
        if (devParamGroups.size() >= 1){
            throw LogicException.of("参数分类关联其他分类组，不允许删除！");
        }
        Example example = new Example(DevParamCategory.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devParamCategoryMapper.deleteByExample(example);
    }

    @Override
    public DevParamCategory getDevParamCategoryInfo(String id) {
        return devParamCategoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<DevParamCategory> getDevParamCategoryList(String keyword) {
        Example example = new Example(DevParamCategory.class);
        Example.Criteria criteria = example.createCriteria();
        Example.Criteria criteria2 = example.createCriteria();
        criteria2.andLike("name", SqlUtil.likeEscapeH(keyword));
        example.and(criteria2);
        example.orderBy("sort").asc();
        return devParamCategoryMapper.selectByExample(example);
    }

    @Override
    public PageResult<DevParamCategory> getDevParamCategoryPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevParamCategoryList(keyword), page.getTotal());
    }

}



