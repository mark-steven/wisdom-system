package com.wisdom.system.service.dev;

import java.io.FileInputStream;


public interface DevExcelImportDataService {

    /**
     * 根据业务模块编码导入Excel数据
     *
     * @param code
     * @param fileInputStream
     * @return
     */
    int importData(String code, FileInputStream fileInputStream);
}
