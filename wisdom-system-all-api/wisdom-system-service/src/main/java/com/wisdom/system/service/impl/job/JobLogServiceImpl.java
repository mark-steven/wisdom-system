package com.wisdom.system.service.impl.job;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.job.JobLogMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.job.JobLog;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.job.JobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class JobLogServiceImpl implements JobLogService {

    @Autowired
    JobLogMapper jobLogMapper;

    @Override
    public int addJobLogInfo(JobLog jobLog) {
        jobLog.setAddTime(new Date());
        return jobLogMapper.insert(jobLog);
    }

    @Override
    public int delJobLogInfo(String id) {
        return jobLogMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delJobLogList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(JobLog.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return jobLogMapper.deleteByExample(example);
    }

    @Override
    public List<JobLog> getJobLogList(String startTime,String endTime,String fkJobLogCode,String keyword) {
       return jobLogMapper.getJobLogList(startTime,endTime,fkJobLogCode,SqlUtil.likeEscapeH(keyword));
    }

    @Override
    public PageResult<JobLog> getJobLogPageList(String startTime,String endTime,String fkJobLogCode,String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getJobLogList(startTime,endTime,fkJobLogCode,keyword), page.getTotal());
    }
}




