package com.wisdom.system.service.impl.job;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.job.JobTaskMapper;
import com.wisdom.system.entity.job.JobTask;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.job.JobTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class JobTaskServiceImpl implements JobTaskService {
    @Autowired
    JobTaskMapper jobTaskMapper;


    @Override
    public int addJobTaskInfo(JobTask jobTask) {
        jobTask.setAddTime(new Date());
        return jobTaskMapper.insert(jobTask);
    }

    @Override
    public int updateJobTaskInfo(JobTask jobTask) {
        return jobTaskMapper.updateByPrimaryKeySelective(jobTask);
    }

    @Override
    public int delJobTaskInfo(String id) {
        return jobTaskMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delJobTaskList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(JobTask.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return jobTaskMapper.deleteByExample(example);
    }

    @Override
    public JobTask getJobTaskInfo(String id) {
        return jobTaskMapper.getJobTaskInfo(id);
    }

    @Override
    public List<JobTask> getJobTaskList(String keyword) {
        return jobTaskMapper.getJobTaskList(SqlUtil.likeEscapeH(keyword));
    }

    @Override
    public PageResult<JobTask> getJobTaskPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getJobTaskList(keyword), page.getTotal());
    }

    @Override
    public int updateJobTaskState(String id, int state) {
        HashMap<String, Object> hashMap = new HashMap<>(2);
        hashMap.put("id", id);
        hashMap.put("state", state);
        return jobTaskMapper.updateJobTaskState(hashMap);
    }

    @Override
    public List<JobTask> getJobTaskStartList() {
        return jobTaskMapper.getJobTaskStartList();
    }

}




