package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevExcelImportRule;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface DevExcelImportRuleService {

    int addDevExcelImportRuleInfo(DevExcelImportRule devExcelImportRule);

    int updateDevExcelImportRuleInfo(DevExcelImportRule devExcelImportRule);

    int delDevExcelImportRuleInfo(String id);

    int delDevExcelImportRuleList(String ids);

    //根据表名获取表列信息
    PageResult<DevExcelImportRule> getTableColumnList(String tableName, PageRequest pageRequest);

    DevExcelImportRule getDevExcelImportRuleInfo(String id);

    List<DevExcelImportRule> getDevExcelImportRuleList(String fkExcelImportConfigId);

    PageResult<DevExcelImportRule> getDevExcelImportRulePageList(String fkExcelImportConfigId, PageRequest pageRequest);
}


