package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevReportConfig;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;
import java.util.Map;

public interface DevReportConfigService {

    int addDevReportConfigInfo(DevReportConfig devReportConfig);

    int updateDevReportConfigInfo(DevReportConfig devReportConfig);

    int delDevReportConfigInfo(String id);

    int delDevReportConfigList(String ids);

    DevReportConfig getDevReportConfigInfo(String id);

    DevReportConfig getDevReportConfigInfoByCode(String code);

    List<DevReportConfig> getDevReportConfigList(String keyword);

    PageResult<DevReportConfig> getDevReportConfigPageList(String keyword, PageRequest pageRequest);

    PageResult<Map<String,Object>> getReportInfoPageList(String code, Map<String,Object> param, PageRequest pageRequest);
}

