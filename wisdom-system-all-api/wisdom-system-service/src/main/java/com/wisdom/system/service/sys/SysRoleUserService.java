package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysRoleUser;

import java.util.List;

/**
 * 用户角色 服务类
 *
 * @Date 2019/3/8 14:40
 * author: GuoHonghui
 */
public interface SysRoleUserService {

    /**
     * 新增用户角色
     *
     * @param sysUserRole
     * @return
     */
    int addUserRoleInfo(SysRoleUser sysUserRole);

    /**
     * 修改用户角色
     *
     * @param sysUserRole
     * @return
     */
    int updateSysUserRoleInfo(SysRoleUser sysUserRole);

    /**
     * 删除用户角色（单个）
     *
     * @param id
     * @return
     */
    int delSysUserRoleInfo(String id);

    /**
     * 删除用户角色（批量删除）
     *
     * @param ids
     * @return
     */
    int delSysUserRoleList(String ids);

    /**
     * 根据主键id获取用户角色详情
     *
     * @param id
     * @return
     */
    SysRoleUser getSysUserRoleInfo(String id);

    /**
     * 获取用户角色列表
     *
     * @param roleId
     * @param keyword
     * @return
     */
    List<SysRoleUser> getSysUserRoleList(String roleId, String keyword);

    /**
     * 获取分页用户角色列表
     *
     * @param fkRoleId
     * @param keyword
     * @param pageRequest
     * @return
     */
    PageResult<SysRoleUser> getSysUserRolePageList(String fkRoleId, String keyword, PageRequest pageRequest);

    /**
     * 根据用户角色ID批量更新用户角色关系
     *
     * @param fkRoleId
     * @param sysUserRoleList
     * @return
     */
    int updateSysUserRoleList(String fkRoleId, List<SysRoleUser> sysUserRoleList);
}
