package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysDictItem;

import java.util.List;

public interface SysDictItemService {

    int addSysDictItemInfo(SysDictItem sysDictItem);

    int updateSysDictItemInfo(SysDictItem sysDictItem);

    int delSysDictItemInfo(String id);

    int delSysDictItemList(String ids);

    SysDictItem getSysDictItemInfo(String id);

    List<SysDictItem> getSysDictItemList(String fkDictCategoryCode, String keyword);

    PageResult<SysDictItem> getSysDictItemPageList(String fkDictCategoryCode, String keyword, PageRequest pageRequest);

}

