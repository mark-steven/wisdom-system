package com.wisdom.system.service.job;

import com.wisdom.system.entity.job.JobTask;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface JobTaskService {

    int addJobTaskInfo(JobTask jobTask);

    int updateJobTaskInfo(JobTask jobTask);

    int delJobTaskInfo(String id);

    int delJobTaskList(String ids);

    JobTask getJobTaskInfo(String id);

    List<JobTask> getJobTaskList(String keyword);

    PageResult<JobTask> getJobTaskPageList(String keyword, PageRequest pageRequest);

    int updateJobTaskState(String id, int state);

    List<JobTask> getJobTaskStartList();

}


