package com.wisdom.system.service.sys;

import com.wisdom.system.entity.sys.SysTree;

import java.util.List;

public interface SysTreeService {

    List<SysTree> getSysDeptTree(String keyword, Integer state,String code,String fkDeptTypeCode);

}
