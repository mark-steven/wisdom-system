package com.wisdom.system.service.impl.sys;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.sys.SysDepartmentMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysDepartment;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.sys.SysDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class SysDepartmentServiceImpl implements SysDepartmentService {

    @Autowired
    private SysDepartmentMapper sysDepartmentMapper;


    @Override
    @ValidationUnique(key = {"code"},msg = {"部门编码已存在！"})
    public int addSysDepartmentInfo(SysDepartment sysDepartment) {
        return sysDepartmentMapper.insert(sysDepartment);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"部门编码已存在！"},excludeSelf = true)
    public int updateSysDepartmentInfo(SysDepartment sysDepartment) {
        return sysDepartmentMapper.updateByPrimaryKeySelective(sysDepartment);
    }

    @Override
    public int delSysDepartmentInfo(String id) {
        return sysDepartmentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delSysDepartmentList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(SysDepartment.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return sysDepartmentMapper.deleteByExample(example);
    }

    @Override
    public SysDepartment getSysDepartmentInfo(String id) {
        return sysDepartmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysDepartment> getSysDepartmentList(Integer state,String fkDeptTypeCode, String keyword) {
        return sysDepartmentMapper.getSysDepartmentList(state,fkDeptTypeCode,SqlUtil.likeEscapeH(keyword));
    }

    @Override
    public PageResult<SysDepartment> getSysDepartmentPageList(Integer state,String fkDeptTypeCode, String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getSysDepartmentList(state,fkDeptTypeCode, keyword), page.getTotal());
    }

    @Override
    public int updateSysDepartmentState(String id, int state) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("state", state);
        return sysDepartmentMapper.updateSysDepartmentState(hashMap);
    }

}



