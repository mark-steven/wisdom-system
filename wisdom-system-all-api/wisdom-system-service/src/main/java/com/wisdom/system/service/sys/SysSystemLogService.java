package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysSystemLog;

import java.util.List;

public interface SysSystemLogService {

    int addSysSystemLogInfo(SysSystemLog sysSystemLog);

    int updateSysSystemLogInfo(SysSystemLog sysSystemLog);

    int delSysSystemLogInfo(String id);

    int delSysSystemLogList(String ids);

    SysSystemLog getSysSystemLogInfo(String id);

    List<SysSystemLog> getSysSystemLogList(String keyword);

    PageResult<SysSystemLog> getSysSystemLogPageList(String keyword, PageRequest pageRequest);
}


