package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevParamGroup;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface DevParamGroupService {

    int addDevParamGroupInfo(DevParamGroup sysParamGroup);

    int updateDevParamGroupInfo(DevParamGroup sysParamGroup);

    int delDevParamGroupInfo(String id);

    int delDevParamGroupList(String ids);

    DevParamGroup getDevParamGroupInfo(String id);

    List<DevParamGroup> getDevParamGroupList(String fkParamCategoryCode, String keyword);

    PageResult<DevParamGroup> getDevParamGroupPageList(String fkParamCategoryCode, String keyword, PageRequest pageRequest);
}

