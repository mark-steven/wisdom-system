package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.dev.DevParamConfigMapper;
import com.wisdom.system.entity.dev.DevParamConfig;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevParamConfigService;
import com.wisdom.system.service.sys.SysParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@Service
public class DevParamConfigServiceImpl implements DevParamConfigService {

    @Autowired
    private DevParamConfigMapper devParamConfigMapper;
    @Autowired
    private SysParamService sysParamService;

    @Override
    @ValidationUnique(key = {"paramKey"}, msg = {"参数编码已存在！"})
    public int addDevParamConfigInfo(DevParamConfig sysParamConfig) {
        return devParamConfigMapper.insert(sysParamConfig);
    }

    @Override
    @ValidationUnique(key = {"paramKey"}, msg = {"参数编码已存在！"}, excludeSelf = true)
    public int updateDevParamConfigInfo(DevParamConfig sysParamConfig) {
        return devParamConfigMapper.updateByPrimaryKeySelective(sysParamConfig);
    }

    @Override
    public int delDevParamConfigInfo(String id) {
        DevParamConfig devParamConfig = this.getDevParamConfigInfo(id);
        sysParamService.deleteByKey(devParamConfig.getParamKey());
        return devParamConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delDevParamConfigList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(DevParamConfig.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devParamConfigMapper.deleteByExample(example);
    }

    @Override
    public DevParamConfig getDevParamConfigInfo(String id) {
        return devParamConfigMapper.getDevParamConfigInfo(id);
    }

    @Override
    public List<DevParamConfig> getDevParamConfigList(String fkParamCategoryCode, String fkParamGroupCode, String keyword) {
        return devParamConfigMapper.getDevParamConfigList(fkParamCategoryCode, fkParamGroupCode, SqlUtil.likeEscapeH(keyword));
    }

    @Override
    public PageResult<DevParamConfig> getDevParamConfigPageList(String fkParamCategoryCode, String fkParamGroupCode, String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevParamConfigList(fkParamCategoryCode, fkParamGroupCode, keyword), page.getTotal());
    }

}



