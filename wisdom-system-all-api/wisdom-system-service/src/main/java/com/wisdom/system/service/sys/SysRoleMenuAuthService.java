package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysRoleMenuAuth;

import java.util.List;

/**
 * 角色菜单 服务类
 *
 * @Date 2019/3/12 11:11
 * author: GuoHonghui
 */
public interface SysRoleMenuAuthService {

    /**
     * 新增角色菜单
     *
     * @param sysRoleMenuAuth
     * @return
     */
    int addRoleMenuAuthInfo(SysRoleMenuAuth sysRoleMenuAuth);

    /**
     * 修改角色菜单
     *
     * @param sysRoleMenuAuth
     * @return
     */
    int updateSysRoleMenuAuthInfo(SysRoleMenuAuth sysRoleMenuAuth);

    /**
     * 删除角色菜单（单个）
     *
     * @param id
     * @return
     */
    int delSysRoleMenuAuthInfo(String id);

    /**
     * 删除角色菜单（批量删除）
     *
     * @param ids
     * @return
     */
    int delSysRoleMenuAuthList(String ids);

    /**
     * 根据主键id获取角色菜单详情
     *
     * @param id
     * @return
     */
    SysRoleMenuAuth getSysRoleMenuAuthInfo(String id);

    /**
     * 获取角色菜单列表
     *
     * @param fkRoleId
     * @param keyword
     * @return
     */
    List<SysRoleMenuAuth> getSysRoleMenuAuthList(String fkRoleId, String keyword);

    /**
     * 获取分页列表
     *
     * @param fkRoleId
     * @param keyword
     * @param pageRequest
     * @return
     */
    PageResult<SysRoleMenuAuth> getSysRoleMenuAuthPageList(String fkRoleId, String keyword, PageRequest pageRequest);

    /**
     * 批量更新角色菜单权限
     *
     * @param fkRoleId
     * @param roleMenuList
     * @return
     */
    int updateSysRoleMenuAuthList(String fkRoleId, List<SysRoleMenuAuth> roleMenuList);

}
