package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevParamConfig;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface DevParamConfigService {

    int addDevParamConfigInfo(DevParamConfig sysParamConfig);

    int updateDevParamConfigInfo(DevParamConfig sysParamConfig);

    int delDevParamConfigInfo(String id);

    int delDevParamConfigList(String ids);

    DevParamConfig getDevParamConfigInfo(String id);

    List<DevParamConfig> getDevParamConfigList(String fkParamCategoryCode, String fkParamGroupCode, String keyword);

    PageResult<DevParamConfig> getDevParamConfigPageList(String fkParamCategoryCode, String fkParamGroupCode, String keyword, PageRequest pageRequest);
}

