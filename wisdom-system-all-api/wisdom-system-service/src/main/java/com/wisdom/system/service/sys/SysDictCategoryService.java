package com.wisdom.system.service.sys;

import com.wisdom.system.entity.sys.SysDictCategory;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface SysDictCategoryService {

    int addSysDictCategoryInfo(SysDictCategory sysDictCategory);

    int updateSysDictCategoryInfo(SysDictCategory sysDictCategory);

    int delSysDictCategoryInfo(String id);

    int delSysDictCategoryList(String ids);

    SysDictCategory getSysDictCategoryInfo(String id);

    List<SysDictCategory> getSysDictCategoryList(String keyword);

    PageResult<SysDictCategory> getSysDictCategoryPageList(String keyword, PageRequest pageRequest);
}

