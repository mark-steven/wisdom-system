package com.wisdom.system.service.impl.dev;

import com.github.pagehelper.Page;
import com.wisdom.system.dao.common.CommonMapper;
import com.wisdom.system.dao.dev.DevExcelOutputConfigMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevExcelOutputConfig;
import com.wisdom.system.entity.dev.DevReportConfig;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevExcelOutputConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 【报表配置】业务逻辑实现类
 * Generate by CodeSmith
 * 2019年09月24日 04:03
 */

@Service
public class DevExcelOutputConfigServiceImpl implements DevExcelOutputConfigService {

    @Autowired
    DevExcelOutputConfigMapper devExcelOutputConfigMapper;

    @Autowired
    private CommonMapper commonMapper;

    @Override
    @ValidationUnique(key = {"code"},  msg = {"excel编码已存在！"})
    public int addDevExcelOutputConfigInfo(DevExcelOutputConfig devExcelOutputConfig) {
        devExcelOutputConfig.setAddTime(new Date());
        return devExcelOutputConfigMapper.insert(devExcelOutputConfig);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"excel编码已存在！"}, excludeSelf = true)
    public int updateDevExcelOutputConfigInfo(DevExcelOutputConfig devExcelOutputConfig) {
        return devExcelOutputConfigMapper.updateByPrimaryKeySelective(devExcelOutputConfig);
    }

    @Override
    public int delDevExcelOutputConfigInfo(String id) {
        return devExcelOutputConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delDevExcelOutputConfigList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(DevExcelOutputConfig.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devExcelOutputConfigMapper.deleteByExample(example);
    }

    @Override
    public DevExcelOutputConfig getDevExcelOutputConfigInfo(String id) {
        return devExcelOutputConfigMapper.selectByPrimaryKey(id);
    }
    @Override
    public DevExcelOutputConfig getDevExcelOutputConfigByCode(String code) {
        Example example = new Example(DevExcelOutputConfig.class);
        example.createCriteria().andEqualTo("code",code);
        return devExcelOutputConfigMapper.selectOneByExample(example);
    }

    @Override
    public List<DevExcelOutputConfig> getDevExcelOutputConfigList(String keyword) {
        Example example = new Example(DevExcelOutputConfig.class);
        Example.Criteria criteria2 = example.createCriteria();
        criteria2.andLike("code", SqlUtil.likeEscapeH(keyword))
                .orLike("name", SqlUtil.likeEscapeH(keyword))
                .orLike("pyCode", SqlUtil.likeEscapeH(keyword))
                .orLike("wbCode", SqlUtil.likeEscapeH(keyword));
        example.and(criteria2);
        example.orderBy("sort").asc().orderBy("addTime").desc();
        return devExcelOutputConfigMapper.selectByExample(example);
    }

    @Override
    public PageResult<DevExcelOutputConfig> getDevExcelOutputConfigPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevExcelOutputConfigList(keyword), page.getTotal());
    }

    @Override
    public PageResult<Map<String, Object>> getExcelOutputInfoPageList(String code, Map<String, Object> param, PageRequest pageRequest) {
        //根据机构编码和表报编码获取报表配置相关信息
        Example example = new Example(DevReportConfig.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", code);
        DevExcelOutputConfig devExcelOutputConfig = devExcelOutputConfigMapper.selectOneByExample(example);
        if (null != devExcelOutputConfig) {
            String sql = devExcelOutputConfig.getSqlConfigContent();
            sql = SqlUtil.sqlConvert(sql, param);
            List<Map<String, Object>> list = null;
            if(pageRequest == null || pageRequest.getPageNum() == null || pageRequest.getPageSize() == null){
                list = commonMapper.getCustomSQLResult(sql);
            }else{
                list = commonMapper.getCustomSQLResultOfPage(sql, pageRequest.getPageNum() - 1, pageRequest.getPageSize());
            }
            return new PageResult<>(list, Long.valueOf(commonMapper.getCustomSQLCount(sql)));
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> getExcelOutputInfoList(String code, Map<String, Object> param) {
        Example example = new Example(DevReportConfig.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", code);
        DevExcelOutputConfig devExcelOutputConfig = devExcelOutputConfigMapper.selectOneByExample(example);
        if (null != devExcelOutputConfig) {
            String sql = devExcelOutputConfig.getSqlConfigContent();
            sql = SqlUtil.sqlConvert(sql, param);
            List<Map<String, Object>> list = commonMapper.getCustomSQLResult(sql);
            return list;
        }
        return null;
    }
}

