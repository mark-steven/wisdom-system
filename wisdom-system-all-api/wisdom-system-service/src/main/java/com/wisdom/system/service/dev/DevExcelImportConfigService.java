package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevExcelImportConfig;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface DevExcelImportConfigService {

    int addDevExcelImportConfigInfo(DevExcelImportConfig devExcelImportConfig);

    int updateDevExcelImportConfigInfo(DevExcelImportConfig devExcelImportConfig);

    int delDevExcelImportConfigInfo(String id);

    int delDevExcelImportConfigList(String ids);

    DevExcelImportConfig getDevExcelImportConfigInfo(String id);

    DevExcelImportConfig getDevExcelImportConfigByCode(String code);

    List<DevExcelImportConfig> getDevExcelImportConfigList(String keyword);

    PageResult<DevExcelImportConfig> getDevExcelImportConfigPageList(String keyword, PageRequest pageRequest);
}


