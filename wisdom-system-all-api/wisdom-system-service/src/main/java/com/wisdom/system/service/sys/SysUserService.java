package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.pojo.SysUserInfo;
import com.wisdom.system.entity.sys.SysUser;

import java.util.List;

/**
 * 用户 服务类
 *
 * @Date 2019/3/7 10:52
 * @author: GuoHonghui
 */
public interface SysUserService {


    /**
     * 新增用户
     *
     * @param sysUser
     * @return
     */
    int addSysUserInfo(SysUser sysUser);

    /**
     * 修改用户
     *
     * @param sysUser
     * @return
     */
    int updateSysUserInfo(SysUser sysUser);

    /**
     * 删除用户（单个）
     *
     * @param id
     * @return
     */
    int delSysUserInfo(String id);

    /**
     * 删除用户（批量删除）
     *
     * @param ids
     * @return
     */
    int delSysUserList(String ids);

    /**
     * 根据主键id获取用户详情
     *
     * @param id
     * @return
     */
    SysUser getSysUserInfo(String id);

    /**
     * 获取用户列表
     *
     * @param keyword 关键字
     * @return
     */
    List<SysUser> getSysUserList(String keyword);

    PageResult<SysUser> getSysUserPageList(String keyword, PageRequest pageRequest);

    /**
     * 获取分页用户列表
     *
     * @param fkDepartmentCode
     * @param keyword
     * @param pageRequest
     * @return
     */
    PageResult<SysUser> getSysUserPageListByDepartmentCode(String fkDepartmentCode, String keyword, PageRequest pageRequest);

    /**
     * 获取用户列表
     *
     * @param fkDepartmentCode
     * @param keyword
     * @return
     */
    List<SysUser> getSysUserListByDepartmentCode(String fkDepartmentCode, String keyword);


    /**
     * 更新用户状态
     *
     * @param id
     * @param state
     * @return
     */
    int updateSysUserState(String id, int state);

    /**
     * 修改用户密码
     *
     * @return
     */
    int updateSysUserPassword(SysUserInfo sysUserInfo);

    /**
     * 修改用户信息
     *
     * @param sysUserInfo
     * @return
     */
    int updateSysUserInfo(SysUserInfo sysUserInfo);

}
