package com.wisdom.system.service.job;

import com.wisdom.system.entity.job.JobCron;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface JobCronService {

    int addJobCronInfo(JobCron jobCron);

    int updateJobCronInfo(JobCron jobCron);

    int delJobCronInfo(String id);

    int delJobCronList(String ids);

    JobCron getJobCronInfo(String id);

    List<JobCron> getJobCronList(String keyword);

    PageResult<JobCron> getJobCronPageList(String keyword, PageRequest pageRequest);
}


