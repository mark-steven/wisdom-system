package com.wisdom.system.service.impl.per;

import com.github.pagehelper.Page;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.dao.per.AttmanagementMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.job.JobLog;
import com.wisdom.system.entity.per.Attmanagement;
import com.wisdom.system.service.per.AttmanagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@Service
public class AttmanagementServiceImpl implements AttmanagementService {

    @Autowired
    private AttmanagementMapper attmanagementMapper;

    @Override
    public int delAttmanagementInfo(String id) {
        return attmanagementMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delAttmanagementList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(Attmanagement.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return attmanagementMapper.deleteByExample(example);
    }

    @Override
    public List<Attmanagement> getAttmanagementList(String startTime, String endTime,String keyword) {
        Example example = new Example(Attmanagement.class);
        example.createCriteria().andEqualTo("name", SqlUtil.likeEscapeH(keyword))
                .andEqualTo("dept",SqlUtil.likeEscapeH(keyword))
                .andBetween("date",startTime,endTime);
        example.orderBy("week").asc();
        return attmanagementMapper.selectByExample(example);
    }

    @Override
    public PageResult<Attmanagement> getAttmanagementPageList(String startTime, String endTime,String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getAttmanagementList(startTime,endTime,keyword), page.getTotal());
    }
}
