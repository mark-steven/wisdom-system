package com.wisdom.system.service.impl.dev;

import com.wisdom.system.dao.dev.DevTableMapper;
import com.wisdom.system.entity.dev.DevTable;
import com.wisdom.system.entity.dev.DevTableColumn;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class DevTableServiceImpl implements DevTableService {

    @Autowired
    DevTableMapper devTableMapper;
    @Autowired
    DataSource dataSource;

    @Override
    public List<DevTable> getDevTableList(String keyword) {
        String tableSchema = SqlUtil.getTableSchema(dataSource);
        return devTableMapper.getDevTableList(tableSchema, SqlUtil.likeEscapeH(keyword));
    }

    @Override
    public List<DevTableColumn> getDevTableColumnsList(String keyword) {
        String tableSchema = SqlUtil.getTableSchema(dataSource);
        return devTableMapper.getDevTableColumns(tableSchema, keyword);
    }
}
