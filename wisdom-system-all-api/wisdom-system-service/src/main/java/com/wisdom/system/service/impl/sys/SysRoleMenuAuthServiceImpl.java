package com.wisdom.system.service.impl.sys;

import com.github.pagehelper.Page;
import com.wisdom.system.dao.sys.SysRoleMenuAuthMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysRoleMenuAuth;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.sys.SysRoleMenuAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

/**
 * 角色菜单 服务实现类
 *
 * @Date 2019/3/12 11:12
 * author: GuoHonghui
 */
@Service
public class SysRoleMenuAuthServiceImpl implements SysRoleMenuAuthService {

    @Autowired
    private SysRoleMenuAuthMapper sysRoleMenuAuthMapper;

    /**
     * 新增角色菜单
     *
     * @param sysRoleMenuAuth
     * @return
     */
    @Override
    public int addRoleMenuAuthInfo(SysRoleMenuAuth sysRoleMenuAuth) {
        return sysRoleMenuAuthMapper.insert(sysRoleMenuAuth);
    }

    /**
     * 修改角色菜单
     *
     * @param sysRoleMenuAuth
     * @return
     */
    @Override
    public int updateSysRoleMenuAuthInfo(SysRoleMenuAuth sysRoleMenuAuth) {
        return sysRoleMenuAuthMapper.updateByPrimaryKeySelective(sysRoleMenuAuth);
    }

    /**
     * 删除角色菜单（单个）
     *
     * @param id
     * @return
     */
    @Override
    public int delSysRoleMenuAuthInfo(String id) {
        return sysRoleMenuAuthMapper.deleteByPrimaryKey(id);
    }

    /**
     * 删除角色菜单（批量删除）
     *
     * @param ids
     * @return
     */
    @Override
    public int delSysRoleMenuAuthList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(SysRoleMenuAuth.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return sysRoleMenuAuthMapper.deleteByExample(example);
    }

    /**
     * 根据主键id获取角色菜单详情
     *
     * @param id
     * @return
     */
    @Override
    public SysRoleMenuAuth getSysRoleMenuAuthInfo(String id) {
        return sysRoleMenuAuthMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取角色菜单列表
     *
     * @param fkRoleId
     * @param keyword
     * @return
     */
    @Override
    public List<SysRoleMenuAuth> getSysRoleMenuAuthList(String fkRoleId, String keyword) {
        return sysRoleMenuAuthMapper.getSysRoleMenuAuthList(fkRoleId, SqlUtil.likeEscapeH(keyword));
    }

    /**
     * 获取角色菜单列表
     *
     * @param fkRoleId
     * @param keyword
     * @param pageRequest
     * @return
     */
    @Override
    public PageResult<SysRoleMenuAuth> getSysRoleMenuAuthPageList(String fkRoleId, String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getSysRoleMenuAuthList(fkRoleId, keyword), page.getTotal());
    }

    /**
     * 批量更新角色菜单权限
     *
     * @param fkRoleId
     * @param roleMenuList
     * @return
     */
    @Override
    @Transactional
    public int updateSysRoleMenuAuthList(String fkRoleId, List<SysRoleMenuAuth> roleMenuList) {
        Example example = new Example(SysRoleMenuAuth.class);
        example.createCriteria().andEqualTo("fkRoleId", fkRoleId);
        sysRoleMenuAuthMapper.deleteByExample(example);
        return sysRoleMenuAuthMapper.insertList(roleMenuList);
    }
}
