package com.wisdom.system.service.impl.sys;


import com.github.pagehelper.Page;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.dao.sys.SysSystemLogMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysSystemLog;
import com.wisdom.system.service.sys.SysSystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@Service
public class SysSystemLogServiceImpl implements SysSystemLogService {

    @Autowired
    SysSystemLogMapper sysSystemLogMapper;

    @Override
    public int addSysSystemLogInfo(SysSystemLog sysSystemLog) {
        return sysSystemLogMapper.insert(sysSystemLog);
    }

    @Override
    public int updateSysSystemLogInfo(SysSystemLog sysSystemLog) {
        return sysSystemLogMapper.updateByPrimaryKeySelective(sysSystemLog);
    }

    @Override
    public int delSysSystemLogInfo(String id) {
        return sysSystemLogMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delSysSystemLogList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(SysSystemLog.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return sysSystemLogMapper.deleteByExample(example);
    }

    @Override
    public SysSystemLog getSysSystemLogInfo(String id) {
        return sysSystemLogMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysSystemLog> getSysSystemLogList(String keyword) {
        Example example = new Example(SysSystemLog.class);
        example.createCriteria().andLike("requestTypeName", SqlUtil.likeEscapeH(keyword));
        example.orderBy("addTime").desc();
        return sysSystemLogMapper.selectByExample(example);
    }

    @Override
    public PageResult<SysSystemLog> getSysSystemLogPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<SysSystemLog>(getSysSystemLogList(keyword), page.getTotal());
    }

}




