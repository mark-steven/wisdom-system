package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.dev.DevParamConfigMapper;
import com.wisdom.system.dao.dev.DevParamGroupMapper;
import com.wisdom.system.entity.dev.DevParamConfig;
import com.wisdom.system.entity.dev.DevParamGroup;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.exception.LogicException;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.common.util.StringUtil;
import com.wisdom.system.service.dev.DevParamGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DevParamGroupServiceImpl implements DevParamGroupService {

    @Autowired
    private DevParamGroupMapper devParamGroupMapper;
    @Autowired
    private DevParamConfigMapper devParamConfigMapper;

    @Override
    @ValidationUnique(key = {"code"}, msg = {"分组编码已存在！"})
    public int addDevParamGroupInfo(DevParamGroup sysParamGroup) {
        return devParamGroupMapper.insert(sysParamGroup);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"分组编码已存在！"},excludeSelf = true)
    public int updateDevParamGroupInfo(DevParamGroup sysParamGroup) {
        return devParamGroupMapper.updateByPrimaryKeySelective(sysParamGroup);
    }

    @Override
    @Transactional
    public int delDevParamGroupInfo(String id) {
        DevParamGroup devParamGroupInfo = getDevParamGroupInfo(id);
        Example example = new Example(DevParamConfig.class);
        example.createCriteria().andEqualTo("fkParamGroupCode",devParamGroupInfo.getCode());
        List<DevParamConfig> devParamConfigs = devParamConfigMapper.selectByExample(example);
        if (devParamConfigs.size() >= 1){
            throw LogicException.of("参数分组关联其他参数配置，不允许删除！");
        }
        return devParamGroupMapper.deleteByPrimaryKey(id);
    }

    @Override
    @Transactional
    public int delDevParamGroupList(String ids) {
        String[] idArray = ids.split(",");
        DevParamGroup devParamGroupInfo = null;
        for (String id : idArray) {
            devParamGroupInfo = getDevParamGroupInfo(id);
        }
        Example example1 = new Example(DevParamConfig.class);
        example1.createCriteria().andEqualTo("fkParamGroupCode",devParamGroupInfo.getCode());
        List<DevParamConfig> devParamConfigs = devParamConfigMapper.selectByExample(example1);
        if (devParamConfigs.size() >= 1){
            throw LogicException.of("参数分组关联其他参数配置，不允许删除！");
        }
        Example example = new Example(DevParamGroup.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devParamGroupMapper.deleteByExample(example);
    }

    @Override
    public DevParamGroup getDevParamGroupInfo(String id) {
        return devParamGroupMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<DevParamGroup> getDevParamGroupList(String fkParamCategoryCode, String keyword) {
        if (!StringUtil.isEmpty(fkParamCategoryCode)) {
            return devParamGroupMapper.getDevParamGroupList(fkParamCategoryCode, SqlUtil.likeEscapeH(keyword));
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public PageResult<DevParamGroup> getDevParamGroupPageList(String fkParamCategoryCode, String keyword, PageRequest pageRequest) {
        if (!StringUtil.isEmpty(fkParamCategoryCode)) {
            Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
            return new PageResult<>(getDevParamGroupList(fkParamCategoryCode, keyword), page.getTotal());
        } else {
            return new PageResult<>(new ArrayList<>(), 0l);
        }
    }

}



