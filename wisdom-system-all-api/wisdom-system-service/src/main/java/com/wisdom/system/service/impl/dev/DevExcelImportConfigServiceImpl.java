package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.dev.DevExcelImportConfigMapper;
import com.wisdom.system.dao.dev.DevExcelImportRuleMapper;
import com.wisdom.system.entity.dev.DevExcelImportConfig;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevExcelImportRule;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevExcelImportConfigService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;


import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class DevExcelImportConfigServiceImpl implements DevExcelImportConfigService {

    @Autowired
    DevExcelImportConfigMapper devExcelImportConfigMapper;

    @Autowired
    DevExcelImportRuleMapper devExcelImportRuleMapper;


    @Override
    @Transactional
    @ValidationUnique(key = {"code"},msg = {"调用编码不能重复！"})
    public int addDevExcelImportConfigInfo(DevExcelImportConfig devExcelImportConfig) {
        String guid = UUID.randomUUID().toString();
        devExcelImportConfig.setId(guid);
        int num = devExcelImportConfigMapper.insert(devExcelImportConfig);
        Integer sort = 0;
        for (DevExcelImportRule devExcelImportRule : devExcelImportConfig.getDevExcelImportRuleList()) {
            sort++;
            devExcelImportRule.setFkExcelImportConfigId(guid);
            devExcelImportRule.setSort(sort);
        }
        devExcelImportRuleMapper.insertList(devExcelImportConfig.getDevExcelImportRuleList());
        return num;
    }

    @Override
    @Transactional
    @ValidationUnique(key = {"code"},msg = {"调用编码不能重复！"},excludeSelf = true)
    public int updateDevExcelImportConfigInfo(DevExcelImportConfig devExcelImportConfig) {
        //更新主表
        int num = devExcelImportConfigMapper.updateByPrimaryKeySelective(devExcelImportConfig);
        //更新附属规则
        Example example = new Example(DevExcelImportRule.class);
        example.createCriteria().andEqualTo("fkExcelImportConfigId", devExcelImportConfig.getId());
        devExcelImportRuleMapper.deleteByExample(example);
        Integer sort = 0;
        for (DevExcelImportRule devExcelImportRule : devExcelImportConfig.getDevExcelImportRuleList()) {
            sort++;
            devExcelImportRule.setFkExcelImportConfigId(devExcelImportConfig.getId());
            devExcelImportRule.setSort(sort);
        }
        devExcelImportRuleMapper.insertList(devExcelImportConfig.getDevExcelImportRuleList());
        return num;
    }

    @Override
    public int delDevExcelImportConfigInfo(String id) {
        return devExcelImportConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delDevExcelImportConfigList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(DevExcelImportConfig.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devExcelImportConfigMapper.deleteByExample(example);
    }

    @Override
    public DevExcelImportConfig getDevExcelImportConfigInfo(String id) {
        DevExcelImportConfig devExcelImportConfig = devExcelImportConfigMapper.selectByPrimaryKey(id);
        Example example = new Example(DevExcelImportRule.class);
        example.createCriteria().andEqualTo("fkExcelImportConfigId", id);
        example.orderBy("sort").asc();
        List<DevExcelImportRule> devExcelImportRuleList = devExcelImportRuleMapper.selectByExample(example);
        devExcelImportConfig.setDevExcelImportRuleList(devExcelImportRuleList);
        return devExcelImportConfig;
    }

    @Override
    public DevExcelImportConfig getDevExcelImportConfigByCode(String code) {
        Example configExample = new Example(DevExcelImportConfig.class);
        configExample.createCriteria().andEqualTo("code", code);
        DevExcelImportConfig devExcelImportConfig = devExcelImportConfigMapper.selectOneByExample(configExample);
        Example ruleExample = new Example(DevExcelImportRule.class);
        ruleExample.createCriteria().andEqualTo("fkExcelImportConfigId", devExcelImportConfig.getId());
        List<DevExcelImportRule> devExcelImportRuleList = devExcelImportRuleMapper.selectByExample(ruleExample);
        devExcelImportConfig.setDevExcelImportRuleList(devExcelImportRuleList);
        return devExcelImportConfig;
    }

    @Override
    public List<DevExcelImportConfig> getDevExcelImportConfigList(String keyword) {
        Example example = new Example(DevExcelImportConfig.class);
        Example.Criteria criteria2=example.createCriteria();
        criteria2.andLike("name", SqlUtil.likeEscapeH(keyword));
        example.and(criteria2);
        return devExcelImportConfigMapper.selectByExample(example);
    }

    @Override
    public PageResult<DevExcelImportConfig> getDevExcelImportConfigPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevExcelImportConfigList(keyword), page.getTotal());
    }

}




