package com.wisdom.system.service.per;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.per.Attmanagement;

import java.util.List;

public interface AttmanagementService {

    int delAttmanagementInfo(String id);

    int delAttmanagementList(String ids);

    List<Attmanagement> getAttmanagementList(String startTime, String endTime,String keyword);

    PageResult<Attmanagement> getAttmanagementPageList(String startTime, String endTime,String keyword, PageRequest pageRequest);
}
