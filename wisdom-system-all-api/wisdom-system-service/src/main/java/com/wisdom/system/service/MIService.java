package com.wisdom.system.service;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.out.ResultBody;


import java.util.HashMap;


public interface MIService {

    boolean isFieldValueExist(HashMap<String, Object> map);

    ResultBody getCustomSQLResultPageList(String sql, PageRequest pageRequest);
}
