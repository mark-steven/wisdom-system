package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.dev.DevExcelImportRuleMapper;
import com.wisdom.system.entity.dev.DevExcelImportRule;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevExcelImportRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;

@Service
public class DevExcelImportRuleServiceImpl implements DevExcelImportRuleService {

    @Autowired
    DevExcelImportRuleMapper devExcelImportRuleMapper;
    @Autowired
    DataSource dataSource;

    @Override
    public int addDevExcelImportRuleInfo(DevExcelImportRule devExcelImportRule) {
        return devExcelImportRuleMapper.insert(devExcelImportRule);
    }

    @Override
    public int updateDevExcelImportRuleInfo(DevExcelImportRule devExcelImportRule) {
        return devExcelImportRuleMapper.updateByPrimaryKeySelective(devExcelImportRule);
    }

    @Override
    public int delDevExcelImportRuleInfo(String id) {
        return devExcelImportRuleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delDevExcelImportRuleList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(DevExcelImportRule.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devExcelImportRuleMapper.deleteByExample(example);
    }

    @Override
    public PageResult<DevExcelImportRule> getTableColumnList(String tableName, PageRequest pageRequest) {
        String tableSchema = SqlUtil.getTableSchema(dataSource);
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(devExcelImportRuleMapper.getTableColumnList(tableName, tableSchema), page.getTotal());
    }

    @Override
    public DevExcelImportRule getDevExcelImportRuleInfo(String id) {
        return devExcelImportRuleMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<DevExcelImportRule> getDevExcelImportRuleList(String keyword) {
        return getDevExcelImportRuleExample(keyword);
    }

    @Override
    public PageResult<DevExcelImportRule> getDevExcelImportRulePageList(String fkExcelImportConfigId, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevExcelImportRuleExample(fkExcelImportConfigId), page.getTotal());
    }


    /**
     * 获取EXCEL导入规则列表、EXCEL导入规则分页列表公用方法
     *
     * @param fkExcelImportConfigId
     * @return
     */
    private List<DevExcelImportRule> getDevExcelImportRuleExample(String fkExcelImportConfigId) {
        Example example = new Example(DevExcelImportRule.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("fkExcelImportConfigId", fkExcelImportConfigId);
        return devExcelImportRuleMapper.selectByExample(example);
    }
}




