package com.wisdom.system.service.impl.sys;

import com.wisdom.system.common.redis.CacheKeyConst;
import com.wisdom.system.common.redis.RedisUtil;
import com.wisdom.system.common.util.ObjectUtil;
import com.wisdom.system.dao.sys.SysParamMapper;
import com.wisdom.system.entity.sys.SysParam;
import com.wisdom.system.service.sys.SysParamService;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class SysParamServiceImpl implements SysParamService {

    @Autowired
    private SysParamMapper sysParamMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 根据系统参数key值获取系统参数对应值
     *
     * @param paramKey
     * @return
     */
    @Override
    public String getSysParamValue(String paramKey) {
        //获取key值， 使用AOP调用，通过缓存获取数据。
        SysParam sysParam = ((SysParamService) AopContext.currentProxy()).getSysParamInfo(paramKey);
        String returnValue = "";
        if (ObjectUtil.isNotNull(sysParam)) {
            returnValue = sysParam.getParamValue();
        }
        return returnValue;
    }

    /**
     * 根据系统参数key值获取系统参数对象
     *
     * @param paramKey
     * @return
     */
    @Override
    public SysParam getSysParamInfo(String paramKey) {
        //获取缓存中的参数key
        String key = getSysParamRedisKey(paramKey);
        SysParam sysParam = (SysParam) redisUtil.get(key);
        if (ObjectUtil.isNull(sysParam)) {
            Example example = new Example(SysParam.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("paramKey", paramKey);
            sysParam = sysParamMapper.selectOneByExample(example);
            //保存redis缓存
            if (ObjectUtil.isNotNull(sysParam)) {
                redisUtil.set(key, sysParam);
            }
        }
        return sysParam;
    }

    /**
     * 根据数组类别删除系统参数信息
     *
     * @param paramGroupCode
     * @return
     */
    private int deleteSysParamList(String paramGroupCode) {
        return sysParamMapper.deleteSysParamByGroupCode(paramGroupCode);
    }


    @Override
    public int deleteByKey(String key){
        redisUtil.remove(getSysParamRedisKey(key));
        Example example = new Example(SysParam.class);
        example.createCriteria().orEqualTo("paramKey",key);
        return sysParamMapper.deleteByExample(example);
    }


    /**
     * 保存系统参数列表
     *
     * @param sysParamList
     * @return
     */
    @Override
    @Transactional
    public int updateSysParamList(String paramGroupCode, List<SysParam> sysParamList) {
        // 删除旧数据
        deleteSysParamList(paramGroupCode);
        int i = sysParamMapper.insertList(sysParamList);
        if (i > 0) {
            // 更新redis数据
            for (SysParam sysParam : sysParamList) {
                redisUtil.set(getSysParamRedisKey(sysParam.getParamKey()), sysParam);
            }
        }
        return i;
    }

    /**
     * 获取系统参数缓存中的key值
     *
     * @param paramKey
     * @return
     */
    public String getSysParamRedisKey(String paramKey) {
        return CacheKeyConst.SYS_PARAM  + ":" + paramKey;
    }

}



