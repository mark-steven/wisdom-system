package com.wisdom.system.service.sys;

import com.wisdom.system.entity.sys.SysMenu;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysMenuTree;

import java.util.List;

public interface SysMenuService {

    int addSysMenuInfo(SysMenu sysMenu);

    int updateSysMenuInfo(SysMenu sysMenu);

    int delSysMenuInfo(String id);

    int delSysMenuList(String ids);

    SysMenu getSysMenuInfo(String id);

    List<SysMenu> getSysMenuList(String keyword, String fkSystem);

    PageResult<SysMenu> getSysMenuPageList(String keyword,String fkSystem, PageRequest pageRequest);

    int updateSysMenuState(String id, int state);

    PageResult<SysMenu> getUserSysMenuPageList(String fkUserId, String fkSystem,String state,PageRequest pageRequest);

    List<SysMenuTree> getUserSysMenuTree(String fkUserId, String fkSystem);
}