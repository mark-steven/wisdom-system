package com.wisdom.system.service.impl.sys;

import com.wisdom.system.dao.sys.SysTreeMapper;

import com.wisdom.system.entity.sys.SysTree;
import com.wisdom.system.service.sys.SysTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysTreeServiceImpl implements SysTreeService {
    @Autowired
    SysTreeMapper sysTreeMapper;

    @Override
    public List<SysTree> getSysDeptTree(String keyword, Integer state,String code,String fkDeptTypeCode) {
        return sysTreeMapper.getSysDeptTree(keyword, state,code,fkDeptTypeCode);
    }
}
