package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevTable;
import com.wisdom.system.entity.dev.DevTableColumn;

import java.util.List;

public interface DevTableService {

    List<DevTable> getDevTableList(String keyword);

    List<DevTableColumn> getDevTableColumnsList(String keyword);
}
