package com.wisdom.system.service.impl;

import com.wisdom.system.dao.MIMapper;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.out.ResultBody;
import com.wisdom.system.service.MIService;
import org.springframework.beans.factory.annotation.Autowired;
import com.wisdom.system.entity.base.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MIServiceImpl implements MIService {
    @Autowired
    MIMapper miMapper;

    @Override
    public boolean isFieldValueExist(HashMap<String, Object> map) {
        int num = miMapper.isFieldValueExist(map);
        if (num > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ResultBody getCustomSQLResultPageList(String sql, PageRequest pageRequest) {
        sql = "SELECT * FROM SYS_USER";
        long total = (int) miMapper.getCustomSQLCount(sql);
        Integer pageSize = pageRequest.getPageSize();
        Integer formSize = (pageRequest.getPageNum() - 1) * pageSize;
        List<Map<String, Object>> list = miMapper.getCustomSQLResult(sql, formSize, pageSize);
        PageResult pageResult = new PageResult();
        pageResult.setList(list);
        pageResult.setTotal(total);
        ResultBody resultBody = new ResultBody();
        resultBody.setResult(pageResult);
        resultBody.setCode("0");
        resultBody.setMsg("获取数据成功");
        return resultBody;
    }


}
