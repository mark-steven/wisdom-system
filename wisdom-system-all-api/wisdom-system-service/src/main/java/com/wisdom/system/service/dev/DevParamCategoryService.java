package com.wisdom.system.service.dev;

import com.wisdom.system.entity.dev.DevParamCategory;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;

import java.util.List;

public interface DevParamCategoryService {

    int addDevParamCategoryInfo(DevParamCategory sysParamCategory);

    int updateDevParamCategoryInfo(DevParamCategory sysParamCategory);

    int delDevParamCategoryInfo(String id);

    int delDevParamCategoryList(String ids);

    DevParamCategory getDevParamCategoryInfo(String id);

    List<DevParamCategory> getDevParamCategoryList(String keyword);

    PageResult<DevParamCategory> getDevParamCategoryPageList(String keyword, PageRequest pageRequest);
}

