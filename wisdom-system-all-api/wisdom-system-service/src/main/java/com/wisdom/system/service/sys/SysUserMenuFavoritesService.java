package com.wisdom.system.service.sys;

import com.wisdom.system.entity.sys.SysUserMenuFavorites;

import java.util.List;

/**
 * 用户常用菜单设置 服务类
 *
 * @Date 2019/3/15 9:06
 * author: GuoHonghui
 */
public interface SysUserMenuFavoritesService {

    /**
     * 新增用户常用菜单设置
     *
     * @param sysUserMenuFavorites
     * @return
     */
    int addUserMenuFavoritesInfo(SysUserMenuFavorites sysUserMenuFavorites);

    /**
     * 修改用户常用菜单设置
     *
     * @param sysUserMenuFavorites
     * @return
     */
    int updateSysUserMenuFavoritesInfo(SysUserMenuFavorites sysUserMenuFavorites);

    /**
     * 删除用户常用菜单设置（单个）
     *
     * @param id
     * @return
     */
    int delSysUserMenuFavoritesInfo(String id);

    /**
     * 删除用户常用菜单设置（批量删除）
     *
     * @param ids
     * @return
     */
    int delSysUserMenuFavoritesList(String ids);

    /**
     * 根据主键id获取用户常用菜单设置
     *
     * @param id
     * @return
     */
    SysUserMenuFavorites getSysUserMenuFavoritesInfo(String id);

    /**
     * 获取用户常用菜单设置列表
     *
     * @return
     */
    List<SysUserMenuFavorites> getSysUserMenuFavoritesList(String userId, String fkSystem);

    /**
     * 根据菜单ID批量更新用户常用菜单设置列表关系
     *
     * @param userId
     * @param sysUserMenuFavoritesList
     * @return
     */
    int updateSysUserMenuFavoritesList(String userId, List<SysUserMenuFavorites> sysUserMenuFavoritesList, String fkSystem);

    /**
     * 根据用户批量新增用户常用菜单设置列表关系
     *
     * @param sysUserMenuFavorites
     * @return
     */
    int addSysUserMenuFavoritesList(List<SysUserMenuFavorites> sysUserMenuFavorites);

}
