package com.wisdom.system.service.job;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.job.JobLog;

import java.util.List;

public interface JobLogService {

    int addJobLogInfo(JobLog jobLog);

    int delJobLogInfo(String id);

    int delJobLogList(String ids);

    List<JobLog> getJobLogList(String startTime,String endTime,String fkJobLogCode,String keyword);

    PageResult<JobLog> getJobLogPageList(String startTime,String endTime,String fkJobLogCode,String keyword, PageRequest pageRequest);
}


