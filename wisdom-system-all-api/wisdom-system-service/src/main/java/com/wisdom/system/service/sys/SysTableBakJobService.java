package com.wisdom.system.service.sys;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysTableBakJob;

import java.util.List;
import java.util.Map;

public interface SysTableBakJobService {

    int addSysTableBakJobInfo(SysTableBakJob sysTableBakJob);

    int updateSysTableBakJobInfo(SysTableBakJob sysTableBakJob);

    int delSysTableBakJobInfo(String id);

    int delSysTableBakJobList(String ids);

    SysTableBakJob getSysTableBakJobInfo(String id);

    List<SysTableBakJob> getSysTableBakJobList(Integer state, String keyword);

    PageResult<SysTableBakJob> getSysTableBakJobPageList(Integer state, String keyword, PageRequest pageRequest);

    List<SysTableBakJob> getSysTableBakJobExecuteList();

    List<Map> showDatabase();

    List<Map> getTableList();

    int executeTableBak(SysTableBakJob sysTableBakJob);

    int updateSysTableBakJobState(String id, int state);

}
