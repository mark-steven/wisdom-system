package com.wisdom.system.service.impl.dev;

import com.wisdom.system.dao.dev.DevExcelImportDataMapper;
import com.wisdom.system.entity.dev.DevExcelImportConfig;
import com.wisdom.system.entity.dev.DevExcelImportRule;
import com.wisdom.system.common.excel.CellDataInfo;
import com.wisdom.system.common.excel.ExcelImport;
import com.wisdom.system.service.dev.DevExcelImportConfigService;
import com.wisdom.system.service.dev.DevExcelImportDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.util.*;


@Service
public class DevExcelImportDataServiceImpl implements DevExcelImportDataService {

    @Autowired
    private DevExcelImportConfigService devExcelImportConfigService;

    @Autowired
    private DevExcelImportDataMapper devExcelImportDataMapper;

    @Override
    public int importData(String code, FileInputStream fileInputStream) {
        DevExcelImportConfig devExcelImportConfig = devExcelImportConfigService.getDevExcelImportConfigByCode(code);
        List<DevExcelImportRule> devExcelImportRuleList = devExcelImportConfig.getDevExcelImportRuleList();
        List<Map<Integer, CellDataInfo>> cellDataInfoList = ExcelImport.getExcelData(fileInputStream, devExcelImportConfig.getStartNum(), devExcelImportConfig.getWithoutEndNum());
        Map<String, Object> dynamicSQLParamMap = covertDynamicSQLParam(devExcelImportRuleList, cellDataInfoList);
        Map<String, String> columnMap = (Map<String, String>) dynamicSQLParamMap.get("columnMap");
        List<Map<String, Object>> valueList = (List<Map<String, Object>>) dynamicSQLParamMap.get("valueList");
        return devExcelImportDataMapper.importData(devExcelImportConfig.getTableName(), columnMap, valueList);
    }

    //解析动态SQL参数
    private Map<String, Object> covertDynamicSQLParam(List<DevExcelImportRule> devExcelImportRuleList, List<Map<Integer, CellDataInfo>> cellDataInfoList) {
        Map<String, Object> dynamicSQLParamMap = new HashMap<>();
        Map<String, String> columnMap = new HashMap<>();
        List<Map<String, Object>> valueList = new ArrayList<>();//值参数
        Integer rowNum = 0;
        for (Map<Integer, CellDataInfo> cellDataInfo : cellDataInfoList) {
            Map<String, Object> map = new HashMap<>();
            boolean hasId = false;
            for (DevExcelImportRule devExcelImportRule : devExcelImportRuleList) {
                if (devExcelImportRule.getExcelColIndex() != null && devExcelImportRule.getExcelColIndex() > 0) {
                    String colName = devExcelImportRule.getColName();
                    if (colName.toUpperCase().equals("ID")) {
                        hasId = true;
                    }
                    Object colValue = cellDataInfo.get(devExcelImportRule.getExcelColIndex() - 1).getValue();//程序从0开始，操作界面从1开始，所以需要减1
                    map.put(colName, colValue);
                    if (rowNum == 0) {
                        columnMap.put(colName, colName);//添加列信息
                    }
                }
            }
            valueList.add(map);
            if (!hasId) {
                map.put("ID", UUID.randomUUID().toString());
                if (rowNum == 0) {
                    columnMap.put("ID", "ID");//添加列信息
                }
            }
            rowNum += 1;
        }
        dynamicSQLParamMap.put("columnMap", columnMap);
        dynamicSQLParamMap.put("valueList", valueList);
        return dynamicSQLParamMap;

    }

}
