package com.wisdom.system.service.sys;

import com.wisdom.system.entity.sys.SysParam;

import java.util.List;

public interface SysParamService {

    String getSysParamValue(String paramKey);

    SysParam getSysParamInfo(String paramKey);

    int deleteByKey(String key);

    int updateSysParamList(String paramGroupCode, List<SysParam> sysParamList);
}

