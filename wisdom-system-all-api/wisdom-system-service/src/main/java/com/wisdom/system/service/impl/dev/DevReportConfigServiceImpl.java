package com.wisdom.system.service.impl.dev;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.common.CommonMapper;
import com.wisdom.system.dao.dev.DevReportConfigMapper;
import com.wisdom.system.entity.dev.DevReportConfig;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.dev.DevReportConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class DevReportConfigServiceImpl implements DevReportConfigService {
    @Autowired
    DevReportConfigMapper devReportConfigMapper;

    @Autowired
    private CommonMapper commonMapper;



    @Override
    @ValidationUnique(key = {"code"},  msg = {"报表编码已存在！"})
    public int addDevReportConfigInfo(DevReportConfig devReportConfig) {
        devReportConfig.setAddTime(new Date());
        return devReportConfigMapper.insert(devReportConfig);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"报表编码已存在！"},  excludeSelf = true)
    public int updateDevReportConfigInfo(DevReportConfig devReportConfig) {
        return devReportConfigMapper.updateByPrimaryKeySelective(devReportConfig);
    }

    @Override
    public int delDevReportConfigInfo(String id) {
        return devReportConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delDevReportConfigList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(DevReportConfig.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return devReportConfigMapper.deleteByExample(example);
    }

    @Override
    public DevReportConfig getDevReportConfigInfo(String id) {
        return devReportConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public DevReportConfig getDevReportConfigInfoByCode(String code) {
        Example example=new Example(DevReportConfig.class);
        example.createCriteria().andEqualTo("code", code);
        DevReportConfig devReportConfig=devReportConfigMapper.selectOneByExample(example);
        return devReportConfig;
    }

    @Override
    public List<DevReportConfig> getDevReportConfigList(String keyword) {
        return getDevReportConfigExample(keyword);
    }

    @Override
    public PageResult<DevReportConfig> getDevReportConfigPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getDevReportConfigExample(keyword), page.getTotal());
    }

    /**
     * 获取列表、分页列表公用方法
     *
     * @param keyword
     * @return
     */
    private List<DevReportConfig> getDevReportConfigExample(String keyword) {
        Example example = new Example(DevReportConfig.class);
        Example.Criteria criteria2=example.createCriteria();
        criteria2.andLike("name", SqlUtil.likeEscapeH(keyword))
                .orLike("pyCode", SqlUtil.likeEscapeH(keyword))
                .orLike("wbCode", SqlUtil.likeEscapeH(keyword));
        example.and(criteria2);
        example.orderBy("addTime").desc();
        return devReportConfigMapper.selectByExample(example);
    }

    @Override
    public PageResult<Map<String, Object>> getReportInfoPageList(String code, Map<String, Object> param, PageRequest pageRequest) {
        //根据机构编码和表报编码获取报表配置相关信息
        Example example = new Example(DevReportConfig.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", code);
        DevReportConfig devReportConfig = devReportConfigMapper.selectOneByExample(example);
        if (null != devReportConfig) {
            String sql = devReportConfig.getSqlConfigContent();
            sql = SqlUtil.sqlConvert(sql, param);
            List<Map<String, Object>> list = commonMapper.getCustomSQLResultOfPage(sql, pageRequest.getPageNum() - 1, pageRequest.getPageSize());
            return new PageResult<>(list, Long.valueOf(commonMapper.getCustomSQLCount(sql)));
        }
        return null;
    }
}



