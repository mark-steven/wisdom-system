package com.wisdom.system.service.impl.sys;


import com.github.pagehelper.Page;
import com.wisdom.system.dao.sys.SysDictCategoryMapper;
import com.wisdom.system.dao.sys.SysDictItemMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysDictCategory;
import com.wisdom.system.entity.sys.SysDictItem;
import com.wisdom.system.common.annotation.ValidationUnique;
import com.wisdom.system.common.exception.LogicException;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.sys.SysDictCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@Service
public class SysDictCategoryServiceImpl implements SysDictCategoryService {

    @Autowired
    private SysDictCategoryMapper sysDictCategoryMapper;
    @Autowired
    private SysDictItemMapper sysDictItemMapper;

    @Override
    @ValidationUnique(key = {"code"}, msg = {"系统字典编码已存在！"})
    public int addSysDictCategoryInfo(SysDictCategory sysDictCategory) {
        return sysDictCategoryMapper.insert(sysDictCategory);
    }

    @Override
    @ValidationUnique(key = {"code"}, msg = {"系统字典编码已存在！"}, excludeSelf = true)
    public int updateSysDictCategoryInfo(SysDictCategory sysDictCategory) {
        return sysDictCategoryMapper.updateByPrimaryKeySelective(sysDictCategory);
    }

    @Override
    @Transactional
    public int delSysDictCategoryInfo(String id) {
        SysDictCategory sysDictCategoryInfo = this.getSysDictCategoryInfo(id);
        Example example = new Example(SysDictItem.class);
        example.createCriteria().andEqualTo("fkDictCategoryCode", sysDictCategoryInfo.getCode());
        List<SysDictItem> sysDictItems = sysDictItemMapper.selectByExample(example);
        if (sysDictItems.size() >= 1) {
            throw LogicException.of("字典关联相关项,不能删除！");
        }
        return sysDictCategoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delSysDictCategoryList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(SysDictCategory.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return sysDictCategoryMapper.deleteByExample(example);
    }

    @Override
    public SysDictCategory getSysDictCategoryInfo(String id) {
        return sysDictCategoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysDictCategory> getSysDictCategoryList(String keyword) {
        Example example = new Example(SysDictCategory.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLike("name", SqlUtil.likeEscapeH(keyword));
        example.orderBy("addTime").desc();
        return sysDictCategoryMapper.selectByExample(example);
    }

    @Override
    public PageResult<SysDictCategory> getSysDictCategoryPageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getSysDictCategoryList(keyword), page.getTotal());
    }

}



