package com.wisdom.system.service.current;

import com.wisdom.system.entity.current.CurrentUser;

public interface CurrentUserService {

    CurrentUser getCurrentUser();

    CurrentUser getCurrentUser(String loginCode);
}
