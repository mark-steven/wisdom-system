package com.wisdom.system.service.dev;


import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevExcelOutputConfig;

import java.util.List;
import java.util.Map;

/**
 *【报表配置】业务逻辑接口类
 */
public interface DevExcelOutputConfigService {

    int addDevExcelOutputConfigInfo(DevExcelOutputConfig devExcelOutputConfig);

    int updateDevExcelOutputConfigInfo(DevExcelOutputConfig devExcelOutputConfig);

    int delDevExcelOutputConfigInfo(String id);

    int delDevExcelOutputConfigList(String ids);

    DevExcelOutputConfig getDevExcelOutputConfigInfo(String id);

    DevExcelOutputConfig getDevExcelOutputConfigByCode( String code);

    List<DevExcelOutputConfig> getDevExcelOutputConfigList(String keyword);

    PageResult<DevExcelOutputConfig> getDevExcelOutputConfigPageList(String keyword, PageRequest pageRequest);

    PageResult<Map<String,Object>> getExcelOutputInfoPageList(String code, Map<String, Object> param, PageRequest pageRequest);

    List<Map<String,Object>> getExcelOutputInfoList(String code, Map<String, Object> param);
}


