package com.wisdom.system.service.impl.sys;

import com.github.pagehelper.Page;
import com.wisdom.system.dao.sys.SysRoleUserMapper;
import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.sys.SysRoleUser;
import com.wisdom.system.common.mybatis.PageUtil;
import com.wisdom.system.common.mybatis.SqlUtil;
import com.wisdom.system.service.sys.SysRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

/**
 * 用户角色 服务实现类
 *
 * @Date 2019/3/8 14:41
 * author: GuoHonghui
 */
@Service
public class SysRoleUserServiceImpl implements SysRoleUserService {

    @Autowired
    private SysRoleUserMapper sysUserRoleMapper;

    /**
     * 新增用户角色
     *
     * @param sysUserRole
     * @return
     */
    @Override
    public int addUserRoleInfo(SysRoleUser sysUserRole) {
        return sysUserRoleMapper.insert(sysUserRole);
    }

    /**
     * 修改用户角色
     *
     * @param sysUserRole
     * @return
     */
    @Override
    public int updateSysUserRoleInfo(SysRoleUser sysUserRole) {
        return sysUserRoleMapper.updateByPrimaryKeySelective(sysUserRole);
    }

    /**
     * 删除用户角色（单个）
     *
     * @param id
     * @return
     */
    @Override
    public int delSysUserRoleInfo(String id) {
        return sysUserRoleMapper.deleteByPrimaryKey(id);
    }

    /**
     * 删除用户角色（批量删除）
     *
     * @param ids
     * @return
     */
    @Override
    public int delSysUserRoleList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(SysRoleUser.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return sysUserRoleMapper.deleteByExample(example);
    }

    /**
     * 根据主键id获取用户角色详情
     *
     * @param id
     * @return
     */
    @Override
    public SysRoleUser getSysUserRoleInfo(String id) {
        return sysUserRoleMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取用户角色列表
     *
     * @param fkRoleId
     * @param keyword
     * @return
     */
    @Override
    public List<SysRoleUser> getSysUserRoleList(String fkRoleId, String keyword) {
        return sysUserRoleMapper.getSysRoleUserList(fkRoleId, SqlUtil.likeEscapeH(keyword));
    }

    /**
     * 获取分页用户角色列表
     *
     * @param fkRoleId
     * @param keyword
     * @param pageRequest
     * @return
     */
    @Override
    public PageResult<SysRoleUser> getSysUserRolePageList(String fkRoleId, String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<>(getSysUserRoleList(fkRoleId, keyword), page.getTotal());
    }

    /**
     * 批量更新用户角色权限分配
     *
     * @param fkRoleId
     * @param sysUserRoleList
     * @return
     */
    @Override
    @Transactional
    public int updateSysUserRoleList(String fkRoleId, List<SysRoleUser> sysUserRoleList) {
        Example example = new Example(SysRoleUser.class);
        example.createCriteria().andEqualTo("fkRoleId", fkRoleId);
        sysUserRoleMapper.deleteByExample(example);
        return sysUserRoleMapper.insertList(sysUserRoleList);
    }
}
