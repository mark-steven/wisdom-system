package com.wisdom.system.service.impl.sys;

import com.wisdom.system.dao.sys.SysUserMenuFavoritesMapper;
import com.wisdom.system.entity.sys.SysUserMenuFavorites;
import com.wisdom.system.entity.sys.SysRoleUser;
import com.wisdom.system.service.sys.SysUserMenuFavoritesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

/**
 * 用户常用菜单设置 服务实现类
 *
 * @Date 2019/3/15 9:08
 * author: GuoHonghui
 */
@Service
public class SysUserMenuFavoritesServiceImpl implements SysUserMenuFavoritesService {

    @Autowired
    private SysUserMenuFavoritesMapper userMenuFavoritesMapper;

    /**
     * 新增用户常用菜单设置
     *
     * @param sysUserMenuFavorites
     * @return
     */
    @Override
    public int addUserMenuFavoritesInfo(SysUserMenuFavorites sysUserMenuFavorites) {
        return userMenuFavoritesMapper.insert(sysUserMenuFavorites);
    }

    /**
     * 修改用户常用菜单设置
     *
     * @param sysUserMenuFavorites
     * @return
     */
    @Override
    public int updateSysUserMenuFavoritesInfo(SysUserMenuFavorites sysUserMenuFavorites) {
        return userMenuFavoritesMapper.updateByPrimaryKeySelective(sysUserMenuFavorites);
    }

    /**
     * 删除用户常用菜单设置（单个）
     *
     * @param id
     * @return
     */
    @Override
    public int delSysUserMenuFavoritesInfo(String id) {
        return userMenuFavoritesMapper.deleteByPrimaryKey(id);
    }

    /**
     * 删除用户常用菜单设置（批量删除）
     *
     * @param ids
     * @return
     */
    @Override
    public int delSysUserMenuFavoritesList(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(SysRoleUser.class);
        example.createCriteria().andEqualTo("id", Arrays.asList(idArray));
        return userMenuFavoritesMapper.deleteByExample(example);
    }

    /**
     * 根据主键id获取用户常用菜单设置
     *
     * @param id
     * @return
     */
    @Override
    public SysUserMenuFavorites getSysUserMenuFavoritesInfo(String id) {
        return userMenuFavoritesMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取用户常用菜单设置列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<SysUserMenuFavorites> getSysUserMenuFavoritesList(String userId, String fkSystem) {
        return userMenuFavoritesMapper.getSysUserMenuFavoritesList(userId, fkSystem);
    }

    /**
     * 根据菜单ID批量更新用户常用菜单设置列表关系
     *
     * @param userId
     * @param sysUserMenuFavoritesList
     * @return
     */
    @Override
    @Transactional
    public int updateSysUserMenuFavoritesList(String userId, List<SysUserMenuFavorites> sysUserMenuFavoritesList, String fkSystem) {
        userMenuFavoritesMapper.deleteSysUserMenuFavoritesList(userId, fkSystem);
        return userMenuFavoritesMapper.insertList(sysUserMenuFavoritesList);
    }

    /**
     * 根据用户批量新增用户常用菜单设置列表关系
     *
     * @param sysUserMenuFavorites
     * @return
     */
    @Override
    public int addSysUserMenuFavoritesList(List<SysUserMenuFavorites> sysUserMenuFavorites) {
        return userMenuFavoritesMapper.insertList(sysUserMenuFavorites);
    }
}
