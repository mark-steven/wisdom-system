package com.wisdom.system.service.dev;

import com.wisdom.system.entity.base.PageRequest;
import com.wisdom.system.entity.base.PageResult;
import com.wisdom.system.entity.dev.DevCustomModule;


import java.util.List;

public interface DevCustomModuleService {

    int addDevCustomModuleInfo(DevCustomModule devCustomModule);

    int updateDevCustomModuleInfo(DevCustomModule sysGridModule);

    int delDevCustomModuleInfo(String id);

    int delDevCustomModuleList(String ids);

    DevCustomModule getDevCustomModuleInfo(String id);

    DevCustomModule getDevCustomModuleByCode(String departmentCode, String code);

    List<DevCustomModule> getDevCustomModuleList(String fkDeptCode, String keyword);

    PageResult<DevCustomModule> getDevCustomModulePageList(String fkDeptCode, String keyword, PageRequest pageRequest);

}


