package com.wisdom.system.common.enums;

/**
 * GlobalResultCodeEnum 全局返回码定义
 */
public enum GlobalResultCodeEnum {

    SUCCESS("0", "success"),//成功
    NOT_FOUND("-1", "not found"),//"找不到"
    FIELD_VALUE_EXIST("5", "the field value exist"),
    FIELD_VALUE_NOT_EXIST("10", "the field value not exist"),
    PARAMETER_VALIDATION("15", "invalid parameter validation"),//无效的请求参数
    NOT_PERMISSION("20", "not permissions"),//没有权限
    TWO_PASSWORD_NO_SAME("25", "two times the password is not same"),
    SYSTEM_EXCEPTION("99", "system throw exception");//系统异常

    private String code;
    private String message;

    GlobalResultCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
