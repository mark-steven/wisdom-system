package com.wisdom.system.common.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExcelUtil {

    public static Workbook getWorkbook(String file) {
        Workbook workbook = null;
        if (file.toLowerCase().endsWith(".xls")) {
            FileInputStream fileInputStream = getFileInputStream(file);
            workbook = getWorkbook(fileInputStream);
        }
        return workbook;
    }


    public static Workbook getWorkbook(FileInputStream fileInputStream) {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return workbook;
    }

    public static FileInputStream getFileInputStream(String file) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileInputStream;
    }
}
