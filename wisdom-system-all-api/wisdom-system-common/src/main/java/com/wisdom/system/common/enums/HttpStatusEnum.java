package com.wisdom.system.common.enums;

public enum HttpStatusEnum {
    SUCCESS("200"),
    UNAUTHORIZED("401"),
    INTERNAL_SERVER_ERROR("500");
    private String code;
    private String message;
    HttpStatusEnum(String code) {
        this.code = code;
    }


}
