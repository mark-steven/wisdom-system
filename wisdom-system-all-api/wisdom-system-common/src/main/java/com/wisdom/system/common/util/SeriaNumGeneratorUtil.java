package com.wisdom.system.common.util;


public class SeriaNumGeneratorUtil {
    /**
     * 由时间戳+3位随机数
     * 生成流水号
     * @Param prefix 业务前缀    prefix入参请先在SeriaNumConst下定义常量
     * @return 流水号
     */
    public static String seria(String prefix) {
        long t = System.currentTimeMillis();
        int x = (int) (Math.random() * 900) + 100;
        String serial = String.valueOf(prefix + t + x);
        return serial;
    }

}
