package com.wisdom.system.common.excel;

public interface IExcelRow<T> {
    void row(T row, int rowNum);
}
