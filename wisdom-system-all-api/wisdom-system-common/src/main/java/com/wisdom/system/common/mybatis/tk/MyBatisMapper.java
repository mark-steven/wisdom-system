package com.wisdom.system.common.mybatis.tk;

import tk.mybatis.mapper.common.Mapper;

public interface MyBatisMapper<T> extends Mapper<T>, InsertListSpecialMapper<T> {

}
