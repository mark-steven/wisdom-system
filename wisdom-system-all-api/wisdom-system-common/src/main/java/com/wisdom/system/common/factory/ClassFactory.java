package com.wisdom.system.common.factory;

import java.lang.reflect.Method;

/**
 * ClassFactory
 */

public final class ClassFactory {
    /**
     * 根据类名获取列对象
     *
     * @param className 类名称
     * @return
     */
    public static Class<?> getClassObj(String className) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    /**
     * 根据类名称和方法名称获取方法对象
     *
     * @param className  列名称
     * @param methodName 方法名称
     * @return
     */
    public static Method getMethodObj(String className, String methodName) {
        Class<?> clazz = ClassFactory.getClassObj(className);
        Method method = null;
        try {
            method = clazz.getDeclaredMethod(methodName);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return method;
    }
}
