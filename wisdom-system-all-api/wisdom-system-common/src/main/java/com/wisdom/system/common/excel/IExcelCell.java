package com.wisdom.system.common.excel;

public interface IExcelCell<T> {
    void cell(Object row,Object cell, Integer rowNum, Integer columnNum);
}
