package com.wisdom.system.common.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 获取通配的 key 列表
     *
     * @param pattern
     * @return 去掉前缀通配符的key列表
     */
    public List<String> getPatternKeyList(final String pattern) {
        List<String> list = new ArrayList<>();
        Set<Serializable> keys = redisTemplate.keys(pattern);
        for (Serializable key : keys) {
            list.add(key.toString());
        }
        return list;
    }

    public List getPatternObjectList(final String pattern) {
        Set<String> keys = redisTemplate.keys(pattern);
        if(CollectionUtils.isEmpty(keys)){
            return Collections.EMPTY_LIST;
        }
        // 批量获取数据
        return redisTemplate.opsForValue().multiGet(keys);
    }


    /**
     * 获取通配符
     *
     * @param pattern
     * @return
     */
    public int getPatternKeySize(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        return keys.size();
    }

    /**
     * @Description: 批量删除缓存
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            remove(key);
        }
    }

    /**
     * @Description: 批量删除缓存(通配符)
     */
    public void removePattern(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0)
            redisTemplate.delete(keys);
    }

    /**
     * @Description: 删除缓存
     */
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * @Description: 判断缓存中是否有对应的value
     */
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * @Description: 读取缓存
     */
    public Object get(final String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * @Description: 写入缓存
     */
    public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 写入缓存(可以配置过期时间)
     *
     * @param key
     * @param value
     * @param expireTime 单位秒
     * @return
     */
    public boolean set(final String key, Object value, Long expireTime) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 写入缓存(可以配置过期时间)
     *
     * @param key
     * @param value
     * @param expireTime 单位秒
     * @return
     */
    public boolean set(final String key, Object value, Long expireTime, TimeUnit timeUnit) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            redisTemplate.expire(key, expireTime, timeUnit);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }



}

