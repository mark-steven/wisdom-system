package com.wisdom.system.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ValidationUnique 业务逻辑字段唯一性注解
 * author:林青煌
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidationUnique {

    //判断的字段
    String[] key() default {};

    //是否排除自身值（修改需要排除自身值|新增不需要排除自身值）
    boolean excludeSelf() default false;

    //存在是否继续
    boolean isExistContinue() default false;

    //主键属性
    String primaryKeyField() default "id";

    //主键列
    String primaryKeyColumn() default "ID";

    //消息提醒
    String[] msg() default {};

    //分组(用来做分组唯一配置)
    String groupField() default "";
}
