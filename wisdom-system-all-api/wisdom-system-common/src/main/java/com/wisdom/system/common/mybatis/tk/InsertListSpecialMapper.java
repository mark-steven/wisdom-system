package com.wisdom.system.common.mybatis.tk;

import org.apache.ibatis.annotations.InsertProvider;
import tk.mybatis.mapper.annotation.RegisterMapper;

import java.util.List;

@RegisterMapper
public interface InsertListSpecialMapper<T> {
    @InsertProvider(
            type = InsertListSpecialProvider.class,
            method = "dynamicSQL"
    )
    int insertList(List<T> var1);
}
