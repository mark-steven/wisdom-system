package com.wisdom.system.common.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.NumberToTextConverter;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Excel 导入通用类
public class ExcelImport {

    public static void loadFile(FileInputStream fileInputStream, Integer startRowNum, Integer withoutEndRowNum, IExcelRow excelRow, IExcelCell excelCell) {
        Workbook workbook = ExcelUtil.getWorkbook(fileInputStream);//获取workbook
        Sheet sheet = workbook.getSheetAt(0);//获取第一个sheet工作区
        Integer lastRowNum = sheet.getLastRowNum();//获取最后一行
        for (Row row : sheet) {
            if (row.getRowNum() < startRowNum - 1) {
                continue;
            }
            if (withoutEndRowNum != null && withoutEndRowNum > 0) {
                if (row.getRowNum() > lastRowNum - withoutEndRowNum) {
                    break;
                }
            }
            if (excelRow != null) {
                excelRow.row(row, row.getRowNum() + 1);
            }
            for (Cell cell : row) {
                if (excelCell != null) {
                    excelCell.cell(row, cell, row.getRowNum(), cell.getColumnIndex());
                }
            }
        }
    }

    public static List<Map<Integer, CellDataInfo>> getExcelData(FileInputStream fileInputStream, Integer startRowNum, Integer withoutEndRowNum) {
        Map<String, Integer> currentRow = new HashMap<>();
        currentRow.put("rowNum", -1);
        List<Map<Integer, CellDataInfo>> allList = new ArrayList<>();
        loadFile(fileInputStream, startRowNum, withoutEndRowNum, (_row, rowNum) -> {
            Map<Integer, CellDataInfo> cellDataInfoMap = new HashMap<>();
            Row row = (Row) _row;
            for (Cell cell : row) {
                CellDataInfo cellDataInfo = new CellDataInfo();
                cellDataInfo.setRowNum(rowNum);
                cellDataInfo.setColumnNum(cell.getColumnIndex());
                cellDataInfo.setCellType(cell.getCellType());
                if (cell.getCellType() == CellType.NUMERIC) {
                    short format = cell.getCellStyle().getDataFormat();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    if (format == 14 || format == 31 || format == 57 || format == 58) {   //excel中的时间格式
                        double value = cell.getNumericCellValue();
                        cellDataInfo.setValue(value);
                    } else if (DateUtil.isCellDateFormatted(cell)) {
                        cellDataInfo.setValue(sdf.format(cell.getDateCellValue()));
                    } else {
                        cellDataInfo.setValue(NumberToTextConverter.toText(cell.getNumericCellValue()));
                    }
                }
                if (cell.getCellType() == CellType.STRING) {
                    cellDataInfo.setValue(cell.getStringCellValue());
                }
                cellDataInfoMap.put(cell.getColumnIndex(), cellDataInfo);
                //如果当前列是最后一列
                if (row.getLastCellNum() == cell.getColumnIndex() + 1) {
                    allList.add(cellDataInfoMap);
                }
            }
        }, null);
        return allList;
    }

}
