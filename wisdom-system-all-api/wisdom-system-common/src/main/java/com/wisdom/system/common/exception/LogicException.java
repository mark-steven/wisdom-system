package com.wisdom.system.common.exception;

import java.text.MessageFormat;

public class LogicException extends RuntimeException {

    public LogicException(String message) {
        super(message);
    }

    public LogicException(String message, Exception e) {
        super(message, e);
    }

    public static LogicException of(String message) {
        return new LogicException(message);
    }

    public static LogicException of(String message,Object... val) {
        String msg = MessageFormat.format(message, val);
        return new LogicException(msg);
    }

    public static LogicException of(String message, Exception e) {
        return new LogicException(message, e);
    }
}
