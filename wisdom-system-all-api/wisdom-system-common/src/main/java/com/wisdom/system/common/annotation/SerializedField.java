package com.wisdom.system.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SerializedField {

    //模块名称
    String moduleName();

    //需要返回的字段
    String[] includes() default {};

    //需要去除的字段
    String[] excludes() default {};

    //返回数据是否加密
    boolean encode() default false;

    //是否输出ResultBody数据格式类型
    boolean outResultBody() default true;
}
