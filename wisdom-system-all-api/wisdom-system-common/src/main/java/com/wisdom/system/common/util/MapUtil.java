package com.wisdom.system.common.util;


import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>map操作工具类</p>
 *
 * @version V1.0
 * @Package util
 * @date 2017/10/26 12:56
 */
public class MapUtil {
    public static Map getValue(String param) {
        Map map = new HashMap();
        String str = "";
        String key = "";
        Object value = "";
        char[] charList = param.toCharArray();
        boolean valueBegin = false;
        for (int i = 0; i < charList.length; i++) {
            char c = charList[i];
            if (c == '{') {
                if (valueBegin == true) {
                    value = getValue(param.substring(i, param.length()));
                    i = param.indexOf('}', i) + 1;
                    map.put(key, value);
                }
            } else if (c == '=') {
                valueBegin = true;
                key = str;
                str = "";
            } else if (c == ',') {
                valueBegin = false;
                value = str;
                str = "";
                map.put(key, value);
            } else if (c == '}') {
                if (str != "") {
                    value = str;
                }
                map.put(key, value);
                return map;
            } else if (c != ' ') {
                str += c;
            }
        }
        return map;
    }
    /**
     * <p>
     *  说明：将对象转换成map
     * </p>
     * <p>
     *  链接：
     * </p>
     * @param thisObj 转换的实体类
     * @return Map
     */
    public static Map<String,String> revertPojo2Map(Object thisObj) {
        Map<String,String> map =  new HashMap<String,String>();
        try {
            Class c = Class.forName(thisObj.getClass().getName());
            Method[] m = c.getMethods();
            for (int i = 0; i < m.length; i++) {
                String method = m[i].getName();
                if (method.startsWith("get")) {
                    try{
                        Object value = m[i].invoke(thisObj);
                        if (value != null) {
                            String key=method.substring(3);
                            key=key.substring(0,1).toLowerCase()+key.substring(1);
                            map.put(key, value.toString());
                        }
                    }catch (Exception e) {
                        System.out.println("error:"+method);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
}
