package com.wisdom.system.common.redis;

/**
 * Cache常量类
 */
public class CacheKeyConst {

    public static final String SYS_PARAM = "sys_param:";

    public static class CacheExpireTime {
        public static final long ONE_WEEK_TIMES = 7 * 24 * 60 * 60;//保存1周
        public static final long ONE_DAY_TIMES = 24 * 60 * 60;//保存1天
        public static final long ONE_HOUT = 60 * 60;//保存1个小时
        public static final long FIVE_MINUTES = 5 * 60;//保存5分钟
        public static final long ONE_MINUTES = 1 * 60;//保存1分钟
    }


}
