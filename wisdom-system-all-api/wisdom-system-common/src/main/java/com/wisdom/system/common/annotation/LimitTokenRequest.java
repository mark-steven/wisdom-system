package com.wisdom.system.common.annotation;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;

/**
 * 频繁操作自定义注解
 *
 * @author ghh
 * 2020-1-3 09:14:28
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Order(Ordered.HIGHEST_PRECEDENCE)// 设置顺序为最高优先级
public @interface LimitTokenRequest {

    //注解的value可自定义，如果没有定义走默认
    String value() default "";
}
