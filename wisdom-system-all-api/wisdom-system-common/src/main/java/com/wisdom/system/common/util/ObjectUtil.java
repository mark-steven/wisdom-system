package com.wisdom.system.common.util;

import java.util.Optional;

/**
 * 对象空指针判断Util
 * @version 需jdk 1.8 +
 */
public class ObjectUtil {

    public static boolean isNotNull(Object obj){
        if(Optional.ofNullable(obj).isPresent()){
            return true;
        }
        return false;
    }


    public static boolean isNull(Object obj){
        if(!Optional.ofNullable(obj).isPresent()){
            return true;
        }
        return false;
    }

}
