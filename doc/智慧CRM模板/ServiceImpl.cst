﻿<%-- 
Project:MTR 智慧CRM平台模版
Author: guohonghui
Email:864994945@qq.com
Date:2020年1月
Description: 本模版主要用来生成输出智慧CRM平台的Service文档；
--%>
<%--申请一个C#语言的模版--%>
<%@ Template Language="C#" TargetLanguage="Text" ResponseEncoding="UTF-8" %>
<%--引入数据库引用--%>
<%@ Import Namespace="SchemaExplorer" %>
<%@ Assembly Name="SchemaExplorer" %>
<%--引入正则表达式引用--%>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%--定义属性变了--%>
<%@ Property Name="Table" Type="SchemaExplorer.TableSchema"  Optional="False" Category="数据库表" %>

package com.medical.inspection.service.impl;


import com.github.pagehelper.Page;
import com.medical.inspection.dao.<%=getClassName(Table)%>Mapper;
import com.medical.inspection.domain.<%=getClassName(Table)%>;
import com.medical.inspection.domain.base.PageRequest;
import com.medical.inspection.domain.base.PageResult;
import com.medical.inspection.infrastructure.mybatis.PageUtil;
import com.medical.inspection.infrastructure.mybatis.SqlUtil;
import com.medical.inspection.service.<%=getClassName(Table)%>Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@Service
public class <%=getClassName(Table)%>ServiceImpl implements <%=getClassName(Table)%>Service {
    @Autowired
    <%=getClassName(Table)%>Mapper <%=getClazzName(Table)%>Mapper;

    @Override
    <%int addNum=0; string addUniqueField="";string msg="";%>
    <%foreach(ColumnSchema col in Table.Columns)%>
    <%{%>
        <%bool isUnique=false;string keys="";%>
        <%if(col.IsUnique&&col.Name.ToUpper()!="ID") %>
        <%{ %>
            <%addUniqueField="\""+getCamelName(col.Name)+"\"";%>
            <%msg="\""+col.Description+"已存在！\"";%>
            <%addNum=addNum+1; %>
        <%}%>
    <%}%>
    <%if(addNum>0)%>
    <%{%>
    @ValidationUnique(key = {<%=addUniqueField%>}，msg={<%=msg%>})
    <%}%>
    public int add<%=getClassName(Table)%>Info(<%=getClassName(Table)%> <%=getClazzName(Table)%>) {
        return <%=getClazzName(Table)%>Mapper.insert(<%=getClazzName(Table)%>);
    }

    @Override
    <%int updateNum=0; string updateUniqueField="";%>
    <%foreach(ColumnSchema col in Table.Columns)%>
    <%{%>
        <%bool isUnique=false;string keys="";%>
        <%if(col.IsUnique&&col.Name.ToUpper()!="ID") %>
        <%{ %>
            <%updateUniqueField="\""+getClassName(Table)+"."+getCamelName(col.Name)+"\"";%>
            <%updateNum=updateNum+1; %>
        <%}%>
    <%}%>
    <%if(updateNum>0)%>
    <%{%>
    @ValidationUnique(key = {<%=updateUniqueField%>},excludeSelf = true)
    <%}%>
    public int update<%=getClassName(Table)%>Info(<%=getClassName(Table)%> <%=getClazzName(Table)%>) {
        return <%=getClazzName(Table)%>Mapper.updateByPrimaryKeySelective(<%=getClazzName(Table)%>);
    }

    @Override
    public int del<%=getClassName(Table)%>Info(String id) {
        return <%=getClazzName(Table)%>Mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int del<%=getClassName(Table)%>List(String ids) {
        String[] idArray = ids.split(",");
        Example example = new Example(<%=getClassName(Table)%>.class);
        example.createCriteria().andIn("id", Arrays.asList(idArray));
        return <%=getClazzName(Table)%>Mapper.deleteByExample(example);
    }

    @Override
    public <%=getClassName(Table)%> get<%=getClassName(Table)%>Info(String id) {
        return <%=getClazzName(Table)%>Mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<<%=getClassName(Table)%>> get<%=getClassName(Table)%>List(String keyword) {
       return get<%=getClassName(Table)%>Example(keyword);
    }

    @Override
    public PageResult<<%=getClassName(Table)%>> get<%=getClassName(Table)%>PageList(String keyword, PageRequest pageRequest) {
        Page<?> page = PageUtil.startPageAllowNull(pageRequest.getPageNum(), pageRequest.getPageSize());
        return new PageResult<<%=getClassName(Table)%>>(get<%=getClassName(Table)%>Example(keyword), page.getTotal());
    }
    
     /**
     * 获取<%=Table.Description%>列表、<%=Table.Description%>分页列表公用方法
     *
     * @param keyword
     * @return
     */
    private List<<%=getClassName(Table)%>> get<%=getClassName(Table)%>Example(String keyword){
        Example example = new Example(<%=getClassName(Table)%>.class);
        example.createCriteria().andLike("name", SqlUtil.likeEscapeH(keyword));
        example.orderBy("addTime").desc();
        return <%=getClazzName(Table)%>Mapper.selectByExample(example);
    }
}



<script runat="template">
//PascalName命名法(大驼峰）
public string getPascalName(string name)
{
    string result="";
    string[] arrName = name.Split('_');//按_截取截取数组
    for(int i=0;i<arrName.Length;i++){
        string item=arrName[i];
        item=item.ToLower();
        item=item.Substring(0,1).ToUpper()+item.Substring(1);
        result+=item;
    } 
    return result;
}
//CamelName命名法（小驼峰）
public string getCamelName(string name)
{
    string result="";
    string[] arrName = name.Split('_');//按_截取截取数组
    for(int i=0;i<arrName.Length;i++){
        string item=arrName[i];
        item=item.ToLower();
        item=item.Substring(0,1).ToUpper()+item.Substring(1);
        result+=item;
    }
    result=result.Substring(0,1).ToLower()+result.Substring(1);
    return result;
}
//获取表名（全大写）
public String getTableName(TableSchema table)
{
    return table.Name.ToUpper();
}
//获取类名
public String getClassName(TableSchema table)
{    
    string tableName=getTableName(table);
    return getPascalName(tableName);
}
//获取实例名
public String getClazzName(TableSchema table)
{    
    string tableName=getTableName(table);
    return getCamelName(tableName);
}
//判断是否主表
public bool isPrimaryTable(TableSchema table)
{
    bool isExistAddTime=false,isExistAddUserId=false;
    foreach(SchemaExplorer.ColumnSchema col in table.Columns)
    {
       
        
        if(col.Name.ToUpper()=="ADD_TIME")
        {
            isExistAddTime=true;
        }
        if(col.Name.ToUpper()=="ADD_USER_ID")
        {
            isExistAddUserId=true;
        }
    }
    return isExistAddTime&&isExistAddUserId;
}
public String getExtendClassName(TableSchema table)
{
    if(isPrimaryTable(table)){
        return "MasterEntity";
    }else{
        return "BaseEntity";
    }   
}
//将数据库数据类型转换为JAVA数据类型
public string getJavaType(ColumnSchema column) 
{ 
    if(column.Name.EndsWith("TypeCode"))
        return column.Name;
    switch (column.DataType) 
    { 
        case DbType.AnsiString: 
            return "String"; 
        case DbType.AnsiStringFixedLength: 
            return "String"; 
        case DbType.Binary: 
            return "byte"; 
        case DbType.Boolean: 
            return "bool"; 
        case DbType.Byte: 
            return "byte"; 
        case DbType.Currency: 
            return "decimal"; 
        case DbType.Date: 
            return "Date"; 
        case DbType.DateTime: 
            return "Date"; 
        case DbType.DateTime2: 
            return "Date"; 
        case DbType.DateTimeOffset: 
            return "Date"; 
        case DbType.Decimal: 
            return "decimal"; 
        case DbType.Double: 
            return "double"; 
        case DbType.Guid: 
            return "Guid"; 
        case DbType.Int16: 
            return "short"; 
        case DbType.Int32: 
            return "int"; 
        case DbType.Int64: 
            return "long"; 
        case DbType.Object: 
            return "object"; 
        case DbType.SByte: 
            return "sbyte"; 
        case DbType.Single: 
            return "float"; 
        case DbType.String: 
            return "String"; 
        case DbType.StringFixedLength: 
            return "String"; 
        case DbType.Time: 
            return "Date"; 
        case DbType.UInt16: 
            return "ushort"; 
        case DbType.UInt32: 
            return "uint"; 
        case DbType.UInt64: 
            return "ulong"; 
        case DbType.VarNumeric: 
            return "decimal"; 
        case DbType.Xml: 
            return "String"; 
        default: 
            return "object"; 
        } 
    } 


public string getApiUrl(TableSchema table){
    string apiUrl="";
    string[] arrName = table.Name.Split('_');//按_截取截取数组
    for(int i=0;i<arrName.Length;i++){
        string item=arrName[i];
        item=item.ToLower();
        apiUrl+="/"+item;
    }
    return apiUrl;
    
}

</script>
