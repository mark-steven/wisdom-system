//地区个性化代码模块化加载
define(['request'], function (Request) {
    var AreaCore=function(){

    };
    AreaCore.prototype.url2Path=function(url){
        var path="E:\\code\\medical-inspection\\medical-inspection-all-client\\medical-inspection-manage\\src\\main\\resources\\static\\areas\\fzjqzy\\script\\sys\\menu\\list.js";
        return path;
    };
    AreaCore.prototype.isExistFile=function(url,callback){
        var req = new Request("api/current/file/exist");
        var path=this.url2Path(url);
        req.post({
            data:path,
            isErrorTip:false,
            success: function (data) {
                if (data) {
                    callback();
                }
            }
        })
    };
    AreaCore.prototype.execute=function(url,callback){
        var self=this;
        self.isExistFile(url,function(){
            callback();
        });
    }
   return  AreaCore;
});