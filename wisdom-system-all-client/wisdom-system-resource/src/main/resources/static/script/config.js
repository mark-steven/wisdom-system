(function () {
    requirejs.config({
        baseUrl: window.GLOBAL_WEB_BASE_PATH,
        urlArgs: 'version=' + window.GLOBAL_VERSION,
        paths: {
            //hci 核心插件
            hciCore: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/core",
            hciMask: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/mask",
            hciEmpty: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/empty",
            hciPage: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/page",
            hciDataView: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/dataView",
            hciGrid: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/grid",
            hciDbDesigner: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/dbDesigner",
            hciMouseMenu: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/mouseMenu",
            hciMultiSelect: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/multiSelect",
            hciMultiCheckbox: window.GLOBAL_RESOURCE_PATH + "/script/core/ext/multiCheckbox",
            //hci业务封装
            common: window.GLOBAL_RESOURCE_PATH + "/script/core/common",
            request: window.GLOBAL_RESOURCE_PATH + "/script/core/request",
            baseLayout: window.GLOBAL_RESOURCE_PATH + "/script/core/custom/baseLayout",
            dialogDetail: window.GLOBAL_RESOURCE_PATH + "/script/core/custom/dialogDetail",
            dialogSelect: window.GLOBAL_RESOURCE_PATH + "/script/core/custom/dialogSelect",
            //路径文件
            iconList: window.GLOBAL_RESOURCE_PATH + "/script/src/iconList",
        }
    });
})();
