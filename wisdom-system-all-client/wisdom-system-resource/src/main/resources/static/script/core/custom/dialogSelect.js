﻿/**
 *  @author   ghh
 *  @version  1.0
 *  @description 弹窗选择
 */
define(['baseLayout', 'common'], function (BaseLayout, common) {
        var DialogSelect = BaseLayout;
        DialogSelect.prototype.defCellParam = {
            panelType: '',//main 主要选择面板事件
            attachId: 'grid',
            attachType: 'grid',//grid|tree|list|item
            //width:200,
            isAttachToolbar: true,
            toolbarParam: {
                icons_path: GLOBAL_WEB_BASE_PATH + '/image/icons/',
                searchers: {}
            },
            gridParam: {
                target: "grid",
                width: "100%",
                height: "100%",
                dataAction: "server",
                columns: [],
                ajax: {
                    data: {}
                },
                data: {
                    list: [],
                    total: 0
                },
                isUsePage: true
            },
            treeParam: {
                renderData: function (data) {
                    return data;
                },
                zTree: {
                    view: {
                        dblClickExpand: false,
                        showLine: true,
                        selectedMulti: false
                    },
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id",
                            pIdKey: "pid",
                            rootPId: ""
                        }
                    },
                    callback: {
                        beforeClick: function (treeId, treeNode) {
                        }
                    }
                },
                ajax: {}
            },
            listParam: {
                selectIndex: 0,
                onAfterSelect: null,
                onRenderData: null,//重新渲染数据
                ajax: {}
            }
        };
        BaseLayout.prototype.params = {
            winCallback: null,
            winStorage: null,
            parent: document.body,
            storage: [],
            primaryKeyField: 'id',
            textKeyField: 'name',
            pattern: '1C',
            fields: [],
            cells: {}
        };
        DialogSelect.prototype.storageInit = function () {
            var self = this;
            self.params._storageJson = {};
            self.params._soragePraimaryKeyValue = [];
            var top = common.getTopWindowDom();
            self.params.storage = $.extend(true, {}, top[self.params.winStorage]);
            $.each(self.params.storage, function (index, itemData) {
                self.addItem(itemData);
            });
        };
        DialogSelect.prototype.renderGridColumns = function (index) {
        };
        DialogSelect.prototype.extendBeforeInit = function () {
            var self = this;
            self.params.panelCells = {};
            //部分默认参数修改
            $.each(self.params.cells, function (index, item) {
                if (item["panelType"] == "item") {
                    item["isAttachToolbar"] = false;
                }
                var isAttachPanelTypeName = "isAttachPanel" + item["panelType"].slice(0, 1).toUpperCase() + item["panelType"].slice(1);
                self.params[isAttachPanelTypeName] = true;
                self.params.panelCells[item["panelType"]] = index;
            });
            var mainPanelIndex = self.params.panelCells["main"];
            var mainCellParam = self.params.cells[mainPanelIndex];
            var attachType = mainCellParam["attachType"];
            switch (attachType) {
                case "grid":
                    self.params.cells[mainPanelIndex]["gridParam"]["row"] = self.params.cells[mainPanelIndex]["gridParam"]["row"] || {};
                    self.params.cells[mainPanelIndex]["gridParam"]["row"]["onCheck"] = function (isCheck, rowData) {
                        if (isCheck) {
                            self.addItem(rowData);
                        } else {
                            self.removeItem(rowData);
                        }
                    };
                    self.params.cells[mainPanelIndex]["gridParam"]["id"] = self.params["primaryKeyField"];
                    self.params.cells[mainPanelIndex]["gridParam"]["afterLoadData"] = function () {
                        if (self.params.cells[mainPanelIndex]["gridParam"]["showType"] == "tree") {

                        }
                        self.params.cells[mainPanelIndex].gridObj.setCheckedRows(self.params._soragePraimaryKeyValue);
                    };
                    break;
                case "tree":
                    break;
            }
            top[self.params.winCallback] = function (index, successFn) {
                var data = self.getCheckValue();
                successFn(data);
            };
        };
        DialogSelect.prototype.extendAfterInit = function () {
            var self = this;
            //进行选中数据的转换
            self.storageInit();
        }
        DialogSelect.prototype.addItem = function (itemData) {
            var self = this;
            var primaryKeyValue = itemData[self.params.primaryKeyField]
            var mainPanelIndex = self.params.panelCells["main"];
            var isSingleCheck = self.params.cells[mainPanelIndex]["gridParam"]["row"]["checkbox"] == 1 ? true : false;
            if (isSingleCheck) {
                //如果是单选
                self.params._storageJson = {};//重新置空
                self.params._soragePraimaryKeyValue = [];//重新置空
            }
            if (!self.params._storageJson[primaryKeyValue]) {
                self.params._storageJson[primaryKeyValue] = itemData;
            }
            var index = $.inArray(primaryKeyValue, self.params._soragePraimaryKeyValue);
            if (index < 0) {
                self.params._soragePraimaryKeyValue.push(primaryKeyValue);
            }
            if (self.params["isAttachPanelItem"]) {
                var itemPanelIndex = self.params.panelCells["item"];
                var itemCellParam = self.params.cells[itemPanelIndex];
                var jqItemPanel = $("#" + itemCellParam.attachId);
                if (isSingleCheck) {
                    jqItemPanel.empty();
                }
                if (jqItemPanel.find("div[data-item-id='" + primaryKeyValue + "']").length == 0) {
                    var jqDiv = $("<div></div>")
                        .addClass("dialog-select-item")
                        .attr({"data-item-id": primaryKeyValue})
                        .text(itemData[self.params["textKeyField"]])
                        // .text(itemData["name"])
                        .data("data", itemData)
                        .hover(function () {
                            $(this).find(".remove").show();
                        }, function () {
                            $(this).find(".remove").hide();
                        });
                    var jqRemove = $('<a class="remove">X</a>').on("click", function () {
                        var jqItem = $(this).closest(".dialog-select-item");
                        var dataItemId = jqItem.attr("data-item-id");
                        jqItem.remove();
                        if (self.params._storageJson[dataItemId]) {
                            delete self.params._storageJson[dataItemId];
                        }
                        var index = $.inArray(dataItemId, self.params._soragePraimaryKeyValue);
                        self.params._soragePraimaryKeyValue.splice(index, 1);
                        var mainPanelIndex = self.params.panelCells["main"];
                        var unCheckRowsIds = [];
                        unCheckRowsIds.push(dataItemId);
                        self.params.cells[mainPanelIndex].gridObj.unCheckedRows(unCheckRowsIds);
                    });
                    jqDiv.append(jqRemove);
                    jqItemPanel.append(jqDiv);
                }
            }
        };
        DialogSelect.prototype.removeItem = function (itemData) {
            var self = this;
            var primaryKeyValue = itemData[self.params.primaryKeyField];
            if (self.params._storageJson[primaryKeyValue]) {
                delete self.params._storageJson[primaryKeyValue];
            }
            var index = $.inArray(primaryKeyValue, self.params._soragePraimaryKeyValue);
            if (index > -1) {
                self.params._soragePraimaryKeyValue.splice(index, 1);
            }
            if (self.params["isAttachPanelItem"]) {
                var itemPanelIndex = self.params.panelCells["item"];
                var itemCellParam = self.params.cells[itemPanelIndex];
                var jqItemPanel = $("#" + itemCellParam.attachId);
                jqItemPanel.find("div[data-item-id='" + primaryKeyValue + "']").remove();
            }
        };
        DialogSelect.prototype.getCheckValue = function () {
            var self = this;
            var data = [];
            $.each(self.params._storageJson, function (index, itemData) {
                data.push(itemData);
            });
            return data;
        };
        return DialogSelect;
    }
);