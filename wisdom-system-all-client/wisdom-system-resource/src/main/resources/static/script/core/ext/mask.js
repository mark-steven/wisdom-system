define(['hciCore'], function (hciCore) {
    var HCIMask = function (containers, params) {
        var self = this;
        self.params = {
            text: "数据加载中...",
            imgPath: hciCore.imgPath,
            loadingImg: "loading.gif",
            isShowText: true,
            isShowMask: true,
            isShowImage: true
        };
        self.params = $.extend(true, {}, self.params, params || {});
        /****************参数**********************/
        var _getContainerDim = function (container) {
            var dim = {top: 0, left: 0, width: 0, height: 0};
            if (typeof(container) == "object") {
                var offset = $(container).offset();
                dim.top = offset.top + "px";
                dim.left = offset.left + "px";
                dim.width = container[0].offsetWidth + "px";
                dim.height = container[0].offsetHeight + "px";
            }
            return dim;
        }
        /************遮罩面板自适应************/
        var _adaptive = function (self) {
            $(window).resize(function () {
                for (var t in self.c) {
                    var dim = _getContainerDim(self.c[t]["container"]);
                    var maskId = "hci-mask-" + self.c[t].hciId;
                    var maskObj = $("div[data-hci-id='" + maskId + "']:first");
                    if (maskObj) {
                        $(maskObj).css(dim);
                        $(self.c[t].mask).css(dim);
                    }
                    var maskContentId = "hci-mask-content-" + self.c[t].hciId;
                    var maskContentObj = $("div[data-hci-id='" + maskContentId + "']:first");
                    if (maskContentObj) {
                        maskContentObj.css({
                            top: dim.top,
                            left: dim.left,
                            width: dim.width,
                            marginTop: (parseInt(dim.height) * 2 / 5) + "px"
                        });
                    }
                }
            });
        };
        hciCore.container.call(self, containers);//继承插件容器
        for (var t in self.c) {
            //  this.c[t].container;
            var dim = _getContainerDim(self.c[t].container);
            var b = $("body");
            if (self.params.isShowMask) {
                //create mask
                self.c[t].mask = $("<div></div>").attr({"data-hci-id": "hci-mask-" + self.c[t].hciId}).addClass("hci-mask");
                hciCore.zim.reserve();
                self.c[t].mask.css({
                    "z-index": hciCore.zim.last(),
                    top: dim.top,
                    left: dim.left,
                    width: dim.width,
                    height: dim.height,
                    position: "absolute",
                    opacity: 0.6,
                    filter: "alpha(opacity=60)",
                    "-moz-opacity": 0.6,
                    background: "#f0f0f0",
                    margin: "0 auto"
                });
                b.append(self.c[t].mask);
            }
            //create mask content
            self.c[t].maskContent = $("<div></div>")
                .attr({"data-hci-id": "hci-mask-content-" + self.c[t].hciId})
                .addClass("hci-mask-content")
                .css({"margin-top": (parseInt(dim.height) * 2 / 5) + "px"});
            hciCore.zim.reserve();
            // alert(JSON.stringify(dim));
            self.c[t].maskContent.css({
                position: "absolute",
                "z-index": hciCore.zim.last(),
                top: "2%",
                left: "45%",
                width: dim.width
            });
            if (self.params.isShowImage) {
                self.c[t].maskLoading = $("<img/>").attr({src: self.params.imgPath + self.params.loadingImg});
                self.c[t].maskContent.append(self.c[t].maskLoading);
            }
            if (self.params.isShowText && self.params.text && self.params.text.length > 0) {
                self.c[t].maskText = $("<span></span>").text(self.params.text);
                self.c[t].maskContent.append(self.c[t].maskText);
            }
            b.append(self.c[t].maskContent);
            dim = null;
        }
        _adaptive(self);
    };
    HCIMask.prototype.show = function (index) {
        var _show = function (c) {
            c.mask.show();
            c.maskContent.show();
        };
        if (typeof (index) == "number") {
            _show(this.c[index]);
        } else {
            for (var t in this.c) {
                _show(this.c[t]);
            }
        }
    };
    HCIMask.prototype.hide = function (index) {
        var _hide = function (c) {
            c.mask.hide();
            c.maskContent.hide();
        };
        if (typeof (index) == "number") {
            _hide(this.c[index]);
        } else {
            for (var t in this.c) {
                _hide(this.c[t]);
            }
        }
    };
    HCIMask.prototype.destroy = function (index) {
        var _destroy = function (c) {
            if (c && c.mask && c.maskContent) {
                c.mask.remove();
                c.mask = null;
                delete c.mask;
                c.maskLoading.remove();
                c.maskLoading = null;
                delete c.maskLoading;
                c.maskText.remove();
                c.maskText = null;
                delete c.maskText;
                c.maskContent.remove();
                c.maskContent = null;
                delete c.maskContent;
            }
        };
        if (typeof (index) == "number") {
            _destroy(this.c[index]);
        } else {
            for (var t in this.c) {
                _destroy(this.c[t]);
            }
        }
    };
    return HCIMask;
})
