define(['request', 'hciCore'], function (Reqeust, hciCore) {
    var HCIMultiCheckbox = function (params) {
        var self = this;
        self.params = $.extend(true, {}, self.defParams, params);
        self.init();
    };
    HCIMultiCheckbox.prototype.defParams = {
        target: '',
        width: 0,
        dataAction: "server",//ajax请求方式
        name: '',//跟checkbox的name
        singleCheck: true,
        defaultCheckType: 'value',//value 或 index
        defaultCheckValue: '',
        defaultCheckIndex: '',
        ajaxParam: {
            url: '',
            type: 'get',
            data: {},
            success: function (data) {

            }
        },
        disableField: 'disable',
        disableValue: 2,
        allowDefaultValue: function () {
            return true;
        },
        initReadOnly: function () {
            return false;
        },
        data: [],
        checkboxTextField: 'text',//显示文字字段
        checkboxValueField: 'value',//值字段
        afterLoadData: function (data) {

        },
        onChecked: function (data) {

        },
        renderData: function (data) {
            return data;
        }
    };
    HCIMultiCheckbox.prototype.init = function () {
        var self = this;
        self.jqTarget = $(self.params.target).addClass("hci-multi-checkbox");
        if (self.params.width > 0) {
            self.jqTarget.css({width: self.params.width});
        }
        if (self.params.dataAction == "server") {
            self.loadServerData()
        } else {
            self.loadData()
        }
    };
    HCIMultiCheckbox.prototype.loadServerData = function () {
        var self = this;
        var req = new Reqeust(self.params.ajaxParam.url);
        var ajaxData = self.params.ajaxParam.data;
        if (typeof (ajaxData) == "function") {
            ajaxData = ajaxData();
        }
        req[self.params.ajaxParam.type]({
            async: false,
            data: ajaxData,
            success: function (data) {
                self.params.data = self.params.renderData(data);
                self.loadData();
            }
        });

    };
    HCIMultiCheckbox.prototype.loadData = function () {
        var self = this;
        self.jqTarget.empty();
        var jqBoxWarp = $("<div></div>").addClass("box-warp");
        var jqCheckboxWarp = $("<span></span>").css({"display": "none"});
        if (self.params.initReadOnly()) {
            self.isReadOnly = true;
        }
        $.each(self.params.data, function (index, item) {
            var defaultClass = "";
            var disable = false;
            if (item[self.params.disableField] == self.params.disableValue) {
                disable = true;
            }
            if (self.params.allowDefaultValue()) {
                if (self.params.defaultCheckType == "index") {
                    if (index == self.params.defaultCheckIndex) {
                        defaultClass = "selected"
                    }
                } else {

                }
            }
            if (disable) {
                defaultClass = "disable";
            }
            var jqA = $("<a></a>")
                .attr({"javascript": ";", "data-id": item[self.params.checkboxValueField]})
                .text(item[self.params.checkboxTextField])
                .data("value", item[self.params.checkboxValueField])
                .addClass(defaultClass);

            if (disable == false) {
                jqA.on("click", function () {
                    if (!self.isReadOnly) {
                        if (self.params.singleCheck) {
                            self.jqTarget.find("a").removeClass("selected");
                            $(this).addClass("selected");
                            self.params.onChecked(item[self.params.checkboxValueField]);
                        } else {

                        }
                    }
                });
            }
            jqBoxWarp.append(jqA);
            if (item[self.params.checkboxValueField] && defaultClass == "selected") {
                self.params.onChecked(item[self.params.checkboxValueField]);
            }


        });
        self.jqTarget.append(jqBoxWarp).append(jqCheckboxWarp);
        self.params.afterLoadData(self.params.data);
    }
    HCIMultiCheckbox.prototype.getCheckedValue = function () {
        var self = this;
        var valueArray = [];
        self.jqTarget.find(".selected").each(function () {
            var val = $(this).data("value");
            valueArray.push(val);
        })
        if (valueArray.length == 1) {
            return valueArray[0];
        } else {
            return valueArray;
        }
    };
    HCIMultiCheckbox.prototype.setDefaultChecked = function (callback) {
        var self = this;
        if (self.jqTarget.find(".selected").length == 0) {
            self.jqTarget.find("a").each(function () {
                if (!$(this).hasClass("disable")) {
                    $(this).addClass("selected");
                    if (typeof (callback) == "function") {
                        var value = $(this).data("value");
                        callback(value)
                    }
                    return false;
                }
            })
        }
    };
    HCIMultiCheckbox.prototype.setReadOnly = function () {
        var self = this;
        self.jqTarget.find(".selected").removeClass("selected");
        self.isReadOnly = true;
    };
    HCIMultiCheckbox.prototype.cancelReadOnly = function () {
        var self = this;
        self.isReadOnly = false;
    }
    return HCIMultiCheckbox;
})