define(['hciCore', 'hciMask', 'hciEmpty', 'hciPage'], function (hciCore, HCIMask, HCIEmpty, HCIPage) {
    var HCIDataView = function (params) {
        var self = this;
        self.params = $.extend(true, {}, self.params, params || {});
        hciCore.container.call(self, self.params.target);//继承插件容器
        self.init();
        self.resize();
    };
    HCIDataView.prototype.params = {
        target: '',
        isLoadData: true,
        width: "100%",
        height: "100%",
        item: {
            minWidth: 220,
            height: 250,
            isCheckbox: true,//false 无选择框 | true 多选
            isSelected: null,
            isChecked: null,//
            checkedField: '__checked',
            checkedValue: true,
            render: function () {

            },
            afterRender: function (data) {

            },
            onSelected: function (data) {

            }
        },
        dataAction: 'server',//"server"|"local"
        ajax: {},
        dataType: "json",
        data: {list: [], total: 0},
        isUsePage: true,
        pageInfo: {
            page: 1,
            pageSize: 30,
            totalSize: 0
        }
    };
    HCIDataView.prototype.init = function () {
        var self = this;
        var jqHciDataViewOuter = $("<div></div>").css({
            position: 'relative',
            width: self.params.width,
            height: self.params.height
        });
        var jqHciDataView = $("<div></div>").addClass("hci-data-view");
        var jqUl = $("<ul></ul>").addClass("hci-data-view-ul");
        jqHciDataView.append(jqUl);
        jqHciDataViewOuter.append(jqHciDataView);
        self.c[0].container.append(jqHciDataViewOuter);
        //分页处理
        if (self.params.isUsePage) {
            var jqHciDataViewPage = $("<div class='hci-data-view-page'></div>");
            jqHciDataViewOuter.append(jqHciDataViewPage);
            var divHeight = self.c[0].container.innerHeight();
            jqHciDataView.outerHeight(parseInt(divHeight) - 35);
            self.hciPage = new HCIPage({
                target: jqHciDataViewPage,
                page: self.params.pageInfo["page"],
                pageSize: self.params.pageInfo["pageSize"],
                totalSize: self.params.pageInfo["totalSize"],
                afterChangePage: function () {
                    self.loadData();
                }
            });
        }
        //空数据处理
        self.hciEmpty = new HCIEmpty({
            target: jqHciDataView,
            imageUrl: GLOBAL_RESOURCE_PATH + "/script/core/ext/images/empty-data.png"
        });
        self.hciEmpty.hide();
        //数据加载中动画
        self.hciMask = new HCIMask(jqHciDataView);
        self.hciMask.hide();
        //加载数据
        if (self.params.isLoadData) {
            //初始化未加载数据
            self.loadData();
        } else {
            jqUl.hide();
            self.hciEmpty.show();
        }
    };
    HCIDataView.prototype.resize = function () {
        var self = this;
        var _adaptive = function () {
            var divWidth = self.c[0].container.innerWidth();
            var liSpace = parseFloat(self.c[0].container.find(".hci-data-view-item").css("margin-left")) + 1;
            var rankCount = Math.floor((divWidth - liSpace) / (self.params.item.minWidth + liSpace));
            var liWidth = ((divWidth - 2 * liSpace) / rankCount) - liSpace;
            self.c[0].container.find(".hci-data-view-item").width(liWidth).height(self.params.item.height);
            var divHeight = self.c[0].container.innerHeight();
            var diffHeight = 0;
            if (self.params.isUsePage) {
                diffHeight = parseInt(divHeight) - 35;
            } else {
                diffHeight = parseInt(divHeight);
            }
            self.c[0].container.find(".hci-data-view").outerHeight(diffHeight);
        };
        _adaptive();
        $(window).resize(function () {
            _adaptive();
        });
    };
    HCIDataView.prototype._loadData = function (data, fromNum, toNum) {
        var self = this;
        if (fromNum && toNum) {
            var diffNum = toNum - fromNum;
            for (var i = 0; i <= diffNum; i++) {
                self.addItem(data[fromNum + i - 1]);
            }
        } else {
            $.each(data, function (index, item) {
                self.addItem(item);
            })
        }
        self.resize();
        self.hciMask.hide();

    };
    HCIDataView.prototype.loadData = function () {
        var self = this;
        self.hciMask.show();
        self.c[0].container.find(".hci-data-view-ul").empty().show();
        if (self.params.dataAction == "server") {
            self.loadServerData();
        } else {
            self.loadLocalData();
        }
    };
    HCIDataView.prototype.reload = function () {

    };
    HCIDataView.prototype.loadLocalData = function () {
        var self = this;
        var list = self.params.data.list;
        if (self.params.isUsePage) {
            self.params.data.total = list.length;
            var total = list.length;
            self.hciPage.setTotalSize(total);
            var fromNum = self.hciPage.params.fromNum;
            var toNum = self.hciPage.params.toNum;
            self._loadData(list, fromNum, toNum);
        } else {
            self._loadData(list);
        }
    };
    HCIDataView.prototype.loadServerData = function () {

    };
    HCIDataView.prototype.addItem = function (data) {
        var self = this;
        var jqLi = $("<li></li>").data("data", data).addClass("hci-data-view-item hci-data-view-item-def");
        var jqContent = $("<div></div>").addClass("hci-data-view-item-content");
        if (self.params.item.isCheckbox) {
            var jqCheckbox = $('<div class="hci-data-view-checkbox"><div class="hci-data-view-item-checkbox hci-data-view-item-checked"></div></div>');
            jqLi.append(jqCheckbox);
        }
        jqLi.append(jqContent);
        if (typeof(self.params.item.render) == "function") {
            var jqContentInner = self.params.item.render(data);
            jqContent.append(jqContentInner);
        }
        self.c[0].container.find(".hci-data-view-ul").append(jqLi);
    };
    return HCIDataView;
});


