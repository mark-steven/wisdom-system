# common 公用函数使用

# request ajax请求
 
# HCIGrid 表格应用 

# baseLayout 参数配置
**布局参数**

**工具条配置**

**工具条时间配置**
```
startDate: {
    label: '开始时间:',
    name: 'startDate',
    type: 'date',
    width: 150,
    attr: {
        "id": "startDate",
        "data-default-value-type": "d",
        "data-default-value": -1,
        "data-max-value": 'endDate'
    }
},
endDate: {
   label: '结束时间:',
   name: 'endDate',
   type: 'date',
   width: 150,
   attr: {
        "id": "endDate",
        "data-default-value-type": "d",
        "data-default-value": 0,
        "data-min-value": 'startDate'
    }
}
```
# dialogDetail 参数配置


# validForm
$.Tipmsg={//默认提示文字;
    tit:"提示信息",
    w:{
        "*":"不能为空！",
        "*6-16":"请填写6到16位任意字符！",
        "n":"请填写数字！",
        "n6-16":"请填写6到16位数字！",
        "s":"不能输入特殊字符！",
        "s6-18":"请填写6到18位字符！",
        "p":"请填写邮政编码！",
        "m":"请填写手机号码！",
        "e":"邮箱地址格式不对！",
        "url":"请填写网址！"
    },
    def:"请填写正确信息！",
    undef:"datatype未定义！",
    reck:"两次输入的内容不一致！",
    r:"通过信息验证！",
    c:"正在检测信息…",
    s:"请{填写|选择}{0|信息}！",
    v:"所填信息没有经过验证，请稍后…",
    p:"正在提交数据…"
};