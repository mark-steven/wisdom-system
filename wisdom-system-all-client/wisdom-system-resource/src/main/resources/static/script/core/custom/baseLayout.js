/**
 *  @author   ghh
 *  @version  1.0
 *  @description 布局插件
 */
define(['common', 'request', 'hciCore', 'hciGrid'], function (common, Request, hciCore, HCIGrid) {
    //根据id获取toolbar 某个item项的DOM对象
    dhtmlXToolbarObject.prototype.getItem = function (itemId) {
        if (this.objPull[this.idPrefix + itemId] == null) {
            return null;
        }
        for (var q = 0; q < this.base.childNodes.length; q++) {
            if (this.base.childNodes[q].idd != null) {
                if (this.base.childNodes[q].idd == itemId) {
                    return this.base.childNodes[q];
                }
            }
        }
    };
    var BaseLayout = function (params) {
        var self = this;
        self.params = $.extend(true, {}, self.params, params);
        self.extendBeforeInit();//预留重写方法，支持DialogSelect在此封装基础上进行重写
        self.init();
        self.extendAfterInit();//预留重写方法，支持DialogSelect在此封装基础上进行重写
    };
    BaseLayout.prototype.extendBeforeInit = function () {
        //预留继承的空方法
    };
    BaseLayout.prototype.extendAfterInit = function () {
        //预留继承的空方法
    };
    BaseLayout.prototype.params = {
        parent: document.body,
        pattern: '1C',
        cells: {}
    };
    BaseLayout.prototype.defCellParam = {
        isAttachTabBar: false,
        attachId: 'grid',
        attachType: 'grid',//grid|tree|list|dataView|other
        //width:200,
        isAttachToolbar: true,
        arrow: '',
        toolbarParam: {
            icons_path: GLOBAL_WEB_BASE_PATH + '/image/icons/',
            showText: true,
            buttons: {"add": true, "edit": true, "del": true},
            searchers: {},
            // items: [],
            addParam: {
                //新增传过去的URL参数（支持方法）
                urlParam: null,
                //是否可执行新增按钮点击事件
                enableAdd: function () {
                    return true;
                }
            },
            editParam: {
                //编辑传过去的URL参数（支持方法）
                urlParam: null,
                //是否可执行编辑按钮点击事件
                enableEdit: function () {
                    return true
                }
            },
            delParam: {
                //是否可执行删除按钮点击事件
                enableDel: function () {
                    return true
                },
                confirmMsg: "是否删除当前选中的数据",
                ajax: {
                    url: null,
                    type: 'delete'
                }
            }
        },
        winParam: {
            winCallback: null,
            all: {
                title: '',
                href: '',
                area: ["600px", "500px"]
            },
            add: {
                title: '新增信息'
            },
            edit: {
                showButton: true,
                enableEdit: true,
                title: '编辑信息',
                titleKey: null,
                primaryKeyField: 'id',
                stateField: 'state',
                updateState: "update"//修改状态标志
            }
        },
        gridParam: {
            primaryKey: 'id',
            target: "grid",
            width: "100%",
            height: "100%",
            dataAction: "server",
            columns: [],
            data: {
                list: [],
                total: 0
            },
            isUsePage: true
        },
        treeParam: {
            selectIndex: 0,
            ajax: {},
            renderData: function (data) {
                return data;
            },
            onClick: null,//这个配合zTree中的beforeClick进行重写onClick逻辑
            zTree: {
                view: {
                    dblClickExpand: false,
                    showLine: true,
                    selectedMulti: false,
                    autoCancelSelected: true
                },
                data: {
                    simpleData: {
                        enable: true,
                        idKey: "id",
                        pIdKey: "pid",
                        rootPId: ""
                    }
                },
                callback: {
                    beforeClick: function (treeId, treeNode) {

                    },
                    onClick: function (event, treeId, treeNode) {

                    }
                }
            }
        },
        listParam: {
            selectIndex: 0,
            onAfterSelect: null,
            ajax: {},
            onRenderData: function (data) {
                //重新渲染数据
                return data["result"];
            }
        },
        dataViewParam: {
            isInitLoadData: true,
            dataAction: "server",
            data: [],
            ftype: "ficon",
            ajax: {
                url: null
            },
            onAfterSelect: null,
            //重新渲染数据
            onRenderData: function (data) {
                return data["result"]["list"];
            },
            onDblClick: null
        },
        onResizeFinish: null,
        onPanelResizeFinish: null,
        onCollapse: null,
        onExpand: null,
        tabs: {
            // a: {
            //     name: '',
            //     isActive: false,
            //     hasCloseBtn: false,
            //     cells: {}
            // }
        }
    };
    BaseLayout.prototype.init = function () {
        var self = this;
        var layout = new dhtmlXLayoutObject({
            parent: self.params.parent,
            pattern: self.params.pattern,
            offsets: {top: 0, right: 0, bottom: 0, left: 0},
            cells: {}
        });
        self.layout = layout;
        layout.detachHeader();
        var cellIndex = 0;
        $.each(self.params.cells, function (layoutCellId, layoutCellItem) {


            if (layoutCellItem["toolbarParam"] && layoutCellItem["toolbarParam"]["buttons"]) {
                delete self.defCellParam["toolbarParam"]["buttons"]
            }
            layoutCellItem = $.extend(true, {}, self.defCellParam, layoutCellItem);
            self.params.cells[layoutCellId] = layoutCellItem;
            //获取面板
            var cell = layout.cells(layoutCellId);
            cell.hideHeader();
            //判断并设置面板宽度
            if (layoutCellItem.width) {
                cell.setWidth(layoutCellItem.width);
            }
            //设置并设置面板高度
            if (layoutCellItem.height) {
                cell.setHeight(layoutCellItem.height);
            }
            //标题
            if (layoutCellItem.title) {
                cell.setText(layoutCellItem.title);
            }

            //默认是否展开关闭
            if (layoutCellItem.collapse) {
                cell.collapse();
            }

            //是否附加tabs
            if (layoutCellItem["isAttachTabBar"]) {
                //附加工具条
                if (layoutCellItem.isAttachToolbar) {
                    self.renderToolbarItems(layoutCellId);
                    self.params.cells[layoutCellId].toolbarObj = cell.attachToolbar(layoutCellItem.toolbarParam);//附加工具条
                }
                var _tabs = [];
                var tabIndex = 0;
                $.each(layoutCellItem["tabs"], function (tabId, tabItem) {
                    _tabs.push({
                        id: tabId,
                        text: tabItem["name"],
                        active: tabIndex == 0 ? true : false
                    });
                    tabIndex += 1;
                });
                self.params.cells[layoutCellId].tabbarObj = cell.attachTabbar({
                    tabs: _tabs
                });
                self.params.cells[layoutCellId].tabbarObj.forEachTab(function (tabCell) {
                    var tabCellId = tabCell.getId();
                    var tabCellItem = layoutCellItem["tabs"][tabCellId];
                    tabCellItem = $.extend(true, {}, self.defCellParam, tabCellItem);
                    self.params.cells[layoutCellId].tabs[tabCellId] = tabCellItem;
                    //工具条
                    if (tabCellItem.isAttachToolbar) {
                        self.renderToolbarItems(layoutCellId, tabCellId);
                        self.params.cells[layoutCellId].tabs[tabCellId].toolbarObj = tabCell.attachToolbar(tabCellItem.toolbarParam);//附加工具条
                        self.params.cells[layoutCellId].tabs[tabCellId].toolbarObj.forEachItem(function (itemId) {
                            $.each(tabCellItem.toolbarParam.items, function (idx, itm) {
                                if (itm['id'] == itemId && typeof (itm['render']) === 'function') {
                                    //支持自动渲染工具项
                                    $(self.params.cells[layoutCellId].tabs[tabCellId].toolbarObj.getItem(itemId)).empty().append(itm['render']());
                                }
                            });
                        });
                        self.params.cells[layoutCellId].tabs[tabCellId].toolbarObj.attachEvent('onClick', function (id) {
                            $.each(tabCellItem.toolbarParam.items, function (idx, itm) {
                                if (itm['type'] === 'button' && itm['id'] === id && typeof (itm['onClick']) === 'function') {
                                    itm['onClick'](id);
                                } else if (itm["type"] == "buttonSelect" && itm["options"]) {
                                    $.each(itm["options"], function (idx2, itm2) {
                                        if (itm2["id"] == id) {
                                            itm2["onClick"]();
                                        }
                                    });
                                }
                            });
                        });
                        var hasDateSearchers = false;
                        $.each(self.params.cells[layoutCellId].tabs[tabCellId].toolbarSearchers.dates, function (dateIndex, dateId) {
                            var dateOuterDom = self.params.cells[layoutCellId].tabs[tabCellId].toolbarObj.getItem(dateId);
                            var _attr = self.params.cells[layoutCellId].tabs[tabCellId].toolbarParam["searchers"][dateId]["attr"];
                            var attr = {"data-type": "date"};
                            attr = $.extend(true, attr, _attr);
                            $(dateOuterDom).find("input").attr(attr).addClass("hci-date");
                            hasDateSearchers = true;
                        });

                        if (hasDateSearchers) {
                            common.initCalendar();
                        }

                        $.each(self.params.cells[layoutCellId].tabs[tabCellId].toolbarSearchers.selects, function (selIndex, selId) {

                            var selOuterDom = self.params.cells[layoutCellId].tabs[tabCellId].toolbarObj.getItem(selId);
                            $(selOuterDom).empty().append("<div style='padding: 4px;'></div>");
                            var comboObj = new dhtmlXCombo({
                                parent: $(selOuterDom).find("div")[0],
                                width: 230,
                                filter: true,
                                name: "combo",
                                items: [
                                    {value: "1", text: "The Adventures of Tom Sawyer"},
                                    {value: "2", text: "The Dead Zone", selected: true},
                                    {value: "3", text: "The First Men in the Moon"},
                                    {value: "4", text: "The Girl Who Loved Tom Gordon"},
                                    {value: "5", text: "The Green Mile"},
                                    {value: "6", text: "The Invisible Man"},
                                    {value: "7", text: "The Island of Doctor Moreau"},
                                    {value: "8", text: "The Prince and the Pauper"}
                                ]
                            });
                            comboObj.attachEvent("onChange", function (value, text) {

                            });
                        });
                    }
                    //tabCell
                    if (tabCellItem.attachId) {
                        switch (tabCellItem.attachType) {
                            case 'grid':
                                var gridHtml = '<div id="{attachId}" class="layout-attach-cell-outer">' +
                                    '</div>';
                                gridHtml = hciCore.template(gridHtml, {attachId: tabCellItem.attachId});
                                $('body').append(gridHtml);
                                tabCell.attachObject(tabCellItem.attachId);
                                self.gridInit(layoutCellId, tabCellId);
                                break;
                            case 'tree':
                                break;
                            case 'list':
                                break;
                            case "dataView":
                                break;
                            default:
                                if ($("#" + tabCellItem.attachId).length == 0) {
                                    var divHtml = '<div id="{attachId}" class="layout-attach-cell-outer"></div>';
                                    divHtml = hciCore.template(divHtml, {attachId: tabCellItem.attachId});
                                    $('body').append(divHtml);
                                }
                                tabCell.attachObject(tabCellItem.attachId);
                                break;
                        }
                    }
                });
            } else {
                //判断是否附加工具条，如果附件工具条则把工具条附加上去
                if (layoutCellItem.isAttachToolbar) {
                    self.renderToolbarItems(layoutCellId);
                    self.params.cells[layoutCellId].toolbarObj = cell.attachToolbar(layoutCellItem.toolbarParam);//附加工具条
                    self.params.cells[layoutCellId].toolbarObj.forEachItem(function (itemId) {
                        $.each(layoutCellItem.toolbarParam.items, function (idx, itm) {
                            if (itm['id'] == itemId && typeof (itm['render']) === 'function') {
                                //支持自动渲染工具项
                                $(self.params.cells[layoutCellId].toolbarObj.getItem(itemId)).empty().append(itm['render']());
                            }
                        });
                    });
                    self.params.cells[layoutCellId].toolbarObj.attachEvent('onClick', function (id) {
                        $.each(layoutCellItem.toolbarParam.items, function (idx, itm) {
                            if (itm['type'] === 'button' && itm['id'] === id && typeof (itm['onClick']) === 'function') {
                                itm['onClick'](id);
                            } else if (itm["type"] == "buttonSelect" && itm["options"]) {
                                $.each(itm["options"], function (idx2, itm2) {
                                    if (itm2["id"] == id) {
                                        itm2["onClick"]();
                                    }
                                });
                            }
                        });
                    });
                    self.params.cells[layoutCellId].toolbarObj.attachEvent('onEnter', function (id, value) {
                        $.each(layoutCellItem.toolbarParam.items, function (idx, itm) {
                            if (itm['type'] === 'buttonInput' && itm['id'] === id && typeof (itm['onEnter']) === 'function') {
                                itm['onEnter'](id, value);
                            }
                        });
                    });

                    var hasDateSearchers = false;
                    $.each(self.params.cells[layoutCellId].toolbarSearchers.dates, function (dateIndex, dateId) {
                        var dateOuterDom = self.params.cells[layoutCellId].toolbarObj.getItem(dateId);
                        var _attr = self.params.cells[layoutCellId].toolbarParam["searchers"][dateId]["attr"];
                        var attr = {"data-type": "date"};
                        attr = $.extend(true, attr, _attr);
                        $(dateOuterDom).find("input").attr(attr).addClass("hci-date");
                        hasDateSearchers = true;
                    });
                    if (hasDateSearchers) {
                        common.initCalendar();
                    }

                    //todo combo
                    $.each(self.params.cells[layoutCellId].toolbarSearchers.selects, function (selIndex, selId) {
                        var selOuterDom = self.params.cells[layoutCellId].toolbarObj.getItem(selId);
                        $(selOuterDom).empty().append("<div style='padding: 4px;'></div>");
                        var selParam = self.params.cells[layoutCellId].toolbarParam["searchers"][selId];
                        var defSelParam = {
                            label: '下拉选择',
                            type: "select",
                            name: "select",//跟后端字段对应(相当于value)
                            displayField: '',//相当于text
                            ajaxParam: {
                                resultRender: function (data) {
                                    return data["result"];
                                }
                            },//与Ajax一致
                            isReadOnly: false,
                            selectValueField: 'id',
                            selectTextField: 'name',
                            defaultSelectType: 'value',//初始化默认值选择方式：index 索引；value 值；
                            dataAction: 'server',
                            width: 180,
                            placeholder: '---请选择---',
                            onChange: function () {

                            }
                        };
                        selParam = $.extend(true, {}, defSelParam, selParam);
                        self.params.cells[layoutCellId][selId] = new dhtmlXCombo({
                            parent: $(selOuterDom).find("div")[0],
                            width: selParam["width"],
                            filter: true
                        });
                        self.params.cells[layoutCellId][selId].enableFilteringMode("between");
                        self.loadToolbarSelect(layoutCellId, selId);
                        self.params.cells[layoutCellId][selId].attachEvent("onChange", function (value, text) {
                            if (selParam["onChange"] && typeof (selParam["onChange"]) == "function") {
                                selParam["onChange"](value, text);
                            }
                        });
                        if (typeof (selParam["isReadOnly"]) == "function" && selParam["isReadOnly"]) {
                            self.params.cells[layoutCellId][selId].disable();
                        } else if (typeof (selParam["isReadOnly"]) != "function" && selParam["isReadOnly"]) {
                            self.params.cells[layoutCellId][selId].disable();
                        }
                    });
                    // todo combo end
                }
                if (layoutCellItem.attachId) {
                    switch (layoutCellItem.attachType) {
                        case 'grid':
                            var gridHtml = '<div id="{attachId}" class="layout-attach-cell-outer">' +
                                '</div>';
                            gridHtml = hciCore.template(gridHtml, {attachId: layoutCellItem.attachId});
                            $('body').append(gridHtml);
                            cell.attachObject(layoutCellItem.attachId);
                            self.gridInit(layoutCellId);
                            self.layout.attachEvent("onResizeFinish", function () {
                                self.params.cells[layoutCellId].gridObj.resize();
                            });
                            self.layout.attachEvent("onPanelResizeFinish", function () {
                                self.params.cells[layoutCellId].gridObj.resize();
                            });
                            self.layout.attachEvent("onCollapse", function () {
                                self.params.cells[layoutCellId].gridObj.resize();
                            });
                            self.layout.attachEvent("onExpand", function () {
                                self.params.cells[layoutCellId].gridObj.resize();
                            });
                            break;
                        case 'tree':
                            var treeHtml = '<div id="{attachId}" class="tree-panel">' +
                                '<div id="{treeId}" class="ztree"></div>' +
                                '</div>';
                            treeHtml = hciCore.template(treeHtml, {
                                attachId: layoutCellItem.attachId,
                                treeId: layoutCellItem.treeId
                            });
                            $('body').append(treeHtml);
                            cell.attachObject(layoutCellItem.attachId);
                            self.treeInit(layoutCellId);
                            break;
                        case 'list':
                            var listHtml = '<div id="{attachId}" class="layout-attach-cell-outer">' +
                                '</div>';
                            listHtml = hciCore.template(listHtml, {attachId: layoutCellItem.attachId});
                            $('body').append(listHtml);
                            cell.attachObject(layoutCellItem.attachId);
                            self.listInit(layoutCellId);
                            break;
                        case "dataView":
                            var dataViewHtml = '<div id="{attachId}" class="layout-attach-cell-outer" style="padding: 5px;">' +
                                '</div>';
                            dataViewHtml = hciCore.template(dataViewHtml, {attachId: layoutCellItem.attachId});
                            $('body').append(dataViewHtml);
                            cell.attachObject(layoutCellItem.attachId);
                            // self.listInit(layoutCellId);
                            self.dataViewInit(layoutCellId);
                            break;
                        default:
                            if ($("#" + layoutCellItem.attachId).length == 0) {
                                var divHtml = '<div id="{attachId}" class="layout-attach-cell-outer"></div>';
                                divHtml = hciCore.template(divHtml, {attachId: layoutCellItem.attachId});
                                $('body').append(divHtml);
                            }
                            cell.attachObject(layoutCellItem.attachId);
                            self.layout.attachEvent("onResizeFinish", function () {
                                if (typeof (self.params.cells[layoutCellId].onResizeFinish) == "function") {
                                    self.params.cells[layoutCellId].onResizeFinish()
                                }
                            });
                            self.layout.attachEvent("onPanelResizeFinish", function () {
                                if (typeof (self.params.cells[layoutCellId].onPanelResizeFinish) == "function") {
                                    self.params.cells[layoutCellId].onPanelResizeFinish()
                                }
                            });
                            self.layout.attachEvent("onCollapse", function () {
                                if (typeof (self.params.cells[layoutCellId].onCollapse) == "function") {
                                    self.params.cells[layoutCellId].onCollapse()
                                }
                            });
                            self.layout.attachEvent("onExpand", function () {
                                if (typeof (self.params.cells[layoutCellId].onExpand) == "function") {
                                    self.params.cells[layoutCellId].onExpand()
                                }
                            });
                            break;
                    }
                    if (layoutCellItem["arrow"]) {
                        $("#" + layoutCellItem.attachId).prepend('<div class="layout-arrow layout-arrow-' + layoutCellItem["arrow"] + '"></div>');
                        $("#" + layoutCellItem.attachId).find(".layout-arrow").on("click", function () {
                            cell.collapse();
                        })
                    }
                }
            }

            layout.setSeparatorSize(cellIndex, 3);
            cellIndex += 1;
        });
    };
    BaseLayout.prototype.loadToolbarSelect = function (layoutCellId, searchId, tabCellId) {
        var self = this;
        var layoutCellItemOrTabCellItem;
        if (tabCellId) {
            layoutCellItemOrTabCellItem = self.params.cells[layoutCellId].tabs[tabCellId];
        } else {
            layoutCellItemOrTabCellItem = self.params.cells[layoutCellId];
        }

        var selParam = layoutCellItemOrTabCellItem.toolbarParam["searchers"][searchId];
        var defSelParam = {
            label: '下拉选择',
            type: "select",
            name: "select",//跟后端字段对应(相当于value)
            displayField: '',//相当于text
            ajaxParam: {
                resultRender: function (data) {
                    return data["result"];
                }
            },//与Ajax一致
            selectValueField: 'id',
            selectTextField: 'name',
            defaultSelectType: 'value',//初始化默认值选择方式：index 索引；value 值；
            dataAction: 'server',
            width: 180,
            placeholder: '---请选择---',
            onChange: function () {
            }
        };
        selParam = $.extend(true, {}, defSelParam, selParam);
        layoutCellItemOrTabCellItem[searchId].clearAll();
        if (selParam["dataAction"] === "local") {
            var list = selParam["data"];
            var result = [];
            $.each(list, function (idx, itm) {
                itm = {
                    id: itm[selParam["selectValueField"]],
                    name: itm[selParam["selectTextField"]],
                    text: itm[selParam["selectTextField"]],
                    value: itm[selParam["selectValueField"]]
                };
                result.push(itm);
            });
            layoutCellItemOrTabCellItem[searchId].load({options: result});
            if (selParam["defaultSelectType"] == "value") {
                layoutCellItemOrTabCellItem[searchId].setComboValue(selParam["defaultSelectValue"]);
            } else if ((selParam["defaultSelectValue"] == 0 || selParam["defaultSelectValue"]) && selParam["defaultSelectType"] == "index") {
                if (result && result.length > selParam["defaultSelectValue"]) {
                    layoutCellItemOrTabCellItem[searchId].setComboValue(result[selParam["defaultSelectValue"]]['value']);
                } else if (result && result.length == selParam["defaultSelectValue"]) {
                    layoutCellItemOrTabCellItem[searchId].setComboText("");
                }
            }
        } else {
            var ajaxUrlParam = typeof (selParam.ajaxParam["urlParam"]) == "function" ? selParam.ajaxParam["urlParam"]() : selParam.ajaxParam["urlParam"];
            var ajaxDataParam = typeof (selParam.ajaxParam["data"]) == "function" ? selParam.ajaxParam["data"]() : selParam.ajaxParam["data"];
            var req = new Request(selParam.ajaxParam["url"], ajaxUrlParam);
            req.get({
                data: ajaxDataParam,
                success: function (data) {
                    var list = selParam.ajaxParam["resultRender"](data);
                    var result = [];
                    $.each(list, function (idx, itm) {
                        itm = {
                            id: itm[selParam["selectValueField"]],
                            name: itm[selParam["selectTextField"]],
                            text: itm[selParam["selectTextField"]],
                            value: itm[selParam["selectValueField"]]
                        };
                        result.push(itm);
                    });
                    layoutCellItemOrTabCellItem[searchId].load({options: result});
                    if (selParam["defaultSelectType"] == "value") {
                        layoutCellItemOrTabCellItem[searchId].setComboValue(selParam["defaultSelectValue"]);
                    } else if ((selParam["defaultSelectValue"] == 0 || selParam["defaultSelectValue"]) && selParam["defaultSelectType"] == "index") {
                        if (result && result.length > selParam["defaultSelectValue"]) {
                            layoutCellItemOrTabCellItem[searchId].setComboValue(result[selParam["defaultSelectValue"]]['value']);
                        } else if (result && result.length == selParam["defaultSelectValue"]) {
                            layoutCellItemOrTabCellItem[searchId].setComboText("");
                        }
                    }
                }
            });
        }
    };
    BaseLayout.prototype.renderToolbarItems = function (layoutCellId, tabCellIndex) {
        var self = this;
        var layoutCellItemOrTabCellItem;
        if (tabCellIndex) {
            layoutCellItemOrTabCellItem = self.params.cells[layoutCellId].tabs[tabCellIndex];
        } else {
            layoutCellItemOrTabCellItem = self.params.cells[layoutCellId];
        }
        layoutCellItemOrTabCellItem.toolbarParam.items = [];
        var buttons = $.extend(true, {}, layoutCellItemOrTabCellItem.toolbarParam.buttons);
        $.each(buttons, function (itemIndex, itemInfo) {
            var btnJSON = {"add": "toolbarBtnAdd", "edit": "toolbarBtnEdit", "del": "toolbarBtnDel"};
            switch (itemIndex) {
                case "add":
                case "edit":
                case "del":
                    if (itemInfo) {
                        layoutCellItemOrTabCellItem.toolbarParam.items.push(self[btnJSON[itemIndex]](layoutCellId));
                    }
                    break;
                default:
                    layoutCellItemOrTabCellItem.toolbarParam.items.push(itemInfo);
                    break;
            }
            if (itemInfo) {
                layoutCellItemOrTabCellItem.toolbarParam.items.push(self.toolbarBtnSeparator(layoutCellId));
            }
        });
        var searchers = layoutCellItemOrTabCellItem.toolbarParam.searchers;
        layoutCellItemOrTabCellItem.toolbarSearchers = {
            dates: [],
            selects: []
        };
        $.each(searchers, function (itemIndex, itemInfo) {
            switch (itemIndex) {
                case "btnSearch":
                    if (itemInfo) {
                        layoutCellItemOrTabCellItem.toolbarParam.items.push(self.toolbarSearchBtn(layoutCellId));
                    }
                    break;
                default:
                    if (itemInfo["label"]) {
                        layoutCellItemOrTabCellItem.toolbarParam.items.push({
                            type: "text",
                            text: itemInfo["label"]
                        });
                    }
                    switch (itemInfo["type"]) {
                        case "text":
                            layoutCellItemOrTabCellItem.toolbarParam.items.push(self.toolbarSearchText(layoutCellId, itemInfo));
                            break;
                        case "select":
                            layoutCellItemOrTabCellItem.toolbarParam.items.push(self.toolbarSearchSelect(layoutCellId, itemInfo));
                            layoutCellItemOrTabCellItem.toolbarSearchers.selects.push(itemInfo['name']);
                            break;
                        case "date":
                            layoutCellItemOrTabCellItem.toolbarParam.items.push(self.toolbarSearchDate(layoutCellId, itemInfo));
                            layoutCellItemOrTabCellItem.toolbarSearchers.dates.push(itemInfo["name"]);
                            break;
                    }
                    break;
            }
        });

    };
    BaseLayout.prototype.renderGridColumns = function (layoutCellId, tabCellId) {
        var self = this;
        var layoutCellItemOrTabCellItem;
        if (tabCellId) {
            layoutCellItemOrTabCellItem = self.params.cells[layoutCellId].tabs[tabCellId];
        } else {
            layoutCellItemOrTabCellItem = self.params.cells[layoutCellId];
        }
        var columns = layoutCellItemOrTabCellItem.gridParam.columns;
        $.each(columns, function (columnIndex, columnInfo) {
            if (columnInfo["renderItems"]) {
                columnInfo["render"] = function (rowData, value, colInfo, jqContent) {
                    var jqDiv = $("<div></div>");
                    var primaryKeyField = layoutCellItemOrTabCellItem.winParam.edit.primaryKeyField;
                    var primaryKeyValue = rowData[primaryKeyField];
                    $.each(columnInfo["renderItems"], function (itemIndex, itemInfo) {
                        switch (itemIndex) {
                            case "del":
                                var jqDel = $("<span></span>").addClass("hci-icon hci-icon-del").attr({title: "删除"}).on("click", function () {
                                    var defaultDelParam = {
                                        url: '',
                                        enableDel: true,
                                        confirmMsg: '是否删除当前数据?'
                                    };
                                    itemInfo = $.extend(true, {}, defaultDelParam, itemInfo);
                                    var enableDel = true;
                                    if (typeof (itemInfo["enableDel"]) == "function") {
                                        enableDel = itemInfo["enableDel"](rowData, value, colInfo, jqContent);
                                    } else {
                                        enableDel = itemInfo["enableDel"];
                                    }
                                    if (enableDel) {
                                        common.confirm({
                                            text: itemInfo["confirmMsg"],
                                            callback: function (isOk) {
                                                if (isOk) {
                                                    var url = itemInfo["url"];
                                                    var urlParam = {};
                                                    urlParam[primaryKeyField] = primaryKeyValue;
                                                    // $.each(urlParam, function (key, value) {
                                                    //     urlParam[key] = encodeURIComponent(value);
                                                    // });

                                                    var req = new Request(url, urlParam);
                                                    req.del({
                                                        isSuccessTip: true,
                                                        success: function (data) {
                                                            if (data.code == 0) {
                                                                layoutCellItemOrTabCellItem.gridObj.loadData();
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        });
                                    }
                                });
                                jqDiv.append(jqDel);
                                break;
                            case "edit":
                                var jqEdit = $("<span></span>").addClass("hci-icon hci-icon-edit").attr({title: '编辑'}).on("click", function () {
                                    layoutCellItemOrTabCellItem.winParam.edit = $.extend(true, {enableEdit: true}, layoutCellItemOrTabCellItem.winParam.all, layoutCellItemOrTabCellItem.winParam.edit);

                                    var enableEdit = true;
                                    if (typeof (layoutCellItemOrTabCellItem.winParam.edit["enableEdit"]) == "function") {
                                        enableEdit = layoutCellItemOrTabCellItem.winParam.edit["enableEdit"](rowData, value, colInfo, jqContent);
                                    } else {
                                        enableEdit = layoutCellItemOrTabCellItem.winParam.edit["enableEdit"];
                                    }
                                    if (!enableEdit) {
                                        return;
                                    }

                                    var href = layoutCellItemOrTabCellItem.winParam.edit.href + "?";
                                    var keys = [];
                                    var stateField = layoutCellItemOrTabCellItem.winParam.edit.stateField;
                                    var updateState = layoutCellItemOrTabCellItem.winParam.edit.updateState;
                                    keys.push(stateField + "=" + updateState);
                                    if (typeof (layoutCellItemOrTabCellItem.toolbarParam.editParam.urlParam) == "function") {
                                        var urlParam = layoutCellItemOrTabCellItem.toolbarParam.editParam.urlParam();
                                        $.each(urlParam, function (key, value) {
                                            keys.push(key + "=" + encodeURIComponent(value));
                                        });
                                    }
                                    keys.push(primaryKeyField + "=" + primaryKeyValue);
                                    href += keys.join("&");
                                    var titleKey = layoutCellItemOrTabCellItem.winParam.edit.titleKey || "";
                                    if (titleKey.length > 0) {
                                        titleKey = "---" + rowData[titleKey];
                                    }
                                    var dialogParam = {
                                        title: layoutCellItemOrTabCellItem.winParam.edit.title + titleKey,
                                        content: GLOBAL_WEB_BASE_PATH + "/" + href,
                                        area: layoutCellItemOrTabCellItem.winParam.edit.area,
                                        btn: layoutCellItemOrTabCellItem.winParam.edit["btn"] || ['保存', '关闭']
                                    };
                                    if (!layoutCellItemOrTabCellItem.winParam.edit["btn"]) {
                                        dialogParam["btn1"] = function (dialogIndex, dialogDom) {
                                            var curBtnText = $(dialogDom).find(".layui-layer-btn0").text();
                                            if (curBtnText == "保存") {
                                                var top = common.getTopWindowDom();
                                                top[layoutCellItemOrTabCellItem.winParam.winCallback](dialogIndex, function () {
                                                    $(dialogDom).find(".layui-layer-btn0").text("保存中...");
                                                }, function (data) {
                                                    setTimeout(function () {
                                                        $(dialogDom).find(".layui-layer-btn0").text("保存");
                                                    }, 900);
                                                    if (data.code == 0) {
                                                        setTimeout(function () {
                                                            top.layer.close(dialogIndex);
                                                        }, 1000);
                                                        layoutCellItemOrTabCellItem.gridObj.loadData();
                                                    }
                                                });
                                            }
                                        }
                                    }
                                    common.dialog(dialogParam);
                                });
                                jqDiv.append(jqEdit);
                                break;
                            case "btnSwitch":
                                var defaultBtnSwitchParam = {
                                    url: "",
                                    onValue: 1,
                                    offValue: 0,
                                    disable: false,
                                    confirmMsg: '是否修改当前状态?',
                                    customFn: null
                                };
                                itemInfo = $.extend(true, {}, defaultBtnSwitchParam, itemInfo);
                                var switchState = value == itemInfo["onValue"] ? "btn-switch-on" : "btn-switch-off";
                                var jqBtnSwitch = $('<a class="btn-switch-outer">' +
                                    '<span class="btn-switch ' + switchState + '">' +
                                    '<b class="btn-switch-inner"></b>' +
                                    '</span>' +
                                    '</a>');
                                if (!itemInfo["disable"]) {
                                    jqBtnSwitch.on("click", function () {
                                        common.confirm({
                                            text: itemInfo["confirmMsg"],
                                            callback: function (result) {
                                                if (result) {
                                                    var newData = {};
                                                    var primaryKey = layoutCellItemOrTabCellItem.gridParam.primaryKey;
                                                    newData = rowData;
                                                    if (typeof (itemInfo["customFn"]) == "function") {
                                                        itemInfo["customFn"](newData);
                                                    } else {
                                                        var colName = colInfo.name;
                                                        newData[colName] = value == itemInfo["onValue"] ? itemInfo["offValue"] : itemInfo["onValue"];
                                                        var req = new Request(itemInfo["url"], newData);
                                                        req.put({
                                                            isSuccessTip: true,
                                                            success: function (data) {
                                                                if (data.code == "0") {
                                                                    layoutCellItemOrTabCellItem.gridObj.loadData();
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        })
                                    });
                                } else {
                                    jqBtnSwitch.addClass("btn-disable");
                                }
                                jqDiv.append(jqBtnSwitch);
                                break;
                            default:
                                var defCustomIconParam = {
                                    clazz: '',
                                    onClick: null,
                                    title: '',
                                    winCallback: "",
                                    href: '',
                                    area: ['100%', '100%'],
                                    reqInfoKey: 'id',
                                    zIndex: 9999
                                };
                                var itemInfo = $.extend(true, {}, defCustomIconParam, itemInfo);
                                if (typeof (itemInfo["clazz"]) == "function") {
                                    itemInfo["clazz"] = itemInfo["clazz"](rowData);
                                }
                                if (typeof (itemInfo["title"]) == "function") {
                                    itemInfo["title"] = itemInfo["title"](rowData);
                                }
                                var jqIcon = $("<span></span>").attr({title: itemInfo["title"]}).addClass(itemInfo["clazz"]).on("click", function () {
                                    if (typeof (itemInfo["onClick"]) == "function") {
                                        itemInfo.onClick(rowData, value, colInfo, jqContent);
                                    } else {
                                        var href = typeof (itemInfo["href"]) == "function" ? itemInfo["href"](rowData) : itemInfo["href"];
                                        var dialogParam = {
                                            title: itemInfo["title"],
                                            content: GLOBAL_WEB_BASE_PATH + "/" + href,
                                            area: itemInfo["area"],
                                            btn: ['保存', '关闭'],
                                            zIndex: itemInfo["zIndex"],
                                            btn1: function (dialogIndex, dialogDom) {
                                                var curBtnText = $(dialogDom).find(".layui-layer-btn0").text();
                                                if (curBtnText == "保存") {
                                                    var top = common.getTopWindowDom();
                                                    top[itemInfo["winCallback"]](dialogIndex, function () {
                                                        $(dialogDom).find(".layui-layer-btn0").text("保存中...");
                                                    }, function (data) {
                                                        setTimeout(function () {
                                                            $(dialogDom).find(".layui-layer-btn0").text("保存");
                                                        }, 900);
                                                        if (data.code == 0) {
                                                            setTimeout(function () {
                                                                top.layer.close(dialogIndex);
                                                            }, 1000);
                                                            layoutCellItemOrTabCellItem.gridObj.loadData();
                                                        }
                                                    });
                                                }
                                            }
                                        };
                                        common.dialog(dialogParam);
                                    }
                                });
                                jqDiv.append(jqIcon);
                                break;
                        }
                    });
                    return jqDiv;
                };
                layoutCellItemOrTabCellItem.gridParam.columns[columnIndex] = columnInfo;
            } else {
                layoutCellItemOrTabCellItem.gridParam.columns[columnIndex] = columnInfo;
            }
        })
    };
    BaseLayout.prototype.onDataViewDblClick = function (layoutCellId, id) {
        var self = this;
        var primaryKeyField = self.params.cells[layoutCellId].winParam.edit.primaryKeyField;
        var itemData = self.params.cells[layoutCellId].dataViewObj.get(id);
        var primaryKeyValue = itemData[primaryKeyField];
        self.params.cells[layoutCellId].winParam.edit = $.extend(true, {}, self.params.cells[layoutCellId].winParam.all, self.params.cells[layoutCellId].winParam.edit);

        var enableEdit = true;
        if (typeof (self.params.cells[layoutCellId].winParam.edit["enableEdit"]) == "function") {
            enableEdit = self.params.cells[layoutCellId].winParam.edit["enableEdit"](rowData, value, colInfo, jqContent);
        } else {
            enableEdit = self.params.cells[layoutCellId].winParam.edit["enableEdit"];
        }
        if (!enableEdit) {
            return;
        }

        var href = self.params.cells[layoutCellId].winParam.edit.href + "?";
        var keys = [];
        var stateField = self.params.cells[layoutCellId].winParam.edit.stateField;
        var updateState = self.params.cells[layoutCellId].winParam.edit.updateState;
        keys.push(stateField + "=" + updateState);
        if (typeof (self.params.cells[layoutCellId].toolbarParam.editParam.urlParam) == "function") {
            var urlParam = self.params.cells[layoutCellId].toolbarParam.editParam.urlParam();
            $.each(urlParam, function (key, value) {
                keys.push(key + "=" + encodeURIComponent(value));
            });
        }
        keys.push(primaryKeyField + "=" + primaryKeyValue);
        href += keys.join("&");
        var titleKey = self.params.cells[layoutCellId].winParam.edit.titleKey || "";
        if (titleKey.length > 0) {
            titleKey = "---" + itemData[titleKey];
        }
        var dialogParam = {
            title: self.params.cells[layoutCellId].winParam.edit.title + titleKey,
            content: GLOBAL_WEB_BASE_PATH + "/" + href,
            area: self.params.cells[layoutCellId].winParam.edit.area,
            showButton: self.params.cells[layoutCellId].winParam.edit.showButton,
            btn: self.params.cells[layoutCellId].winParam.edit.showButton ? ['保存', '关闭'] : undefined,
            // btn: ['保存', '关闭'],
            btn1: function (dialogIndex, dialogDom) {
                var curBtnText = $(dialogDom).find(".layui-layer-btn0").text();
                if (curBtnText == "保存") {

                    var top = common.getTopWindowDom();
                    top[self.params.cells[layoutCellId].winParam.winCallback](dialogIndex, function () {
                        $(dialogDom).find(".layui-layer-btn0").text("保存中...");
                    }, function (data) {
                        setTimeout(function () {
                            $(dialogDom).find(".layui-layer-btn0").text("保存");
                        }, 900);
                        if (data.code == 0) {
                            setTimeout(function () {
                                top.layer.close(dialogIndex);
                            }, 1000);
                            self.dataViewLoadData(layoutCellId);
                        }
                    });
                }
            }
        };
        common.dialog(dialogParam);


    };
    BaseLayout.prototype.toolbarBtnAdd = function (layoutCellId, tabCellIndex) {
        var self = this;
        var item = {
            id: 'btnAdd',
            type: 'button',
            img: 'add.png',
            title: '新增',
            onClick: function () {
                var enable = true;
                if (typeof (self.params.cells[layoutCellId].toolbarParam.addParam.enableAdd) == "function") {
                    enable = self.params.cells[layoutCellId].toolbarParam.addParam.enableAdd();
                }
                if (enable) {
                    self.params.cells[layoutCellId].winParam.add = $.extend(true, {}, self.params.cells[layoutCellId].winParam.all, self.params.cells[layoutCellId].winParam.add);

                    if (typeof (self.params.cells[layoutCellId].winParam.add.beforeShow) == "function") {
                        if (!self.params.cells[layoutCellId].winParam.add.beforeShow())
                            return
                    }

                    var href = self.params.cells[layoutCellId].winParam.add.href;
                    if (typeof (self.params.cells[layoutCellId].toolbarParam.addParam.urlParam) == "function") {
                        var urlParam = self.params.cells[layoutCellId].toolbarParam.addParam.urlParam();
                        href += "?";
                        var keys = [];
                        $.each(urlParam, function (key, value) {
                            keys.push(key + "=" + encodeURIComponent(value));
                        });
                        href += keys.join("&");
                    }
                    common.dialog({
                        title: self.params.cells[layoutCellId].winParam.add.title,
                        content: GLOBAL_WEB_BASE_PATH + "/" + href,
                        area: self.params.cells[layoutCellId].winParam.add.area,
                        btn: ['保存', '关闭'],
                        btn1: function (dialogIndex, dialogDom) {
                            var curBtnText = $(dialogDom).find(".layui-layer-btn0").text();
                            if (curBtnText == "保存") {
                                var top = common.getTopWindowDom();
                                top[self.params.cells[layoutCellId].winParam.winCallback](dialogIndex, function () {
                                    $(dialogDom).find(".layui-layer-btn0").text("保存中...");
                                }, function (data) {
                                    setTimeout(function () {
                                        $(dialogDom).find(".layui-layer-btn0").text("保存");
                                    }, 900);
                                    if (data.code == 0) {
                                        setTimeout(function () {
                                            top.layer.close(dialogIndex);
                                        }, 1000);
                                        var attachType = self.params.cells[layoutCellId].attachType;
                                        switch (attachType) {
                                            case "grid":
                                                self.params.cells[layoutCellId].gridObj.loadData();
                                                break;
                                            case "list":
                                                self.listLoadData(layoutCellId);
                                                break;
                                            case "tree":
                                                self.treeLoadData(layoutCellId);
                                                break;
                                            case "dataView":
                                                self.dataViewLoadData(layoutCellId);
                                                break;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        };
        if (self.params.cells[layoutCellId].toolbarParam.showText) {
            var title = self.params.cells[layoutCellId].toolbarParam.addParam.title;
            item["text"] = title ? title : '新增';
        }
        return item;
    };
    BaseLayout.prototype.toolbarBtnDel = function (layoutCellId, tabCellIndex) {
        var self = this;
        var item = {
            id: 'btnDel',
            type: 'button',
            img: 'del.png',
            title: '删除',
            onClick: function () {
                var enable = true;
                if (typeof (self.params.cells[layoutCellId].toolbarParam.delParam.enableDel) == "function") {
                    enable = self.params.cells[layoutCellId].toolbarParam.delParam.enableDel();
                }
                if (enable) {
                    var attachType = self.params.cells[layoutCellId].attachType;
                    switch (attachType) {
                        case "grid":
                            var ids = self.params.cells[layoutCellId].gridObj.getCheckedPrimaryKeyValues(",");
                            if (ids.length > 0) {
                                common.confirm({
                                    text: self.params.cells[layoutCellId].toolbarParam.delParam.confirmMsg, //"是否删除当前勾选的数据?",
                                    callback: function (isOk) {
                                        if (isOk) {
                                            var url = self.params.cells[layoutCellId].toolbarParam.delParam.ajax.url;
                                            var req = new Request(url);
                                            req.del({
                                                isSuccessTip: true,
                                                data: ids,
                                                success: function (data) {
                                                    if (data.code == 0) {
                                                        self.params.cells[layoutCellId].gridObj.loadData();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                })
                            } else {
                                common.errorMsg("请勾选要删除的数据！");
                            }
                            break;
                        case "list":
                            var ids = self.params.cells[layoutCellId].listObj.getSelected();
                            if (ids.length > 0) {
                                common.confirm({
                                    text: self.params.cells[layoutCellId].toolbarParam.delParam.confirmMsg,
                                    callback: function (isOk) {
                                        if (isOk) {
                                            var url = self.params.cells[layoutCellId].toolbarParam.delParam.ajax.url;
                                            var req = new Request(url);
                                            req.del({
                                                data: ids,
                                                isSuccessTip: true,
                                                success: function (data) {
                                                    if (data.code == 0) {
                                                        self.listLoadData(layoutCellId);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                })
                            } else {
                                common.errorMsg("请选择要删除的数据！");
                            }
                            break;
                        case "tree":
                            var nodeData = self.params.cells[layoutCellId].treeObj.getSelectedNodes();
                            if (nodeData.length > 0) {
                                common.confirm({
                                    text: self.params.cells[layoutCellId].toolbarParam.delParam.confirmMsg,
                                    callback: function (isOk) {
                                        if (isOk) {
                                            var url = self.params.cells[layoutCellId].toolbarParam.delParam.ajax.url;
                                            var req = new Request(url);
                                            req.del({
                                                data: nodeData[0]["id"],
                                                isSuccessTip: true,
                                                success: function (data) {
                                                    if (data.code == 0) {
                                                        self.treeLoadData(layoutCellId);
                                                    }
                                                }
                                            })
                                        }
                                    }
                                })
                            } else {
                                common.errorMsg("请勾选要删除的树节点！")
                            }
                            break;
                        case "dataView":
                            var ids = self.params.cells[layoutCellId].dataViewObj.getSelected();
                            if (ids.length > 0) {
                                common.confirm({
                                    text: "是否删除当前选中的数据？",
                                    callback: function (isOk) {
                                        if (isOk) {
                                            var url = self.params.cells[layoutCellId].toolbarParam.delParam.ajax.url;
                                            var req = new Request(url);
                                            req.del({
                                                data: ids,
                                                isSuccessTip: true,
                                                success: function (data) {
                                                    if (data.code == 0) {
                                                        self.dataViewLoadData(layoutCellId);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                })
                            } else {
                                common.errorMsg("请选择要删除的数据！");
                            }
                            break;

                    }
                }
            }
        };
        if (self.params.cells[layoutCellId].toolbarParam.showText) {
            var title = self.params.cells[layoutCellId].toolbarParam.delParam.title;
            item["text"] = title ? title : '删除';
        }
        return item;
    };
    BaseLayout.prototype.toolbarBtnEdit = function (layoutCellId, tabCellIndex) {
        var self = this;
        var item = {
            id: 'btnEdit',
            type: 'button',
            img: 'edit.png',
            title: '编辑',
            onClick: function () {
                var enable = true;
                if (typeof (self.params.cells[layoutCellId].toolbarParam.editParam.enableEdit) == "function") {
                    enable = self.params.cells[layoutCellId].toolbarParam.editParam.enableEdit();
                }
                if (enable) {
                    self.params.cells[layoutCellId].winParam.edit = $.extend(true, {}, self.params.cells[layoutCellId].winParam.all, self.params.cells[layoutCellId].winParam.edit);

                    if (typeof (self.params.cells[layoutCellId].winParam.edit.beforeShow) == "function") {
                        if (!self.params.cells[layoutCellId].winParam.edit.beforeShow())
                            return
                    }

                    var href = self.params.cells[layoutCellId].winParam.edit.href + "?";
                    var keys = [];
                    var stateField = self.params.cells[layoutCellId].winParam.edit.stateField;
                    var updateState = self.params.cells[layoutCellId].winParam.edit.updateState;
                    keys.push(stateField + "=" + updateState);
                    if (typeof (self.params.cells[layoutCellId].toolbarParam.editParam.urlParam) == "function") {
                        var urlParam = self.params.cells[layoutCellId].toolbarParam.editParam.urlParam();
                        $.each(urlParam, function (key, value) {
                            keys.push(key + "=" + encodeURIComponent(value));
                        });
                    }
                    var attachType = self.params.cells[layoutCellId].attachType;
                    var primaryKeyField = self.params.cells[layoutCellId].winParam.edit.primaryKeyField;
                    var data = null;
                    var callbackJSON = {};
                    switch (attachType) {
                        case "grid":
                            data = self.params.cells[layoutCellId].gridObj.getSelectedRow();
                            if (!data) {
                                common.errorMsg("请选择要编辑的数据!");
                                return;
                            }
                            var primaryKeyValue = data[primaryKeyField];
                            keys.push(primaryKeyField + "=" + primaryKeyValue);
                            href += keys.join("&");
                            callbackJSON["grid"] = function () {
                                self.params.cells[layoutCellId].gridObj.loadData();
                            };
                            break;
                        case "tree":
                            var nodeData = self.params.cells[layoutCellId].treeObj.getSelectedNodes();
                            var primaryKeyValue = "";
                            if (nodeData.length == 0) {
                                common.errorMsg("请选择要编辑的数据！")
                            } else if (nodeData.length == 1) {
                                data = nodeData[0];
                                primaryKeyValue = data[primaryKeyField];
                                keys.push(primaryKeyField + "=" + primaryKeyValue);
                                href += keys.join("&");
                            }
                            callbackJSON["tree"] = function () {
                                self.treeLoadData(layoutCellId, primaryKeyValue, primaryKeyField);
                            };
                            break;
                        case "list":
                            var id = self.params.cells[layoutCellId].listObj.getSelected();
                            data = self.params.cells[layoutCellId].listObj.get(id);
                            var primaryKeyValue = data[primaryKeyField];
                            keys.push(primaryKeyField + "=" + primaryKeyValue);
                            href += keys.join("&");
                            callbackJSON["list"] = function () {
                                self.listLoadData(layoutCellId, id);
                            };
                            break;
                        case "dataView":
                            var id = self.params.cells[layoutCellId].dataViewObj.getSelected();
                            data = self.params.cells[layoutCellId].dataViewObj.get(id);
                            var primaryKeyValue = data[primaryKeyField];
                            keys.push(primaryKeyField + "=" + primaryKeyValue);
                            href += keys.join('&');
                            callbackJSON["dataView"] = function () {
                                self.dataViewLoadData(layoutCellId, id);
                            };
                            break;
                    }

                    var titleKey = self.params.cells[layoutCellId].winParam.edit.titleKey || "";
                    if (titleKey.length > 0) {
                        if (data) {
                            titleKey = "---" + data[titleKey];
                        }
                    }
                    common.dialog({
                        title: self.params.cells[layoutCellId].winParam.edit.title + titleKey,
                        area: self.params.cells[layoutCellId].winParam.edit.area,
                        content: GLOBAL_WEB_BASE_PATH + '/' + href,
                        btn: ['保存', '关闭'],
                        btn1: function (dialogIndex, dialogDom) {
                            var curBtnText = $(dialogDom).find(".layui-layer-btn0").text();
                            if (curBtnText == "保存") {
                                var top = common.getTopWindowDom();
                                top[self.params.cells[layoutCellId].winParam.winCallback](dialogIndex, function () {
                                    $(dialogDom).find(".layui-layer-btn0").text("保存中...");
                                }, function (data) {
                                    setTimeout(function () {
                                        $(dialogDom).find(".layui-layer-btn0").text("保存");
                                    }, 900);
                                    if (data.code == 0) {
                                        setTimeout(function () {
                                            top.layer.close(dialogIndex);
                                        }, 1000);
                                        callbackJSON[attachType](data)
                                    }
                                });
                            }
                        }
                    });
                }
            }
        };
        if (self.params.cells[layoutCellId].toolbarParam.showText) {
            var title = self.params.cells[layoutCellId].toolbarParam.editParam.title;
            item["text"] = title ? title : '编辑';
        }
        return item;
    };
    BaseLayout.prototype.toolbarBtnSeparator = function () {
        return {type: 'separator'};
    };
    BaseLayout.prototype.toolbarSearchText = function (layoutCellId, info) {

        var self = this;
        var item = {
            id: info["name"],
            type: 'buttonInput',
            width: info["width"],
            onEnter: function (id, value) {
                var attachType = self.params.cells[layoutCellId].attachType;
                switch (attachType) {
                    case "grid":
                        self.params.cells[layoutCellId].gridObj.initLoadData();
                        break;
                    case "tree":
                        break;
                    case "list":
                        break;
                    case "dataView":
                        self.dataViewLoadData(layoutCellId);
                        break;
                }
            }
        };
        if (typeof (info["onEnter"]) == "function") {
            item["onEnter"] = info["onEnter"];
        }
        return item;
    };
    BaseLayout.prototype.toolbarSearchSelect = function (layoutCellId, info) {
        var item = {
            id: info["name"],
            type: 'buttonInput',
            width: info["width"]
        };
        return item;
    };
    BaseLayout.prototype.toolbarSearchDate = function (layoutCellId, info) {
        var item = {
            id: info["name"],
            type: 'buttonInput',
            width: info["width"]
        };
        return item;
    };
    BaseLayout.prototype.toolbarSearchBtn = function (layoutCellId) {
        var self = this;
        var item = {
            id: 'btnSearch',
            type: 'button',
            img: 'search.png',
            onClick: function () {
                var searchers = self.params.cells[layoutCellId].toolbarParam.searchers;
                if (searchers && searchers['btnSearchClick'] && typeof (searchers['btnSearchClick']) === "function") {
                    searchers['btnSearchClick'].call()
                } else {
                    var attachType = self.params.cells[layoutCellId].attachType;
                    switch (attachType) {
                        case "grid":
                            self.params.cells[layoutCellId].gridObj.initLoadData();
                            break;
                        case "tree":
                            break;
                        case "list":
                            self.listLoadData(layoutCellId, 0);
                            break;
                        case "dataView":
                            self.dataViewLoadData(layoutCellId);
                            break;
                    }
                }
            }
        };
        return item;
    };
    BaseLayout.prototype.gridInit = function (layoutCellId, tabCellId) {
        var self = this;
        var gridParam = null;
        if (tabCellId) {
            if (self.params.cells[layoutCellId].tabs[tabCellId].gridParam.dataAction == "server") {
                self.renderGridColumns(layoutCellId, tabCellId);
            } else {
                gridParam = self.params.cells[layoutCellId].tabs[tabCellId].gridParam;
            }
            self.params.cells[layoutCellId].tabs[tabCellId].gridObj = new HCIGrid(gridParam);
        } else {
            if (self.params.cells[layoutCellId].gridParam.dataAction == "server") {
                self.renderGridColumns(layoutCellId);
                gridParam = self.renderGridAjaxData(layoutCellId);
            } else {
                gridParam = self.params.cells[layoutCellId].gridParam;
            }
            self.params.cells[layoutCellId].gridObj = new HCIGrid(gridParam);
        }
    };
    BaseLayout.prototype.gridLoadData = function (layoutCellId) {
        var self = this;
        self.params.cells[layoutCellId].gridObj.loadData();
    };
    BaseLayout.prototype.renderGridAjaxData = function (layoutCellId) {
        var self = this;
        var gridParam = $.extend(true, {}, self.params.cells[layoutCellId].gridParam);
        if (gridParam.ajax) {
            gridParam.ajax.data = function () {
                var data = {};
                if (typeof (self.params.cells[layoutCellId].gridParam.ajax.data) == "function") {
                    data = self.params.cells[layoutCellId].gridParam.ajax.data() || {};
                } else {
                    data = self.params.cells[layoutCellId].gridParam.ajax.data || {};
                }
                //工具条过滤
                var searcherKeys = [];
                var searchers = self.params.cells[layoutCellId].toolbarParam.searchers;
                $.each(searchers, function (searcherIndex) {
                    if (searcherIndex != "btnSearch") {
                        searcherKeys.push(searcherIndex);
                    }
                });
                $.each(searcherKeys, function (keyIndex, keyInfo) {
                    var searcherInfo = self.params.cells[layoutCellId].toolbarParam.searchers[keyInfo];
                    switch (searcherInfo["type"]) {
                        case "text":
                        case "date":
                            var searchValue = self.params.cells[layoutCellId].toolbarObj.getValue(keyInfo);
                            searchValue = $.trim(searchValue);
                            data[keyInfo] = searchValue;
                            break;
                        case "select":
                            // var selId = keyInfo["id"];
                            // alert(selId);
                            var selId = keyInfo;
                            data[keyInfo] = self.params.cells[layoutCellId][selId].getSelectedValue();
                            break;
                    }
                });
                if (gridParam.param && gridParam.param.paramType && gridParam.param.paramType === 1) {
                    var paramObj = {};
                    paramObj[gridParam.param.paramName || "condition"] = JSON.stringify(data);
                    return paramObj;
                }
                return data;
            };
        }

        return gridParam;
    };
    BaseLayout.prototype.treeInit = function (layoutCellId, selectValue, selectField) {
        var self = this;
        var url = self.params.cells[layoutCellId].treeParam.ajax.url;

        self.params.cells[layoutCellId].treeParam.ajax["success"] = function (data) {
            var zNodes = self.params.cells[layoutCellId].treeParam.renderData(data);
            var jqTarget = $("#" + self.params.cells[layoutCellId].treeId);
            var zTreeParam = $.extend(true, {}, self.params.cells[layoutCellId].treeParam.zTree);
            //重写点击选择和点击取消选中
            zTreeParam.callback.beforeClick = function (treeId, treeNode) {
                if (typeof (self.params.cells[layoutCellId].treeParam.onClick) == "function") {
                    var treeObj = $.fn.zTree.getZTreeObj(treeId);
                    var nodes = treeObj.getSelectedNodes();
                    if (nodes && nodes.length > 0 && nodes[0]["id"] == treeNode["id"]) {
                        treeObj.cancelSelectedNode(treeNode);
                        self.params.cells[layoutCellId].treeParam.onClick(false, treeNode, treeId);
                        return false;
                    } else {
                        treeObj.selectNode(treeNode);
                        self.params.cells[layoutCellId].treeParam.onClick(true, treeNode, treeId);
                        return false;
                    }
                }
            };
            self.params.cells[layoutCellId].treeObj = $.fn.zTree.init(jqTarget, zTreeParam, zNodes);
            self.params.cells[layoutCellId].treeObj.expandAll(true);
            if (selectValue) {
                var allNodes = self.params.cells[layoutCellId].treeObj.transformToArray(self.params.cells[layoutCellId].treeObj.getNodes());
                $.each(allNodes, function (idx, item) {
                    if (item[selectField] == selectValue) {
                        var treeNode = allNodes[idx];
                        self.params.cells[layoutCellId].treeObj.selectNode(treeNode, false, true);
                        var treeId = self.params.cells[layoutCellId].treeId;
                        self.params.cells[layoutCellId].treeParam.onClick(true, treeNode, treeId);
                    }
                })
            } else {
                var selectIndex = self.params.cells[layoutCellId].treeParam["selectIndex"];
                if (selectIndex > -1) {
                    var allNodes = self.params.cells[layoutCellId].treeObj.transformToArray(self.params.cells[layoutCellId].treeObj.getNodes());
                    if (allNodes.length > 0) {
                        var treeNode = allNodes[selectIndex];
                        self.params.cells[layoutCellId].treeObj.selectNode(treeNode, false, true);
                        var treeId = self.params.cells[layoutCellId].treeId;
                        self.params.cells[layoutCellId].treeParam.onClick(true, treeNode, treeId);
                    }
                }
            }
        };
        var req = new Request(url);
        delete self.params.cells[layoutCellId].treeParam.ajax.url;
        req.get(self.params.cells[layoutCellId].treeParam.ajax);
    };
    BaseLayout.prototype.treeLoadData = function (layoutCellId, primaryKeyValue, primaryKeyField) {
        var self = this;
        var treeId = self.params.cells[layoutCellId].attachId;
        $.fn.zTree.destroy(treeId);
        self.treeInit(layoutCellId, primaryKeyValue, primaryKeyField);
    };
    BaseLayout.prototype.listInit = function (layoutCellId, tabCellIndex) {
        var self = this;
        self.params.cells[layoutCellId].listParam.container = self.params.cells[layoutCellId].attachId;
        self.params.cells[layoutCellId].listObj = new dhtmlXList(self.params.cells[layoutCellId].listParam);
        self.listLoadData(layoutCellId);
    };
    BaseLayout.prototype.listLoadData = function (layoutCellId, selectValue) {
        var self = this;
        var url = self.params.cells[layoutCellId].listParam.ajax.url;

        var data = {};
        if (typeof (self.params.cells[layoutCellId].listParam.ajax.data) == "function") {
            data = self.params.cells[layoutCellId].listParam.ajax.data() || {};
        } else {
            data = self.params.cells[layoutCellId].listParam.ajax.data || {};
        }

        var reqList = new Request(url);
        reqList.get({
            data: data,
            success: function (data) {
                self.params.cells[layoutCellId].listObj.clearAll();
                if (typeof (self.params.cells[layoutCellId].listParam.onRenderData) == "function") {
                    data = self.params.cells[layoutCellId].listParam.onRenderData(data);
                }
                self.params.cells[layoutCellId].listObj.parse(data, 'json');
                self.params.cells[layoutCellId].listObj.attachEvent("onAfterSelect", function (id) {
                    if (typeof (self.params.cells[layoutCellId].listParam.onAfterSelect) == "function") {
                        self.params.cells[layoutCellId].listParam.onAfterSelect(id);
                    }
                });
                if (selectValue) {
                    self.params.cells[layoutCellId].listObj.select(selectValue);
                } else {
                    var selectIndex = self.params.cells[layoutCellId].listParam.selectIndex;
                    var id = self.params.cells[layoutCellId].listObj.idByIndex(selectIndex);
                    self.params.cells[layoutCellId].listObj.select(id);
                }
            }
        });
    };
    BaseLayout.prototype.dataViewInit = function (layoutCellId, tabCellIndex) {
        var self = this;
        self.params.cells[layoutCellId].dataViewObj = new dhtmlXDataView(self.params.cells[layoutCellId].attachId);
        var ftype = self.params.cells[layoutCellId].dataViewParam.ftype || "ficon";
        dhtmlXDataView.prototype.types[ftype].icons_src_dir = GLOBAL_WEB_BASE_PATH + "/image/icons/file";
        self.params.cells[layoutCellId].dataViewObj.define("drag", true);

        self.params.cells[layoutCellId].dataViewObj.define("type", ftype);
        self.dataViewLoadData(layoutCellId);
        self.params.cells[layoutCellId].dataViewObj.attachEvent("onAfterSelect", function (id) {
            if (typeof (self.params.cells[layoutCellId].dataViewParam.onAfterSelect) == "function") {
                self.params.cells[layoutCellId].dataViewParam.onAfterSelect(id);
            }
        });
        self.params.cells[layoutCellId].dataViewObj.attachEvent("onItemDblClick", function (id, ev, html) {
            if (typeof (self.params.cells[layoutCellId].dataViewParam.onDblClick) == "function") {
                self.params.cells[layoutCellId].dataViewParam.onDblClick(id);
            } else {
                self.onDataViewDblClick(layoutCellId, id);
            }
            return true;
        });
    };
    BaseLayout.prototype.dataViewLoadData = function (layoutCellId) {
        var self = this;
        self.params.cells[layoutCellId].dataViewObj.clearAll();
        if (self.params.cells[layoutCellId].dataViewParam.isInitLoadData && self.params.cells[layoutCellId].dataViewParam["dataAction"] == "server") {
            self.dataViewLoadServiceData(layoutCellId);
        } else {
            var data = self.params.cells[layoutCellId].dataViewParam.data;
            self.dataViewLoadLocalData(layoutCellId, data);
        }
        self.params.cells[layoutCellId].dataViewParam.isInitLoadData = true;

        self.params.cells[layoutCellId].dataViewObj.unselectAll();
    };
    BaseLayout.prototype.dataViewLoadServiceData = function (layoutCellId) {
        var self = this;
        var url = self.params.cells[layoutCellId].dataViewParam.ajax.url;
        var reqList = new Request(url);
        reqList.get({
            data: function () {
                var data = {};
                if (typeof (self.params.cells[layoutCellId].dataViewParam.ajax.data) == "function") {
                    data = self.params.cells[layoutCellId].dataViewParam.ajax.data() || {};
                } else {
                    data = self.params.cells[layoutCellId].dataViewParam.ajax.data || {};
                }
                //工具条过滤
                var searcherKeys = [];
                var searchers = self.params.cells[layoutCellId].toolbarParam.searchers;
                $.each(searchers, function (searcherIndex) {
                    if (searcherIndex != "btnSearch") {
                        searcherKeys.push(searcherIndex);
                    }
                });
                $.each(searcherKeys, function (keyIndex, keyInfo) {
                    var searcherInfo = self.params.cells[layoutCellId].toolbarParam.searchers[keyInfo];
                    switch (searcherInfo["type"]) {
                        case "text":
                        case "date":
                            data[keyInfo] = self.params.cells[layoutCellId].toolbarObj.getValue(keyInfo);
                            break;
                        case "select":
                            break;
                    }
                });
                return data;
            },
            success: function (data) {
                if (typeof (self.params.cells[layoutCellId].dataViewParam.onRenderData) == "function") {
                    data = self.params.cells[layoutCellId].dataViewParam.onRenderData(data);
                }
                self.dataViewLoadLocalData(layoutCellId, data);
            }
        });
    };
    BaseLayout.prototype.dataViewLoadLocalData = function (layoutCellId, data) {
        var self = this;
        self.params.cells[layoutCellId].dataViewObj.clearAll();
        self.params.cells[layoutCellId].dataViewObj.parse(data, 'json');
    };
    return BaseLayout;
});