define(function () {
    var hciCore = {
        name: 'HCI插件',
        version: '1.0',
        description: 'hci ui 框架',
        path: GLOBAL_WEB_BASE_PATH,//根路径
        imgPath: GLOBAL_RESOURCE_PATH,
        lastId: 1,
        newId: function () {
            return this.lastId++;
        },
        randomId: function () {
            var num = Math.random();
            var timestamp = (new Date()).getTime();
            return timestamp + "" + num;
        },
        template: function (str, dataObj) {
            str = str.replace(/\{(\w+)\}/g, function (m, i) {
                if (dataObj[i] != undefined) {
                    return dataObj[i];
                } else {
                    return m;
                }
            });
            return str;
        },
        toNumber: function (num, format) {
            if (!format) {
                return num;
            } else {

            }
        },
        tree: {
            array2json: function (data, idKey, pIdKey, _pIdKey) {
                var newData = {};
                $.each(data, function (index, item) {
                    var itemData = $.extend(true, {}, item);
                    itemData[_pIdKey] = itemData[pIdKey];
                    newData[item[idKey]] = itemData;
                });
                return newData;
            },
            //统一顶级节点的父节点
            setFirstLayerPId: function (data, idKey, pIdKey, _pIdKey, rootPIdValue) {
                var newData = new Array();
                var jsonData = hciCore.tree.array2json(data, idKey, pIdKey, _pIdKey);
                $.each(data, function (index, item) {
                    //如果数据的父节点不存在说明是第一次层级
                    var itemData = $.extend(true, {}, item);
                    if (!jsonData[item[pIdKey]]) {
                        itemData[_pIdKey] = rootPIdValue || "___hci_first_layer_001";
                    } else {
                        itemData[_pIdKey] = itemData["pid"];
                    }
                    newData.push(itemData);
                });
                return newData;
            },
            getNextLayerChildren: function (data, idValue, _pIdKey) {
                var newData = new Array();
                for (var i in data) {
                    if (data[i][_pIdKey] == idValue) {
                        newData.push(data[i]);
                    }
                }
                return newData;
            }
        },
        array2group: function (data, name, display) {
            var newData = {};
            $.each(data, function (index, item) {
                if (!newData[item[name]]) {
                    newData[item[name]] = {
                        name: item[display],
                        list: []
                    };
                }
                newData[item[name]]["list"].push(item);
            });
            return newData;
        },
        //z-index 层级管理
        zim: {
            step: 5,
            data: {},
            //第一层级（最小层级）
            first: function () {
                return 100;
            },
            //最后一层极（最大层级）
            last: function () {
                var t = this.first();
                for (var a in this.data) {
                    t = Math.max(t, this.data[a]);
                }
                return t;
            },
            //存储
            reserve: function (id) {
                this.data[id] = this.last() + this.step;
                return this.data[id];
            },
            //清空
            clear: function (id) {
                if (this.data[id] != null) {
                    this.data[id] = null;
                    delete this.data[id];
                }
            }

        },
        container: function (containers) {
            this.c = {}; // the container storage
            // single elem id
            if (typeof (containers) == "string") {
                containers = [containers];
            }
            //single elem object
            if (typeof (containers) == "object" && !containers.length) {
                containers = [containers];
            }
            var index = 0;
            for (var q = 0; q < containers.length; q++) {
                if (typeof (containers[q]) == "string") {
                    containers[q] = ($("#" + containers[q]) || null);
                }
                if (containers[q] != null) {
                    this.c[index] = {hciId: hciCore.newId(), container: $(containers[q])};
                    index++;
                }
                containers[q] = null;
            }
        },
        //derection:v为竖向滚动条；h为横向滚动条；
        hasScrollBar: function (dom, direction) {
            if (dom) {
                if (direction === "v") {
                    return dom.scrollHeight > dom.clientHeight;
                } else if (direction === "h") {
                    return dom.scrollWidth > dom.clientWidth;
                }
            }
            return false;
        },
        //获取滚动条宽度
        getScrollBarWidth: function () {
            var jqDiv = $("<div></div>").css({width: '100px', height: '100px', overflowY: 'scroll'})
            $("body").append(jqDiv);
            var scrollbarWidth = jqDiv[0].offsetWidth - jqDiv[0].clientWidth;//相减
            jqDiv.remove();//移除创建的div
            return scrollbarWidth;//返回滚动条宽度
        },
        //调用方式：数组对象arrayObj.sort(arraySortFun('asc||desc','ws'));其中arrayObj为JSON的数组对象，ws为json中的字段
        arraySortFun: function (order, sortBy) {
            var ordAlpah = (order == "desc") ? '>' : '<';
            var sortFun = new Function('a', 'b', 'return a.' + sortBy + ordAlpah + 'b.' + sortBy + '?1:-1');
            return sortFun;
        },
        deepClone: function (obj) {
            var newObj = obj.constructor === Array ? [] : {};
            if (typeof obj !== 'object') {
                return
            } else {
                for (var i in obj) {
                    if (obj.hasOwnProperty(i)) {
                        newObj[i] = typeof obj[i] === 'object' ? hciCore.deepClone(obj[i]) : obj[i];
                    }
                }
            }
            return newObj
        }
    };
    return hciCore
});