/**
 *
 *  @author   ghh
 *  @version  1.0
 */
define(['hciCore'], function (hciCore) {
    var HCIEmpty = function (params) {
        var self = this;
        self.params = $.extend(true, {}, self.params, params);
        self.id = self.params.id || "hci-empty-" + hciCore.newId();
        var jqEmptyOuter = $("<div></div>").addClass("hci-empty-note").attr({id: self.id});
        var jqEmptyImage = $("<img  />").attr({src: self.params.imageUrl});
        var jqEmptyText = $("<p></p>").addClass("hci-empty-note-txt").text(self.params.text);
        jqEmptyOuter.append(jqEmptyImage).append(jqEmptyText);
        $(self.params.target).append(jqEmptyOuter);
    };
    HCIEmpty.prototype.params = {
        target: '',
        text: '无相关数据',
        imageUrl: ""
    };
    HCIEmpty.prototype.setText = function (text) {
        var self = this;
        $("#" + self.id).find(".hci-empty-note-txt").text(text);
    };
    HCIEmpty.prototype.hide = function () {
        var self = this;
        $("#" + self.id).hide();
    };
    HCIEmpty.prototype.show = function () {
        var self = this;
        $("#" + self.id).show();
    };
    return HCIEmpty;
});
