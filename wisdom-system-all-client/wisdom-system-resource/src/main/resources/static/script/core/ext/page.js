/**
 *  分页插件
 *  结合dhtmlx 插件进行使用
 *  @author   ghh
 *  @version  1.0
 */
define(['hciCore'], function (hciCore) {
    var HCIPage = function (params) {
        var self = this;
        self.params = $.extend(true, {}, self.params, params);
        hciCore.container.call(self, self.params.target);//继承插件容器
        self.init(self.c[0].container);
        // alert($(self.params.target).width());

    };
    HCIPage.prototype.params = {
        target: '',//创建分页的容器
        imgPath: window.GLOBAL_RESOURCE_PATH + "/lib/dhtmlx/imgs/dhxgrid_material/",
        page: 1,//当前页
        pageSize: 20,//每页20条
        pageSizeOptions: [10, 20, 30, 40, 50],//选择设置每页条数
        totalSize: 0,//总条数
        afterChangePage: null
    };
    //分页计算
    HCIPage.prototype._celPage = function () {
        var self = this;
        var page = self.params.page,
            pageSize = self.params.pageSize,
            totalSize = self.params.totalSize,
            totalPage = Math.ceil(totalSize / pageSize),//总页数
            fromNum = 0,//当前页从第{fromNum}条开始
            toNum = 0;//至 第{toNum} 条结束
        //总条数大于0
        if (totalSize > 0) {
            if (totalPage == page) {
                //最后一页
                fromNum = pageSize * (totalPage - 1) + 1;
                toNum = totalSize;
            } else {
                fromNum = (page - 1) * pageSize + 1;
                toNum = page * pageSize;
            }
        }
        self.params.totalPage = totalPage;
        self.params.fromNum = fromNum;
        self.params.toNum = toNum;
    };
    HCIPage.prototype.init = function (parent) {
        var self = this;
        var pageSizeOptions = [];
        var pageSizeJson = {};
        $.each(self.params.pageSizeOptions, function (index, item) {
            var optionsInfo = {
                id: 'page' + item,
                img: 'paging_page.gif',
                text: "每页" + item + "条",
                type: 'obj'
            }

            pageSizeOptions.push(optionsInfo);
            pageSizeJson['page' + item] = item;
        });
        self.pageToolBar = new dhtmlXToolbarObject({
            parent: parent[0],
            icon_path: self.params.imgPath,
            items: [
                {
                    id: "first",
                    type: "button",
                    img: 'ar_left_abs.gif',
                    imgdis: 'ar_left_abs_dis.gif',
                    toolTip: '第一页'
                },
                {
                    id: "pre",
                    type: "button",
                    img: "ar_left.gif",
                    imgdis: 'ar_left_dis.gif',
                    toolTip: '上一页'
                },
                {id: "labelPageInfo", type: "text", text: " 当前第1-20条 "},
                {id: 'labelTotalSize', type: "text", text: "共" + self.params.totalSize + "条"},
                {
                    id: "next",
                    type: "button",
                    img: "ar_right.gif",
                    imgdis: "ar_right_dis.gif",
                    toolTip: '下一页'
                },
                {
                    id: "last",
                    type: "button",
                    img: 'ar_right_abs.gif',
                    imgdis: 'ar_right_abs_dis.gif',
                    toolTip: '最后一页'
                },
                {type: "separator", id: "sep1"},
                {
                    id: "selectOption",
                    type: "buttonSelect",
                    text: "每页10条",
                    img: "paging_page.gif",
                    img_disabled: "paging_page.gif",
                    mode: "select",
                    options: pageSizeOptions
                },
                {id: "sep2", type: "separator"},
                {id: "label1", type: "text", text: "当前第"},
                {id: "inp", type: "buttonInput", text: "1", width: 25},
                {id: "label2", type: "text", text: "页"},
                {id: "labelTotalPage", type: "text", text: "共1页"}
            ]
        });
        self.pageToolBar.setListOptionSelected("selectOption", "page" + self.params.pageSize);
        self._setPageState();
        self.pageToolBar.attachEvent("onClick", function (id) {
            switch (id) {
                case "first":
                    self.params.page = 1;
                    break;
                case "pre":
                    self.params.page = self.params.page - 1;
                    break;
                case "next":
                    self.params.page = self.params.page + 1;
                    break;
                case "last":
                    self.params.page = self.params.totalPage;
                    break;
            }
            self._celPage();
            self._setPageState();
            if (typeof(self.params.afterChangePage) == "function") {
                self.params.afterChangePage();
            }
        });
        self.pageToolBar.getInput("inp").onkeyup = function () {
            var _this = this;
            var value = _this.value;
            if (!isNaN(value)) {
                if (value > self.params.totalPage) {
                    _this.value = self.params.totalPage;
                }
            } else {
                _this.value = 1;
            }
            self.params.page = _this.value;
            self._celPage();
            self._setPageState();
            if (typeof(self.params.afterChangePage) == "function") {
                self.params.afterChangePage();
            }
        };
        self.pageToolBar.attachEvent("onButtonSelectHide", function (id) {
            switch (id) {
                case "selectOption":
                    var optionId = self.pageToolBar.getListOptionSelected(id);
                    var value = pageSizeJson[optionId];
                    self.params.page = 1;
                    self.params.pageSize = value;
                    self._celPage();
                    self._setPageState();
                    break;
            }
            // if (typeof(self.params.afterChangePage) == "function") {
            //     self.params.afterChangePage();
            // }
        });


        var pageWidth = $("#" + self.params.target).width();
        if (pageWidth < 500) {
            self.pageToolBar.hideItem("first");
            self.pageToolBar.hideItem("pre");
            self.pageToolBar.hideItem("labelPageInfo");
            self.pageToolBar.hideItem("labelTotalSize");
            self.pageToolBar.hideItem("next");
            self.pageToolBar.hideItem("last");
            self.pageToolBar.hideItem("sep1");
        }
        // myToolbar.hideItem(itemId)
    };
    HCIPage.prototype._setPageState = function () {
        var self = this;
        self._celPage();
        if (self.params.totalSize > 0) {
            self.pageToolBar.setItemText("labelPageInfo", "当前第" + self.params.fromNum + "-" + self.params.toNum + "条");
            self.pageToolBar.setItemText("labelTotalSize", "共" + self.params.totalSize + "条");
            self.pageToolBar.showItem("labelTotalSize");

        } else {
            self.pageToolBar.setItemText("labelPageInfo", "当前无数据");
            self.pageToolBar.hideItem("labelTotalSize");
        }


        self.pageToolBar.getInput("inp").value = self.params.page;
        if (self.params.page <= 1) {
            self.pageToolBar.disableItem("first");
            self.pageToolBar.disableItem("pre");
        } else {
            self.pageToolBar.enableItem("first");
            self.pageToolBar.enableItem("pre");
        }

        if (self.params.page == self.params.totalPage) {
            self.pageToolBar.disableItem("next");
            self.pageToolBar.disableItem("last");
        } else {
            self.pageToolBar.enableItem("next");
            self.pageToolBar.enableItem("last");
        }
        self.pageToolBar.setItemText("labelTotalPage", "共" + self.params.totalPage + "页");


    }
    HCIPage.prototype.setCurPage = function (num) {
        var self = this;
        self.params.page = num;
        self._celPage();
        self._setPageState();
    };
    HCIPage.prototype.setTotalSize = function (totalSize) {
        var self = this;
        self.params.totalSize = totalSize;
        self._celPage();
        self._setPageState();
    };
    return HCIPage;
});

