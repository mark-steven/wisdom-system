define(['common'], function (common) {
    jQuery.support.cors = true;
    /**
     * Request 封装【ajax请求】
     **/
    var Request = function (ajaxUrl, urlParam, noAccessToken) {
        var self = this;
        var isAccessToken = false;
        if (!noAccessToken) {
            isAccessToken = true;
        }
        self.init(ajaxUrl, urlParam, isAccessToken);
    };

    Request.prototype.params = {
        urlPath: GLOBAL_API_PATH,
        isErrorTip: true,//是否提示错误消息
        isSuccessTip: false,//是否提示成功消息
        cache: false,
        timeout: 180000,
        isStringify: true,
        success: function (data) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    };
    Request.prototype.init = function (ajaxUrl, urlParam, isAccessToken) {
        var self = this;
        self.request = null;
        if (urlParam) {
            ajaxUrl = ajaxUrl.replace(/\{(\w+)\}/g, function (m, i) {
                return urlParam[i];
            })
        }
        self.params.url = self.params.urlPath + "/" + ajaxUrl;
        var accessToken = "";
        if (isAccessToken) {
            accessToken = "access_token=" + $.cookie("access_token");
        }
        if (self.params.url.indexOf("?") > 0) {
            self.params.url = self.params.url + "&" + accessToken;
        } else {
            self.params.url = self.params.url + "?" + accessToken;
        }
        self.run = false;
    };
    Request.prototype.ajax = function () {
        var self = this;
        if (arguments) {
            self.run = true;
            self.params = $.extend(true, {}, self.params, arguments[0]);
            if (self.params.data && typeof (self.params.data) == "function") {
                self.params.data = self.params.data();
            }
            self.params.data = self.params.data || {};
            var callbackObj = $.extend(true, {}, {success: self.params["success"], error: self.params["error"]});
            delete self.params["success"];
            self.params["success"] = function (data) {
                if (typeof (data) === "string") {
                    data = $.parseJSON(data);
                }
                var that = eval(self)
                //成功提醒由用户自定义
                if (data && data.code == "0" && self.params.isSuccessTip) {
                    common.successMsg(data.msg);
                } else if (data && data.code != "0" && self.params.isErrorTip) {
                    common.errorMsg(data.msg);
                }
                callbackObj.success(data)
            };
            delete self.params["error"];
            self.params["error"] = function (XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 401) {
                    var top = common.getTopWindowDom();
                    if (top.WIN_LOGIN_AGAIN == null) {
                        top.WIN_LOGIN_AGAIN = common.dialog({
                            title: "会话超时，请重新登录！",
                            type: 2,
                            closeBtn: 0,
                            area: ['330px', '250px'],
                            content: window.GLOBAL_WEB_BASE_PATH + "/login/again.html",
                            btn: ['登录', '退出'],
                            btn1: function (dialogIndex) {
                                var data = top.win_login_again_callback(dialogIndex);
                            },
                            btn2: function (dialogIndex) {
                                top.layer.close(dialogIndex);
                                top.WIN_LOGIN_AGAIN = null;
                                common.getTopWindowDom().window.location.href = window.GLOBAL_WEB_BASE_PATH + "/login.html"
                            }
                        })
                    }
                }
                callbackObj.error(XMLHttpRequest, textStatus, errorThrown)
            };
            self.request = $.ajax(self.params);
        }

    };
    Request.prototype.get = function () {
        var self = this;
        var data = {};
        self.params = $.extend(true, {}, self.params, arguments[0]);
        if (typeof (arguments[0]["data"]) == "function") {
            data = arguments[0]["data"]();
        } else {
            data = arguments[0]["data"];
        }
        arguments[0]["data"] = data;
        self.ajax($.extend(true, {type: "GET"}, arguments[0]));
    };
    Request.prototype.post = function () {
        var self = this;
        var data = {};
        self.params = $.extend(true, {}, self.params, arguments[0]);
        if (typeof (arguments[0]["data"]) == "function") {
            data = arguments[0]["data"]();
        } else {
            data = arguments[0]["data"];
        }
        if (self.params.isStringify) {
            arguments[0]["data"] = JSON.stringify(data);
        } else {
            arguments[0]["data"] = data;
        }
        self.ajax($.extend(true, {type: "POST", contentType: "application/json; charset=utf-8"}, arguments[0]));
    };
    Request.prototype.put = function () {
        var self = this;
        var data = {};
        self.params = $.extend(true, {}, self.params, arguments[0] || {});
        if (arguments[0]) {
            if (typeof (arguments[0]["data"]) == "function") {
                data = arguments[0]["data"]();
            } else {
                data = arguments[0]["data"];
            }
            if (self.params.isStringify) {
                arguments[0]["data"] = JSON.stringify(data);
            } else {
                arguments[0]["data"] = data;
            }
        }
        this.ajax($.extend(true, {type: "put", contentType: "application/json; charset=utf-8"}, arguments[0]));
    };
    Request.prototype.del = function () {
        var data = {};
        self.params = $.extend(true, {}, self.params, arguments[0]);
        if (typeof (arguments[0]["data"]) == "function") {
            data = arguments[0]["data"]();
        } else {
            data = arguments[0]["data"];
        }
        arguments[0]["data"] = data;
        this.ajax($.extend(true, {
            type: "delete",
            contentType: "application/json; charset=utf-8"
        }, arguments[0]));
    };
    Request.prototype.reload = function () {
        /// <summary>重载</summary>
        if (this.run) {
            this.abort();
            this.request = $.ajax(this.param);
        }
    };
    Request.prototype.abort = function () {
        // <summary>退出数据请求</summary>
        if (this.request) {
            this.request.abort();
        }
    };
    return Request;
});

