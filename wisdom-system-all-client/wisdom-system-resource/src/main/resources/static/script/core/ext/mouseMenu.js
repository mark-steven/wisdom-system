/**
 *
 *  @author   ghh
 *  @version  1.0
 */
define(['common', 'hciCore'], function (common, hciCore) {
    var HCIMouseMenu = function (params) {
        var self = this;
        self._Store = $(document).data("func", {});
        self.params = $.extend(true, {}, self.params, params);
        self.htmlCreateMenu();
        self.funMouseMenu();
        $(document).on("click", function () {
            self.hide();
        });
    };
    HCIMouseMenu.prototype.params = {
        name: "",
        data: [],
        offsetX: 2,
        offsetY: 2,
        textLimit: 12,
        beforeShow: $.noop,
        afterShow: $.noop
    };
    HCIMouseMenu.prototype.htmlCreateMenu = function (datum) {
        var self = this;
        var dataMenu = datum || self.params.data,
            nameMenu = self.params.name ? self.params.name : hciCore.lastId,
            htmlMenu = "",
            clKey = "hci-mouse-menu-";
        self.params.name = nameMenu;
        if ($.isArray(dataMenu) && dataMenu.length) {
            htmlMenu = '<div id="hci-mouse-menu-' + nameMenu + '" class="' + clKey + 'box">' +
                '<div class="' + clKey + 'body">' +
                '<ul class="' + clKey + 'ul">';
            $.each(dataMenu, function (i, arr) {
                if (i) {
                    htmlMenu = htmlMenu + '<li class="' + clKey + 'li-separate" >&nbsp;</li>';
                }
                if ($.isArray(arr)) {
                    $.each(arr, function (j, obj) {
                        var text = obj.text, htmlMenuLi = "", strTitle = "", rand = common.guid(), itemId = obj.itemId;
                        if (text) {
                            if (text.length > self.params.textLimit) {
                                text = text.slice(0, self.params.textLimit) + "…";
                                strTitle = ' title="' + obj.text + '"';
                            }

                            if ($.isArray(obj.data) && obj.data.length) {
                                htmlMenuLi = '<li class="' + clKey + 'li" data-hover="true" data-item-id="' + itemId + '">' + self.htmlCreateMenu(obj.data) +
                                    '<a href="javascript:" class="' + clKey + 'a"' + strTitle + ' data-key="' + rand + '"><i class="' + clKey + 'triangle"></i>' + text + '</a>' +
                                    '</li>';
                            } else {
                                htmlMenuLi = '<li class="' + clKey + 'li" data-item-id="' + itemId + '">' +
                                    '<a href="javascript:" class="' + clKey + 'a"' + strTitle + ' data-key="' + rand + '">' + text + '</a>' +
                                    '</li>';
                            }
                            htmlMenu += htmlMenuLi;
                            self.setFunc(rand, obj.func);
                        }
                    });
                }
            });
            htmlMenu = htmlMenu + '</ul>' +
                '</div>' +
                '</div>';
        }
        return htmlMenu;
    };
    HCIMouseMenu.prototype.funMouseMenu = function () {
        var self = this;
        var idKey = "#hci-mouse-menu-",
            clKey = "hci-mouse-menu-",
            jqueryMenu = $(idKey + self.params.name);
        if (!jqueryMenu.size()) {
            $("body").append(self.htmlCreateMenu());
            $(idKey + self.params.name + " a").on("click", function () {
                var key = $(this).attr("data-key"),
                    callback = self.getFunc(key);
                if ($.isFunction(callback)) {
                    callback.call(self.getFunc(key));
                }
                self.hide();
                return false;
            });
            $(idKey + self.params.name + " li").each(function () {
                var isHover = $(this).attr("data-hover"), clHover = clKey + "li-hover";
                $(this).hover(function () {
                    var jqueryHover = $(this).siblings("." + clHover);
                    jqueryHover.removeClass(clHover).children("." + clKey + "box").hide();
                    jqueryHover.children("." + clKey + "a").removeClass(clKey + "a-hover");
                    if (isHover) {
                        $(this).addClass(clHover).children("." + clKey + "box").show();
                        $(this).children("." + clKey + "a").addClass(clKey + "a-hover");
                    }
                });
            });
            return $(idKey + self.params.name);
        }
        return jqueryMenu;
    }
    HCIMouseMenu.prototype.setFunc = function (key, func) {
        var self = this;
        var objFunc = self._Store.data("func");
        objFunc[key] = func;
        self._Store.data("func", objFunc);
    };
    HCIMouseMenu.prototype.getFunc = function (key) {
        var self = this;
        var objFunc = self._Store.data("func");
        return objFunc[key];
    };
    HCIMouseMenu.prototype.hide = function () {
        var self = this;
        var target = self.getFunc("target");
        if (target && target.css("display") === "block") {
            target.hide();
        }
    };
    HCIMouseMenu.prototype.remove = function () {
        var self = this;
        var target = self.getFunc("target");
        if (target) {
            target.remove();
        }
    };
    HCIMouseMenu.prototype.show = function (target, nonShowArray) {
        var self = this;
        var target = $(target).size() ? target : self.params.target;
        $(target).on("contextmenu", function (e) {
            if ($.isFunction(self.params.beforeShow)) {
                self.params.beforeShow.call(this);
            }
            e = e || window.event;
            //阻止冒泡
            e.cancelBubble = true;
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            var jqueryMenu = self.funMouseMenu();
            if (jqueryMenu) {
                jqueryMenu.css({
                    display: "block",
                    left: e.clientX + self.params.offsetX,
                    top: e.clientY + self.params.offsetY
                });
                jqueryMenu.find("[data-item-id]").show();
                jqueryMenu.find(".hci-mouse-menu-li-separate").show();
                self.setFunc("target", jqueryMenu);
                if (nonShowArray && nonShowArray.length > 0) {
                    $.each(nonShowArray, function (index, item) {
                        if (item.indexOf("_separate-") > -1) {
                            var idx = item.split('-')[1] - 1;
                            $(jqueryMenu.find(".hci-mouse-menu-li-separate")[idx]).hide();
                        } else {
                            jqueryMenu.find("[data-item-id='" + item + "']").hide();
                        }
                    })
                }
                return false;
            }
        });
    };

    return HCIMouseMenu;
});



