package com.wisdom.system.resource.beetl;

import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;

import java.util.HashMap;

@Configuration
public class BeetlConf {

    @Autowired
    Functions fn;

    @Bean(initMethod = "init", name = "beetlConfig")
    public BeetlGroupUtilConfiguration getBeetlGroupUtilConfiguration(@Qualifier("functions") Functions fn) {
        BeetlGroupUtilConfiguration beetlGroupUtilConfiguration = new BeetlGroupUtilConfiguration();
        ResourcePatternResolver patternResolver = ResourcePatternUtils.getResourcePatternResolver(new DefaultResourceLoader());
        try {
            //内部路径写法
            ClasspathResourceLoader cploder = new ClasspathResourceLoader("/templates");
            beetlGroupUtilConfiguration.setResourceLoader(cploder);
            //外部路径写法
            // FileResourceLoader resourceLoader = new FileResourceLoader("F:\\projects\\zoe-medical-technology-reservation-project\\app\\medical-technology-reservation-manage\\src\\main\\resources\\templates\\", "utf-8");
            //beetlGroupUtilConfiguration.setResourceLoader(resourceLoader);
            beetlGroupUtilConfiguration.setConfigFileResource(patternResolver.getResource("classpath:Beetl.properties"));
            HashMap<String, Object> map = new HashMap<>();
            map.put("c", fn);
            beetlGroupUtilConfiguration.setFunctionPackages(map);
            return beetlGroupUtilConfiguration;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Bean(name = "beetlViewResolver")
    public BeetlSpringViewResolver getBeetlSpringViewResolver(@Qualifier("beetlConfig") BeetlGroupUtilConfiguration beetlGroupUtilConfiguration) {
        BeetlSpringViewResolver beetlSpringViewResolver = new BeetlSpringViewResolver();
        beetlSpringViewResolver.setContentType("text/html;charset=UTF-8");
        beetlSpringViewResolver.setOrder(0);
        beetlSpringViewResolver.setSuffix(".html");
        beetlSpringViewResolver.setConfig(beetlGroupUtilConfiguration);
        return beetlSpringViewResolver;
    }
}
