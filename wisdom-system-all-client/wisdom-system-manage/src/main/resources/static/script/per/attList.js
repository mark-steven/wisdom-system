define(['common', 'request', 'hciCore', 'hciMask', 'baseLayout'], function (common, Request, hciCore, HciMask, BaseLayout) {
    var AttList = function () {
    };
    //用来根据索引存储数据
    AttList.prototype.gridIndexData = {};
    //用来根据key存储索引（行数）数据
    AttList.prototype.gridKeyIndex = {};
    //布局
    AttList.prototype.layout = function (layoutParam) {
        var self = this;
        layoutParam["cells"]["a"]["gridParam"]["beforeLoadData"] = function (data) {
            if (data) {
                var gridData = data["list"];
                $.each(gridData, function (index, rowdata) {
                    self.gridIndexData[index] = rowdata;
                    self.gridKeyIndex[rowdata["id"]] = index;
                });
            }
        };
        self.baseLayout = new BaseLayout(layoutParam);
        return self.baseLayout;
    };

    //列信息配置
    AttList.prototype.getColumns = function () {
        return {
            "attendanceNum": {display: '考勤号码', name: 'attendanceNum', width: 90, align: 'center'},
            "name": {display: '姓名', name: 'name', width: 100, align: 'center'},
            "date": {display: '日期', name: 'date', width: 100, align: 'center'},
            "week": {display: '星期', name: 'week', width: 100, align: 'center'},
            "dept": {display: '部门', name: 'dept', width: 100, align: 'center'},
            "signInTime": {display: '签到时间', name: 'signInTime', width: 100, align: 'center'},
            "signBackTime": {display: '签退时间', name: 'signBackTime', width: 100, align: 'center'},
            "operation": {
                display: '操作', width: 60, align: 'center', name: "operation",
                renderItems: {
                    'del': {
                        url: "api/att/manage/{id}"
                    }
                }
            }
        }
    };
    return AttList;
});