/**
 *  考勤列表
 *
 *  @author   ghh
 *  @version  1.0
 */
require([GLOBAL_WEB_BASE_PATH + '/script/per/attList.js', 'baseLayout', 'common', 'request'], function (AttList, BaseLayout, common, Request) {
    var attList = new AttList();
    var columnJSON = attList.getColumns();
    //搜索框走配置可以配成json类型
    var searcherParam = {
        startTime: {
            label: "开始日期：",
            name: 'startTime',
            type: "date",
            width: 100,
            attr: {
                "id": "startTime",
                "data-default-value-type": "d",
                "data-default-value": 0,
                "data-max-value": 'endTime'
            }
        },
        endTime: {
            label: "结束日期：",
            name: 'endTime',
            type: "date",
            width: 100,
            attr: {
                "id": "endTime",
                "data-default-value-type": "d",
                "data-default-value": 7,
                "data-min-value": 'startTime'
            }
        },
        keyword: {
            label: "关键字：",
            name: 'keyword',
            type: "text",
            width: 150
        },
        btnSearch: true
    };

    //走配置
    var columnParam = {
        attendanceNum:columnJSON.attendanceNum,
        name:columnJSON.name,
        date:columnJSON.date,
        week:columnJSON.week,
        dept:columnJSON.dept,
        signInTime:columnJSON.signInTime,
        signBackTime:columnJSON.signBackTime,
        operation:columnJSON.operation
    };

    //不配置时默认组装好的数据
    var column = [
        columnJSON.attendanceNum,
        columnJSON.name,
        columnJSON.date,
        columnJSON.week,
        columnJSON.dept,
        columnJSON.signInTime,
        columnJSON.signBackTime,
        columnJSON.operation
    ];

    //定义变量
    var searchers = {};
    var columns = {};
    //如果有配置
    var customModule = common.getParamFromUrl("custom_module");
    if (customModule) {
        var req = new Request("api/dev/custom/module/code/{code}", {code: customModule});
        req.get({
            async: false,
            success: function (data) {
                var configContent = data["result"]["configContent"];
                configContent = eval(configContent);
                searchers = {};//搜索工具条
                columns = {};//列表
                //判断如果配置为空的时候走默认
                if (configContent["searchers"] == null) {
                    searchers = searcherParam
                } else {
                    $.each(configContent["searchers"], function (index, item) {
                        if (typeof (item) == "object") {
                            for (var key in item) {
                                searchers[key] = item[key];
                            }
                        } else {
                            searchers[item] = searcherParam[item];
                        }
                    })
                }
                //判断如果配置为空的时候走默认
                if (configContent["columns"] == null) {
                    columns = column
                } else {
                    $.each(configContent["columns"], function (index, item) {
                        if (typeof (item) == "object") {
                            for (var key in item) {
                                columns[key] = item[key];
                            }
                        } else {
                            columns[item] = columnParam[item];
                        }
                    })
                }
            }
        })
    } else {
        searchers = searcherParam;
        columns = columnParam;
    }
    //=======================上面的都是配置的东西==========================//
    //界面开始
    attList.layout({
        pattern: '1C',
        cells: {
            a: {
                attachId: 'grid',
                attachType: 'grid',
                toolbarParam: {
                    buttons: {'add': false, 'edit': false, "del": true},
                    searchers: searchers//这边走配置化
                },
                gridParam: {
                    isInitLoadData: true,
                    target: 'grid',
                    ajax: {
                        url: "api/att/manage/page/list"
                    },
                    columns: columns,
                    showType: 'list',//这边走配置化
                    row: {
                        selectIndex: 0
                    }
                }
            }
        }
    });
    //界面结束
});
