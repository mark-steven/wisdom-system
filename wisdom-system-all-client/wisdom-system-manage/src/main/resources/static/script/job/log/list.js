/**
 *  调度日志
 *  @author   ghh
 *  @version  1.0
 */
require(['baseLayout', 'common', 'request'], function (BaseLayout, common, Request) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: "调度日志",
                    attachId: 'treeGrid',
                    attachType: 'grid',
                    arrow: 'left',
                    width: 180,
                    isAttachToolbar: false,
                    gridParam: {
                        target: 'treeGrid',
                        ajax: {
                            url: "api/sys/dict/item/page/list?fkDictCategoryCode=JOB_LOG"
                        },
                        columns: [
                            {display: '调度名称', name: 'itemValue', width: 110, align: 'left'},
                        ],
                        showType: 'tree',
                        tree: {
                            simpleData: {
                                textField: 'code'
                            }
                        },
                        isUsePage: false,
                        row: {
                            checkbox: 0,
                            selectIndex: 0,
                            onSelect: function () {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    }
                },
                b: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {
                            'add': false,
                            'edit': false,
                            'del': true
                        },
                        searchers: {
                            startTime: {
                                label: '执行日志时间:',
                                name: 'startTime',
                                type: 'date',
                                width: 100,
                                attr: {
                                    "id": "startTime",
                                    "data-default-value-type": "d",
                                    "data-default-value": -30,
                                    "data-max-value": "endTime"
                                }
                            },
                            endTime: {
                                label: '-',
                                name: 'endTime',
                                type: 'date',
                                width: 100,
                                attr: {
                                    "id": "endTime",
                                    "data-default-value-type": "d",
                                    "data-default-value": +1,
                                    "data-min-value": "startTime"
                                }
                            },
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/job/log"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: false,
                        target: 'grid',
                        ajax: {
                            url: "api/job/log/page/list",
                            data: function () {
                                if (baseLayout) {
                                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (rowData) {
                                        return {
                                            fkJobLogCode: rowData["code"]
                                        }
                                    } else {
                                        return {};
                                    }
                                }
                            }
                        },
                        columns: [
                            {display: '调度日志类型', name: 'jobLogType', width: 110, align: 'center'},
                            {display: '调度日志内容', name: 'content', width: 790, align: 'left'},
                            {
                                display: '调度状态', name: 'state', width: 80, align: 'center',
                                render: function (rowData, value) {
                                    var stateJson = {'1': '执行前', '2': '执行完成'};
                                    return stateJson[value] || "";
                                }
                            },
                            {display: '调度时间', name: 'addTime', width: 120, align: 'left',type: 'date', format: '%Y-%m-%d %H:%i',},
                            {
                                display: '操作', width: 60, align: 'center', fixed: 'right',
                                renderItems: {
                                    'del': {
                                        url: "api/job/log/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    }
                }
            }
        });
    });
});
