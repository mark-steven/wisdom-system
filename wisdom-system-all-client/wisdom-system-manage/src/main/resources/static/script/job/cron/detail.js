/**
 *  @author   ghh
 *  @version  1.0
 */
require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_job_cron_detail_callback',
            formId: 'frmJobCronDetail',
            getUrl: 'api/job/cron/{id}',
            addUrl: 'api/job/cron',
            updateUrl: 'api/job/cron'
        });
    })
});