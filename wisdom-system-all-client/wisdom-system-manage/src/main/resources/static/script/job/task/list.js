/**
 *  项目管理列表
 *
 *  @author   ghh
 *  @version  1.0
 */
require(['baseLayout', 'request', 'common'], function (BaseLayout, Request, common) {
    $(function () {
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        //增、删、改、查等按钮
                        buttons: {'add': true, 'edit': true, 'del': true},
                        //搜索
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/job/task"
                            }
                        }
                    },
                    // 列表
                    gridParam: {
                        target: 'grid',
                        ajax: {
                            url: "api/job/task/page/list"
                        },
                        columns: [
                            {display: '编码', name: 'code', width: 180, align: 'left'},
                            {display: '名称', name: 'name', width: 150, align: 'left'},
                            {display: '触发类', name: 'clazzName', width: 120, align: 'left'},
                            {display: '频率(Cron)', name: 'fkCronName', width: 180, align: 'left'},

                            {
                                display: '运行状态',
                                name: 'state',
                                width: 80,
                                align: 'center',
                                render: function (rowData, value) {
                                    var stateJson = {"0": "停止", "1": "启用"};
                                    var colorJson = {"0": "red", "1": "green"};
                                    return $("<span></span>").text(stateJson[value]).css({"color": colorJson[value]});
                                }
                            },
                            {
                                display: '操作', width: 80, align: 'center',
                                renderItems: {
                                    'state': {
                                        clazz: function (rowData) {
                                            var stateJson = {"0": "stop", "1": "start"}
                                            return 'hci-icon hci-icon-' + stateJson[rowData["state"]] + ' padding-left-8';
                                        },
                                        title: function (rowData) {
                                            var stateJson = {"0": "启用", "1": "停用"}
                                            return stateJson[rowData["state"]];
                                        },
                                        onClick: function (rowData) {
                                            var textJson = {"0": "启用", "1": "停用"}
                                            common.confirm({
                                                text: "是否" + textJson[rowData["state"]] + "当前任务？",
                                                callback: function (result) {
                                                    if (result) {
                                                        var stateJson = {"0": "start", "1": "stop"};
                                                        var req = new Request("api/job/task/{id}/" + stateJson[rowData["state"]], {id: rowData["id"]});
                                                        req.get({
                                                            isSuccessTip: true,
                                                            success: function (data) {
                                                                if (data.code == "0") {
                                                                    baseLayout.params.cells["a"].gridObj.loadData();
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        },
                                        reqInfoKey: 'id'
                                    },
                                    'edit': true,
                                    'del': {
                                        url: "api/job/task/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_job_task_detail_callback",
                        all: {
                            href: "job/task/detail.html",
                            area: ["630px", "350px"]
                        },
                        add: {
                            title: '新增任务'
                        },
                        edit: {
                            title: '编辑任务'
                        }
                    }
                }
            }
        })
    });
})
