/**
 *  @author   ghh
 *  @version  1.0
 */
require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_job_task_detail_callback',
            formId: 'frmJobTaskDetail',
            getUrl: 'api/job/task/{id}',
            addUrl: 'api/job/task',
            updateUrl: 'api/job/task',
            selectParams: {
                fkCronCode: {
                    id: 'selCron',
                    value: 'fkCronCode',
                    ajaxParam: {
                        url: "api/job/cron/list"//select 获取列表的url
                    },
                    placeholder: '请选择执行频率',
                    dataAction: 'server',
                    defaultSelectType: 'value',
                    selectValueField: 'code',
                    selectTextField: 'name'
                }
            }
        });
    })
});
