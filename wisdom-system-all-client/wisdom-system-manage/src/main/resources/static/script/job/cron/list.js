/**
 *  项目管理列表
 *
 *  @author   ghh
 *  @version  1.0
 */
require(['baseLayout'], function (BaseLayout) {
    $(function () {
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        //增、删、改、查等按钮
                        buttons: {'add': true, 'edit': true, 'del': true},
                        //搜索
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/job/cron"
                            }
                        }
                    },
                    // 列表
                    gridParam: {
                        target: 'grid',
                        ajax: {
                            url: "api/job/cron/page/list"
                        },
                        columns: [
                            {display: '编码', name: 'code', width: 120, align: 'left'},
                            {display: '名称', name: 'name', width: 180, align: 'left'},
                            {display: 'Cron表达式', name: 'cronExpression', width: 200, align: 'left'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/job/cron/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_job_cron_detail_callback",
                        all: {
                            href: "job/cron/detail.html",
                            area: ["630px", "430px"]
                        },
                        add: {
                            title: '新增Cron配置'
                        },
                        edit: {
                            title: '编辑Cron配置'
                        }
                    }
                }
            }
        })
    });
})
