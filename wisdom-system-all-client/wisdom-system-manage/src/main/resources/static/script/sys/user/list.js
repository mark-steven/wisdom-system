require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: '部门树',
                    attachId: 'tree',
                    attachType: 'tree',
                    treeId: 'treeId',
                    width: 200,
                    arrow: 'left',
                    isAttachToolbar: false,
                    treeParam: {
                        ajax: {
                            url: "api/sys/tree/dept",
                            data: {"state": 1}
                        },
                        renderData: function (data) {
                            var result = data["result"];
                            for (var i = 0; i < result.length; i++) {
                                var res = result[i];
                                if (res["type"] == 2) {
                                    res["icon"] = GLOBAL_RESOURCE_PATH + "/image/icons/dept.png"
                                } else if (res['type'] == 3) {
                                    res["icon"] = GLOBAL_RESOURCE_PATH + "/image/icons/consulting.png"
                                }
                            }
                            return result;
                        },
                        zTree: {
                            view: {
                                dblClickExpand: false,
                                showLine: true,
                                selectedMulti: false,
                                autoCancelSelected: true
                            }
                        },
                        type: {
                            template: "<div style='height: 32px;line-height: 32px;padding-left: 10px;'>#name#</div>",
                            height: 'auto',
                            padding: 5
                        },
                        onClick: function (isSelected, treeNode, treeId) {
                            if (baseLayout) {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    }
                },
                b: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, "del": true},
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/user"
                            }
                        },
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var selectNodes = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                                    if (selectNodes && selectNodes.length > 0) {
                                        return {
                                            fkDepartmentCode: selectNodes[0]["code"],
                                            departmentName: selectNodes[0]["name"]
                                        };
                                    } else {
                                        return [];
                                    }
                                }
                            },
                            enableAdd: function () {
                                if (baseLayout) {
                                    var selectNodes = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                                    if (selectNodes && selectNodes.length > 0 && selectNodes[0]["type"] == '3') {
                                        return true;
                                    } else {
                                        common.errorMsg("请先选择左侧树需要配置的部门。");
                                        return false;
                                    }
                                }
                                return false;
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: false,
                        target: 'grid',
                        ajax: {
                            url: "api/sys/user/department/page/list",
                            data: function () {
                                if (baseLayout) {
                                    var selectNodes = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                                    if (selectNodes && selectNodes.length > 0) {
                                        return {fkDepartmentCode: selectNodes[0]["code"]};
                                    }
                                }
                            }
                        },
                        columns: [
                            {display: '用户姓名', name: 'name', width: 100, align: 'center'},
                            {display: '登录账号', name: 'loginCode', width: 150, align: 'center'},
                            {display: '岗位', name: 'jobName', width: 150, align: 'center'},
                            {display: '用户邮箱', name: 'email', width: 150, align: 'center'},
                            {display: '手机号码', name: 'phone', width: 120, align: 'center'},
                            {
                                display: '性别', name: 'sex', width: 60, align: 'center',
                                render: function (data) {
                                    switch (data["sex"]) {
                                        case 0:
                                            return "女";
                                        case 1:
                                            return "男";
                                        case 2:
                                            return "保密";
                                    }
                                }
                            },
                            {
                                display: '用户状态', width: 80, align: 'center', name: 'state',
                                renderItems: {
                                    'btnSwitch': {
                                        url: "api/sys/user/{id}/{state}",
                                        onValue: 1,
                                        offValue: 0,
                                        confirmMsg: '是否修改当前用户状态?'
                                    }
                                }
                            },
                            {display: '注册时间', name: 'addTime', width: 140, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/sys/user/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'tree',
                        tree: {
                            simpleData: {
                                textField: 'code'
                            }
                        },
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_sys_user_detail_callback",
                        all: {
                            href: "sys/user/detail.html",
                            area: ["630px", "340px"]
                        },
                        add: {
                            title: '新增用户信息'
                        },
                        edit: {
                            title: '编辑用户信息',
                            titleKey: 'name'
                        }
                    }
                }
            }
        })
    })
});
