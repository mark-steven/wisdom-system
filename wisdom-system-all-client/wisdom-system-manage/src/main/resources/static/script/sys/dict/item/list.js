require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        var fkDictCategoryCode = common.getParamFromUrl("code");
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, 'del': true},
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (rowData) {
                                        return {
                                            "pid": rowData["id"],
                                            "pname": rowData["name"],
                                            "fkDictCategoryCode": fkDictCategoryCode,
                                            "itemShowType": 1
                                        };
                                    } else {
                                        return {
                                            "pid": "0",
                                            "pname": "无【本身为顶级节点】",
                                            "fkDictCategoryCode": fkDictCategoryCode["code"],
                                            "itemShowType": 2
                                        };
                                    }
                                }
                                return {};
                            }
                        },
                        editParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (rowData) {
                                        return {
                                            "pid": rowData["id"],
                                            "pname": rowData["name"],
                                            "fkDictCategoryCode": fkDictCategoryCode,
                                            "itemShowType": 1
                                        };
                                    } else {
                                        return {
                                            "pid": "0",
                                            "pname": "无【本身为顶级节点】",
                                            "fkDictCategoryCode": fkDictCategoryCode["code"],
                                            "itemShowType": 2
                                        };
                                    }
                                }
                                return {};
                            }
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/dict/item"
                            }
                        }
                    },
                    gridParam: {
                        target: 'grid',
                        ajax: {
                            url: "api/sys/dict/item/page/list",
                            data: {fkDictCategoryCode: fkDictCategoryCode}
                        },
                        columns: [
                            {display: '角色编码', name: 'code', width: 120, align: 'left'},
                            {display: '角色名称', name: 'name', width: 100, align: 'center'},
                            {display: '角色值', name: 'itemValue', width: 100, align: 'center'},
                            {display: '排序', name: 'sort', width: 60, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',fixed:'right',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/sys/dict/item/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'tree',
                        tree: {
                            simpleData: {
                                textField: 'code'
                            }
                        },
                        row: {
                            selectIndex: 0,
                            onSelect: function () {

                            }
                        }
                    },
                    winParam: {
                        winCallback: "win_sys_dict_item_detail_callback",
                        all: {
                            href: "sys/dict/item/detail.html",
                            area: ["325px", "300px"]
                        },
                        add: {
                            title: '新增角色信息'
                        },
                        edit: {
                            title: '编辑角色信息',
                            titleKey: 'name'
                        }
                    }
                }
            }
        })
    });
})
