require(['common', 'request'], function (common, Request) {
    var menuFavorite = {
        data: "",
        color: ["#4f93fe", "#7415da", "#ffa14e", "#f344f5", "#f7d523"],
        _init: function(){
            this._initMenu();
            this._initEvent();
        },
        _initMenu: function(){
            var that = this;
            var req = new Request('api/sys/user/menu/favorites/list')
            req.get({
                data: {
                    fkSystem: '001'
                },
                success: function(msg){
                    if(msg.code == 0){
                        that.data = msg.result;
                        var panel = $('#menu-favorite');
                        panel.html("");
                        $.each(msg.result, function(idx, item) {
                            var model = $($('#menu-favorite-item-template').html()).clone();
                            model.find('.menu-item-name').html(item['fkMenuName']);
                            var i = model.find('.menu-item-img i');
                            if(item['fkIcon']) {
                                i.addClass(item['fkIcon']);
                            }
                            else {
                                // 默认显示图标
                                i.addClass('fa-file-archive-o');
                            }
                            i.css({
                                "color": that.color[idx%that.color.length]
                            });

                            model.data('data', item);
                            panel.append(model);
                        });
                    }
                }
            })
        },
        _initEvent: function () {
            var that = this;
            common.initDialogSelect({
                id: {
                    type: 'btn',
                    target: $('#favorite-menu-add'),
                    textName: 'name',//跟实体对应
                    valueName: 'id',
                    data: function () {
                        var data = [];
                        $.each(that.data, function (index, row) {
                            data.push({id: row["fkMenuId"], name: row["fkMenuName"]});
                        });
                        return data;
                    },
                    callback: function (data) {
                        var menu = [];
                        $.each(data, function (index, item) {
                            menu.push({fkMenuId: item["id"]})
                        });
                        var req = new Request("api/sys/user/menu/favorites/list/{fkSystem}", {fkSystem: '001'});
                        req.put({
                            data: menu,
                            isStringify: true,
                            success: function (data) {
                                if (data["code"] == "0") {
                                    that._initMenu();
                                }
                            }
                        })
                    },
                    urlParam: function () {
                        return {
                            partName: 'menuFavorites',
                            checkbox: 2,
                            fkSystem: '001'
                        };
                    }
                }
            });
            $("#menu-favorite").on('click', '.menu-item', function(){
                var data = $(this).data('data');
                var top = common.getTopWindowDom();
                var field = {
                    href: data["fkMenuUrl"],
                    icon: data["fkIcon"],
                    title: data["fkMenuName"],
                    id: data["fkMenuId"]
                };
                top.admintab.tabAdd(field)
            })
        }
    };

    $(function(){
        menuFavorite._init();
    })
});
