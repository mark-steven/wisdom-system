require(['baseLayout', 'request', 'common'], function (BaseLayout, Request, common) {
    $(function () {
        var internal = {
            baseLayout: '',
            tabbar: null,
            sidebars: {},
            forms: {},
            fkParamGroupCode: '',
            init: function () {
                internal.initBaseLayout();
                internal.initTabbar();
            },
            initBaseLayout: function () {
                internal.baseLayout = new BaseLayout({
                    parent: document.body,
                    pattern: '1C',
                    cells: {
                        a: {
                            isAttachToolbar: false,
                            attachType: "other"
                        }
                    }
                });
            },
            initTabbar: function () {
                if (!internal.tabbar) {
                    var req = new Request("api/dev/param/category/list");
                    req.get({
                        success: function (data) {
                            var tabs = [];
                            var nameJSON = {};
                            $.each(data.result, function (index, item) {
                                var _index = index + 1;
                                var id = 'a' + _index;
                                var tabInfo = {
                                    id: id,
                                    text: item["name"],
                                    code: item["code"]
                                };
                                if (_index === 1) {
                                    tabInfo["active"] = true;
                                }
                                nameJSON[id] = item["code"];
                                tabs.push(tabInfo);
                            });
                            internal.tabbar = internal.baseLayout.layout.cells("a").attachTabbar({
                                tabs: tabs
                            });
                            internal.initSidebar("a1", nameJSON["a1"]);
                            internal.tabbar.attachEvent("onTabClick", function (id) {
                                internal.initSidebar(id, nameJSON[id]);
                            });
                        }
                    });
                }
            },
            initSidebar: function (tabbarId, fkParamCategoryCode) {
                if (!internal.sidebars[tabbarId]) {
                    var items = [];
                    var req = new Request("api/dev/param/group/list");
                    req.get({
                        data: {fkParamCategoryCode: fkParamCategoryCode},
                        success: function (data) {
                            //分类编码 fkParamCategoryCode
                            var sideJSON = {};
                            $.each(data.result, function (index, item) {
                                //分组编码=item["code"]
                                var _index = index + 1;
                                var id = tabbarId + '_a' + _index;
                                var itemInfo = {
                                    id: id,
                                    text: item["name"],
                                    code: item["code"]
                                };
                                sideJSON[id] = itemInfo;
                                if (index === 0) {
                                    itemInfo["selected"] = true;
                                }
                                items.push(itemInfo);
                            });
                            internal.sidebars[tabbarId] = internal.tabbar.cells(tabbarId).attachSidebar({
                                width: 180,
                                items: items,
                                template: "text"
                            });

                            internal.initForm(tabbarId, tabbarId + "_a1", fkParamCategoryCode, sideJSON[tabbarId + "_a1"].code);
                            internal.sidebars[tabbarId].attachEvent("onSelect", function (id) {
                                internal.initForm(tabbarId, id, fkParamCategoryCode, sideJSON[id].code);
                            });
                        }
                    });
                }
            },
            initForm: function (tabbarId, sidebarId, fkParamCategoryCode, fkParamGroupCode) {
                this.fkParamGroupCode = fkParamGroupCode;
                if (!internal.forms[sidebarId]) {
                    internal.forms[sidebarId] = true;
                    //html界面组装逻辑
                    var req = new Request("api/dev/param/config/list");
                    req.get({
                        data: {fkParamCategoryCode: fkParamCategoryCode, fkParamGroupCode: fkParamGroupCode},
                        success: function (data) {
                            internal.buildForm(tabbarId, sidebarId, data.result);
                        }
                    })
                }
            },
            buildForm: function (tabbarId, sidebarId, data) {
                var that = this;
                var type = {"1": "Input", "2": "Select", "3": "Textarea", "4": "Switch"};
                var htmlStr = "<form id='" + sidebarId + "' class='formParam'><div>"
                    + '<div class="layui-layer-btn param-btn"><a  id="btn-' + sidebarId + '" class="layui-layer-btn0">保存</a></div>'
                    + "<table>";
                $.each(data, function (index, item) {
                    htmlStr += internal["build" + type[item["type"]]](item)
                });
                htmlStr += "</table></div></form>";
                internal.sidebars[tabbarId].cells(sidebarId).attachHTMLString(htmlStr);
                $("#btn-" + sidebarId).on("click", function () {
                    that.saveParam($("#" + sidebarId).form2json());
                });

            },
            buildInput: function (item) {
                var html = "";
                var v = !item["paramValue"] ? "" : item["paramValue"];
                html += "<tr>"
                    + "<td class='label-td'><label>" + item["tagName"] + "：</label></td>"
                    + "<td class='text-td'><span><input type='text' name='" + item["paramKey"]
                    + "' value='" + v + "'></span></td>"
                    + "</tr>";
                return html;
            },
            buildSelect: function () {
                //todo 用于下拉选择
            },
            buildTextarea: function (item) {
                //todo 多行文本框
                var html = "";
                var v = !item["paramValue"] ? "" : item["paramValue"];
                html += "<tr>"
                    + "<td class='label-td' style='vertical-align: top;'><label>" + item["tagName"] + "：</label></td>"
                    + "<td class='text-td'><span><textarea type='text' name='" + item["paramKey"]
                    + "'>" + v + "</textarea></span></td>"
                    + "</tr>";
                return html;
            },
            buildSwitch: function (item) {
                //todo switch
                var html = "";
                var v = !item["paramValue"] ? "" : item["paramValue"];
                html += "<tr>"
                    + "<td class='label-td'><label>" + item["tagName"] + "：</label></td>"
                    + "<td class='text-td'><span><input type='text' name='" + item["paramKey"]
                    + "' value='" + v + "'></span></td>"
                    + "</tr>";
                return html;
            },
            saveParam: function (data) {
                var list = [];
                for (var key in data) {
                    list.push({
                        paramKey: key,
                        paramValue: data[key]
                    });
                }
                var req = new Request('api/sys/param/list/{paramGroupCode}', {paramGroupCode: this.fkParamGroupCode});
                req.put({
                    data: list,
                    success: function (msg) {
                        if (msg.code == 0) {
                            common.successMsg('保存参数信息成功。');
                        }
                    }
                })
            }
        };
        internal.init();
    });
});