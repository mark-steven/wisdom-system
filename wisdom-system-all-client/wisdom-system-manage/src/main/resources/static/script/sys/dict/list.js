require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: "字典分类管理",
                    attachId: 'grid',
                    attachType: 'grid',
                    width: 500,
                    arrow: 'left',
                    isAttachToolbar: true,
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, 'del': false},
                        delParam: {
                            enableDel: function () {
                                var rowCount = baseLayout.params.cells["b"].gridObj.getRowCountInPage();
                                if (rowCount > 0) {
                                    common.errorMsg("字典关联相关项,不能删除！");
                                    return false;
                                }
                                return true;
                            },
                            ajax: {
                                url: 'api/sys/dict/category'
                            }
                        }
                    },
                    gridParam: {
                        ajax: {
                            url: 'api/sys/dict/category/page/list'
                        },
                        columns: [
                            {display: '分类编码', name: 'code', width: 150, align: 'left'},
                            {display: '分类名称', name: 'name', width: 100},
                            {
                                display: '操作', width: 60, align: 'center',fixed:'right',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        enableDel: function () {
                                            var rowCount = baseLayout.params.cells["b"].gridObj.getRowCountInPage();
                                            if (rowCount > 0) {
                                                common.errorMsg("字典关联相关项,不能删除！");
                                                return false;
                                            }
                                            return true;
                                        },
                                        url: 'api/sys/dict/category/{id}'
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0,
                            onSelect: function (id) {
                                if (baseLayout) {
                                    baseLayout.params.cells["b"].gridObj.loadData();
                                }
                            }
                        },
                        isUsePage: false
                    },
                    winParam: {
                        winCallback: 'win_sys_dict_category_detail_callback',
                        all: {
                            href: "sys/dict/category/detail.html",
                            area: ["320px", "260px"]
                        },
                        add: {
                            title: '新增字典'
                        },
                        edit: {
                            title: '编辑字典',
                            reqInfoKey: 'id',
                            titleKey: 'name'
                        }
                    }
                },
                b: {
                    attachId: 'grid1',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, 'del': true},
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var dictCategoryRowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    var rowData = baseLayout.params.cells["b"].gridObj.getSelectedRow();
                                    if (rowData & dictCategoryRowData["itemShowType"]) {
                                        return {
                                            "pid": rowData["id"],
                                            "pname": rowData["name"],
                                            "fkDictCategoryCode": dictCategoryRowData["code"],
                                            "itemShowType": dictCategoryRowData["itemShowType"]
                                        };
                                    } else {
                                        return {
                                            "pid": "0",
                                            "pname": "无【本身为顶级节点】",
                                            fkDictCategoryCode: dictCategoryRowData["code"],
                                            itemShowType: dictCategoryRowData["itemShowType"]
                                        };
                                    }

                                } else {
                                    return {};
                                }
                            }
                        },
                        editParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var dictCategoryRowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    return {
                                        fkDictCategoryCode: dictCategoryRowData["code"],
                                        itemShowType: dictCategoryRowData["itemShowType"]
                                    };
                                } else {
                                    return {};
                                }
                            }
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/dict/item"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: false,
                        target: 'grid1',
                        ajax: {
                            url: "api/sys/dict/item/page/list",
                            data: function () {
                                if (baseLayout) {
                                    var dictCategoryRowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (dictCategoryRowData && dictCategoryRowData["code"].length > 0){
                                        return {
                                            fkDictCategoryCode: dictCategoryRowData["code"],
                                            itemShowType: dictCategoryRowData["itemShowType"]
                                        };
                                    }else{
                                        return {
                                            fkDictCategoryCode: '',
                                            itemShowType: ''
                                        };
                                    }
                                } else {
                                    return {};
                                }
                            }
                        },
                        columns: [
                            {display: '项编码', name: 'code', width: 100, align: 'center'},
                            {display: '项名称', name: 'name', width: 100, align: 'center'},
                            {display: '项值', name: 'itemValue', width: 100, align: 'center'},
                            {display: '排序', name: 'sort', width: 60, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',fixed:'right',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/sys/dict/item/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'tree',
                        tree: {
                            simpleData: {
                                textField: 'code'
                            }
                        },
                        isUsePage: false
                    },
                    winParam: {
                        winCallback: "win_sys_dict_detail_callback",
                        all: {
                            href: "sys/dict/detail.html",
                            area: ["325px", "300px"]
                        },
                        add: {
                            title: '新增字典项信息'
                        },
                        edit: {
                            title: '编辑字典项信息',
                            titleKey: 'name'
                        }
                    }
                }
            }
        });
    })
});
