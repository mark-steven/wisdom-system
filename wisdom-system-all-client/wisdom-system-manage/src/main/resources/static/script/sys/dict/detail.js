require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var urlData = common.getParamFromUrl2Json();
        new DialogDetail({
            winCallback: 'win_sys_dict_detail_callback',
            formId: 'frmSysDictDetail',
            getUrl: 'api/sys/dict/item/{id}',
            addUrl: 'api/sys/dict/item',
            updateUrl: 'api/sys/dict/item',
            beforeBindEvent: function (data) {
                //如果父节点pid为0，添加父节点说明
                if (data["pid"] == 0) {
                    data["pname"] = '无【本身为顶级节点】';
                }
                return data;
            },
            dialogSelectParams: {
                pid: {
                    textName: 'pname',//跟实体对应
                    urlParam: function () {
                        var checkTypeParentValue = $("#frmSysDictDetail").find("[name='id']").val() || '';
                        return {
                            partName: 'dictItem',
                            btnType: 2,
                            treeCheckboxType: 2,
                            checkTypeParentValue: checkTypeParentValue,
                            fkDictCategoryCode: urlData["fkDictCategoryCode"]
                        };
                    }
                }
            },
            initBindDataEvent: function () {
                if (urlData["itemShowType"] == "1") {
                    $("#item-tree-select").show();
                }
                return urlData;
            },
            beforeSaveEvent: function (data) {
                data["fkDictCategoryCode"] = urlData["fkDictCategoryCode"];
                return data;
            }
        });
    })
});