require(['common', 'baseLayout'], function (common, BaseLayout) {
    $(function () {
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        buttons: {"add": true, "edit": true, "del": true},
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (rowData) {
                                        return {
                                            "pid": rowData["id"], "pname": rowData["name"],
                                            "fkSystem": rowData["fkSystem"]
                                        };
                                    } else {
                                        return {"pid": "0", "pname": "无【本身为顶级节点】"};
                                    }
                                }
                            }
                        },
                        editParam: {
                            enableEdit: function () {
                                var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                if (!rowData) {
                                    common.errorMsg("请选择要编辑的菜单！");
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/menu"
                            }
                        }
                    },
                    gridParam: {
                        ajax: {
                            url: "api/sys/menu/page/list"
                        },
                        showType: 'tree',
                        columns: [
                            {display: '所属系统', name: 'fkSystemName', width: 100, align: 'center'},
                            {display: '名称', name: 'name', width: 150, align: 'left'},
                            {display: '路径', name: 'url', width: 300, align: 'left'},
                            {display: '排序', name: 'sort', width: 60, align: 'center'},
                            {
                                display: '状态', width: 80, align: 'center', name: 'state', fixed: 'right',
                                renderItems: {
                                    'btnSwitch': {
                                        url: "api/sys/menu/{id}/{state}",
                                        onValue: 1,
                                        offValue: 0,
                                        disable: false,
                                        confirmMsg: '是否修改当前状态?'
                                    }
                                }
                            },
                            {
                                display: '操作', width: 60, align: 'center', fixed: 'right',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/sys/menu/{id}"
                                    }
                                }
                            }
                        ],
                        mergeColumnRule: function () {
                            return {
                                byColumnName: ['fkSystemName'],
                                mergeColumnIndex: {left: [], def: [0], right: []}
                            }
                        },
                        isUsePage: false
                    },
                    winParam: {
                        winCallback: "win_sys_menu_detail_callback",
                        all: {
                            href: "sys/menu/detail.html",
                        },
                        add: {
                            title: '新增菜单记录',
                            area: ["620px", "300px"]
                        },
                        edit: {
                            title: '编辑菜单记录',
                            reqInfoKey: 'id',
                            area: ["620px", "300px"]
                        }
                    }
                }
            }
        });
    })
});
