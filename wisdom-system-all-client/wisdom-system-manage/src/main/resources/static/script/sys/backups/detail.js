/**
 *
 *  @author   zxn
 *  @version  1.0
 */
require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var conditionsEditor;
        var dialogInit = function () {
            new DialogDetail({
                winCallback: 'win_sys_table_bak_callback',
                formId: 'formTableBak',
                getUrl: 'api/sys/table/bak/{id}',
                addUrl: 'api/sys/table/bak',
                updateUrl: 'api/sys/table/bak',
                beforeBindEvent: function (data) {
                    if (data["conditions"]) {
                        conditionsEditor.setValue(data["conditions"]);
                    }
                    return data;
                },
                beforeSaveEvent: function (data) {
                    data["conditions"] = conditionsEditor.getValue();
                    return data;
                },
                switchBtnParams: {
                    state: {
                        onValue: 1,
                        offValue: 0
                    },
                    isClearTableData: {
                        onValue: 1,
                        offValue: 0
                    },
                    isClearBakData: {
                        onValue: 1,
                        offValue: 0
                    }
                },
                selectParams: {
                    selTableName: {
                        id: 'tableName',
                        value: 'tableName',
                        text: 'showName',
                        isInitLoadData: true,
                        ajaxParam: {
                            url: "api/sys/table/bak/tab/list",//select 获取列表的url
                        },
                        selectValueField: 'tableName',
                        selectTextField: 'showName',
                        dataAction: 'server',
                        defaultSelectType: 'index',
                        defaultSelectValue: 1,
                        width: 495,
                        placeholder: '请选择表！'
                    }
                }
            });
        };
        //设置插件路径
        require.config({paths: {'vs': GLOBAL_RESOURCE_PATH + '/lib/monaco-editor/min/vs'}});
        //绑定对象
        require(['vs/editor/editor.main'], function () {
            //表格参数配置
            conditionsEditor = monaco.editor.create(document.getElementById('conditions-editor'), {
                language: 'txt',
                value: ''
            });
            monaco.editor.setTheme("vs-dark");
            dialogInit()
        })
    })
});