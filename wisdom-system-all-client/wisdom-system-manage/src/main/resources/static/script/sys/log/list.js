require(['baseLayout'], function (BaseLayout) {
    $(function () {
        new BaseLayout({
            pattern: '1C',
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {'add': false, 'edit': false, "del": true},
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/system/log"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: true,
                        target: 'grid',
                        ajax: {
                            url: "api/sys/system/log/page/list"
                        },
                        columns: [
                            {
                                display: '请求类型', name: 'requestTypeCode', width: 90, align: 'center',
                                render: function (rowData, value, colInfo, jqContent, jqTr) {
                                    var dist = {'01': '请求参数', '02': '返回参数'};
                                    var colorJSON = {'01': 'green', '02': 'red'};
                                    jqContent.css({"background": colorJSON[value]}).text(dist[value] || "");
                                }
                            },
                            {display: '请求IP', name: 'ipAddress', width: 150, align: 'center'},
                            {display: '请求路由', name: 'requestRouting', width: 200, align: 'center'},
                            {display: '参数', name: 'param', width: 300, align: 'center'},
                            {
                                display: '操作', width: 80, align: 'center', fixed: 'right',
                                renderItems: {
                                    'edit': false,
                                    'del': {
                                        url: "api/sys/system/log/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    }
                }
            }
        });
    })
});
