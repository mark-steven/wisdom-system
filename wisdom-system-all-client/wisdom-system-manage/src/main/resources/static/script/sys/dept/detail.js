require(['dialogDetail'], function (DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_sys_dept_detail_callback',
            formId: 'frmSysDeptDetail',
            getUrl: 'api/sys/department/{id}',
            addUrl: 'api/sys/department',
            updateUrl: 'api/sys/department',
            switchBtnParams: {
                state: {
                    onValue: 1,
                    offValue: 0
                }
            },
            selectParams: {
                fkDeptTypeCode: {
                    id: 'selDeptType',
                    text: 'deptTypeName',
                    value: 'fkDeptTypeCode',//
                    isInitLoadData: true,
                    ajaxParam: {
                        url: "api/sys/dict/item/list",//select 获取列表的url
                        data: {fkDictCategoryCode: "DEPT_TYPE"},
                        resultRender: function (data) {
                            var result = data["result"];
                            return result;
                        }
                    },
                    selectValueField: 'code',
                    selectTextField: 'name',
                    dataAction: 'server',
                    defaultSelectType: 'value',
                    width: 180,
                    placeholder: '请选择公司属性！'
                }
            }
        });
    })
});