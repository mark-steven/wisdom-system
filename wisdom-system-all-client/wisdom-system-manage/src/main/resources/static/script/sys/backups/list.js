/**
 *  @author   zxn
 *  @version  1.0
 */
require(['baseLayout', 'common', 'request'], function (BaseLayout, common, Request) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '1C',
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, "del": true},
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/table/bak"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: true,
                        target: 'grid',
                        ajax: {
                            url: "api/sys/table/bak/page/list"
                        },
                        columns: [
                            {display: '备份编码', name: 'code', width: 90, align: 'center'},
                            {display: '备份名称', name: 'name', width: 150, align: 'center'},
                            {display: '主表名称', name: 'tableName', width: 200, align: 'center'},
                            {display: '表空间名', name: 'tableSchema', width: 200, align: 'center'},
                            {
                                display: '表备份状态', width: 80, align: 'center', name: 'state',
                                renderItems: {
                                    'btnSwitch': {
                                        url: "api/sys/table/bak/{id}/{state}",
                                        onValue: 1,
                                        offValue: 0,
                                        confirmMsg: '是否修改当前备备份任务状态?'
                                    }
                                }
                            },
                            {
                                display: '操作', width: 80, align: 'center', fixed: 'right',
                                renderItems: {
                                    'execute': {
                                        clazz: 'hci-icon hci-icon-stop  padding-left-8',
                                        title: "立即备份",
                                        onClick: function (rowData) {
                                            common.confirm({
                                                text: "立即备份" + rowData["tableName"] + "表？",
                                                callback: function (result) {
                                                    if (result) {
                                                        var req = new Request("api/sys/table/bak/execute");
                                                        req.post({
                                                            data: rowData,
                                                            isSuccessTip: true,
                                                            success: function (data) {
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        },
                                        reqInfoKey: 'id'
                                    },
                                    'edit': true,
                                    'del': {
                                        url: "api/sys/table/bak/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_sys_table_bak_callback",
                        all: {
                            href: "sys/table/bak/detail.html",
                            area: ["43%", "56%"]
                        },
                        add: {
                            title: '新增表备份转移信息'
                        },
                        edit: {
                            title: '编辑表备份转移信息',
                            titleKey: 'name'
                        }
                    }
                }
            }
        });
    })
});
