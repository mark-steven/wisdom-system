require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var initData = common.getParamFromUrl2Json();
        new DialogDetail({
            winCallback: 'win_sys_user_detail_callback',
            formId: 'frmSysUserDetail',
            getUrl: 'api/sys/user/{id}',
            addUrl: 'api/sys/user',
            updateUrl: 'api/sys/user',
            initBindDataEvent: function () {
                var initData = common.getParamFromUrl2Json();
                return initData;
            },
            switchBtnParams: {
                state: {
                    onValue: 1,
                    offValue: 0
                }
            },
            selectParams: {
                sex: {
                    id: 'selSex',
                    name: 'sex',//跟界面对应，用来绑定值也用到
                    value: 'sex',
                    data: [{text: '保密', value: "2"},{text: '男', value: "1"}, {text: '女', value: "0"}],
                    selectValueField: 'value',
                    selectTextField: 'text',
                    dataAction: 'local',
                    defaultSelectType: 'index',//初始化默认值选择方式：index 索引；value 值；
                    defaultSelectValue: 1,
                    width: 180,
                    placeholder: '---请选择性别---'
                },
                fkDepartmentCode: {
                    id: 'selDept',
                    value: 'fkDepartmentCode',
                    text: 'departmentName',
                    isInitLoadData: true,
                    ajaxParam: {
                        url: "api/sys/department/list",//select 获取列表的url
                    },
                    selectValueField: 'code',
                    selectTextField: 'name',
                    dataAction: 'server',
                    defaultSelectType: 'value',
                    defaultSelectValue: initData["fkDepartmentCode"],
                    width: 180,
                    placeholder: '请选择部门！'
                },
                fkJobCode: {
                    id: 'selJob',
                    text: 'jobName',
                    value: 'fkJobCode',
                    isInitLoadData: true,
                    ajaxParam: {
                        url: "api/sys/dict/item/list",//select 获取列表的url
                        data: {fkDictCategoryCode: "JOB_TYPE"}
                    },
                    selectValueField: 'code',
                    selectTextField: 'name',
                    dataAction: 'server',
                    defaultSelectType: 'value',
                    width: 180,
                    placeholder: '请选择岗位名称！'
                }
            }
        });
    })
});