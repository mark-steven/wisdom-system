require(['dialogDetail', 'common', 'request'], function (DialogDetail, common, Request) {
    $(function () {
        var initData = common.getParamFromUrl2Json();
        var dialogDetail = new DialogDetail({
            winCallback: 'win_sys_menu_detail_callback',
            formId: 'frmSysMenuDetail',
            getUrl: 'api/sys/menu/{id}',
            addUrl: 'api/sys/menu',
            updateUrl: 'api/sys/menu',
            beforeBindEvent: function (data) {
                //如果父节点pid为0，添加父节点说明
                if (data["pid"] == 0) {
                    data["pname"] = '无【本身为顶级节点】';
                } else {
                    dialogDetail['selectSystem'].disable();
                }
                return data;
            },
            initBindDataEvent: function () {
                var initData = common.getParamFromUrl2Json();
                return initData;
            },
            // 加载完成事件
            afterPageLoad: function (detail) {
                var initData = common.getParamFromUrl2Json();
                if ((initData.pid && initData.pid != 0)) {
                    detail['selectSystem'].disable();
                }
            },
            switchBtnParams: {
                state: {
                    onValue: 1,
                    offValue: 0
                }
            },
            dialogSelectParams: {
                pid: {
                    type: 'input',
                    textName: 'pname',//跟实体对应
                    urlParam: function () {
                        var checkTypeParentValue = $("#frmSysMenuDetail").find("[name='id']").val() || '';
                        return {
                            partName: 'menu',
                            treeCheckboxType: 2,
                            checkTypeParentValue: checkTypeParentValue
                        };
                    },
                    onChange: function (val, txt) {
                        if (val == '0') {
                            dialogDetail['selectSystem'].enable();
                        } else {
                            dialogDetail['selectSystem'].disable();
                            var req = new Request('api/sys/menu/' + val);
                            req.get({
                                success: function (msg) {
                                    if (msg.code == '0' && msg.result) {
                                        dialogDetail['selectSystem'].setComboValue(msg.result.fkSystem);
                                    }
                                }
                            })
                        }
                    }
                }
            },
            selectParams: {
                fkSystem: {
                    id: 'selectSystem',
                    text: 'fkSystemName',
                    value: 'fkSystem',
                    isInitLoadData: true,
                    ajaxParam: {
                        url: "api/sys/dict/item/list",//select 获取列表的url
                        data: {fkDictCategoryCode: "MENU_SYSTEM"},
                        resultRender: function (data) {
                            var result = data["result"];
                            return result;
                        }
                    },
                    selectValueField: 'code',
                    selectTextField: 'name',
                    dataAction: 'server',
                    defaultSelectType: 'value',
                    defaultSelectValue: initData["fkSystem"],
                    width: 180,
                    placeholder: '请选择所属系统！'
                }
            }
        })
    })
});