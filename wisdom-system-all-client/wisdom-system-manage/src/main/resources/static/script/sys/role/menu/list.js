require(['baseLayout', 'common', 'request'], function (BaseLayout, common, Request) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    attachId: 'treeGrid',
                    attachType: 'grid',
                    width: 300,
                    arrow: 'left',
                    isAttachToolbar: false,
                    gridParam: {
                        target: 'treeGrid',
                        ajax: {
                            url: "api/sys/dict/item/page/list?fkDictCategoryCode=ROLE_CATEGORY"
                        },
                        columns: [
                            {display: '角色编码', name: 'code', width: 100, align: 'center'},
                            {display: '角色名称', name: 'name', width: 100, align: 'center'}
                        ],
                        showType: 'tree',
                        tree: {
                            simpleData: {
                                textField: 'code'
                            }
                        },
                        isUsePage: false,
                        row: {
                            selectIndex: 0,
                            onSelect: function () {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    }
                },
                b: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {
                            btnCustomAdd: {
                                id: 'btnCustomAdd',
                                type: 'button',
                                text: '新增',
                                img: 'add.png'
                            },
                            'add': false,
                            'edit': false,
                            'del': true
                        },
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/role/menu"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: false,
                        target: 'grid',
                        ajax: {
                            url: "api/sys/role/menu/page/list",
                            data: function () {
                                if (baseLayout) {
                                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (rowData) {
                                        return {
                                            fkRoleId: rowData["id"]
                                        }
                                    } else {
                                        return {};
                                    }
                                }
                            }
                        },
                        columns: [
                            {display: '所属系统', name: 'menuSystemName', width: 100, align: 'center'},
                            {display: '名称', name: 'menuName', width: 180},
                            {display: '路径', name: 'url', width: 320},
                            {display: '排序', name: 'sort', width: 60},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'del': {
                                        url: "api/sys/role/menu/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'tree',
                        row: {
                            selectIndex: 0
                        },
                        tree: {
                            simpleData: {
                                enable: true,
                                idKey: "fkMenuId",
                                pIdKey: 'pid',
                                textField: 'menuName',
                            }

                        },
                        isUsePage: false
                    }
                }
            }
        });
        var btnCustomAdd = baseLayout.params.cells["b"].toolbarObj.getItem("btnCustomAdd");
        common.initDialogSelect({
            id: {
                type: 'btn',
                target: btnCustomAdd,
                textName: 'name',//跟实体对应
                valueName: 'id',
                data: function () {
                    var data1 = [];
                    var fkRoleId = "";
                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                    if (rowData) {
                        fkRoleId = rowData["id"]
                    }
                    var req = new Request("api/sys/role/menu/list");
                    req.get({
                        data: {
                            fkRoleId: fkRoleId
                        },
                        success: function (data) {
                            $.each(data.result, function (index, row) {
                                data1.push({id: row["fkMenuId"], name: row["menuName"]});
                            });
                        }
                    });
                    return data1;
                },
                callback: function (data) {
                    var sysRoleMenu = [];
                    var fkRoleId = "";
                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                    if (rowData) {
                        fkRoleId = rowData["id"]
                    }
                    $.each(data, function (index, item) {
                        sysRoleMenu.push({fkMenuId: item["id"], fkRoleId: fkRoleId})
                    });
                    var req = new Request("api/sys/role/menu/list/{fkRoleId}", {fkRoleId: fkRoleId});
                    req.put({
                        data: sysRoleMenu,
                        isStringify: true,
                        success: function (data) {
                            if (data["code"] == "0") {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    })

                },
                urlParam: function () {
                    return {
                        partName: 'menuAuth',
                        checkbox: 2
                    };
                }
            }
        });
    });
});
