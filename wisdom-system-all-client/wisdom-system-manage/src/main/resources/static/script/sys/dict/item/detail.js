require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_sys_dict_item_detail_callback',
            formId: 'frmSysDictItemDetail',
            getUrl: 'api/sys/dict/item/{id}',
            addUrl: 'api/sys/dict/item',
            updateUrl: 'api/sys/dict/item',
            beforeBindEvent: function (data) {
                //如果父节点pid为0，添加父节点说明
                if (data["pid"] == 0) {
                    data["pname"] = '无【本身为顶级节点】';
                }
                return data;
            },
            dialogSelectParams: {
                pid: {
                    textName: 'pname',//跟实体对应
                    urlParam: function () {
                        var checkTypeParentValue = $("#frmSysDictItemDetail").find("[name='id']").val() || '';
                        var initData = common.getParamFromUrl2Json();
                        return {
                            partName: 'dictItem',
                            btnType: 2,
                            treeCheckboxType: 2,
                            checkTypeParentValue: checkTypeParentValue,
                            fkDictCategoryCode: initData["fkDictCategoryCode"]
                        };
                    }
                }
            },
            initBindDataEvent: function () {
                var initData = common.getParamFromUrl2Json();
                if (initData["itemShowType"] == "1") {
                    $("#item-tree-select").show();
                }
                return initData;
            },
            beforeSaveEvent: function (data) {
                var initData = common.getParamFromUrl2Json();
                data["fkDictCategoryCode"] = initData["fkDictCategoryCode"];
                return data;
            }
        });
    })
});