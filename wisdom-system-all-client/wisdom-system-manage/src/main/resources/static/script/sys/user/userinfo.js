require(['common', 'request'], function (common, Request) {
    $(function () {
        var __userinfo_win = {
            formId: 'formUserInfo',
            validObj: "",
            init: function () {
                this.validObj = common.initValidForm($('#' + this.formId));
                var req = new Request('api/current/user/info');
                req.get({
                    isErrorTip: false,
                    success: function (data) {
                        $('#userId').val(data['userId']);
                        $('#fkDeptTypeName').val("");
                        $('#departmentName').val("");
                        $('#fkDeptTypeName').text(data['fkDeptTypeName']?data['fkDeptTypeName']:'总公司');
                        $('#departmentName').text(data['departmentName']?data['departmentName']:'总裁办');
                        $('#name').val(data['realName']);
                        $('#loginCode').val(data['loginCode']);
                        $('#email').val(data['email']);
                        $('#phone').val(data['phone']);
                    }
                });
            },
            validate: function () {
                if (!this.validObj.check()) {
                    common.errorMsg('验证不通过，请根据错误信息修改');
                    return false;
                }
                var name = $('#name').val();
                var loginCode = $('#loginCode').val();
                if (!$.trim(name)) {
                    common.errorMsg('您输入的用户名不能为空或非法字符，请确认后再提交');
                    return false;
                }
                if (!$.trim(loginCode)) {
                    common.errorMsg('您输入的登录名不能为空或非法字符，请确认后再提交');
                    return false;
                }
                return true;
            },
            getData: function () {
                var userId = $('#userId').val();
                var name = $('#name').val();
                var loginCode = $('#loginCode').val();
                var email = $('#email').val();
                var phone = $('#phone').val();
                var params = {
                    id: userId,
                    name: name,
                    loginCode: loginCode,
                    email: email,
                    phone: phone
                };
                return params;
            },
            postData: function (beforeFn, afterFn) {
                var that = this;
                if (this.validate()) {
                    beforeFn && beforeFn();
                    var p = that.getData();
                    var req = new Request('api/sys/user/userinfo');
                    req.post({
                        data: p,
                        success: function (data) {
                            afterFn(data);
                        },
                        error: function (xhr) {
                            var msg = "";
                            if (xhr && xhr.responseJSON && xhr.responseJSON.msg) {
                                msg = xhr.responseJSON.msg;
                                common.errorMsg(msg);
                            } else {
                                msg = "请求接口异常，稍后尝试！";
                            }
                            afterFn({code: "100", msg: msg});
                        }
                    })
                }
            }
        };
        __userinfo_win.init();
        window.__userinfo_win = __userinfo_win;
    })

});