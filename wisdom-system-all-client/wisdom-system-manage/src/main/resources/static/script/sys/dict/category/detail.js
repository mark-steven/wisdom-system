require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_sys_dict_category_detail_callback',
            formId: 'frmSysDictCategoryDetail',
            getUrl: 'api/sys/dict/category/{id}',
            addUrl: 'api/sys/dict/category',
            updateUrl: 'api/sys/dict/category',
            initBindDataEvent: function () {
                var initData = common.getParamFromUrl2Json();
                return initData;
            },
            switchBtnParams: {
                itemShowType: {
                    onValue: 1,
                    offValue: 0
                }
            }
        });
    })
});