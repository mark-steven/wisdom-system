require(['baseLayout', 'common', 'request'], function (BaseLayout, common, Request) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: "角色管理",
                    attachId: 'treeGrid',
                    attachType: 'grid',
                    arrow: 'left',
                    width: 400,
                    isAttachToolbar: false,
                    gridParam: {
                        target: 'treeGrid',
                        ajax: {
                            url: "api/sys/dict/item/page/list?fkDictCategoryCode=ROLE_CATEGORY"
                        },
                        columns: [
                            {display: '角色编码', name: 'code', width: 120, align: 'center'},
                            {display: '角色名称', name: 'name', width: 180, align: 'left'}
                        ],
                        showType: 'tree',
                        tree: {
                            simpleData: {
                                textField: 'code'
                            }
                        },
                        isUsePage: false,
                        row: {
                            selectIndex: 0,
                            onSelect: function () {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    }
                },
                b: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {
                            btnCustomAdd: {
                                id: 'btnCustomAdd',
                                type: 'button',
                                text: '新增',
                                img: 'add.png'
                            },
                            'add': false,
                            'edit': false,
                            'del': true
                        },
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    return {};
                                } else {
                                    return {};
                                }
                            }
                        },
                        delParam: {
                            ajax: {
                                url: "api/sys/role/user"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: false,
                        target: 'grid',
                        ajax: {
                            url: "api/sys/role/user/page/list",
                            data: function () {
                                if (baseLayout) {
                                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (rowData) {
                                        return {
                                            fkRoleId: rowData["id"]
                                        }
                                    } else {
                                        return {};
                                    }
                                }
                            }
                        },
                        columns: [
                            {display: '用户姓名', name: 'userName', width: 120, align: 'left', fixed: 'left'},
                            {display: '登录账号', name: 'loginCode', width: 120, align: 'left', fixed: 'left'},
                            {display: '用户邮箱', name: 'email', width: 150, align: 'center'},
                            {display: '手机号码', name: 'phone', width: 150, align: 'center'},
                            {
                                display: '性别', name: 'sex', width: 80, align: 'center',
                                render: function (data) {
                                    switch (data["sex"]) {
                                        case 0:
                                            return "女";
                                        case 1:
                                            return "男";
                                    }
                                }
                            },
                            {
                                display: '用户状态', width: 80, align: 'center', name: 'state',
                                renderItems: {
                                    'btnSwitch': {
                                        url: "api/sys/user/{fkUserId}/{state}",
                                        onValue: 1,
                                        offValue: 0,
                                        disable: false,
                                        confirmMsg: '是否修改当前用户状态?'
                                    }
                                }
                            },
                            {display: '注册时间', name: 'addTime', width: 140, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center', fixed: 'right',
                                renderItems: {
                                    'del': {
                                        url: "api/sys/role/user/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_sys_dict_item_detail_callback",
                        all: {
                            href: "sys/dict/item/detail.html",
                            area: ["325px", "300px"]
                        },
                        add: {
                            title: '新增项信息'
                        },
                        edit: {
                            title: '编辑项信息',
                            titleKey: 'name'
                        }
                    }
                }
            }
        });
        var btnCustomAdd = baseLayout.params.cells["b"].toolbarObj.getItem("btnCustomAdd");
        common.initDialogSelect({
            id: {
                type: 'btn',
                target: btnCustomAdd,
                textName: 'name',//跟实体对应
                valueName: 'id',
                data: function () {
                    var rightData = [];
                    var fkRoleId = "";
                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                    if (rowData) {
                        fkRoleId = rowData["id"]
                    }
                    var req = new Request("api/sys/role/user/list");
                    req.get({
                        data: function () {
                            return {
                                fkRoleId: fkRoleId
                            }
                        },
                        success: function (data) {
                            $.each(data.result, function (index, row) {
                                rightData.push({id: row["fkUserId"], name: row["userName"]});
                            });
                        }
                    })

                    return rightData;
                },
                callback: function (data) {
                    var sysRoleUser = [];
                    var fkRoleId = "";
                    // var nodeData = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                    var rowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                    if (rowData) {
                        fkRoleId = rowData["id"]
                    }
                    $.each(data, function (index, item) {
                        sysRoleUser.push({fkUserId: item["id"], fkRoleId: fkRoleId})
                    });
                    var req = new Request("api/sys/role/user/list/{fkRoleId}", {fkRoleId: fkRoleId});
                    req.put({
                        data: sysRoleUser,
                        isStringify: true,
                        success: function (data) {
                            if (data["code"] == "0") {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    })

                },
                urlParam: function () {
                    return {
                        partName: 'user',
                        checkbox: 2
                    };
                }
            }
        });

    });
});
