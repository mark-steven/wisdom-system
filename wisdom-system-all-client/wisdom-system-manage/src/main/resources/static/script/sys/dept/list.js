require(['baseLayout'], function (BaseLayout) {
    $(function () {
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        searchers: {
                            fkDeptTypeCode: {
                                label: '公司名称：',
                                type: "select",
                                name: 'fkDeptTypeCode',
                                ajaxParam: {
                                    url: "api/sys/dict/item/list",
                                    data: {
                                        fkDictCategoryCode: "DEPT_TYPE"
                                    },
                                    resultRender: function (data) {
                                        data["result"].unshift({
                                            "name": "全部",
                                            "code": '',
                                        })
                                        return data["result"];
                                    }
                                },
                                placeholder: '---请选择公司---',
                                selectValueField: 'code',
                                selectTextField: 'name',
                                defaultSelectValue: 0,
                                defaultSelectType: 'index',//初
                                dataAction: 'server',
                                width: 120,
                                onChange: function (value, text) {
                                    if (baseLayout) {
                                        baseLayout.params.cells['a'].gridObj.initLoadData();
                                    }
                                }
                            },
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        buttons: {"add": true, "edit": true, "del": true},
                        delParam: {
                            ajax: {
                                url: "api/sys/department"
                            }
                        }
                    },
                    gridParam: {
                        ajax: {
                            url: "api/sys/department/page/list"
                        },
                        columns: [
                            {display: '部门编码', name: 'code', width: 260, align: 'center'},
                            {display: '公司名称', name: 'deptTypeName', width: 150, align: 'center'},
                            {display: '部门名称', name: 'name', width: 100, align: 'center'},
                            {display: '排序', name: 'sort', width: 80, align: 'center'},
                            {
                                display: '状态', width: 80, align: 'center', name: 'state',
                                renderItems: {
                                    'btnSwitch': {
                                        url: "api/sys/department/{id}/{state}",
                                        onValue: 1,
                                        offValue: 0,
                                        disable: false,
                                        confirmMsg: '是否修改当前状态?'
                                    }
                                }
                            },
                            {
                                display: '编辑', width: 60, align: 'center', fixed: 'right',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/sys/department/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        isUsePage: true,
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_sys_dept_detail_callback",
                        all: {
                            href: "sys/dept/detail.html",
                            area: ["632px", "400px"]
                        },
                        add: {
                            title: '新增部门信息'
                        },
                        edit: {
                            title: '编辑部门信息',
                            reqInfoKey: 'name'
                        }
                    }
                }
            }
        })
    })
});
