/**
 *  @author   ghh
 *  @version  1.0
 */
require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: "系统参数分类",
                    attachId: 'grid',
                    attachType: 'grid',
                    width: 500,
                    arrow: 'left',
                    isAttachToolbar: true,
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, 'del': true},
                        delParam: {
                            enableDel: function () {
                                var rowCount = baseLayout.params.cells["b"].gridObj.getRowCountInPage();
                                if (rowCount > 0) {
                                    common.errorMsg("参数分类存在相关分组,不能删除！");
                                    return false;
                                }
                                return true;
                            },
                            ajax: {
                                url: 'api/dev/param/category'
                            }
                        }
                    },
                    gridParam: {
                        ajax: {
                            url: 'api/dev/param/category/page/list'
                        },
                        columns: [
                            {display: '分类编码', name: 'code', width: 160, align: 'left'},
                            {display: '分类名称', name: 'name', width: 120},
                            {display: '排序', name: 'sort', width: 80, align: 'center'}
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0,
                            onSelect: function (id) {
                                if (baseLayout) {
                                    baseLayout.params.cells["b"].gridObj.loadData();
                                }
                            }
                        }
                    },
                    winParam: {
                        winCallback: 'win_dev_param_category_detail_callback',
                        titleKey: 'name',
                        isCutTitle: true,
                        all: {
                            href: "dev/param/category/detail.html",
                            area: ["320px", "260px"]
                        },
                        add: {
                            title: '新增系统参数分类'
                        },
                        edit: {
                            title: '编辑字典',
                            reqInfoKey: 'id'
                        }
                    }
                },
                b: {
                    attachId: 'grid1',
                    attachType: 'grid',
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, 'del': true},
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var paramCategoryRowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (paramCategoryRowData) {
                                        return {
                                            fkParamCategoryCode: paramCategoryRowData["code"]
                                        };
                                    }
                                } else {
                                    return {};
                                }
                            }
                        },
                        editParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var paramCategoryRowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (paramCategoryRowData) {
                                        return {
                                            fkParamCategoryCode: paramCategoryRowData["code"]
                                        };
                                    }
                                } else {
                                    return {};
                                }
                            }
                        },
                        delParam: {
                            ajax: {
                                url: "api/dev/param/group"
                            }
                        }
                    },
                    gridParam: {
                        isInitLoadData: false,
                        target: 'grid1',
                        ajax: {
                            url: "api/dev/param/group/page/list",
                            data: function () {
                                if (baseLayout) {
                                    var paramCategoryRowData = baseLayout.params.cells["a"].gridObj.getSelectedRow();
                                    if (paramCategoryRowData) {
                                        return {
                                            fkParamCategoryCode: paramCategoryRowData["code"]
                                        };
                                    }
                                } else {
                                    return {};
                                }
                            }
                        },
                        columns: [
                            {display: '分组编码', name: 'code', width: 180, align: 'center'},

                            {display: '分组名称', name: 'name', width: 200, align: 'center'},
                            {display: '排序', name: 'sort', width: 60, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/dev/param/group/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list'
                    },
                    winParam: {
                        winCallback: "win_dev_param_group_detail_callback",
                        all: {
                            href: "dev/param/group/detail.html",
                            area: ["325px", "260px"]
                        },
                        add: {
                            title: '新增参数分组'
                        },
                        edit: {
                            title: '编辑参数分组',
                            titleKey: 'name'
                        }
                    }
                }
            }
        });
    })
});
