require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var sqlConfigContentEditor;

        //设置插件路径
        require.config({paths: {'vs': GLOBAL_RESOURCE_PATH + '/lib/monaco-editor/min/vs'}});
        //绑定对象
        require(['vs/editor/editor.main'], function () {
            sqlConfigContentEditor = monaco.editor.create(document.getElementById('sql-config-content-editor'), {
                language: 'sql'
            });
            monaco.editor.setTheme("vs-dark");
            dialogInit()
        })
        var dialogInit = function () {
            var initData = common.getParamFromUrl2Json();
            new DialogDetail({
                winCallback: 'win_dev_excel_output_detail_callback',
                formId: 'frmExcelOutputDetail',
                getUrl: 'api/dev/excel/output/{id}',
                addUrl: 'api/dev/excel/output',
                updateUrl: 'api/dev/excel/output',
                beforeBindEvent: function (data) {
                    if (data["sqlConfigContent"]) {
                        sqlConfigContentEditor.setValue(data["sqlConfigContent"]);
                    }
                    return data;
                },
                beforeSaveEvent: function (data) {
                    data["sqlConfigContent"] = sqlConfigContentEditor.getValue();
                    return data;
                }
            })
        };
    })
});
