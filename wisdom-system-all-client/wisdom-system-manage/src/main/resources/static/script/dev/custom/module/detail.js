/**
 * 系统模块参数详情
 *
 * @author guohonghui
 * @version 1.0
 */
require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var configContentEditor;
        var dialogInit = function () {
            var initData = common.getParamFromUrl2Json();
            new DialogDetail({
                winCallback: 'win_dev_custom_module_detail_callback',
                formId: 'frmDevCustomModuleDetail',
                getUrl: 'api/dev/custom/module/{id}',
                addUrl: 'api/dev/custom/module',
                updateUrl: 'api/dev/custom/module',
                beforeBindEvent: function (data) {
                    if (data["configContent"]) {
                        configContentEditor.setValue(data["configContent"]);
                    }
                    return data;
                },
                beforeSaveEvent: function (data) {
                    data["configContent"] = configContentEditor.getValue();
                    return data;
                },
                switchBtnParams: {
                    state: {
                        onValue: 1,
                        offValue: 0
                    }
                },
                selectParams: {
                    fkDeptCode: {
                        id: 'selDept',
                        value: 'fkDeptCode',
                        text: 'fkDeptName',
                        isInitLoadData: true,
                        ajaxParam:{
                                url: "api/sys/department/list",//select 获取列表的url
                                data:{fkDeptTypeCode: "01"},
                                resultRender: function (data) {
                                    var result = data["result"];
                                    result.unshift({code: "", name: "全部"})
                                    return data["result"];
                                }
                            },
                        selectValueField: 'code',
                        selectTextField: 'name',
                        dataAction: 'server',
                        defaultSelectValue: initData["fkDeptCode"] ? initData["fkDeptCode"] : "",
                        defaultSelectType: 'value',
                        width: 180
                    }
                }
            })
        };
        //设置插件路径
        require.config({paths: {'vs': GLOBAL_RESOURCE_PATH + '/lib/monaco-editor/min/vs'}});
        //绑定对象
        require(['vs/editor/editor.main'], function () {
            //表格参数配置
            configContentEditor = monaco.editor.create(document.getElementById('config-content-editor'), {
                language: 'javascript',
                value: ''
            });
            monaco.editor.setTheme("vs-dark");
            dialogInit()
        })
    })
});