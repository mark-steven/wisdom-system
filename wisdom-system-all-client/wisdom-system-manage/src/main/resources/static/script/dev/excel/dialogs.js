require(['dialogDetail', 'baseLayout', 'common', 'request'], function (DialogDetail, BaseLayout, common, Request) {
    var baseLayout = new BaseLayout({
        pattern: '4L',
        cells: {
            a: {
                title: "Excel导入配置",
                attachId: 'frmExcelConfigDetail',
                attachType: 'other',
                isAttachToolbar: false,
                width: 320
            },
            b: {
                title: "列信息",
                attachId: 'grid',
                attachType: 'grid',
                isAttachToolbar: true,
                toolbarParam: {
                    buttons: {
                        'add': false,
                        'edit': false,
                        'del': false,
                        'btnCustomAdd': {
                            id: 'btnCustomAdd',
                            type: 'button',
                            text: '生成列',
                            img: 'add.png',
                            onClick: function () {
                                var tableName = $("#frmExcelConfigDetail").find("input[name='tableName']").val();
                                var req = new Request("api/dev/excel/import/rule/columns/{tableName}", {tableName: tableName});
                                req.get({
                                    success: function (data) {
                                        baseLayout.params.cells["b"].gridObj.loadData(data.result);
                                    }
                                })

                            }
                        }
                    }
                },
                gridParam: {
                    dataAction: 'local',
                    columns: [
                        {display: '列名', name: 'colName', width: 180},
                        {display: '列说明', name: 'colDescr', width: 180, align: 'left'},
                        {display: '数据类型', name: 'colDataType', width: 120},
                        {display: '是否可空', name: 'isNullable', width: 120, align: 'center'},
                        {
                            display: 'Excel列索引', name: 'excelColIndex', width: 120,
                            align: 'center',
                            isEdit: true,
                            editor: {
                                stateType: 1,
                                type: 'text'
                            }
                        }
                    ],
                    showType: 'list',
                    row: {
                        checkbox: 0,
                        onSelect: function (rowData) {
                            // alert(JSON.stringify(rowData));
                        }
                    },
                    isUsePage: false
                }
            },
            c: {
                title: "列值转换规则",
                attachId: 'panel-column-value-convert-monaco-editor',
                attachType: 'other',
                width: 350,
                isAttachToolbar: false
            },
            d: {
                title: "列值验证规则",
                attachId: 'panel-column-value-verify-monaco-editor',
                attachType: 'other',
                width: 350,
                isAttachToolbar: false
            }
        }
    });
    var aTab = new dhtmlXTabBar("panel-column-value-convert-monaco-editor");
    aTab.addTab("a1", "验证转换规则", null, null, true, false);
    // aTab.attach
    aTab.tabs("a1").attachObject("convert-monaco-editor");
    var bTab = new dhtmlXTabBar("panel-column-value-verify-monaco-editor");
    bTab.addTab("b1", "验证转换规则", null, null, true, false);
    bTab.tabs("b1").attachObject("verify-monaco-editor");

    var convertMonacoEditor, verifyMonacoEditor;
    //设置插件路径
    require.config({paths: {'vs': GLOBAL_RESOURCE_PATH + '/lib/monaco-editor/min/vs'}});
    //绑定对象
    require(['vs/editor/editor.main'], function () {
        convertMonacoEditor = monaco.editor.create(document.getElementById('convert-monaco-editor'), {
            language: 'json',
            value: '{}'
        });
    });
    new DialogDetail({
        winCallback: 'win_dev_excel_config_detail_callback',
        formId: 'frmExcelConfigDetail',
        getUrl: 'api/dev/excel/import/config/{id}',
        addUrl: 'api/dev/excel/import/config',
        updateUrl: 'api/dev/excel/import/config',
        beforeSaveEvent: function (data) {
            data["devExcelImportRuleList"] = baseLayout.params.cells["b"].gridObj.getData();
            return data;
        },
        beforeBindEvent: function (data) {
            baseLayout.params.cells["b"].gridObj.loadData({
                list: data["devExcelImportRuleList"], total: data["devExcelImportRuleList"].length
            });
            return data;
        },
        selectParams: {
            tableName: {
                id: 'selTable',
                value: 'tableName',
                text: 'tableName',
                isInitLoadData: true,
                ajaxParam: {
                    url: "api/dev/table/list"//select 获取列表的url
                },
                selectValueField: 'name',
                selectTextField: 'name',
                dataAction: 'server',
                defaultSelectType: 'value',
                width: 180,
                placeholder: '请选择导入数据的表！'
            }
        }
    })

});