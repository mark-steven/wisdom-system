/**
 *  参数配置列表
 *
 *  @author   GuoHonghui
 *  @version  1.0
 */
require(['baseLayout'], function (BaseLayout) {
    $(function () {
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        //增、删、改、查等按钮
                        buttons: {'add': true, 'edit': true, 'del': true},
                        //搜索
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/dev/param/config"
                            }
                        }
                    },
                    // 列表
                    gridParam: {
                        target: 'grid',
                        ajax: {
                            url: "api/dev/param/config/page/list"
                        },
                        columns: [
                            {display: '标签名称', name: 'tagName', width: 120, align: 'left', fixed: 'left'},
                            {display: 'ParamKey', name: 'paramKey', width: 160, align: 'left', fixed: 'left'},
                            {
                                display: '控件类型',
                                name: 'type',
                                width: 100,
                                align: 'center',
                                render: function (rowData, value) {
                                    var pluginType = {"1": "input", '2': "select", "3": "textarea", "4": "switch"};
                                    return pluginType[value];
                                }
                            },
                            {display: '分类', name: 'paramCategoryName', width: 120, align: 'left'},
                            {display: '分组', name: 'paramGroupName', width: 120, align: 'left'},
                            {display: '排序', name: 'sort', width: 60, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/dev/param/config/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_dev_param_config_callback",
                        all: {
                            href: "dev/param/config/detail.html",
                            area: ["620px", "520px"]
                        },
                        add: {
                            title: '新增参数配置信息'
                        },
                        edit: {
                            title: '编辑参数配置信息',
                            titleKey: 'tagName'
                        }
                    }
                }
            }
        })
    });
})
