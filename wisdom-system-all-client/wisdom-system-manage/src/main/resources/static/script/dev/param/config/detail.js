require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var monacoEditor;
        var dialogInit = function () {
            var dialogDetail = new DialogDetail({
                winCallback: 'win_dev_param_config_callback',
                formId: 'frmDevParamConfigDetail',
                getUrl: 'api/dev/param/config/{id}',
                addUrl: 'api/dev/param/config',
                updateUrl: 'api/dev/param/config',
                beforeBindEvent: function (data) {
                    monacoEditor.setValue(data["rule"] || '{}');
                    return data;
                },
                beforeSaveEvent: function (data) {
                    data["rule"] = monacoEditor.getValue();
                    return data;
                },
                selectParams: {
                    type: {
                        id: 'selType',
                        value: 'type',
                        dataAction: 'local',
                        placeholder: '请选择控件类型',
                        data: [
                            {text: "input", value: '1'},
                            {text: "select", value: '2'},
                            {text: "textarea", value: '3'},
                            {text: "switch", value: '4'}
                        ],
                        selectValueField: 'value',
                        selectTextField: 'text',
                        defaultSelectType: 'value'
                    },
                    fkParamCategoryCode: {
                        id: 'selParamCategory',
                        value: 'fkParamCategoryCode',
                        isInitLoadData: true,
                        ajaxParam: {
                            url: "api/dev/param/category/list"//select 获取列表的url
                        },
                        text: 'paramCategoryName',
                        selectValueField: 'code',
                        selectTextField: 'name',
                        dataAction: 'server',
                        defaultSelectType: 'value',
                        width: 180,
                        placeholder: '请选择分类',
                        onChange: function (value, text) {
                            if (dialogDetail) {
                                dialogDetail.selLoadServerData('selParamConfigGroup');
                            }
                        }
                    },
                    fkParamGroupCode: {
                        id: 'selParamConfigGroup',
                        value: 'fkParamGroupCode',
                        isInitLoadData: false,
                        text: 'paramGroupName',
                        ajaxParam: {
                            url: "api/dev/param/group/list",//select 获取列表的url
                            data: function () {
                                if (dialogDetail && dialogDetail["selParamCategory"]) {
                                    var fkParamCategoryCode = dialogDetail["selParamCategory"].getSelectedValue();
                                    return {fkParamCategoryCode: fkParamCategoryCode};
                                } else {
                                    return {};
                                }
                            }
                        },
                        selectValueField: 'code',
                        selectTextField: 'name',
                        dataAction: 'server',
                        defaultSelectType: 'value',
                        width: 180,
                        placeholder: '请选择分组'

                    }
                }

            });
        }
        //设置插件路径
        require.config({paths: {'vs': GLOBAL_RESOURCE_PATH + '/lib/monaco-editor/min/vs'}});
        //绑定对象
        require(['vs/editor/editor.main'], function () {
            monacoEditor = monaco.editor.create(document.getElementById('monaco-editor'), {
                language: 'json',
                value: '{}'
            });
            monaco.editor.setTheme("vs-dark");
            dialogInit();
        });
    })
});