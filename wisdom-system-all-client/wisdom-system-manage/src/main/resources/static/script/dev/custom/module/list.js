/**
 *  自定义模块列表
 *
 *  @author   GuoHonghui
 *  @version  1.0
 */
require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        var baseLayout = new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: '部门列表',
                    attachId: 'tree',
                    attachType: 'tree',
                    treeId: 'treeId',
                    width: 200,
                    arrow: 'left',
                    isAttachToolbar: false,
                    treeParam: {
                        selectIndex: 1,
                        ajax: {
                            url: "api/sys/tree/dept",
                            data: {"state": 1, code: "01", fkDeptTypeCode: "01"}
                        },
                        renderData: function (data) {
                            var result = data["result"];
                            for (var i = 0; i < result.length; i++) {
                                var res = result[i];
                                if (res["type"] == 2) {
                                    res["icon"] = GLOBAL_RESOURCE_PATH + "/image/icons/dept.png"
                                } else if (res['type'] == 3) {
                                    res["icon"] = GLOBAL_RESOURCE_PATH + "/image/icons/consulting.png"
                                }
                            }
                            return result;
                        },
                        zTree: {
                            view: {
                                dblClickExpand: false,
                                showLine: true,
                                selectedMulti: false,
                                autoCancelSelected: true
                            }
                        },
                        type: {
                            template: "<div style='height: 32px;line-height: 32px;padding-left: 10px;'>#name#</div>",
                            height: 'auto',
                            padding: 5
                        },
                        onClick: function (isSelected, treeNode, treeId) {
                            if (baseLayout) {
                                baseLayout.params.cells["b"].gridObj.loadData();
                            }
                        }
                    }
                },
                b: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        //增、删、改、查等按钮
                        buttons: {'add': true, 'edit': true, 'del': true},
                        //搜索
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/dev/custom/module"
                            }
                        },
                        addParam: {
                            urlParam: function () {
                                if (baseLayout) {
                                    var selectNodes = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                                    if (selectNodes && selectNodes.length > 0) {
                                        return {
                                            fkDeptCode: selectNodes[0]["code"]
                                        };
                                    } else {
                                        return [];
                                    }
                                }
                            }
                            /*enableAdd: function () {
                                if (baseLayout) {
                                    var selectNodes = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                                    if (selectNodes && selectNodes.length > 0 && selectNodes[0]["type"] == '3') {
                                        return true;
                                    } else {
                                        common.errorMsg("请先选择左侧树需要配置的部门。");
                                        return false;
                                    }
                                }
                                return false;
                            }*/
                        }
                    },
                    // 列表
                    gridParam: {
                        target: 'grid',
                        isInitLoadData: false,
                        ajax: {
                            url: "api/dev/custom/module/page/list",
                            data: function () {
                                if (baseLayout) {//诊室查询（通过归属部门来查）
                                    var selectNodes = baseLayout.params.cells["a"].treeObj.getSelectedNodes();
                                    if (selectNodes && selectNodes.length > 0) {
                                        return {
                                            fkDeptCode: selectNodes[0]["code"]
                                        };
                                    }
                                }
                            }
                        },
                        columns: [
                            {display: '模块编码', name: 'code', width: 160, align: 'center', fixed: 'left'},
                            {display: '模块名称', name: 'name', width: 220, align: 'center', fixed: 'left'},
                            {display: '排序', name: 'sort', width: 80, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/dev/custom/module/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_dev_custom_module_detail_callback",
                        all: {
                            href: "dev/custom/module/detail.html",
                            area: ['660px', '500px']
                        },
                        add: {
                            title: '模块自定义配置'
                        },
                        edit: {
                            title: '模块自定义配置',
                            titleKey: 'name'
                        }
                    }
                }
            }
        })
    });
})
