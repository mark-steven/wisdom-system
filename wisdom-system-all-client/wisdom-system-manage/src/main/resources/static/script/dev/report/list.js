require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        var baseLayout = new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        //增、删、改、查等按钮
                        buttons: {'add': true, 'edit': true, 'del': true},
                        //搜索
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/dev/report/config"
                            }
                        }
                    },
                    // 列表
                    gridParam: {
                        target: 'grid',
                        ajax: {
                            url: "api/dev/report/config/page/list"
                        },
                        columns: [
                            {display: '报表编码', name: 'code', width: 220, align: 'left'},
                            {display: '报表名称', name: 'name', width: 260, align: 'left'},
                            {display: '排序', name: 'sort', width: 80, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/dev/report/config/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_dev_report_config_detail_callback",
                        all: {
                            href: "dev/report/detail.html",
                            area: ['660px', '500px']
                        },
                        add: {
                            title: '模块自定义配置'
                        },
                        edit: {
                            title: '模块自定义配置',
                            titleKey: 'name'
                        }
                    }
                }
            }
        })
    });
});
