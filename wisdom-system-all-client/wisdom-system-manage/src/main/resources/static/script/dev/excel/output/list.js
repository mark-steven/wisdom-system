require(['baseLayout', 'common'], function (BaseLayout, common) {
    $(function () {
        new BaseLayout({
            cells: {
                a: {
                    attachId: 'grid',
                    attachType: 'grid',
                    toolbarParam: {
                        //增、删、改、查等按钮
                        buttons: {'add': true, 'edit': true, 'del': true},
                        //搜索
                        searchers: {
                            keyword: {
                                label: "关键字：",
                                name: 'keyword',
                                type: "text",
                                width: 150
                            },
                            btnSearch: true
                        },
                        delParam: {
                            ajax: {
                                url: "api/dev/excel/output"
                            }
                        }
                    },
                    // 列表
                    gridParam: {
                        target: 'grid',
                        ajax: {
                            url: "api/dev/excel/output/page/list"
                        },
                        columns: [
                            {display: 'Excel编码', name: 'code', width: 150, align: 'center'},
                            {display: 'Excel名称', name: 'name', width: 120, align: 'center'},
                            {display: '标题', name: 'title', width: 120, align: 'center'},
                            {display: '脚注', name: 'sheet', width: 120, align: 'center'},
                            {display: '排序', name: 'sort', width: 80, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'download':{
                                        title :function (jqContent) {
                                            var printText = "导出";
                                            $('span').replaceWith(function(){
                                                return $("<a />", {html: $(this).html(), class:$(this).attr('class')});
                                            });
                                        }
                                    },
                                    'edit': true,
                                    'del': {
                                        url: "api/dev/excel/output/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0
                        }
                    },
                    winParam: {
                        winCallback: "win_dev_excel_output_detail_callback",
                        all: {
                            href: "dev/excel/output/detail.html",
                            area: ['660px', '500px']
                        },
                        add: {
                            title: '模块自定义配置'
                        },
                        edit: {
                            title: '模块自定义配置',
                            titleKey: 'name'
                        }
                    }
                }
            }
        })
    });
})
