require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        var pageConfigContentEditor;
        var sqlConfigContentEditor;

        //设置插件路径
        require.config({paths: {'vs': GLOBAL_RESOURCE_PATH + '/lib/monaco-editor/min/vs'}});
        //绑定对象
        require(['vs/editor/editor.main'], function () {
            //表格参数配置
            pageConfigContentEditor = monaco.editor.create(document.getElementById('page-config-content-editor'), {
                language: 'javascript'
            });
            sqlConfigContentEditor = monaco.editor.create(document.getElementById('sql-config-content-editor'), {
                language: 'sql'
            });
            monaco.editor.setTheme("vs-dark");
            dialogInit()
        })
        var dialogInit = function () {
            var initData = common.getParamFromUrl2Json();
            new DialogDetail({
                winCallback: 'win_dev_report_config_detail_callback',
                formId: 'frmDevReportDetail',
                getUrl: 'api/dev/report/config/{id}',
                addUrl: 'api/dev/report/config',
                updateUrl: 'api/dev/report/config',
                beforeBindEvent: function (data) {
                    if (data["pageConfigContent"]) {
                        pageConfigContentEditor.setValue(data["pageConfigContent"]);
                    }
                    if (data["sqlConfigContent"]) {
                        sqlConfigContentEditor.setValue(data["sqlConfigContent"]);
                    }
                    return data;
                },
                beforeSaveEvent: function (data) {
                    data["pageConfigContent"] = pageConfigContentEditor.getValue();
                    data["sqlConfigContent"] = sqlConfigContentEditor.getValue();
                    return data;
                }
            })
        };
    })
});