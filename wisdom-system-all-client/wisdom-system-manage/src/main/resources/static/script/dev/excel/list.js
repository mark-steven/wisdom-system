/**
 *  @author   ghh
 *  @version  1.0
 */
require(['baseLayout', 'common', 'request', 'dialogDetail','hciMask'], function (BaseLayout, common, Request, DialogDetail,HciMask) {
    $(function () {
        new BaseLayout({
            pattern: '2U',
            cells: {
                a: {
                    title: "Excel导入配置",
                    attachId: 'grid',
                    attachType: 'grid',
                    width: 500,
                    isAttachToolbar: true,
                    toolbarParam: {
                        buttons: {'add': true, 'edit': true, 'del': true},
                        delParam: {
                            ajax: {
                                url: "api/dev/excel/import/config"
                            }
                        }
                    },
                    gridParam: {
                        ajax: {
                            url: 'api/dev/excel/import/config/page/list'
                        },
                        columns: [
                            {display: '调用编码', name: 'code', width: 180},
                            {display: '业务名称', name: 'name', width: 160, align: 'center'},
                            {display: '表名', name: 'tableName', width: 180,align: 'center'},
                            {display: '开始行数', name: 'startNum', width: 80, align: 'center'},
                            {display: '排除尾行数', name: 'withoutEndNum', width: 80, align: 'center'},
                            {
                                display: '操作', width: 60, align: 'center',
                                renderItems: {
                                    'edit': true,
                                    'del': {
                                        url: "api/dev/excel/import/config/{id}"
                                    }
                                }
                            }
                        ],
                        showType: 'list',
                        row: {
                            selectIndex: 0,
                            onSelect: function (id) {

                            }
                        }
                    },
                    winParam: {
                        winCallback: 'win_dev_excel_config_detail_callback',
                        titleKey: 'name',
                        isCutTitle: true,
                        all: {
                            href: "dev/excel/config/detail.html",
                            area: ["95%", "90%"]
                        },
                        add: {
                            title: 'Excel导入配置'
                        },
                        edit: {
                            title: 'Excel导入配置',
                            titleKey: 'name'
                        }
                    }
                },
                b: {
                    attachId: 'import-excel-file',
                    attachType: "other",
                    width: 320,
                    isAttachToolbar: false,
                    arrow: 'right',
                    title: 'Excel导入',
                    collapse: false//true隐藏，false默认打开
                }
            }
        });

        new DialogDetail({
            formId: 'import-excel-detail',
            selectParams: {
                code: {
                    id: 'selExcel',
                    value: 'code',
                    text: 'name',
                    isInitLoadData: true,
                    ajaxParam: {
                        url: "api/dev/excel/import/config/list"
                    },
                    selectValueField: 'code',
                    selectTextField: 'name',
                    dataAction: 'server',
                    width: 185,
                    placeholder: '--请选择调用业务名称--'
                }
            },
            isDialog: false
        });

        $("#btn-add").click(function () {
            //获取from表单
            var data = $("#import-excel-detail").form2json();
            //判断调用编码是否为空
            if (!data['code']) {
                return common.errorMsg('调用业务名称不能为空！')
            }
            //获取上传文件
            var fileInput = $('#file').get(0).files[0];
            //判断是否有上传文件
            if (!fileInput) {
                return common.errorMsg('上传文件不能为空！')
            }
            //判断是否为excel文件
            var excel = $("#file").val();
            if (excel != null) {
                var reg = /^.*\.(?:xls|xlsx)$/i;//文件名可以带空格
                if (!reg.test(excel)) {//校验不通过
                    return common.errorMsg("你上传的不是excel文件,请上传excel格式的文件!")
                }
            }
            var file = document.getElementById("file").files[0];//获取excel文件
            var formData = new FormData();// FormData 对象
            formData.append("file", file);// 文件对象
            var mask = null;
            var req = new Request('api/dev/excel/import/data/{code}', {code: data['code']});
            req.ajax({
                //进度条
                beforeSend: function () {
                    var jqBody = $("body");
                    mask = new HciMask(jqBody, {
                        imgPath: GLOBAL_RESOURCE_PATH + "/script/core/ext/images/",
                        text: "正在上传Excel文件..."
                    });
                    mask.show();
                },
                type: 'POST',//上传方式
                cache: false,//不缓存
                data: formData,//上传formData封装的数据
                isErrorTip: false,//是否提示错误消息
                processData: false,//jQuery不要去处理发送的数据
                contentType: false, //jQuery不要去设置Content-Type请求头
                success: function (data) {
                    mask.hide();
                    if (data.code == 0) {
                        common.successMsg("成功导入" + data.result + "条数据！");
                    } else {
                        common.successMsg("导入失败或重复导入数据,请移步数据库表查看是否已经导入过数据！");
                    }
                }
            })
        });

        $('#import-excel-detail').find("input").bind('keyup', function (event) {
            if (event.keyCode == "13") {
                //回车执行查询
                $("#btn-add").click();
            }
        });
    })
});
