/**
 *
 *  @author   ghh
 *  @version  1.0
 */
require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_dev_param_group_detail_callback',
            formId: 'frmDevParamGroupDetail',
            getUrl: 'api/dev/param/group/{id}',
            addUrl: 'api/dev/param/group',
            updateUrl: 'api/dev/param/group',
            beforeSaveEvent: function (data) {
                var initData = common.getParamFromUrl2Json();
                data["fkParamCategoryCode"] = initData["fkParamCategoryCode"];
                return data;
            }
        });
    })
});