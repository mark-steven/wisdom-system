/**
 *
 *  @author   ghh
 *  @version  1.0
 */
require(['common', 'dialogDetail'], function (common, DialogDetail) {
    $(function () {
        new DialogDetail({
            winCallback: 'win_dev_param_category_detail_callback',
            formId: 'frmDevParamCategoryDetail',
            getUrl: 'api/dev/param/category/{id}',
            addUrl: 'api/dev/param/category',
            updateUrl: 'api/dev/param/category'
        });
    })
});