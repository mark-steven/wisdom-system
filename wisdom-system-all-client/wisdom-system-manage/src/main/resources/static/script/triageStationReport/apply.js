
require(['common', 'baseLayout','request'], function (common, BaseLayout,Request) {
    $(function () {
        var code = common.getParamFromUrl("code");
        var paramJson = common.getParamFromUrl("paramJson");
        var req = new Request('api/dev/report/config/code/'+code);
        req.get({
            success: function (data) {
                if (data!=null&&data.code=='0') {
                    var pageConfigContent=data.result.pageConfigContent;
                    var pageConfigContentObj=JSON.parse(pageConfigContent,function(k,v){
                        if(v.indexOf&&v.indexOf('function')>-1){
                            return eval("(function(){return "+v+" })()")
                        }
                        return v;
                    });
                    debugger
                    var url="api/dev/report/config/apply?code="+code;
                    pageConfigContentObj.cells.a.gridParam.ajax.url=url;
                    pageConfigContentObj.cells.a.gridParam.ajax.data={"paramJson":paramJson};
                    var baseLayout=new BaseLayout(pageConfigContentObj);
                    baseLayout.params.cells.a.gridParam.ajax.data = function () {
                        var data={};
                        var searcherKeys = [];
                        var searchers = baseLayout.params.cells.a.toolbarParam.searchers;
                        $.each(searchers, function (searcherIndex) {
                            if (searcherIndex != "btnSearch") {
                                searcherKeys.push(searcherIndex);
                            }
                        });
                        $.each(searcherKeys, function (keyIndex, keyInfo) {
                            var searcherInfo = baseLayout.params.cells.a.toolbarParam.searchers[keyInfo];
                            switch (searcherInfo["type"]) {
                                case "text":
                                case "date":
                                    var searchValue = baseLayout.params.cells.a.toolbarObj.getValue(keyInfo);
                                    searchValue = $.trim(searchValue);
                                    data[keyInfo] = searchValue;
                                    break;
                                case "select":
                                    var selId = keyInfo;
                                    data[keyInfo] = baseLayout.params.cells.a[selId].getSelectedValue();
                                    break;
                            }
                        });
                        debugger
                        $.extend(data, JSON.parse(paramJson));
                        var params={"paramJson":JSON.stringify(data)};
                        return params;
                    };

                }
            }
        });

    })
});
