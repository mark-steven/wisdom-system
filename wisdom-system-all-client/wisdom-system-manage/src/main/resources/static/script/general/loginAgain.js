/**
 *
 *  @author   ghh
 *  @version  1.0
 */
require(['common'], function (common) {
    var internal = {
        validForm: null,
        init: function () {
            internal.validForm = common.initValidForm($("#frm-login-again"));//验证表达
            var top = common.getTopWindowDom();
            top.win_login_again_callback = function (dialogIndex) {
                if (internal.validForm.check()) {
                    var formData = $("#frm-login-again").form2json();
                    $.ajax({
                        type: "post",
                        url: GLOBAL_API_PATH + "/oauth/token",
                        data: {
                            "username": formData["username"],
                            "password": formData["password"],
                            "grant_type": "password",
                            "scope": "select",
                            "client_id": "client",
                            "client_secret": "123456"
                        },
                        success: function (data) {
                            $.cookie("access_token","");
                            if (data["access_token"]) {
                                var access_token = data["access_token"];
                                $.cookie("access_token", access_token, {path: '/'});
                                top.layer.close(dialogIndex);
                                common.successMsg('登录成功');
                                common.getTopWindowDom().window.location.reload();
                                top.WIN_LOGIN_AGAIN = null;
                            } else {
                                common.errorMsg("账号或密码不正确！")
                            }
                        },
                        error:function(jqXHR){
                            common.errorMsg("账号或密码不正确！")
                        }

                    })
                }
            }
        }
    };
    internal.init();
});