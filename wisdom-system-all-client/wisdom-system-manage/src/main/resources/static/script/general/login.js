require(['common'], function (common) {
    $(function () {
        var internal = {
            init: function () {
                $("#frmLogin").find("[name='username']").focus();
                var fnValidForm = internal.validForm();
                // common.getSysParam('SYS_MANAGE_NAME', function(data){
                //     // console.log(data);
                //     if(data.result) {
                //         $('#systemName').text(data.result);
                //     }
                // });
                // common.getSysParam('SYS_COPYRIGHT', function(data){
                //     // console.log(data);
                //     if(data.result) {
                //         $('.copyright').html(data.result);
                //     }
                // });
                internal.login(fnValidForm);
                internal.shortKeyEvent();
            },
            validForm: function () {
                var self = this,
                    jqUsername = $("#frmLogin").find("[name='username']"),
                    jqPassword = $("#frmLogin").find("[name='password']");
                jqUsername.keyup(function () {
                    if ($(".login-error-tip").hasClass("empty-username")) {
                        $(".login-error-tip").removeClass("empty-username").css({"visibility": "hidden"}).text("");
                    }
                }).blur(function () {
                    if ($.trim(this.value) == '') {
                        $(".login-error-tip").css({"visibility": "visible"}).text("用户账号不能为空！").addClass("empty-username");
                    }
                });
                jqPassword.keyup(function () {
                    if ($(".login-error-tip").hasClass("empty-password")) {
                        $(".login-error-tip").removeClass("empty-password").css({"visibility": "hidden"}).text("");
                    }
                }).blur(function () {
                    if ($.trim(this.value) == '') {
                        $(".login-error-tip").css({"visibility": "visible"}).text("用户密码不能为空！").addClass("empty-password");
                    }
                });


                self.check = function () {
                    if (jqUsername.val().trim().length == 0 && jqPassword.val().trim().length > 0) {
                        $(".login-error-tip").css({"visibility": "visible"}).text("用户账号不能为空！").addClass("empty-username");
                    }
                    if (jqUsername.val().trim().length > 0 && jqPassword.val().trim().length == 0) {
                        $(".login-error-tip").css({"visibility": "visible"}).text("用户密码不能为空！").addClass("empty-password");
                    }
                    if (jqUsername.val().trim().length == 0 && jqPassword.val().trim().length == 0) {
                        $(".login-error-tip").css({"visibility": "visible"}).text("用户账号密码不能为空！").addClass("empty-username empty-password");
                    }
                    if (jqUsername.val().trim().length > 0 && jqPassword.val().trim().length > 0) {
                        return true
                    }
                };
                return self;
            },
            login: function (fnValidForm) {
                $("#btnLogin").on("click", function () {
                    var self = this;
                    if (!$(self).hasClass("login-loading") && fnValidForm.check()) {
                        $(".login-error-tip").css({"visibility": "hidden"}).text("");
                        var username = $("#frmLogin").find("[name='username']").val();
                        var password = $("#frmLogin").find("[name='password']").val();
                        $.ajax({
                            type: "post",
                            url: GLOBAL_API_PATH + "/oauth/token",
                            data: {
                                "username": $.trim(username),
                                "password": password,
                                "grant_type": "password",
                                "scope": "select",
                                "client_id": "client",
                                "client_secret": "123456"
                            },
                            beforeSend: function () {
                                self.value = "登录中...";
                                $(self).text(self.value);
                                $(self).addClass("login-loading");
                            },
                            success: function (data) {
                                if (data["access_token"]) {
                                    var access_token = data["access_token"];
                                    $.cookie("access_token", access_token);
                                    window.location.href = GLOBAL_WEB_BASE_PATH + "/index.html";
                                } else {
                                    $(".login-error-tip").css({"visibility": "visible"}).text("用户账号或密码错误！");
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $(".login-error-tip").css({"visibility": "visible"}).text("用户账号或密码错误！");
                            },
                            complete: function () {
                                self.value = "登录";
                                $(self).text(self.value);
                                $(self).removeClass("login-loading");
                            }
                        })
                    }
                })
            },
            shortKeyEvent: function () {
                document.onkeydown = function (event) {

                    var e = event || window.event || arguments.callee.caller.arguments[0];
                    if (e && e.keyCode == 13) { // enter 键
                        if ($("#frmLogin").find("[name='username']")[0] == document.activeElement) {
                            $("#frmLogin").find("[name='password']").focus();
                        } else {
                            $("#btnLogin").click();
                        }
                    }
                }
            }
        };
        internal.init();
    });
});