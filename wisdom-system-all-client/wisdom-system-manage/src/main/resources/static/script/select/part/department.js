define(['common', 'dialogSelect'], function (common, DialogSelect) {
    var internal = {
        dialogSelectInputParam: function (urlParam) {
            return {
                winCallback: 'win_test',
                winStorage: 'win_storage',
                title: '所属部门选择',
                width: "520px",
                height: "500px",
                textField: 'name',
                valueField: 'code',
                btn: ['确定', '取消'],
                btn1: "ok",
                btn2: "cancel"
            }
        },
        dialogSelectInit: function (urlParam) {
            var checkbox = urlParam["checkbox"] || 1;
            new DialogSelect({
                pattern: '1C',
                winCallback: 'win_test',
                winStorage: 'win_storage',
                storage: [],
                primaryKeyField: 'code',
                cells: {
                    a: {
                        attachId: 'grid',
                        attachType: 'grid',
                        panelType: "main",
                        primaryKey: 'code',
                        toolbarParam: {
                            searchers: {
                                keyword: {
                                    label: "关键字：",
                                    name: 'keyword',
                                    type: "text",
                                    width: 150,
                                    hidden: false
                                },
                                btnSearch: true
                            }
                        },
                        gridParam: {
                            isLoadData: false,
                            target: 'grid',
                            primaryKey: 'code',
                            ajax: {
                                url: "api/sys/department/page/list"
                            },
                            columns: [
                                {display: '部门编码', name: 'code', width: 180},
                                {display: '部门名称', name: 'name', width: 220}
                            ],
                            showType: "list",
                            isUsePage: false,
                            row: {
                                checkbox: checkbox
                            }
                        }
                    }
                }
            });
        }
    };
    return internal;
})