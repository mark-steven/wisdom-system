define(['common', 'dialogSelect'], function (common, DialogSelect) {
    var internal = {
        dialogSelectInputParam: function (urlParam) {
            return {
                winCallback: 'win_test',
                winStorage: 'win_storage',
                title: '用户选择',
                width: "1080px",
                height: "560px",
                textField: 'name',
                valueField: 'id',
                btn: ['确定', '取消'],
                btn1: "ok",
                btn2: "cancel"
            }
        },
        dialogSelectInit: function (urlParam) {
            var checkbox = urlParam["checkbox"] || 1;
            new DialogSelect({
                pattern: '2U',
                winCallback: 'win_test',
                winStorage: 'win_storage',
                storage: [],
                cells: {
                    a: {
                        attachId: 'grid1',
                        attachType: 'grid',
                        panelType: "main",
                        toolbarParam: {
                            searchers: {
                                keyword: {
                                    label: "关键字：",
                                    name: 'keyword',
                                    type: "text",
                                    width: 150,
                                    hidden: false
                                },
                                btnSearch: true
                            }
                        },
                        gridParam: {
                            isLoadData: false,
                            target: 'grid1',
                            ajax: {
                                url: "api/sys/user/page/list"
                            },
                            columns: [
                                {display: '用户名称', name: 'name', width: 150},
                                {display: '电话号码', name: 'phone', width: 150, align: 'center'},
                                {display: '邮箱', name: 'email', width: 160, align: 'right'},
                                {
                                    display: '状态', name: 'state', width: 80, align: 'center', render: function (data) {
                                        var stateJSON = {"0": "禁用", "1": "启用"};
                                        return stateJSON[data["state"]];
                                    }
                                }
                            ],
                            showType: "list",
                            isUsePage: true,
                            row: {
                                checkbox: checkbox
                            }
                        }
                    },
                    b: {
                        attachId: 'item',
                        attachType: 'item',
                        panelType: 'item',
                        width: 400
                    }
                }
            });
        }
    };
    return internal;
})