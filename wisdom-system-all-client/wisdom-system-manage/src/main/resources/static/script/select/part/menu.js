define(['common', 'dialogSelect'], function (common, DialogSelect) {
    var internal = {
        dialogSelectInputParam: function (urlParam) {
            return {
                winCallback: 'win_test',
                winStorage: 'win_storage',
                title: '菜单父节点选择',
                width: "960px",
                height: "500px",
                textField: 'name',
                valueField: 'id',
                btn: ['确定', '设置顶级节点', '取消'],
                btn1: "ok",
                btn2: function (dialogIndex, fnCallback) {
                    var data = [{id: 0, name: "无【本身为顶级节点】", pid: "-1"}];
                    fnCallback(data);
                    var top = common.getTopWindowDom();
                    top.layer.close(dialogIndex);
                },
                btn3: "cancel"
            }
        },
        dialogSelectInit: function (urlParam) {
            var checkTypeParentValue = urlParam["checkTypeParentValue"] || '';
            var checkbox = urlParam["checkbox"] || 1;
            new DialogSelect({
                pattern: '1C',
                winCallback: 'win_test',
                winStorage: 'win_storage',
                storage: [],
                cells: {
                    a: {
                        attachId: 'grid1',
                        attachType: 'grid',
                        panelType: "main",
                        toolbarParam: {
                            searchers: {
                                keyword: {
                                    label: "关键字：",
                                    name: 'keyword',
                                    type: "text",
                                    width: 150,
                                    hidden: false
                                },
                                btnSearch: true
                            }
                        },
                        gridParam: {
                            isLoadData: false,
                            target: 'grid1',
                            ajax: {
                                url: "api/sys/menu/page/list"
                            },
                            columns: [
                                {display: '名称', name: 'name', width: 300},
                                {display: '路径', name: 'url', width: 480},
                                {display: '排序', name: 'sort', width: 80},
                            ],
                            showType: "tree",
                            isUsePage: false,
                            row: {
                                checkbox: checkbox,
                                treeCheckboxType: 2,//0：正常选；1：disable;2:hidden
                                checkTypeParentValue: checkTypeParentValue
                            }
                        }
                    }
                }
            });
        }
    };
    return internal;
})