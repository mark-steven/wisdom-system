define(['common', 'dialogSelect'], function (common, DialogSelect) {
    var internal = {
        dialogSelectInputParam: function (urlParam) {
            return {
                winCallback: 'win_test',
                winStorage: 'win_storage',
                title: '个人菜单选择',
                width: "80%",
                height: "80%",
                textField: 'name',
                valueField: 'id',
                btn: ['确定', '取消'],
                btn1: "ok",
                btn2: "cancel"
            }
        },
        dialogSelectInit: function (urlParam) {
            new DialogSelect({
                pattern: '2U',
                winCallback: 'win_test',
                winStorage: 'win_storage',
                storage: [],
                cells: {
                    a: {
                        attachId: 'grid1',
                        attachType: 'grid',
                        panelType: "main",
                        toolbarParam: {
                            searchers: {
                                keyword: {
                                    label: "关键字：",
                                    name: 'keyword',
                                    type: "text",
                                    width: 150,
                                    hidden: false
                                },
                                btnSearch: true
                            }
                        },
                        gridParam: {
                            isLoadData: false,
                            target: 'grid1',
                            ajax: {
                                url: "api/sys/menu/auth/page/list",
                                data: function () {
                                    if (urlParam) {
                                        return urlParam;
                                    }
                                    return {}
                                }
                            },
                            columns: [
                                {display: '所属系统', name: 'fkSystemName', width: 100, align: 'center'},
                                {display: '名称', name: 'name', width: 250},
                                {display: '路径', name: 'url', width: 300},
                                {
                                    display: '状态', name: 'state', align: 'center', width: 80, render: function (data) {
                                        switch (data["state"]) {
                                            case 0:
                                                return "禁用";
                                            case 1:
                                                return "启用";
                                        }
                                    }
                                }
                            ],
                            showType: "tree",
                            isUsePage: false,
                            row: {
                                checkbox: 2,
                                treeCheckboxType: 2, //0：正常选；1：disable;2:hidden
                                onlyCheckChildren: true
                            }
                        }
                    },
                    b: {
                        attachId: 'item',
                        attachType: 'item',
                        panelType: 'item',
                        width: 300
                    }
                }
            });
        }
    };
    return internal;
});
