require(['common'], function (common) {
    $(function () {
        var urlParam = common.getParamFromUrl2Json();
        require([GLOBAL_WEB_BASE_PATH + '/script/select/part/' + urlParam["partName"] + '.js'], function (internal) {
            internal.dialogSelectInit(urlParam);
        });
    })

});