define(['dialogSelect', 'common'], function (DialogSelect, common) {
    var internal = {
        dialogSelectInputParam: function (urlParam) {
            var btnType = urlParam["btnType"] || 1;
            var dialogTitle = urlParam["dialogTitle"] || "字典项选择";
            var bntJSON = {
                1: {
                    btn: ['确定', '取消'],
                    btn1: "ok",
                    btn2: "cancel"
                },
                2: {
                    btn: ['确定', '设置顶级节点', '取消'],
                    btn1: "ok",
                    btn2: function (dialogIndex, fnCallback) {
                        var data = [{id: 0, name: "无【本身为顶级节点】", pid: "-1"}];
                        fnCallback(data);
                        var top = common.getTopWindowDom();
                        top.layer.close(dialogIndex);
                    },
                    btn3: "cancel"
                }
            };
            var dialogParam = {
                winCallback: 'win_dict_item_select_callback',
                winStorage: 'win_dict_item_select_storage',
                title: dialogTitle,
                width: "560px",
                height: "500px",
                textField: 'name',
                valueField: 'id'
            };
            dialogParam = $.extend(true, {}, dialogParam, bntJSON[btnType]);
            return dialogParam;
        },
        dialogSelectInit: function (urlParam) {
            var checkTypeParentValue = urlParam["checkTypeParentValue"] || '';
            var checkbox = urlParam["checkbox"] || 1;
            var fkDictCategoryCode = urlParam["fkDictCategoryCode"];
            var codeFieldText = urlParam["codeFieldText"] || '项编码';
            var nameFieldText = urlParam["nameFieldText"] || '项名称';
            var btnType = urlParam["btnType"];
            var isUsePage = false;
            var showType = "tree";
            if (btnType == 1) {
                isUsePage = true;
                showType = "list";

            } else {
                isUsePage = false;
                showType = "tree";
            }
            new DialogSelect({
                pattern: '1C',
                winCallback: 'win_dict_item_select_callback',
                winStorage: 'win_dict_item_select_storage',
                storage: [],
                cells: {
                    a: {
                        attachId: 'grid1',
                        attachType: 'grid',
                        panelType: "main",
                        toolbarParam: {
                            searchers: {
                                keyword: {
                                    label: "关键字：",
                                    name: 'keyword',
                                    type: "text",
                                    width: 150,
                                    hidden: false
                                },
                                btnSearch: true
                            }
                        },
                        gridParam: {
                            isLoadData: false,
                            target: 'grid1',
                            ajax: {
                                url: "api/sys/dict/item/page/list",
                                data: {fkDictCategoryCode: fkDictCategoryCode}
                            },
                            tree: {
                                simpleData: {
                                    textField: 'code'
                                }
                            },
                            columns: [
                                {display: codeFieldText, name: 'code', width: 160},
                                {display: nameFieldText, name: 'name', width: 280}
                            ],
                            showType: showType,
                            isUsePage: isUsePage,
                            row: {
                                checkbox: checkbox,
                                treeCheckboxType: 2,//0：正常选；1：disable;2:hidden
                                checkTypeParentValue: checkTypeParentValue
                            }
                        }
                    }
                }
            });
        }
    };
    return internal;
})