package com.wisdom.system.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "sys")
public class SysController {

    @RequestMapping(value = "/index/index.html")
    public String ToSysIndexPage() {
        return "sys/index/index";
    }

    @RequestMapping(value = "/menu/list.html")
    public String ToSysMenuList() {
        return "sys/menu/list";
    }

    @RequestMapping(value = "/menu/detail.html")
    public String ToSysMenuDetail() {
        return "sys/menu/detail";
    }

    @RequestMapping(value = "/dict/list.html")
    public String ToDictList() {
        return "sys/dict/list";
    }

    @RequestMapping(value = "/dict/detail.html")
    public String ToDicDetail() {
        return "sys/dict/detail";
    }

    @RequestMapping(value = "/dict/category/detail.html")
    public String ToDictCategoryDetail() {
        return "sys/dict/category/detail";
    }

    @RequestMapping(value = "/dict/item/list.html")
    public String ToDictItemList() {
        return "sys/dict/item/list";
    }

    @RequestMapping(value = "/dict/item/detail.html")
    public String ToDictItemDetail() {
        return "sys/dict/item/detail";
    }

    //部门列表
    @RequestMapping(value = "/dept/list.html")
    public String ToDeptList() {
        return "sys/dept/list";
    }

    //部门详情
    @RequestMapping(value = "/dept/detail.html")
    public String ToDeptDetail() {
        return "sys/dept/detail";
    }

    //部门人员分配
    @RequestMapping(value = "/dept/user/list.html")
    public String ToDeptUserList() {
        return "sys/dept/user/list";
    }

    //详情页
    @RequestMapping(value = "/dept/user/detail.html")
    public String ToDeptUserDetail() {
        return "sys/dept/user/dept_user_detail";
    }

    @RequestMapping(value = "/user/list.html")
    public String ToUserList() {
        return "/sys/user/list";
    }

    @RequestMapping(value = "/user/detail.html")
    public String ToUserDetail() {
        return "/sys/user/detail";
    }

    // 修改密码页面
    @RequestMapping(value = "/user/password.html")
    public String ToUserPassword() {
        return "/sys/user/password";
    }

    @RequestMapping(value = "role/user/list.html")
    public String ToRoleUserList() {
        return "/sys/role/user/list";
    }

    @RequestMapping(value = "role/menu/list.html")
    public String ToRoleMenuAuthList() {
        return "/sys/role/menu/list";
    }

    @RequestMapping(value = "user/updatePassword.html")
    public String ToUpdatePasswordList() {
        return "/sys/user/updatePassword";
    }

    @RequestMapping(value = "/param/list.html")
    public String ToDictParamList() {
        return "sys/param/list";
    }

    /**
     * 修改个人信息页面
     *
     * @return
     */
    @RequestMapping(value = "/user/userinfo.html")
    public String ToUserInfo() {
        return "/sys/user/userinfo";
    }

    /**
     * 表备份转移
     *
     * @return
     */
    @RequestMapping(value = "/table/bak/list.html")
    public String ToTableBakJobList() {
        return "sys/backups/list";
    }

    /**
     * 表备份转移
     *
     * @return
     */
    @RequestMapping(value = "/table/bak/detail.html")
    public String ToTableBakJobDetail() {
        return "sys/backups/detail";
    }

    /**
     * 系统日志
     *
     * @return
     */
    @RequestMapping(value = "/log/list.html")
    public String ToSystemLogList() {
        return "sys/log/list";
    }
}
