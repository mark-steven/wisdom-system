package com.wisdom.system.manage.core;


import com.alibaba.fastjson.JSONObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class APIRequest {

    public String get(String accessToken, HttpServletRequest request) throws IOException {
        HttpClient client = new HttpClient();
        //String queryString = request.getQueryString();
        String url = java.net.URLDecoder.decode(request.getParameter("__url"), "UTF-8");
        if (StringUtil.isNotEmpty(accessToken)) {
            if (url.indexOf("?") > 0) {
                url += "&access_token=" + accessToken;
            } else {
                url += "?access_token=" + accessToken;
            }
        }
        GetMethod method = new GetMethod(url);
        client.executeMethod(method);
        //打印服务器返回状态
        System.out.println(method.getStatusLine());
        String result = method.getResponseBodyAsString();
        //释放连接
        method.releaseConnection();
        return result;
    }

    public String post(String accessToken, HttpServletRequest request) throws IOException {
        String url = java.net.URLDecoder.decode(request.getParameter("__url"), "UTF-8");
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
        /*获取url参数*/
        if (StringUtil.isNotEmpty(accessToken)) {
            method.addParameter("access_token", accessToken);
        }
        Map<String, String[]> map = request.getParameterMap();
        for (String key : map.keySet()) {
            String value = request.getParameter(key);
            method.addParameter(key, value);
        }
        /*获取requestBody参数*/
        BufferedReader streamReader = null;
        try {
            streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = streamReader.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }
        if (StringUtil.isNotEmpty(responseStrBuilder.toString())) {
            JSONObject jsonObject = JSONObject.parseObject(responseStrBuilder.toString());
            String requestBodyParam = jsonObject.toJSONString();
            //method.setRequestBody(requestBodyParam);//方法过时了
            method.setRequestEntity(new StringRequestEntity(requestBodyParam, "text/xml", "ISO-8859-1"));
        }
        client.executeMethod(method);
        String result = method.getResponseBodyAsString();
        method.releaseConnection();
        return result;

    }

    /*token请求*/
    public String requestToken(String url, HttpServletRequest request) throws IOException {
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
        /*这个参数主要获取登录账号和密码*/
        Map<String, String[]> map = request.getParameterMap();
        for (String key : map.keySet()) {
            String value = request.getParameter(key);
            method.addParameter(key, value);
        }
        /*新增客户端参数和授权模式等参数*/
        method.addParameter("grant_type", "password");
        method.addParameter("scope", "select");
        method.addParameter("client_id", "client");
        method.addParameter("client_secret", "123456");
        client.executeMethod(method);
        String result = method.getResponseBodyAsString();
        method.releaseConnection();
        return result;
    }
}
