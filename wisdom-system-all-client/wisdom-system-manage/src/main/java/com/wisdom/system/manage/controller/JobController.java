package com.wisdom.system.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "job")
public class JobController {

    @RequestMapping(value = "/cron/list.html")
    public String ToCronList() {
        return "/job/cron/list";
    }

    @RequestMapping(value = "/cron/detail.html")
    public String ToCronDetail() {
        return "/job/cron/detail";
    }

    @RequestMapping(value = "/task/list.html")
    public String ToTaskList() {
        return "/job/task/list";
    }

    @RequestMapping(value = "/task/detail.html")
    public String ToTaskDetail() {
        return "/job/task/detail";
    }

    @RequestMapping(value = "/log/list.html")
    public String ToLogList() {
        return "/job/log/list";
    }

}
