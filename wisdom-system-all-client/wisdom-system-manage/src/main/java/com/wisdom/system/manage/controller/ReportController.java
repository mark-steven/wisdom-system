package com.wisdom.system.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "triageStationReport")
public class ReportController {

    @RequestMapping(value = "/apply.html")
    public String ToReportApply() {
        return "/triageStationReport/apply";
    }
}
