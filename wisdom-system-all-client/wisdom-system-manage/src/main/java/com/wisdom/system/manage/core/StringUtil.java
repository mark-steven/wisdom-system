package com.wisdom.system.manage.core;

/**
 * 字符串工具类
 */
public class StringUtil {

    /**
     * 判断为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str != null && str.trim().length() > 0) {
            return false;
        }
        return true;
    }


    /**
     * 判断不为空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        if (str != null && str.trim().length() > 0) {
            return true;
        }
        return false;
    }
}
