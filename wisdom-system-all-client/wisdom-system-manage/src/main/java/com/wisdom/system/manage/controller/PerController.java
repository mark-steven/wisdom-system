package com.wisdom.system.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "per")
public class PerController {

    @RequestMapping(value = "/att/list.html")
    public String ToCronList() {
        return "/per/att/list";
    }
}
