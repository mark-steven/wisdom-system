package com.wisdom.system.manage.beetl;


import com.wisdom.system.manage.beetlFn.Functions;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.core.resource.FileResourceLoader;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;

import java.util.HashMap;

@Configuration
public class BeetlConf {

    @Autowired
    Functions fn;
    @Value("${wisdom.system.path.resource}")
    private String resourcePath;
    @Value("${wisdom.system.path.api}")
    private String apiPath;
    @Value("${wisdom.system.path.html-locations}")
    private String htmlLocations;
    @Value("${wisdom.system.version}")
    private String version;

    @Bean(initMethod = "init", name = "beetlConfig")
    public BeetlGroupUtilConfiguration getBeetlGroupUtilConfiguration(@Qualifier("functions") Functions fn) {
        BeetlGroupUtilConfiguration beetlGroupUtilConfiguration = new BeetlGroupUtilConfiguration();
        ResourcePatternResolver patternResolver = ResourcePatternUtils.getResourcePatternResolver(new DefaultResourceLoader());
        try {
            if (htmlLocations == null || htmlLocations.trim().length() == 0) {
                //内部写法
                ClasspathResourceLoader classpathResourceLoader = new ClasspathResourceLoader("/templates");
                beetlGroupUtilConfiguration.setResourceLoader(classpathResourceLoader);
            } else {
                //外部路径写法
                FileResourceLoader resourceLoader = new FileResourceLoader(htmlLocations, "utf-8");
                beetlGroupUtilConfiguration.setResourceLoader(resourceLoader);
            }
            beetlGroupUtilConfiguration.setConfigFileResource(patternResolver.getResource("classpath:Beetl.properties"));
            HashMap<String, Object> map = new HashMap<>();
            map.put("apiPath", apiPath);
            map.put("resourcePath", resourcePath);
            map.put("version",version);
            beetlGroupUtilConfiguration.setSharedVars(map);
            return beetlGroupUtilConfiguration;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Bean(name = "beetlViewResolver")
    public BeetlSpringViewResolver getBeetlSpringViewResolver(@Qualifier("beetlConfig") BeetlGroupUtilConfiguration beetlGroupUtilConfiguration) {
        BeetlSpringViewResolver beetlSpringViewResolver = new BeetlSpringViewResolver();
        beetlSpringViewResolver.setContentType("text/html;charset=UTF-8");
        beetlSpringViewResolver.setOrder(0);
        beetlSpringViewResolver.setSuffix(".html");
        beetlSpringViewResolver.setConfig(beetlGroupUtilConfiguration);
        return beetlSpringViewResolver;
    }

}

