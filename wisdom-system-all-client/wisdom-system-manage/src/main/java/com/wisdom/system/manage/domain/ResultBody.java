package com.wisdom.system.manage.domain;

/**
 * ResultBody 最终接口对外输出实体
 */
public class ResultBody {

    //响应代码
    private String code;

    //响应消息
    private String msg;

    //响应结果
    private Object result;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
