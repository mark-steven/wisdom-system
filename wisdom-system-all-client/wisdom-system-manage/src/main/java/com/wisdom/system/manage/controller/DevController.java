package com.wisdom.system.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "dev")
public class DevController {

    @RequestMapping(value = "param/category/list.html")
    public String ToParamCategoryList() {
        return "/dev/param/category/list";
    }

    @RequestMapping(value = "param/category/detail.html")
    public String ToParamCategoryDetail() {
        return "/dev/param/category/detail";
    }

    @RequestMapping(value = "param/group/detail.html")
    public String ToParamGroupDetail() {
        return "/dev/param/group/detail";
    }

    @RequestMapping(value = "param/config/list.html")
    public String ToParamConfigList() {
        return "/dev/param/config/list";
    }

    @RequestMapping(value = "param/config/detail.html")
    public String ToParamConfigDetail() {
        return "/dev/param/config/detail";
    }

    @RequestMapping(value = "excel/config/list.html")
    public String ToExcelConfigList() {
        return "/dev/excel/list";
    }

    @RequestMapping(value = "excel/config/detail.html")
    public String ToExcelConfigDetail() {
        return "/dev/excel/detail";
    }

    /**
     * 系统自定义模块列表
     */
    @RequestMapping(value = "/custom/module/list.html")
    public String ToDevCustomModuleList() {
        return "/dev/custom/module/list";
    }

    /**
     * 系统自定义模块模块详情
     */
    @RequestMapping(value = "/custom/module/detail.html")
    public String ToDevCustomModuleDetail() {
        return "/dev/custom/module/detail";
    }

    //报表配置列表
    @RequestMapping(value = "/report/list.html")
    public String ToDevReportList() {
        return "/dev/report/list";
    }

    //报表配置详情
    @RequestMapping(value = "/report/detail.html")
    public String ToDevReportDetail() {
        return "/dev/report/detail";
    }

    //Excel导出
    @RequestMapping(value = "excel/output/list.html")
    public String ToExcelOutputList(){
        return "/dev/excel/output/list";
    }

    @RequestMapping(value = "excel/output/detail.html")
    public String ToExcelOutputDetail(){
        return "/dev/excel/output/detail";
    }

}
