package com.wisdom.system.manage.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping
public class GeneralController implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        //获取statusCode:401,404,500
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        switch (statusCode) {
            case 500:
                return "500";
            default:
                return "404";
        }
    }

    @RequestMapping(value = {"/", "login.html"}, method = RequestMethod.GET)
    public String ToLogin() {
        return "login";
    }

    @RequestMapping(value = "login/again.html", method = RequestMethod.GET)
    public String ToLoginAgain() {
        return "loginAgain";
    }

    @RequestMapping(value = "index.html", method = RequestMethod.GET)
    public String ToIndex() {
        return "index";
    }

    @RequestMapping(value = "center.html", method = RequestMethod.GET)
    public String ToCenter() {
        return "center";
    }

    @RequestMapping(value = "select/list.html", method = RequestMethod.GET)
    public String SelectList() {
        return "select/list";
    }

}
