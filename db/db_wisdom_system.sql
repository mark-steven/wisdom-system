/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : db_wisdom_system

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-01-15 21:14:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for att_management
-- ----------------------------
DROP TABLE IF EXISTS `att_management`;
CREATE TABLE `att_management` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `ATTENDANCE_NUM` varchar(36) DEFAULT NULL COMMENT '考勤号码',
  `NAME` varchar(128) DEFAULT NULL COMMENT '姓名',
  `DATE` varchar(128) DEFAULT NULL COMMENT '日期',
  `WEEK` varchar(128) DEFAULT NULL COMMENT '星期',
  `DEPT` varchar(128) DEFAULT NULL COMMENT '部门',
  `SIGN_IN_TIME` varchar(128) DEFAULT NULL COMMENT '签到时间',
  `SIGN_BACK_TIME` varchar(128) DEFAULT NULL COMMENT '签退时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='考勤表';

-- ----------------------------
-- Records of att_management
-- ----------------------------

-- ----------------------------
-- Table structure for dev_custom_module
-- ----------------------------
DROP TABLE IF EXISTS `dev_custom_module`;
CREATE TABLE `dev_custom_module` (
  `ID` varchar(36) NOT NULL COMMENT '主键ID',
  `CODE` varchar(64) NOT NULL COMMENT '模块编码',
  `NAME` varchar(128) NOT NULL COMMENT '模块名称',
  `CONFIG_CONTENT` text COMMENT '配置内容',
  `FK_DEPT_CODE` varchar(64) DEFAULT NULL COMMENT '科室编码',
  `DESCR` varchar(500) DEFAULT NULL COMMENT '描述',
  `SORT` int(11) DEFAULT NULL COMMENT '排序',
  `PY_CODE` varchar(200) DEFAULT NULL COMMENT '拼音首码',
  `WB_CODE` varchar(200) DEFAULT NULL COMMENT '五笔码',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='个性化自定义模板配置维护';

-- ----------------------------
-- Records of dev_custom_module
-- ----------------------------
INSERT INTO `dev_custom_module` VALUES ('9a45dba5-3924-4355-9da8-34aa49eb9fad', 'ATT', '考勤', 't={\r\n    searchers:[\"startTime\",{endTime: {\r\n            label: \"结束日期：\",\r\n            name: \'endTime\',\r\n            type: \"date\",\r\n            width: 100,\r\n            attr: {\r\n                \"id\": \"endTime\",\r\n                \"data-default-value-type\": \"d\",\r\n                \"data-default-value\": 7,\r\n                \"data-min-value\": \'startTime\'\r\n            }\r\n        }},\"btnSearch\"],\r\n        columns:[\"attendanceNum\",\"name\",\"date\",\"week\",\"dept\",\"signInTime\",\"signBackTime\",{state:{display: \'状态\', name: \'state\', width: 100, align: \'center\'}},\"operation\"]\r\n}', '001', null, '1', 'KQ', 'FA', '2020-01-15 20:21:02', '893050be-18eb-4682-9921-1109c10235a4');

-- ----------------------------
-- Table structure for dev_excel_import_config
-- ----------------------------
DROP TABLE IF EXISTS `dev_excel_import_config`;
CREATE TABLE `dev_excel_import_config` (
  `ID` varchar(36) COLLATE utf8_bin NOT NULL COMMENT 'id主键',
  `CODE` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '编码',
  `NAME` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '导入业务模块名称',
  `TABLE_NAME` varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '表名',
  `START_NUM` int(11) DEFAULT NULL COMMENT '开始行数',
  `WITHOUT_END_NUM` int(11) DEFAULT NULL COMMENT '结束行数',
  `DESCR` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='EXCEL导入配置';

-- ----------------------------
-- Records of dev_excel_import_config
-- ----------------------------
INSERT INTO `dev_excel_import_config` VALUES ('1f0c35c5-fade-4219-b56c-59c5920404e4', 'ATT_MANAGE', '考勤管理导入', 'att_management', '2', '0', null);

-- ----------------------------
-- Table structure for dev_excel_import_rule
-- ----------------------------
DROP TABLE IF EXISTS `dev_excel_import_rule`;
CREATE TABLE `dev_excel_import_rule` (
  `ID` varchar(36) COLLATE utf8_bin NOT NULL COMMENT 'id主键',
  `FK_EXCEL_IMPORT_CONFIG_ID` varchar(128) COLLATE utf8_bin NOT NULL COMMENT '关联配置ID',
  `COL_NAME` varchar(128) COLLATE utf8_bin NOT NULL COMMENT '表列名',
  `COL_DESCR` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '表列说明',
  `IS_NULLABLE` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `COL_DATA_TYPE` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '数据类型',
  `EXCEL_COL_INDEX` int(11) DEFAULT NULL COMMENT '对应excle的列索引',
  `EXCEL_COL_VALUE_VERIFY` text COLLATE utf8_bin COMMENT 'excel列值验证规则',
  `EXCEL_COL_VALUE_CONVERT` text COLLATE utf8_bin COMMENT 'excel列值转换规则',
  `SORT` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='EXCEL导入规则';

-- ----------------------------
-- Records of dev_excel_import_rule
-- ----------------------------
INSERT INTO `dev_excel_import_rule` VALUES ('02c8dcb6-177a-4a2a-8951-4fccc966b479', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'ATTENDANCE_NUM', '考勤号码', 'YES', 'varchar(36)', '1', null, null, '2');
INSERT INTO `dev_excel_import_rule` VALUES ('2331012e-4a3b-4c02-8de5-208a89923807', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'WEEK', '星期', 'YES', 'varchar(128)', '4', null, null, '5');
INSERT INTO `dev_excel_import_rule` VALUES ('a82cc886-a49d-4d9e-93a5-d3e967c93bc3', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'DEPT', '部门', 'YES', 'varchar(128)', '5', null, null, '6');
INSERT INTO `dev_excel_import_rule` VALUES ('b01aa32a-8380-42be-9f88-ca57f4031880', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'DATE', '日期', 'YES', 'varchar(128)', '3', null, null, '4');
INSERT INTO `dev_excel_import_rule` VALUES ('dfe2136d-c797-45bb-8b20-dc8c6e2bee46', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'ID', '主键', 'NO', 'varchar(36)', null, null, null, '1');
INSERT INTO `dev_excel_import_rule` VALUES ('e703210c-81c8-49c7-a607-53d8339d6aec', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'SIGN_BACK_TIME', '签退时间', 'YES', 'varchar(128)', '7', null, null, '8');
INSERT INTO `dev_excel_import_rule` VALUES ('f2dd0be5-7c3f-4c00-891c-f5e7607676a0', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'NAME', '姓名', 'YES', 'varchar(128)', '2', null, null, '3');
INSERT INTO `dev_excel_import_rule` VALUES ('ff58f014-09a7-4edc-b9a1-b08d7b120507', '1f0c35c5-fade-4219-b56c-59c5920404e4', 'SIGN_IN_TIME', '签到时间', 'YES', 'varchar(128)', '6', null, null, '7');

-- ----------------------------
-- Table structure for dev_excel_output_config
-- ----------------------------
DROP TABLE IF EXISTS `dev_excel_output_config`;
CREATE TABLE `dev_excel_output_config` (
  `ID` varchar(36) NOT NULL COMMENT '主键ID',
  `CODE` varchar(64) NOT NULL COMMENT 'excel编码',
  `NAME` varchar(64) NOT NULL COMMENT 'excel名称',
  `SQL_CONFIG_CONTENT` text COMMENT '查询语句配置',
  `TITLE` varchar(255) DEFAULT NULL COMMENT '标题名',
  `SHEET` varchar(255) DEFAULT NULL COMMENT '脚注',
  `DESCR` varchar(256) DEFAULT NULL COMMENT '描述',
  `SORT` int(11) DEFAULT NULL COMMENT '排序',
  `PY_CODE` varchar(64) DEFAULT NULL COMMENT '拼音首码',
  `WB_CODE` varchar(64) DEFAULT NULL COMMENT '五笔码',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='报表配置';

-- ----------------------------
-- Records of dev_excel_output_config
-- ----------------------------
INSERT INTO `dev_excel_output_config` VALUES ('a2ecbb1f-831a-4e84-b450-bd5227bce98d', 'att_excel', '考勤查询', 'SELECT\r\n	t.ATTENDANCE_NUM AS 考勤号码,\r\n	t.DATE AS 日期,\r\n	t.`WEEK` AS 星期,\r\n	t.DEPT AS 部门,\r\n	t.SIGN_IN_TIME AS 上班,\r\n	t.SIGN_BACK_TIME AS 下班\r\nFROM\r\n	att_management t\r\nWHERE\r\n	<%=date date_format(t.DATE, \'%Y/%m/%d\') = #{date} %>', '考勤查询', '考勤查询', null, '1', null, null, '2019-11-26 17:47:17', null);

-- ----------------------------
-- Table structure for dev_param_category
-- ----------------------------
DROP TABLE IF EXISTS `dev_param_category`;
CREATE TABLE `dev_param_category` (
  `ID` varchar(36) NOT NULL,
  `CODE` varchar(36) NOT NULL COMMENT '分类编码',
  `NAME` varchar(36) NOT NULL COMMENT '分类名称',
  `SORT` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数分类表';

-- ----------------------------
-- Records of dev_param_category
-- ----------------------------
INSERT INTO `dev_param_category` VALUES ('1ad6b183-a0bc-4140-987d-1d27685852db', 'SYS_PARAM', '系统参数配置', '1');
INSERT INTO `dev_param_category` VALUES ('49f99f64-e61e-4bb0-a689-9b28f7ef346f', 'PASSWORD_CONFIG', '系统密码', '2');

-- ----------------------------
-- Table structure for dev_param_config
-- ----------------------------
DROP TABLE IF EXISTS `dev_param_config`;
CREATE TABLE `dev_param_config` (
  `ID` varchar(36) NOT NULL,
  `PARAM_KEY` varchar(50) NOT NULL COMMENT '参数KEY',
  `FK_PARAM_CATEGORY_CODE` varchar(36) NOT NULL COMMENT '分类编码',
  `FK_PARAM_GROUP_CODE` varchar(36) NOT NULL COMMENT '分组编码',
  `TAG_NAME` varchar(36) NOT NULL COMMENT '标签名称',
  `TYPE` varchar(36) NOT NULL COMMENT '数据类型',
  `RULE` text COMMENT '系统参数规则',
  `SORT` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数配置表';

-- ----------------------------
-- Records of dev_param_config
-- ----------------------------
INSERT INTO `dev_param_config` VALUES ('537eaeb5-40a7-4f9a-9736-d04f823ac326', 'USER_DEFAULT_INIT_PASSWORD', 'PASSWORD_CONFIG', 'PASSWORD_GROUP', '系统默认密码', '1', '{}', '1');
INSERT INTO `dev_param_config` VALUES ('6bfaa9fa-739c-4a12-9ab9-8af960304d44', 'SYS_COPYRIGHT', 'SYS_PARAM', 'SYS_PARAM_ALL', '版权声明', '3', '{}', '1');
INSERT INTO `dev_param_config` VALUES ('b88cd9c9-8c54-41a3-9440-5368ef630e41', 'SYS_MANAGE_NAME', 'SYS_PARAM', 'SYS_MANAGE', '平台名称', '1', '{}', '1');

-- ----------------------------
-- Table structure for dev_param_group
-- ----------------------------
DROP TABLE IF EXISTS `dev_param_group`;
CREATE TABLE `dev_param_group` (
  `ID` varchar(36) NOT NULL,
  `CODE` varchar(36) NOT NULL COMMENT '分组编码',
  `FK_PARAM_CATEGORY_CODE` varchar(36) NOT NULL COMMENT '所属分类编码',
  `NAME` varchar(36) NOT NULL COMMENT '分组名称',
  `SORT` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数分组表';

-- ----------------------------
-- Records of dev_param_group
-- ----------------------------
INSERT INTO `dev_param_group` VALUES ('7009620a-0bae-4421-9469-9ce0e1dbbcb5', 'PASSWORD_GROUP', 'PASSWORD_CONFIG', '系统密码分组', '1');
INSERT INTO `dev_param_group` VALUES ('bbcb9455-f381-4f77-8098-0e503af0375d', 'SYS_MANAGE', 'SYS_PARAM', '管理平台', '1');

-- ----------------------------
-- Table structure for dev_report_config
-- ----------------------------
DROP TABLE IF EXISTS `dev_report_config`;
CREATE TABLE `dev_report_config` (
  `ID` varchar(36) NOT NULL COMMENT '主键ID',
  `CODE` varchar(64) NOT NULL COMMENT '报表编码',
  `NAME` varchar(128) NOT NULL COMMENT '报表名称',
  `PAGE_CONFIG_CONTENT` text COMMENT '报表界面配置',
  `SQL_CONFIG_CONTENT` text COMMENT '报表查询语句配置',
  `DESCR` varchar(500) DEFAULT NULL COMMENT '描述',
  `SORT` int(11) DEFAULT NULL COMMENT '排序',
  `PY_CODE` varchar(200) DEFAULT NULL COMMENT '拼音首码',
  `WB_CODE` varchar(200) DEFAULT NULL COMMENT '五笔码',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='报表配置';

-- ----------------------------
-- Records of dev_report_config
-- ----------------------------
INSERT INTO `dev_report_config` VALUES ('717aed28-702a-4fdf-8741-c455bbac9467', 'ATT_REPORT', '考勤查询', '{\"cells\":{\"a\":{\"attachId\":\"grid\",\"attachType\":\"grid\",\"toolbarParam\":{\"buttons\":{\"add\":false,\"edit\":false,\"del\":false,\"excelExport\":{\"id\":\"excelExport\",\"type\":\"button\",\"text\":\"EXCEL导出\",\"img\":\"btn-add-brother-group-condition.png\",\"onClick\":\"function () {\\n                                common.confirm({\\n                                    text: \\\"EXCEL导出\\\",\\n                                    callback: function (flag) {\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tdebugger\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tvar date = $(\\\"#date\\\").val() ? $(\\\"#date\\\").val() : \\\"\\\";\\n                                        var paramJson = \\\"{date:\\\\\\\"\\\"+date+\\\"\\\\\\\"}\\\";\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tvar access_token = $.cookie(\\\"access_token\\\");\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tconsole.log(access_token);\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tconsole.log(GLOBAL_API_PATH);\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tvar url = encodeURI(GLOBAL_API_PATH+\\\"/api/dev/excel/output/exceldownload?access_token=\\\"+access_token+\\\"&code=att_excel&paramJson=\\\"+paramJson+\\\"&sheetName=考勤查询&titleName=考勤查询&ExcelName=考勤查询\\\");\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tconsole.log(url);\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tcommon.makeDownload(url);\\n                                    }\\n                                })\\n                            }\"}},\"searchers\":{\"date\":{\"label\":\"日期：\",\"name\":\"date\",\"type\":\"date\",\"width\":100,\"attr\":{\"id\":\"date\",\"data-default-value-type\":\"d\",\"data-default-value\":0}},\"keyword\":{\"label\":\"关键字：\",\"name\":\"keyword\",\"type\":\"text\",\"width\":150},\"btnSearch\":true}},\"gridParam\":{\"isInitLoadData\":false,\"target\":\"grid\",\"ajax\":{\"url\":\"api/att/manage/page/list\"},\"columns\":[{\"display\":\"考勤号码\",\"name\":\"attendanceNum\",\"width\":90,\"align\":\"center\"},{\"display\":\"姓名\",\"name\":\"name\",\"width\":100,\"align\":\"center\"},{\"display\":\"日期\",\"name\":\"date\",\"width\":100,\"align\":\"center\"},{\"display\":\"星期\",\"name\":\"week\",\"width\":100,\"align\":\"center\"},{\"display\":\"部门\",\"name\":\"dept\",\"width\":100,\"align\":\"center\"},{\"display\":\"签到时间\",\"name\":\"signInTime\",\"width\":100,\"align\":\"center\"},{\"display\":\"签退时间\",\"name\":\"signBackTime\",\"width\":100,\"align\":\"center\"},{\"display\":\"操作\",\"width\":60,\"align\":\"center\",\"name\":\"operation\",\"renderItems\":{\"del\":{\"url\":\"api/att/manage/{id}\"}}}],\"showType\":\"list\"}}}}', 'SELECT\r\n	t.ID AS id,\r\n	t.ATTENDANCE_NUM AS attendanceNum,\r\n	t.DATE AS date,\r\n	t.`WEEK` AS WEEK,\r\n	t.DEPT AS dept,\r\n	t.SIGN_IN_TIME AS signInTime,\r\n	t.SIGN_BACK_TIME AS signBackTime\r\nFROM\r\n	att_management t\r\nWHERE\r\n	<%=date date_format(t.DATE, \'%Y/%m/%d\') = #{date} %>', null, '1', null, null, '2019-11-26 17:46:04', null);

-- ----------------------------
-- Table structure for job_cron
-- ----------------------------
DROP TABLE IF EXISTS `job_cron`;
CREATE TABLE `job_cron` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `CODE` varchar(100) NOT NULL COMMENT '编码',
  `NAME` varchar(128) DEFAULT NULL COMMENT '名称',
  `CRON_EXPRESSION` varchar(100) NOT NULL COMMENT 'cron表达式',
  `CONFIG_RESTORE` varchar(2000) DEFAULT NULL COMMENT '配置还原',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '添加用户',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CRON规则';

-- ----------------------------
-- Records of job_cron
-- ----------------------------

-- ----------------------------
-- Table structure for job_log
-- ----------------------------
DROP TABLE IF EXISTS `job_log`;
CREATE TABLE `job_log` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `FK_JOB_LOG_CODE` varchar(100) NOT NULL COMMENT '调度日志类型编码',
  `STATE` int(11) DEFAULT '1' COMMENT '调度状态(1.执行前，2.执行完成时)',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '调度添加时间',
  `CONTENT` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='调度日志';

-- ----------------------------
-- Records of job_log
-- ----------------------------

-- ----------------------------
-- Table structure for job_task
-- ----------------------------
DROP TABLE IF EXISTS `job_task`;
CREATE TABLE `job_task` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `CODE` varchar(100) NOT NULL COMMENT '任务编码',
  `NAME` varchar(100) NOT NULL COMMENT '任务名称',
  `CLAZZ_NAME` varchar(100) NOT NULL COMMENT '调用类名称',
  `FK_CRON_CODE` varchar(100) NOT NULL COMMENT 'CRON规则编码',
  `STATE` int(11) DEFAULT NULL COMMENT '状态：0停止；1启用',
  `DESCR` varchar(500) DEFAULT NULL COMMENT '任务说明',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '添加用户',
  `START_TYPE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调度任务';

-- ----------------------------
-- Records of job_task
-- ----------------------------

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  `ID` varchar(36) NOT NULL,
  `CODE` varchar(64) NOT NULL COMMENT '科室编码',
  `NAME` varchar(200) NOT NULL COMMENT '科室名称',
  `ADDRESS` varchar(1000) DEFAULT NULL COMMENT '科室地址',
  `DESCR` varchar(1000) DEFAULT NULL COMMENT '科室详情',
  `STATE` int(11) NOT NULL COMMENT '状态',
  `PY_CODE` varchar(200) DEFAULT NULL COMMENT '拼音首码',
  `WB_CODE` varchar(200) DEFAULT NULL COMMENT '五笔码',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '创建人',
  `SORT` int(11) DEFAULT NULL COMMENT '排序',
  `FK_DEPT_TYPE_CODE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='医技科室';

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('d837a3f7-4b32-4076-9baf-c4392286c2c5', '001', '研发部', '研发大楼', '研发大楼', '1', 'YFB', 'DNU', '2020-01-03 20:47:02', '893050be-18eb-4682-9921-1109c10235a4', '1', '01');

-- ----------------------------
-- Table structure for sys_dict_category
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_category`;
CREATE TABLE `sys_dict_category` (
  `ID` varchar(36) COLLATE utf8_bin NOT NULL,
  `CODE` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '字典编码',
  `NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '字典名称',
  `ITEM_SHOW_TYPE` int(11) DEFAULT '0' COMMENT '关联项展示方式：0 列表方式；1 树层级展示',
  `ADD_USER_ID` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
  `ADD_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `MODIFY_TIME` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `MODIFY_USER_ID` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CODE_UNIQUE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统字典分类';

-- ----------------------------
-- Records of sys_dict_category
-- ----------------------------
INSERT INTO `sys_dict_category` VALUES ('08169bd8-bc39-4e18-9e9e-c2a261eb850a', 'MENU_SYSTEM', '菜单系统分类', '0', null, '2019-04-09 10:11:19', null, null);
INSERT INTO `sys_dict_category` VALUES ('169b7f4a-6c97-4a40-bb47-223dba28468c', 'DEPT_TYPE', '公司名称', '0', '893050be-18eb-4682-9921-1109c10235a4', '2020-01-03 21:57:02', null, null);
INSERT INTO `sys_dict_category` VALUES ('56f06edf-4d6d-4d92-ae14-1e20c641d148', 'ROLE_CATEGORY', '角色分类', '0', null, '2019-04-01 18:41:48', null, null);
INSERT INTO `sys_dict_category` VALUES ('b404c75e-ccd0-4e77-8981-fec2718911a1', 'JOB_LOG', '调度日志', '1', null, '2019-05-11 09:15:04', null, null);
INSERT INTO `sys_dict_category` VALUES ('c848cca1-b452-46d1-ad2f-377333d690e7', 'JOB_TYPE', '岗位类型', '0', '893050be-18eb-4682-9921-1109c10235a4', '2020-01-09 20:53:24', null, null);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item` (
  `ID` varchar(36) COLLATE utf8_bin NOT NULL,
  `CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '字典项编码',
  `NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '字典项名称',
  `ITEM_VALUE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '字典项值',
  `PY_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '拼音码',
  `WB_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '五笔码',
  `PID` varchar(36) COLLATE utf8_bin DEFAULT '0' COMMENT '父节点ID',
  `FK_DICT_CATEGORY_CODE` varchar(50) COLLATE utf8_bin DEFAULT '0' COMMENT '字典分类编码',
  `SORT` int(11) DEFAULT NULL,
  `ADD_USER_ID` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `MODIFY_TIME` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `MODIFY_USER_ID` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`ID`),
  KEY `inx_fdcc` (`FK_DICT_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统字典项';

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('2a884003-a45c-464d-a5e4-782537b8f027', '001', '部门经理', '部门经理', 'BMJL', 'UUXG', '0', 'JOB_TYPE', '1', '893050be-18eb-4682-9921-1109c10235a4', '2020-01-09 20:53:20', null, null);
INSERT INTO `sys_dict_item` VALUES ('412a1d58-5a3f-4c4f-aca1-b87bf9b74f7f', '01', '厦门软件公司', '厦门软件公司', 'RJGS', 'LWWN', '0', 'DEPT_TYPE', '1', '893050be-18eb-4682-9921-1109c10235a4', '2020-01-03 20:45:12', null, null);
INSERT INTO `sys_dict_item` VALUES ('47736f82-416a-4f51-a070-bc925f1a9c4d', '001', '系统管理员', '系统管理员', null, null, '0', 'ROLE_CATEGORY', '1', null, '2019-03-25 14:33:18', null, null);
INSERT INTO `sys_dict_item` VALUES ('6ef96dc0-a5e0-4eb4-a39c-1bc840792cd5', '001', '管理平台', '管理平台', null, null, '0', 'MENU_SYSTEM', '1', null, '2019-04-09 10:11:53', null, null);
INSERT INTO `sys_dict_item` VALUES ('7b2a42f1-5d9a-4837-8d14-2161eeb79d3e', '01', '备份日志', '备份日志', null, null, '0', 'JOB_LOG', '1', null, '2019-05-11 09:16:05', null, null);
INSERT INTO `sys_dict_item` VALUES ('8ed01ab1-2cc8-42f7-b695-606f30b105fd', '003', '普通员工', '普通员工', 'PTYG', 'UCKA', '0', 'JOB_TYPE', '3', '893050be-18eb-4682-9921-1109c10235a4', '2020-01-09 20:54:50', null, null);
INSERT INTO `sys_dict_item` VALUES ('98e59eae-cb8c-4541-a540-f75b7d2e361f', '002', '部门主管', '部门主管', 'BMZG', 'UUYT', '0', 'JOB_TYPE', '2', '893050be-18eb-4682-9921-1109c10235a4', '2020-01-09 20:54:31', null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `URL` varchar(2083) DEFAULT NULL COMMENT '菜单路径',
  `PID` varchar(36) DEFAULT '0' COMMENT '父节点ID',
  `STATE` int(11) DEFAULT NULL,
  `SORT` int(11) DEFAULT '1',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADD_USER_ID` varchar(36) DEFAULT NULL,
  `MODIFY_TIME` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `MODIFY_USER_ID` varchar(36) DEFAULT NULL COMMENT '修改人',
  `FK_SYSTEM` varchar(5) DEFAULT NULL COMMENT '所属系统',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('109f71f1-6fd6-4a40-a0f3-ed2debada3cd', '系统参数', 'sys/param/list.html', 'f5593479-b410-4a30-9a2c-65f8b4da2f3c', '1', '1', '2019-04-16 16:29:43', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('37cb8e99-9d81-44ae-9662-391f2839651a', '系统参数维护', 'dev/param/config/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '2', '2019-04-09 14:09:14', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('3a49c94b-aebe-4bb8-ae08-cca7842a7740', 'Excel导入配置', 'dev/excel/config/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '3', '2019-04-09 14:09:41', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('3ac67163-c32e-4a57-98b1-3ae1cc6b1489', 'Cron配置', 'job/cron/list.html', '77622084-6cfc-46b4-9870-91f64bc636a3', '1', '5', '2019-04-24 18:11:08', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('3bd20929-3c6e-4446-be6d-e3025bf19daf', '任务管理', 'job/task/list.html', '77622084-6cfc-46b4-9870-91f64bc636a3', '1', '10', '2019-04-24 18:12:24', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('4b045ab0-3ef6-4e96-8098-4ee6b559181e', '部门管理', '', '4e278523-efba-47f1-9f5e-796cdc95d399', '1', '4', '2019-03-22 16:54:17', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('4e278523-efba-47f1-9f5e-796cdc95d399', '系统管理', '', '0', '1', '1', '2019-03-22 16:52:01', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('546c6069-5e0f-4700-bd3e-05d8c8fd9a00', '人事管理', '', '4e278523-efba-47f1-9f5e-796cdc95d399', '1', '100', '2020-01-08 21:21:48', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');
INSERT INTO `sys_menu` VALUES ('6de393d8-f69b-4117-be85-93772e8252b5', '开发配置', '', '0', '1', '5', '2019-04-09 14:08:14', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('75182b43-6402-4ccd-adc4-7e512d26b2bb', '调度日志', 'job/log/list.html', '77622084-6cfc-46b4-9870-91f64bc636a3', '1', '100', '2019-05-11 09:48:08', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('77622084-6cfc-46b4-9870-91f64bc636a3', '调度管理', '', '0', '1', '4', '2019-04-24 18:10:01', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('789e133a-9c57-48d6-951e-20111184f1d9', '人员管理', 'sys/user/list.html', '88c2c00a-6459-469d-9ff1-50f185172613', '1', '2', '2019-04-09 11:57:05', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('7a689910-9265-4fd3-b45f-5ba908364c6c', '配置界面', 'triageStationReport/apply.html?code=ATT_REPORT', '546c6069-5e0f-4700-bd3e-05d8c8fd9a00', '1', '100', '2020-01-14 20:41:33', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');
INSERT INTO `sys_menu` VALUES ('84df11db-15cf-40a1-9a66-020e52034578', '考勤管理', 'per/att/list.html?custom_module=ATT', '546c6069-5e0f-4700-bd3e-05d8c8fd9a00', '1', '100', '2020-01-08 21:22:14', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');
INSERT INTO `sys_menu` VALUES ('88c2c00a-6459-469d-9ff1-50f185172613', '权限管理', '', '4e278523-efba-47f1-9f5e-796cdc95d399', '1', '3', '2019-03-22 16:53:26', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('8bb945d5-4abe-4dee-8b2c-0ce345bb8da9', '角色管理', 'sys/dict/item/list.html?code=ROLE_CATEGORY', '88c2c00a-6459-469d-9ff1-50f185172613', '1', '1', '2019-03-22 16:53:01', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('972144b8-c6de-4e16-8fb7-81aa66413f9e', '系统菜单', 'sys/menu/list.html', 'f5593479-b410-4a30-9a2c-65f8b4da2f3c', '1', '4', '2019-03-22 16:52:46', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('986f35eb-8b1b-4077-9034-0850cf360281', '系统字典', 'sys/dict/list.html', 'f5593479-b410-4a30-9a2c-65f8b4da2f3c', '1', '3', '2019-03-22 16:52:31', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('9962c2e4-0914-479f-b269-1bf5a30684bd', '数据备份', 'sys/table/bak/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '100', '2019-12-10 14:51:25', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');
INSERT INTO `sys_menu` VALUES ('a62bcc12-2316-4156-9232-03ddd0b29ada', '系统分类维护', 'dev/param/category/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '1', '2019-04-09 14:08:41', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('aa0c976c-4e75-4b7b-b42b-7971f726e384', '部门管理', 'sys/dept/list.html', '4b045ab0-3ef6-4e96-8098-4ee6b559181e', '1', '1', '2019-04-09 13:56:36', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('ba56945c-4203-4751-b659-d46167df0589', '导出配置', 'dev/excel/output/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '100', '2020-01-14 20:40:05', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');
INSERT INTO `sys_menu` VALUES ('d566dd6f-6cc5-4697-811c-0292f5b266cf', '展示配置', 'dev/report/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '100', '2020-01-14 20:40:37', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');
INSERT INTO `sys_menu` VALUES ('e4773e4d-0f64-426a-a5ec-5256e9ac9d2f', '界面配置', 'dev/custom/module/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '100', '2019-11-18 13:43:03', 'f62073b3-9fa5-44af-bc00-52b9894713dd', null, null, '001');
INSERT INTO `sys_menu` VALUES ('e85046d4-206e-4389-8172-68f7220b730f', '菜单权限管理', 'sys/role/menu/list.html', '88c2c00a-6459-469d-9ff1-50f185172613', '1', '4', '2019-03-22 16:54:03', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('f012e1c9-8e42-4385-a39a-4d62cda97dc1', '角色人员分配', 'sys/role/user/list.html', '88c2c00a-6459-469d-9ff1-50f185172613', '1', '3', '2019-03-22 16:53:50', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('f5593479-b410-4a30-9a2c-65f8b4da2f3c', '基础数据', '', '4e278523-efba-47f1-9f5e-796cdc95d399', '1', '2', '2019-04-09 11:53:58', null, null, null, '001');
INSERT INTO `sys_menu` VALUES ('fd7afc76-6a8d-4b22-8fc8-023e2686fde2', '系统日志', 'sys/log/list.html', '6de393d8-f69b-4117-be85-93772e8252b5', '1', '100', '2020-01-14 20:52:09', '893050be-18eb-4682-9921-1109c10235a4', null, null, '001');

-- ----------------------------
-- Table structure for sys_param
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `ID` varchar(36) NOT NULL,
  `PARAM_KEY` varchar(36) NOT NULL COMMENT '参数KEY',
  `PARAM_VALUE` varchar(200) NOT NULL COMMENT '参数值',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表';

-- ----------------------------
-- Records of sys_param
-- ----------------------------
INSERT INTO `sys_param` VALUES ('142805a5-53af-456e-816e-165596280e8a', 'SMS_APPOINT_USE', '0');
INSERT INTO `sys_param` VALUES ('3508a32c-d19e-40db-88ff-638f3862b2b0', 'CALL_SCREEN_BEFOR_HOUR', '-12');
INSERT INTO `sys_param` VALUES ('5ca295f2-ebeb-41b0-8f67-f2a6af0c70fe', 'SMS_APPOINT_USE_CX', '1');
INSERT INTO `sys_param` VALUES ('60961f9b-0ef4-4bb1-ab10-b60eb70e5a52', 'SMS_CX_URL', 'http://101.37.33.76:7777/yktsms/send');
INSERT INTO `sys_param` VALUES ('6ab693b6-b7d3-4cb7-a1e9-fb78edf884d0', 'SMS_CX_APPID', 'KbkXIFbsIWGqEisaLtpmL7WDCKqyr5DS');
INSERT INTO `sys_param` VALUES ('8fb4a43c-a069-4ba8-b72a-44d304d5fe56', 'SMS_CX_APPKEY', 'MTjGL8xb5Ig08qqLFj3uNm0vpNpDiMZ9');
INSERT INTO `sys_param` VALUES ('c2ee8ade-91fe-4f47-b516-7c3cd547e0b5', 'USER_DEFAULT_INIT_PASSWORD', '123456');
INSERT INTO `sys_param` VALUES ('d27a0a2e-9c6c-4333-aaa4-079b909e6be9', 'SMS_CX_TEMPLATE', '【楚雄州人民医院】\r\n尊敬的#{name}，您的预约检查已申请成功。\r\n项目名称：#{resobject}\r\n候诊时间：#{restime}\r\n检查科室：#{resaddress}\r\n注意事项：#{remain}');
INSERT INTO `sys_param` VALUES ('d96cb168-cd48-43e8-bdd1-c7085dd7d879', 'SYS_MANAGE_NAME', '厦门科技公司');
INSERT INTO `sys_param` VALUES ('f2d44d89-6632-4630-8b05-b8665deb63fd', 'SMS_TYPE', 'CXRMYY');
INSERT INTO `sys_param` VALUES ('fc2efafb-4c34-496c-aaa1-86c6e2f461c8', 'CALL_SCREEN_AFTER_HOUR', '12');

-- ----------------------------
-- Table structure for sys_role_menu_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu_auth`;
CREATE TABLE `sys_role_menu_auth` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `FK_ROLE_ID` varchar(36) NOT NULL COMMENT '角色Id',
  `FK_MENU_ID` varchar(36) NOT NULL COMMENT '菜单Id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单分配';

-- ----------------------------
-- Records of sys_role_menu_auth
-- ----------------------------
INSERT INTO `sys_role_menu_auth` VALUES ('313f42c8-4410-4d4d-9f8c-a6388e61812f', '47736f82-416a-4f51-a070-bc925f1a9c4d', '9962c2e4-0914-479f-b269-1bf5a30684bd');
INSERT INTO `sys_role_menu_auth` VALUES ('3915df73-54e2-49bf-8689-31307942cc31', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'a62bcc12-2316-4156-9232-03ddd0b29ada');
INSERT INTO `sys_role_menu_auth` VALUES ('4bc3ae90-b8f1-4c82-a469-7ee9ef2bffbc', '47736f82-416a-4f51-a070-bc925f1a9c4d', '546c6069-5e0f-4700-bd3e-05d8c8fd9a00');
INSERT INTO `sys_role_menu_auth` VALUES ('51561d7e-73c6-4416-83dc-c67867460eeb', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'e85046d4-206e-4389-8172-68f7220b730f');
INSERT INTO `sys_role_menu_auth` VALUES ('5605e7f1-a870-4e46-8057-404a22bd0bce', '47736f82-416a-4f51-a070-bc925f1a9c4d', '37cb8e99-9d81-44ae-9662-391f2839651a');
INSERT INTO `sys_role_menu_auth` VALUES ('659f8542-201e-434f-93b9-5f273a6214fa', '47736f82-416a-4f51-a070-bc925f1a9c4d', '109f71f1-6fd6-4a40-a0f3-ed2debada3cd');
INSERT INTO `sys_role_menu_auth` VALUES ('681e0449-ccab-4df0-901c-217707b06269', '47736f82-416a-4f51-a070-bc925f1a9c4d', '3ac67163-c32e-4a57-98b1-3ae1cc6b1489');
INSERT INTO `sys_role_menu_auth` VALUES ('7069987b-4823-4027-9b4d-460327206218', '47736f82-416a-4f51-a070-bc925f1a9c4d', '84df11db-15cf-40a1-9a66-020e52034578');
INSERT INTO `sys_role_menu_auth` VALUES ('78d7475e-5785-4224-bcfc-a7f3c2be4c3c', '47736f82-416a-4f51-a070-bc925f1a9c4d', '3a49c94b-aebe-4bb8-ae08-cca7842a7740');
INSERT INTO `sys_role_menu_auth` VALUES ('7c1959f3-f5c6-4d70-b153-6194f3f45a44', '47736f82-416a-4f51-a070-bc925f1a9c4d', '4e278523-efba-47f1-9f5e-796cdc95d399');
INSERT INTO `sys_role_menu_auth` VALUES ('95420945-6ad0-419f-a40b-6fea20a90dcc', '47736f82-416a-4f51-a070-bc925f1a9c4d', '6de393d8-f69b-4117-be85-93772e8252b5');
INSERT INTO `sys_role_menu_auth` VALUES ('a3f852d4-be0d-4944-91df-13d0a0618d24', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'f012e1c9-8e42-4385-a39a-4d62cda97dc1');
INSERT INTO `sys_role_menu_auth` VALUES ('a9c73c07-462d-480c-a871-3044e06c02c3', '47736f82-416a-4f51-a070-bc925f1a9c4d', '77622084-6cfc-46b4-9870-91f64bc636a3');
INSERT INTO `sys_role_menu_auth` VALUES ('ac2dbf39-4715-4f5d-8331-61eb88362602', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'aa0c976c-4e75-4b7b-b42b-7971f726e384');
INSERT INTO `sys_role_menu_auth` VALUES ('b17157ff-4e97-484a-ac97-3c6d06d6b2a6', '47736f82-416a-4f51-a070-bc925f1a9c4d', '4b045ab0-3ef6-4e96-8098-4ee6b559181e');
INSERT INTO `sys_role_menu_auth` VALUES ('b8a2af29-0ac8-476e-9298-f0bd10dab5dc', '47736f82-416a-4f51-a070-bc925f1a9c4d', '789e133a-9c57-48d6-951e-20111184f1d9');
INSERT INTO `sys_role_menu_auth` VALUES ('bde79500-d096-4b0e-ac73-5485dad84fe2', '47736f82-416a-4f51-a070-bc925f1a9c4d', '88c2c00a-6459-469d-9ff1-50f185172613');
INSERT INTO `sys_role_menu_auth` VALUES ('bfd7e038-771f-43d0-bc50-36f256bca431', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'ba56945c-4203-4751-b659-d46167df0589');
INSERT INTO `sys_role_menu_auth` VALUES ('c535fd83-bb83-43c6-83d1-0017111fe258', '47736f82-416a-4f51-a070-bc925f1a9c4d', '7a689910-9265-4fd3-b45f-5ba908364c6c');
INSERT INTO `sys_role_menu_auth` VALUES ('c655ea76-b191-4723-bc63-7c7e3416e704', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'e4773e4d-0f64-426a-a5ec-5256e9ac9d2f');
INSERT INTO `sys_role_menu_auth` VALUES ('c727286a-b29d-40b0-8840-99a71cb79f97', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'f5593479-b410-4a30-9a2c-65f8b4da2f3c');
INSERT INTO `sys_role_menu_auth` VALUES ('ce33174d-b651-4f2d-b270-11630e7e1533', '47736f82-416a-4f51-a070-bc925f1a9c4d', '8bb945d5-4abe-4dee-8b2c-0ce345bb8da9');
INSERT INTO `sys_role_menu_auth` VALUES ('da02bc20-d578-4351-a53d-ce0f4febf355', '47736f82-416a-4f51-a070-bc925f1a9c4d', '972144b8-c6de-4e16-8fb7-81aa66413f9e');
INSERT INTO `sys_role_menu_auth` VALUES ('e06cbd28-20be-46b8-b751-17798a2e195d', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'fd7afc76-6a8d-4b22-8fc8-023e2686fde2');
INSERT INTO `sys_role_menu_auth` VALUES ('e194d968-eeca-41ef-9140-3b0b0853d987', '47736f82-416a-4f51-a070-bc925f1a9c4d', 'd566dd6f-6cc5-4697-811c-0292f5b266cf');
INSERT INTO `sys_role_menu_auth` VALUES ('e70a5756-21cf-412f-9417-d68aab0d102a', '47736f82-416a-4f51-a070-bc925f1a9c4d', '3bd20929-3c6e-4446-be6d-e3025bf19daf');
INSERT INTO `sys_role_menu_auth` VALUES ('ea7f9174-54d7-404e-94f6-fd59d191a2b8', '47736f82-416a-4f51-a070-bc925f1a9c4d', '986f35eb-8b1b-4077-9034-0850cf360281');
INSERT INTO `sys_role_menu_auth` VALUES ('ee63cf95-e080-4fc4-96d7-7e57de40e3a5', '47736f82-416a-4f51-a070-bc925f1a9c4d', '75182b43-6402-4ccd-adc4-7e512d26b2bb');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `FK_USER_ID` varchar(36) NOT NULL COMMENT '用户Id',
  `FK_ROLE_ID` varchar(36) NOT NULL COMMENT '角色Id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色用户分配';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('88b8e7f2-c6ac-4dc5-a73b-a76889356a59', '893050be-18eb-4682-9921-1109c10235a4', '47736f82-416a-4f51-a070-bc925f1a9c4d');

-- ----------------------------
-- Table structure for sys_system_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_system_log`;
CREATE TABLE `sys_system_log` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `IP_ADDRESS` varchar(36) DEFAULT NULL COMMENT 'IP地址',
  `REQUEST_ROUTING` varchar(128) DEFAULT NULL COMMENT '请求路由',
  `PARAM` varchar(2048) DEFAULT NULL COMMENT '参数',
  `REQUEST_TYPE_CODE` varchar(128) DEFAULT NULL COMMENT '请求类型编码',
  `REQUEST_TYPE_NAME` varchar(128) DEFAULT NULL COMMENT '请求类型名称',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '请求时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

-- ----------------------------
-- Records of sys_system_log
-- ----------------------------
INSERT INTO `sys_system_log` VALUES ('006fd657-69c3-43ab-b359-43e76a8e1793', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090865678\"]}}', '01', '请求参数', '2020-01-15 20:21:06');
INSERT INTO `sys_system_log` VALUES ('01052b28-cd7d-4ac5-b0de-0e9328c4ddf0', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579092896245\"]}}', '02', '返回参数', '2020-01-15 20:54:56');
INSERT INTO `sys_system_log` VALUES ('01765d2b-2596-49d9-8288-99da8cb0671d', '127.0.0.1', '/api/dev/excel/import/config/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579093980306\"]}}', '01', '请求参数', '2020-01-15 21:13:00');
INSERT INTO `sys_system_log` VALUES ('018fc24f-1303-4da8-aa9d-7136236fa288', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354384\"]}}', '02', '返回参数', '2020-01-15 21:02:44');
INSERT INTO `sys_system_log` VALUES ('02517e44-7aad-4863-a4a0-4a672e5652e6', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"fkDictCategoryCode\":[\"JOB_LOG\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091153020\"]}}', '01', '请求参数', '2020-01-15 20:25:53');
INSERT INTO `sys_system_log` VALUES ('025a6b0b-346c-4d44-9e16-593b98afc402', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351502\"]}}', '01', '请求参数', '2020-01-15 21:02:32');
INSERT INTO `sys_system_log` VALUES ('0401e2c8-6fae-4247-8047-11ae084f41f2', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093630899\"]}}', '02', '返回参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('06868576-20d9-4896-bd81-bb9b8366d241', '127.0.0.1', '/api/job/task/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091152028\"]}}', '01', '请求参数', '2020-01-15 20:25:52');
INSERT INTO `sys_system_log` VALUES ('06c31632-32d3-4dc8-a09b-03e9c5c7abd2', '127.0.0.1', '/api/dev/param/group/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkParamCategoryCode\":[\"SYS_PARAM\"],\"_\":[\"1579091169244\"]}}', '01', '请求参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('06ee40f3-abc8-4411-af81-52a19e582590', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093715155\"]}}', '02', '返回参数', '2020-01-15 21:08:35');
INSERT INTO `sys_system_log` VALUES ('071d7369-ada9-4b52-bc4a-976011c67d69', '127.0.0.1', '/api/dev/custom/module', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:21:02');
INSERT INTO `sys_system_log` VALUES ('073450c0-b73f-46d4-a0e2-b6ec80e09e7a', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091086808\"]}}', '01', '请求参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('078155cc-94c1-49c1-9dfd-b981a3c89c8f', '127.0.0.1', '/api/dev/excel/import/config/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093980305\"]}}', '01', '请求参数', '2020-01-15 21:13:00');
INSERT INTO `sys_system_log` VALUES ('0a41f984-dba6-4543-9cba-6fb21eeeac1f', '127.0.0.1', '/api/current/logout', '{}', '01', '请求参数', '2020-01-15 21:11:20');
INSERT INTO `sys_system_log` VALUES ('0acd22df-8899-492d-acf9-448925918807', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579092896245\"]}}', '01', '请求参数', '2020-01-15 20:54:56');
INSERT INTO `sys_system_log` VALUES ('0c289ef7-3ed6-41ff-bd5f-eb85d74271b7', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093723311\"]}}', '02', '返回参数', '2020-01-15 21:08:44');
INSERT INTO `sys_system_log` VALUES ('0c9e00f7-dab8-444b-b638-8203d073ff1f', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579093228648\"]}}', '02', '返回参数', '2020-01-15 21:00:29');
INSERT INTO `sys_system_log` VALUES ('0cc362eb-e24f-4851-93b3-8808f741b2ff', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092956717\"]}}', '02', '返回参数', '2020-01-15 20:55:57');
INSERT INTO `sys_system_log` VALUES ('0cd44c95-4369-4b02-a4b9-7dc4ff8b8dd2', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093865283\"]}}', '02', '返回参数', '2020-01-15 21:11:05');
INSERT INTO `sys_system_log` VALUES ('0cdc219f-5673-4903-99a4-ae80579c0a40', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091141015\"]}}', '02', '返回参数', '2020-01-15 20:25:43');
INSERT INTO `sys_system_log` VALUES ('0e0ef54d-fc53-4a20-b8e5-c519f8bcb7f7', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579094006839\"]}}', '02', '返回参数', '2020-01-15 21:13:27');
INSERT INTO `sys_system_log` VALUES ('0e3fc7eb-c137-4fb1-aa5e-1164f7ed539e', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2020-01-15\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2020-01-15\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354382\"]}}', '02', '返回参数', '2020-01-15 21:02:36');
INSERT INTO `sys_system_log` VALUES ('1047d16e-9140-4bf0-b8ca-2716c786b1b6', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090238128\"]}}', '01', '请求参数', '2020-01-15 20:10:38');
INSERT INTO `sys_system_log` VALUES ('11475f7f-d503-4c50-9d4f-f50e6b66fccd', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091153021\"]}}', '02', '返回参数', '2020-01-15 20:25:53');
INSERT INTO `sys_system_log` VALUES ('1157df7b-262f-478b-b4a5-2c3f35259cf3', '127.0.0.1', '/api/sys/menu/7a689910-9265-4fd3-b45f-5ba908364c6c/1', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:53:58');
INSERT INTO `sys_system_log` VALUES ('11ed6c85-b7db-4837-8472-c1a82089f08a', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091183877\"]}}', '01', '请求参数', '2020-01-15 20:26:24');
INSERT INTO `sys_system_log` VALUES ('148b5b38-1db8-492f-9659-23ff30a04ded', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579091136793\"]}}', '02', '返回参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('14e35ef7-0fc6-4317-bbb3-a3242dcde745', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092896620\"]}}', '02', '返回参数', '2020-01-15 20:54:57');
INSERT INTO `sys_system_log` VALUES ('15730d74-a767-4ab8-aa57-7218a96a92e5', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091141013\"]}}', '02', '返回参数', '2020-01-15 20:25:41');
INSERT INTO `sys_system_log` VALUES ('1604ca76-1f82-4b7f-a337-f0fe810dc9df', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999083\"]}}', '01', '请求参数', '2020-01-15 21:13:24');
INSERT INTO `sys_system_log` VALUES ('163fbdd6-9697-4bb0-ae63-08400fe6d166', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091153022\"]}}', '01', '请求参数', '2020-01-15 20:25:54');
INSERT INTO `sys_system_log` VALUES ('16955269-6fe5-4b2d-95e1-9d0c07f02783', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093619285\"]}}', '02', '返回参数', '2020-01-15 21:07:00');
INSERT INTO `sys_system_log` VALUES ('16fe774f-afb4-49c8-a1f0-64d738436048', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354391\"]}}', '01', '请求参数', '2020-01-15 21:03:44');
INSERT INTO `sys_system_log` VALUES ('17cbc602-6df9-49d0-9f46-e2716030606d', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093715155\"]}}', '01', '请求参数', '2020-01-15 21:08:35');
INSERT INTO `sys_system_log` VALUES ('17fa382b-0469-4b8d-ad8e-48f522117a5d', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"789c2078-83b1-458b-a1fd-dc8d395c8e2c\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093881839\"]}}', '01', '请求参数', '2020-01-15 21:11:22');
INSERT INTO `sys_system_log` VALUES ('190327e6-1319-482f-9535-f2dea33527f5', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091091367\"]}}', '02', '返回参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('197893c9-9f18-4a49-9d45-e90e83801952', '127.0.0.1', '/api/dev/custom/module/9a45dba5-3924-4355-9da8-34aa49eb9fad', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090865677\"]}}', '02', '返回参数', '2020-01-15 20:21:06');
INSERT INTO `sys_system_log` VALUES ('19ddf296-180b-4423-b6c7-812198e75db7', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093354381\"]}}', '02', '返回参数', '2020-01-15 21:02:35');
INSERT INTO `sys_system_log` VALUES ('19e374f6-40dd-42c0-a017-9a64a189bcc7', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351505\"]}}', '02', '返回参数', '2020-01-15 21:02:53');
INSERT INTO `sys_system_log` VALUES ('1a3db28d-d7e6-49d5-89bb-dfb430090bed', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090221095\"]}}', '02', '返回参数', '2020-01-15 20:10:22');
INSERT INTO `sys_system_log` VALUES ('1c460db8-6e6f-45de-9296-487457a2e4fc', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091183878\"]}}', '01', '请求参数', '2020-01-15 20:53:40');
INSERT INTO `sys_system_log` VALUES ('1e85a157-9a1f-4d9d-9d97-c54cc2ee053b', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999085\"]}}', '02', '返回参数', '2020-01-15 21:13:51');
INSERT INTO `sys_system_log` VALUES ('1e988787-bfd8-4ce3-bbcf-33fce1d74a96', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999085\"]}}', '01', '请求参数', '2020-01-15 21:13:51');
INSERT INTO `sys_system_log` VALUES ('1fd0b697-6b96-4fa8-a202-ce6a850d9f5e', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006840\"]}}', '02', '返回参数', '2020-01-15 21:13:27');
INSERT INTO `sys_system_log` VALUES ('206912b6-6a9a-4aa8-8142-4a7e7f0ed8f8', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354384\"]}}', '01', '请求参数', '2020-01-15 21:02:44');
INSERT INTO `sys_system_log` VALUES ('2142d8be-1a31-43b1-b05d-68846f1484ae', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579093999082\"]}}', '02', '返回参数', '2020-01-15 21:13:19');
INSERT INTO `sys_system_log` VALUES ('21cec987-41e6-427b-9898-99cd5982c187', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091136459\"]}}', '01', '请求参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('22ec4f5e-72da-4767-a4c7-8208d6ecb9d1', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351502\"]}}', '02', '返回参数', '2020-01-15 21:02:32');
INSERT INTO `sys_system_log` VALUES ('23a68d58-be75-4457-a067-2dd884f2acd1', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090448146\"]}}', '02', '返回参数', '2020-01-15 20:14:08');
INSERT INTO `sys_system_log` VALUES ('2416dfee-44a6-4905-a780-363f9ca7ed60', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092832629\"]}}', '02', '返回参数', '2020-01-15 20:53:58');
INSERT INTO `sys_system_log` VALUES ('2481eb3a-d007-4ff8-998c-448dd3011e77', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093131366\"]}}', '02', '返回参数', '2020-01-15 20:58:52');
INSERT INTO `sys_system_log` VALUES ('2699c6d4-9e15-4864-86af-bcc6587ac85e', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093715154\"]}}', '02', '返回参数', '2020-01-15 21:08:35');
INSERT INTO `sys_system_log` VALUES ('26fb9df2-adea-42a7-ac7c-56dc62359828', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091136794\"]}}', '01', '请求参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('278eb949-43ea-4c36-8b24-b5dbe4f6fe00', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090238129\"]}}', '01', '请求参数', '2020-01-15 20:10:38');
INSERT INTO `sys_system_log` VALUES ('27c0f6d0-5cfd-42ec-958b-c1b943d793bc', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579091136793\"]}}', '01', '请求参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('284cee76-3891-4872-b5e3-956c24168d07', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093174838\"]}}', '02', '返回参数', '2020-01-15 20:59:35');
INSERT INTO `sys_system_log` VALUES ('28c78645-9b65-4171-93c2-e8d816d618f4', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091086808\"]}}', '02', '返回参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('2911b26f-5fa6-41b8-ba95-e53f3f1a1641', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"789c2078-83b1-458b-a1fd-dc8d395c8e2c\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093881861\"]}}', '01', '请求参数', '2020-01-15 21:11:22');
INSERT INTO `sys_system_log` VALUES ('2a1f813f-85c6-40f3-8c36-390c21ed9a22', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093621215\"]}}', '01', '请求参数', '2020-01-15 21:07:02');
INSERT INTO `sys_system_log` VALUES ('2c3e8dc8-ca03-4ccb-83c8-15330ac5ba30', '127.0.0.1', '/api/sys/table/bak/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093730195\"]}}', '01', '请求参数', '2020-01-15 21:08:50');
INSERT INTO `sys_system_log` VALUES ('2d6c1062-6a5a-415c-a116-64ea1278379d', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579090970874\"]}}', '01', '请求参数', '2020-01-15 20:22:51');
INSERT INTO `sys_system_log` VALUES ('2d7acd6e-b251-416a-84cd-19027c02325c', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"JOB_TYPE\"],\"itemShowType\":[\"0\"],\"keyword\":[\"\"],\"_\":[\"1579091169708\"]}}', '02', '返回参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('2e9117fc-9f1b-4858-9588-66e9677291ac', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090448074\"]}}', '01', '请求参数', '2020-01-15 20:14:08');
INSERT INTO `sys_system_log` VALUES ('2f5f6d5d-c189-4549-8b18-9a5df4777d1c', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090448554\"]}}', '01', '请求参数', '2020-01-15 20:14:09');
INSERT INTO `sys_system_log` VALUES ('2fbdaba1-b628-4ff0-9eba-a275913baf6f', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-09\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-09\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999086\"]}}', '02', '返回参数', '2020-01-15 21:13:55');
INSERT INTO `sys_system_log` VALUES ('30231015-69de-4527-9cf3-b6e15a60921d', '127.0.0.1', '/api/sys/menu', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:54:51');
INSERT INTO `sys_system_log` VALUES ('309b359e-0e49-4ef5-a1c6-881f8c5622c4', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354385\"]}}', '01', '请求参数', '2020-01-15 21:02:45');
INSERT INTO `sys_system_log` VALUES ('31be99ba-d0f7-43f4-bfe0-a63b6efbc2e0', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090238129\"]}}', '02', '返回参数', '2020-01-15 20:10:38');
INSERT INTO `sys_system_log` VALUES ('339309b1-422f-4b0b-8fd9-ee130afc1e3f', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"fkDictCategoryCode\":[\"JOB_LOG\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093860142\"]}}', '02', '返回参数', '2020-01-15 21:11:00');
INSERT INTO `sys_system_log` VALUES ('3516fda2-1f7f-44c4-9fb9-243bd3763f9f', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093631260\"]}}', '01', '请求参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('35874024-ce84-461e-afcc-49a5c21ce979', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019/12/02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019/12/02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093634183\"]}}', '01', '请求参数', '2020-01-15 21:07:32');
INSERT INTO `sys_system_log` VALUES ('35c2bff6-6faa-4f53-b078-78a759c1a823', '127.0.0.1', '/api/sys/table/bak/tab/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091176596\"]}}', '02', '返回参数', '2020-01-15 20:26:17');
INSERT INTO `sys_system_log` VALUES ('35ca3951-a2f8-4b93-aa14-1766bd45f17a', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092967793\"]}}', '01', '请求参数', '2020-01-15 20:56:08');
INSERT INTO `sys_system_log` VALUES ('36c7e3cc-0770-4df2-a317-d4f3d48de47e', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351505\"]}}', '01', '请求参数', '2020-01-15 21:02:53');
INSERT INTO `sys_system_log` VALUES ('370e1997-096d-4aec-b49a-0b50f832a0ca', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092900595\"]}}', '01', '请求参数', '2020-01-15 20:55:01');
INSERT INTO `sys_system_log` VALUES ('37598bac-1097-46c3-bc61-f9959cb44a31', '127.0.0.1', '/api/dev/excel/output/a2ecbb1f-831a-4e84-b450-bd5227bce98d', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092979697\"]}}', '01', '请求参数', '2020-01-15 20:56:20');
INSERT INTO `sys_system_log` VALUES ('37fea843-22dc-42a1-9d49-80aa491fa791', '127.0.0.1', '/api/current/logout', '{}', '01', '请求参数', '2020-01-15 21:11:27');
INSERT INTO `sys_system_log` VALUES ('38327df8-bacc-43bb-8c22-d27bc6f88d79', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090221095\"]}}', '01', '请求参数', '2020-01-15 20:10:21');
INSERT INTO `sys_system_log` VALUES ('383a60cf-d8ef-465e-8b67-80cd53e7e1db', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092975336\"]}}', '02', '返回参数', '2020-01-15 20:57:23');
INSERT INTO `sys_system_log` VALUES ('3a092c9a-ba00-4b8e-8429-9384c171d20c', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093634182\"]}}', '01', '请求参数', '2020-01-15 21:07:14');
INSERT INTO `sys_system_log` VALUES ('3a898086-80dc-4912-a3ed-71dbe2a5d5ba', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"JOB_TYPE\"],\"itemShowType\":[\"0\"],\"keyword\":[\"\"],\"_\":[\"1579091169708\"]}}', '01', '请求参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('3acb7233-f521-4f38-9c36-3d5cc64ddb61', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579091087046\"]}}', '01', '请求参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('3b5b84c9-3dec-4aca-ae74-fe33db894a6c', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092846607\"]}}', '01', '请求参数', '2020-01-15 20:54:07');
INSERT INTO `sys_system_log` VALUES ('3c24fc3c-29d4-4e26-858f-71aaea53afc3', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091183878\"]}}', '02', '返回参数', '2020-01-15 20:53:40');
INSERT INTO `sys_system_log` VALUES ('3c7a8fe0-f92c-4966-a854-0d2852faff91', '127.0.0.1', '/api/sys/menu/7a689910-9265-4fd3-b45f-5ba908364c6c', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092843313\"]}}', '01', '请求参数', '2020-01-15 20:54:03');
INSERT INTO `sys_system_log` VALUES ('3ddbc97e-4a9e-40a3-a73c-f783d9bf9042', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354387\"]}}', '01', '请求参数', '2020-01-15 21:02:46');
INSERT INTO `sys_system_log` VALUES ('3f0ce38f-a287-4160-8175-f76ab9260203', '127.0.0.1', '/api/dev/param/category/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091169243\"]}}', '02', '返回参数', '2020-01-15 20:26:09');
INSERT INTO `sys_system_log` VALUES ('3f7269e1-e945-4e0a-85a8-29a73364949e', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351504\"]}}', '02', '返回参数', '2020-01-15 21:02:51');
INSERT INTO `sys_system_log` VALUES ('40b3859d-0258-4ff7-8c55-ce209ed4bb21', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"fkDictCategoryCode\":[\"JOB_LOG\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093860142\"]}}', '01', '请求参数', '2020-01-15 21:11:00');
INSERT INTO `sys_system_log` VALUES ('4273ea40-1404-4d74-a88c-f785aa39b1cd', '127.0.0.1', '/api/sys/dict/item/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"MENU_SYSTEM\"],\"_\":[\"1579092843314\"]}}', '02', '返回参数', '2020-01-15 20:54:04');
INSERT INTO `sys_system_log` VALUES ('43cfdeca-07b0-4a12-923d-bf59fc2f46c7', '127.0.0.1', '/api/dev/excel/import/data/ATT_MANAGE', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"]}}', '01', '请求参数', '2020-01-15 21:13:15');
INSERT INTO `sys_system_log` VALUES ('443191a4-b983-4b74-b719-a684a0ae96b2', '127.0.0.1', '/api/dev/excel/output', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:58:27');
INSERT INTO `sys_system_log` VALUES ('4444cb4d-759c-4b88-8c32-336533657406', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091094272\"]}}', '02', '返回参数', '2020-01-15 20:24:55');
INSERT INTO `sys_system_log` VALUES ('446b7291-c04d-4d87-b501-35a01ae04ba4', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090460620\"]}}', '01', '请求参数', '2020-01-15 20:14:21');
INSERT INTO `sys_system_log` VALUES ('456fecc2-4381-4049-bcfe-96f4bd3f3384', '127.0.0.1', '/api/dev/excel/import/data/ATT_MANAGE', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"]}}', '02', '返回参数', '2020-01-15 21:13:16');
INSERT INTO `sys_system_log` VALUES ('4589c238-5f67-4b48-8167-3630d6b1161f', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"789c2078-83b1-458b-a1fd-dc8d395c8e2c\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093881839\"]}}', '02', '返回参数', '2020-01-15 21:11:22');
INSERT INTO `sys_system_log` VALUES ('4724142c-b17e-45a9-80e5-1f6f388f67f6', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093135978\"]}}', '01', '请求参数', '2020-01-15 20:58:56');
INSERT INTO `sys_system_log` VALUES ('47408c72-6d12-4edd-bc8f-ea38603f411a', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090448073\"]}}', '02', '返回参数', '2020-01-15 20:14:08');
INSERT INTO `sys_system_log` VALUES ('47cb7f05-b6d6-4788-b138-c5b38b650a13', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091185482\"]}}', '02', '返回参数', '2020-01-15 20:26:26');
INSERT INTO `sys_system_log` VALUES ('48977c8f-44f6-4671-9224-ef1cbf23e439', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090221094\"]}}', '01', '请求参数', '2020-01-15 20:10:21');
INSERT INTO `sys_system_log` VALUES ('495581d4-632b-4670-8f6d-6e519dc3f162', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090448146\"]}}', '01', '请求参数', '2020-01-15 20:14:08');
INSERT INTO `sys_system_log` VALUES ('4c1c25a3-49c9-4ef2-983b-6a6aa8927bb4', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093131367\"]}}', '01', '请求参数', '2020-01-15 20:58:52');
INSERT INTO `sys_system_log` VALUES ('4d5195c7-c4a8-4f7a-bf24-8c552367d0f6', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-01\"],\"endTime\":[\"2020-01-22\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090223807\"]}}', '02', '返回参数', '2020-01-15 20:10:30');
INSERT INTO `sys_system_log` VALUES ('4d6bc3af-6c28-4394-9dfa-d15595a986de', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090223806\"]}}', '02', '返回参数', '2020-01-15 20:10:24');
INSERT INTO `sys_system_log` VALUES ('4d7a3b5b-bc42-447f-b90a-48c137fa00c9', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-09\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-09\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999087\"]}}', '02', '返回参数', '2020-01-15 21:13:56');
INSERT INTO `sys_system_log` VALUES ('4dc25061-4154-4ca4-bb15-9fa89ee740b5', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351503\"]}}', '01', '请求参数', '2020-01-15 21:02:33');
INSERT INTO `sys_system_log` VALUES ('4e01ec9f-314a-4c01-943b-29fa9fb09571', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092977059\"]}}', '02', '返回参数', '2020-01-15 20:58:28');
INSERT INTO `sys_system_log` VALUES ('4e845f98-af15-4ac1-bb1e-957274ef48e4', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354389\"]}}', '01', '请求参数', '2020-01-15 21:03:04');
INSERT INTO `sys_system_log` VALUES ('4ed09d4c-30d4-4acf-86d3-a92eee65fa2f', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093721584\"]}}', '01', '请求参数', '2020-01-15 21:08:42');
INSERT INTO `sys_system_log` VALUES ('4f89a049-4dd3-4c01-9d32-a701db897406', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019/12/02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019/12/02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354392\"]}}', '02', '返回参数', '2020-01-15 21:04:54');
INSERT INTO `sys_system_log` VALUES ('501ae7f1-bfc1-4041-9e65-65ff79dcce64', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093894701\"]}}', '01', '请求参数', '2020-01-15 21:11:35');
INSERT INTO `sys_system_log` VALUES ('501fae37-4c47-4da6-9c63-98501791b9a2', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090223808\"]}}', '02', '返回参数', '2020-01-15 20:10:34');
INSERT INTO `sys_system_log` VALUES ('50601934-205a-4f29-bd8b-b2648f993007', '127.0.0.1', '/api/dev/param/config/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkParamCategoryCode\":[\"SYS_PARAM\"],\"fkParamGroupCode\":[\"SYS_MANAGE\"],\"_\":[\"1579091169245\"]}}', '01', '请求参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('5197b1af-1f24-4453-85e4-e086823a4e52', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354386\"]}}', '02', '返回参数', '2020-01-15 21:02:45');
INSERT INTO `sys_system_log` VALUES ('51e0a763-e603-44b1-b28c-99d38b3021c1', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090865678\"]}}', '02', '返回参数', '2020-01-15 20:21:07');
INSERT INTO `sys_system_log` VALUES ('52a1666d-916d-4272-a4e8-e54a8af51df2', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090448073\"]}}', '01', '请求参数', '2020-01-15 20:14:08');
INSERT INTO `sys_system_log` VALUES ('552c180e-b800-4027-87c0-e3e79860a3fe', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:53:40');
INSERT INTO `sys_system_log` VALUES ('56407a67-3a3e-47aa-b73b-999363cddd35', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093619285\"]}}', '01', '请求参数', '2020-01-15 21:07:00');
INSERT INTO `sys_system_log` VALUES ('5668a9c1-8c53-475a-a144-10ffe0726030', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092975335\"]}}', '02', '返回参数', '2020-01-15 20:56:16');
INSERT INTO `sys_system_log` VALUES ('572cbda1-b4ce-4bee-ae4a-092e938d44e9', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090448555\"]}}', '02', '返回参数', '2020-01-15 20:14:09');
INSERT INTO `sys_system_log` VALUES ('5731a2f9-01b8-4603-bbf8-0e12be2e2bef', '127.0.0.1', '/api/dev/excel/import/config/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093339264\"]}}', '01', '请求参数', '2020-01-15 21:02:19');
INSERT INTO `sys_system_log` VALUES ('57364c7d-3ae0-414d-9178-dfe9b9776459', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-01\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-01\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354383\"]}}', '02', '返回参数', '2020-01-15 21:02:39');
INSERT INTO `sys_system_log` VALUES ('577db3c5-0be6-43e7-8292-cf67023fa0e9', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093860143\"]}}', '01', '请求参数', '2020-01-15 21:11:00');
INSERT INTO `sys_system_log` VALUES ('57b9cf40-2b63-44de-a596-5e18f1e4ea45', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091086785\"]}}', '02', '返回参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('585ec182-c07b-4219-8735-f78c47330aa8', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093894747\"]}}', '02', '返回参数', '2020-01-15 21:11:35');
INSERT INTO `sys_system_log` VALUES ('58e7f836-79f3-4ee9-bf78-dc0dfd5853e3', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092956717\"]}}', '01', '请求参数', '2020-01-15 20:55:57');
INSERT INTO `sys_system_log` VALUES ('595a113c-8496-4d6c-9374-8eb35e808329', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091119267\"]}}', '02', '返回参数', '2020-01-15 20:25:20');
INSERT INTO `sys_system_log` VALUES ('5abede10-fc1b-4bfd-a660-23443b4a9a2e', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093860143\"]}}', '02', '返回参数', '2020-01-15 21:11:00');
INSERT INTO `sys_system_log` VALUES ('5ae6a927-9ccf-4a08-ac71-80606e9e1fb3', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093631260\"]}}', '02', '返回参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('5b85523d-5f46-430e-82d8-ac0029f0cb13', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579093228648\"]}}', '01', '请求参数', '2020-01-15 21:00:29');
INSERT INTO `sys_system_log` VALUES ('5d4634d1-4c19-4336-a8f5-97b7a53ab51f', '127.0.0.1', '/api/sys/menu', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:24:44');
INSERT INTO `sys_system_log` VALUES ('5e37f440-3bd9-46e4-bce3-362c5eab46d9', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2020-01-15\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2020-01-15\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092967794\"]}}', '01', '请求参数', '2020-01-15 20:56:09');
INSERT INTO `sys_system_log` VALUES ('5e3d5aa2-69e7-4dbd-9798-c6c2ece8b3db', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091091497\"]}}', '02', '返回参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('5e98afee-1d94-4e38-9409-22c82c74f64b', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090221094\"]}}', '02', '返回参数', '2020-01-15 20:10:22');
INSERT INTO `sys_system_log` VALUES ('6003268a-b434-4ade-86e5-e9cba7f7dbf3', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091119267\"]}}', '01', '请求参数', '2020-01-15 20:25:20');
INSERT INTO `sys_system_log` VALUES ('6058cbb2-056c-4bff-9ff5-a9e45246cecb', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093715252\"]}}', '02', '返回参数', '2020-01-15 21:08:35');
INSERT INTO `sys_system_log` VALUES ('61309af6-bbbe-4c4e-a33b-508751814e08', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579092896275\"]}}', '01', '请求参数', '2020-01-15 20:54:56');
INSERT INTO `sys_system_log` VALUES ('61ade9fe-e1df-46e8-b12a-0fe82c754e0d', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"789c2078-83b1-458b-a1fd-dc8d395c8e2c\"],\"_\":[\"1579093881838\"]}}', '01', '请求参数', '2020-01-15 21:11:22');
INSERT INTO `sys_system_log` VALUES ('632f8c41-5538-40af-b7e5-b83908dbf94b', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090240689\"]}}', '01', '请求参数', '2020-01-15 20:10:41');
INSERT INTO `sys_system_log` VALUES ('63cdca0a-c594-4851-bf82-6c5da294015b', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092977058\"]}}', '02', '返回参数', '2020-01-15 20:57:00');
INSERT INTO `sys_system_log` VALUES ('64fea546-80a4-485c-b136-602d65d03cab', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091086786\"]}}', '02', '返回参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('6541ab7e-dcdb-449a-b470-9bad261f2ab5', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093131366\"]}}', '01', '请求参数', '2020-01-15 20:58:52');
INSERT INTO `sys_system_log` VALUES ('65f7d093-cce0-4e63-bccf-23f22b5829e1', '127.0.0.1', '/api/dev/param/config/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkParamCategoryCode\":[\"SYS_PARAM\"],\"fkParamGroupCode\":[\"SYS_MANAGE\"],\"_\":[\"1579091169245\"]}}', '02', '返回参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('661334d3-4bcc-4083-bf80-5f70222a54ff', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093174838\"]}}', '01', '请求参数', '2020-01-15 20:59:35');
INSERT INTO `sys_system_log` VALUES ('668cdc8d-5f8b-4524-86db-58e2479f4daf', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090221206\"]}}', '01', '请求参数', '2020-01-15 20:10:21');
INSERT INTO `sys_system_log` VALUES ('66a19322-6e0b-4def-a6ee-56d205e4b920', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579093894700\"]}}', '01', '请求参数', '2020-01-15 21:11:35');
INSERT INTO `sys_system_log` VALUES ('671a04de-f32d-4f1f-9330-7f65fce8ea3f', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"789c2078-83b1-458b-a1fd-dc8d395c8e2c\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093881861\"]}}', '02', '返回参数', '2020-01-15 21:11:22');
INSERT INTO `sys_system_log` VALUES ('67bde52f-0834-482f-9fe2-db37cf43e691', '127.0.0.1', '/api/dev/custom/module/9a45dba5-3924-4355-9da8-34aa49eb9fad', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091121030\"]}}', '01', '请求参数', '2020-01-15 20:25:21');
INSERT INTO `sys_system_log` VALUES ('6839e044-2914-42dc-b34d-8ba33598fcfe', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093630900\"]}}', '02', '返回参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('684f3225-406e-48bb-bb82-84fa44c093bc', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999084\"]}}', '01', '请求参数', '2020-01-15 21:13:50');
INSERT INTO `sys_system_log` VALUES ('68d78183-7242-4b46-ab56-6e95962d582a', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090221206\"]}}', '02', '返回参数', '2020-01-15 20:10:22');
INSERT INTO `sys_system_log` VALUES ('6915f736-eeb3-47de-9a54-2c2bdc9a47cf', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091091497\"]}}', '01', '请求参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('6992186a-107d-4bfa-847a-4c0ff0fe5c99', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091094271\"]}}', '02', '返回参数', '2020-01-15 20:24:54');
INSERT INTO `sys_system_log` VALUES ('69a3099d-8fda-42ff-90f7-d58fd546c294', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091141012\"]}}', '02', '返回参数', '2020-01-15 20:25:41');
INSERT INTO `sys_system_log` VALUES ('69ed0282-356f-4956-b234-8fef4af1cb6b', '127.0.0.1', '/api/dev/custom/module', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:25:33');
INSERT INTO `sys_system_log` VALUES ('6ae40222-668b-4751-bd84-a60c54b5db85', '127.0.0.1', '/api/sys/table/bak/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093730195\"]}}', '02', '返回参数', '2020-01-15 21:08:50');
INSERT INTO `sys_system_log` VALUES ('6b8c00de-29eb-49c2-9687-24734eb3a9fc', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092857585\"]}}', '01', '请求参数', '2020-01-15 20:54:18');
INSERT INTO `sys_system_log` VALUES ('6c1d7aaa-8e29-45a5-b41f-de24ed98788a', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579093894700\"]}}', '02', '返回参数', '2020-01-15 21:11:35');
INSERT INTO `sys_system_log` VALUES ('6c394777-98b6-4f9e-823a-7e18ddeadf18', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006843\"]}}', '02', '返回参数', '2020-01-15 21:13:33');
INSERT INTO `sys_system_log` VALUES ('6c86440a-4f27-4ee8-bc16-663008a7e441', '127.0.0.1', '/api/sys/table/bak/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091174484\"]}}', '01', '请求参数', '2020-01-15 20:26:15');
INSERT INTO `sys_system_log` VALUES ('6c896f99-0e11-4cac-b4ee-1b9ef26e2e62', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091091367\"]}}', '01', '请求参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('6d6881fb-4c15-4db8-bc42-402dd39a46fd', '127.0.0.1', '/api/sys/dict/category/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091169707\"]}}', '01', '请求参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('6ea77213-0b90-490c-93ee-dbc1430ae0a0', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091141014\"]}}', '02', '返回参数', '2020-01-15 20:25:43');
INSERT INTO `sys_system_log` VALUES ('6f2a3f69-a415-43ab-8de3-80e5cda9f7f7', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092977059\"]}}', '01', '请求参数', '2020-01-15 20:58:28');
INSERT INTO `sys_system_log` VALUES ('70a8a99d-c2a6-442b-a533-9e94cce1afeb', '127.0.0.1', '/api/sys/menu/7a689910-9265-4fd3-b45f-5ba908364c6c/1', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:53:58');
INSERT INTO `sys_system_log` VALUES ('72e52dc9-bdab-477e-a2f5-b4e9b698564b', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579091091788\"]}}', '01', '请求参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('73e259fb-e9b7-4bc6-a1fd-ecb81098dd0d', '127.0.0.1', '/api/dev/custom/module', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:21:03');
INSERT INTO `sys_system_log` VALUES ('741a564a-5ba8-464d-9e29-d23c5656bfd2', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2020-01-15\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2020-01-15\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092900596\"]}}', '01', '请求参数', '2020-01-15 20:55:04');
INSERT INTO `sys_system_log` VALUES ('74bcd50d-8b3b-4e99-a123-e51fb0611668', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093174837\"]}}', '02', '返回参数', '2020-01-15 20:59:35');
INSERT INTO `sys_system_log` VALUES ('74eafe33-640a-4933-a72a-c226a2053c6f', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351503\"]}}', '02', '返回参数', '2020-01-15 21:02:33');
INSERT INTO `sys_system_log` VALUES ('75961ded-1273-495d-9537-3e616dcd9411', '127.0.0.1', '/api/dev/excel/output/a2ecbb1f-831a-4e84-b450-bd5227bce98d', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093046536\"]}}', '01', '请求参数', '2020-01-15 20:57:27');
INSERT INTO `sys_system_log` VALUES ('773d5046-938a-4c61-ba86-2558bcaefc2c', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093631039\"]}}', '01', '请求参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('77cc7025-b230-4a9b-bf4c-fd97874e85f8', '127.0.0.1', '/api/sys/department/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093717130\"]}}', '01', '请求参数', '2020-01-15 21:08:37');
INSERT INTO `sys_system_log` VALUES ('77ddd464-f13f-45f1-9e2d-b37a5c59017b', '127.0.0.1', '/api/sys/dict/item/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"DEPT_TYPE\"],\"_\":[\"1579093717129\"]}}', '02', '返回参数', '2020-01-15 21:08:37');
INSERT INTO `sys_system_log` VALUES ('79ca14bb-2de1-4902-99b0-fb8fb275e623', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093619286\"]}}', '01', '请求参数', '2020-01-15 21:07:07');
INSERT INTO `sys_system_log` VALUES ('7b7aebed-2943-41ff-85fa-0b6e224b8c1f', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091181765\"]}}', '02', '返回参数', '2020-01-15 20:26:22');
INSERT INTO `sys_system_log` VALUES ('7b9ab5ac-4b2d-4b10-926c-641e425e09ba', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"fkDictCategoryCode\":[\"JOB_LOG\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093189516\"]}}', '01', '请求参数', '2020-01-15 20:59:50');
INSERT INTO `sys_system_log` VALUES ('7ba8a206-1f24-45fc-85e5-dbe0152a2998', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090238128\"]}}', '02', '返回参数', '2020-01-15 20:10:38');
INSERT INTO `sys_system_log` VALUES ('7c3d8575-3a2d-4a74-ad61-7b07a9ef2f24', '127.0.0.1', '/api/sys/table/bak/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091174484\"]}}', '02', '返回参数', '2020-01-15 20:26:15');
INSERT INTO `sys_system_log` VALUES ('7cba9b70-d4ae-4983-90fa-aa79a4138e7e', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093715252\"]}}', '01', '请求参数', '2020-01-15 21:08:35');
INSERT INTO `sys_system_log` VALUES ('7d49a35f-876c-4702-b69b-9d2c440d4de0', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093631039\"]}}', '02', '返回参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('7dc18dfe-4f7a-4de0-9d06-65d173c0a2a7', '127.0.0.1', '/api/sys/menu/84df11db-15cf-40a1-9a66-020e52034578', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090974803\"]}}', '02', '返回参数', '2020-01-15 20:22:55');
INSERT INTO `sys_system_log` VALUES ('7e88a217-527c-4b04-9dbc-284e891a4434', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091141012\"]}}', '01', '请求参数', '2020-01-15 20:25:41');
INSERT INTO `sys_system_log` VALUES ('7ef71163-d54d-45ca-ac01-17403e1c0307', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354388\"]}}', '02', '返回参数', '2020-01-15 21:03:04');
INSERT INTO `sys_system_log` VALUES ('80c68451-52f2-4761-ac7a-8835c115ba47', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092832630\"]}}', '02', '返回参数', '2020-01-15 20:54:51');
INSERT INTO `sys_system_log` VALUES ('813c7575-2cad-4c3f-98a9-18c2e8c9d608', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091136460\"]}}', '02', '返回参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('81d85331-314d-43dd-bea3-f12c9942222d', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006841\"]}}', '02', '返回参数', '2020-01-15 21:13:28');
INSERT INTO `sys_system_log` VALUES ('8241f562-7382-450b-9504-5ddc53e99808', '127.0.0.1', '/api/sys/table/bak/tab/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091176596\"]}}', '01', '请求参数', '2020-01-15 20:26:17');
INSERT INTO `sys_system_log` VALUES ('82e9bebe-e3d2-457a-8447-342a66a778b9', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579091087046\"]}}', '02', '返回参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('830b0852-2048-4d1d-ab05-6bb4bb894536', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092832628\"]}}', '01', '请求参数', '2020-01-15 20:53:53');
INSERT INTO `sys_system_log` VALUES ('83f5312c-fc19-4b27-adea-fbfdf46fef8f', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091141013\"]}}', '01', '请求参数', '2020-01-15 20:25:41');
INSERT INTO `sys_system_log` VALUES ('84520589-ef47-4aa6-86f7-3b3dca079ee4', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090448555\"]}}', '01', '请求参数', '2020-01-15 20:14:09');
INSERT INTO `sys_system_log` VALUES ('845c64d3-32db-4488-96c6-ae0b77035f7c', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354389\"]}}', '02', '返回参数', '2020-01-15 21:03:04');
INSERT INTO `sys_system_log` VALUES ('8500c011-96ab-468e-9409-e0b519677bfd', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091136460\"]}}', '01', '请求参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('865b2e82-747d-4b2f-b701-5827444343df', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579091119266\"]}}', '01', '请求参数', '2020-01-15 20:25:19');
INSERT INTO `sys_system_log` VALUES ('8734b49d-c402-406f-8f44-40c24893d043', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092832630\"]}}', '01', '请求参数', '2020-01-15 20:54:51');
INSERT INTO `sys_system_log` VALUES ('889c2dde-b68f-4bf6-af8c-4434d9d0de2a', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006841\"]}}', '01', '请求参数', '2020-01-15 21:13:28');
INSERT INTO `sys_system_log` VALUES ('893b302c-4915-42f4-80c6-692c167cd4e0', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091086786\"]}}', '01', '请求参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('89bae019-57b7-4ef8-b8b7-f30c1f9ace69', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093894701\"]}}', '02', '返回参数', '2020-01-15 21:11:35');
INSERT INTO `sys_system_log` VALUES ('89f68f63-598a-4914-8cb7-0e169a48dd62', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:56:04');
INSERT INTO `sys_system_log` VALUES ('8a565506-9d36-4b40-aebe-e72e89199bf8', '127.0.0.1', '/api/dev/excel/import/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093339263\"]}}', '02', '返回参数', '2020-01-15 21:02:19');
INSERT INTO `sys_system_log` VALUES ('8a78a524-027b-4729-9a0a-1687f2f5ba40', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:53:40');
INSERT INTO `sys_system_log` VALUES ('8a95282f-e51a-489a-aac3-8ea5fef470dd', '127.0.0.1', '/api/current/logout', '{}', '02', '返回参数', '2020-01-15 21:11:20');
INSERT INTO `sys_system_log` VALUES ('8b8d8e54-2945-4fb5-860c-ce2cd92b4fe2', '127.0.0.1', '/api/sys/menu', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:24:45');
INSERT INTO `sys_system_log` VALUES ('8c351123-b21c-45d6-956b-213b0b956754', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006840\"]}}', '01', '请求参数', '2020-01-15 21:13:27');
INSERT INTO `sys_system_log` VALUES ('8cf85a2d-23e7-4dfa-8e5f-74577307bfe1', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093022545\"]}}', '02', '返回参数', '2020-01-15 20:57:03');
INSERT INTO `sys_system_log` VALUES ('8d0790e8-e5bc-4288-a33e-1464dec2c6e4', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093351501\"]}}', '01', '请求参数', '2020-01-15 21:02:32');
INSERT INTO `sys_system_log` VALUES ('8d24880a-b0d9-49e6-bda1-a42ff940c933', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006842\"]}}', '01', '请求参数', '2020-01-15 21:13:32');
INSERT INTO `sys_system_log` VALUES ('8d2cac95-efba-4dc5-8ba2-27fc7803b54d', '127.0.0.1', '/api/dev/excel/import/data/ATT_MANAGE', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 21:02:27');
INSERT INTO `sys_system_log` VALUES ('8d966b9c-2e25-4342-b4cd-c314920cefa1', '127.0.0.1', '/api/sys/menu/84df11db-15cf-40a1-9a66-020e52034578', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090974803\"]}}', '01', '请求参数', '2020-01-15 20:22:55');
INSERT INTO `sys_system_log` VALUES ('8e04e414-6361-4e27-99f3-57784a9a0cc2', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093131367\"]}}', '02', '返回参数', '2020-01-15 20:58:52');
INSERT INTO `sys_system_log` VALUES ('8e5dd7eb-fb0f-489f-871f-2e43796f0d47', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093135978\"]}}', '02', '返回参数', '2020-01-15 20:58:56');
INSERT INTO `sys_system_log` VALUES ('8f16680a-8bbe-4007-93bc-30c5d960f923', '127.0.0.1', '/api/dev/excel/output/exceldownload', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"code\":[\"att_excel\"],\"paramJson\":[\"{startTime:\\\"\\\",endTime:\\\"\\\"}\"],\"sheetName\":[\"考勤查询\"],\"titleName\":[\"考勤查询\"],\"ExcelName\":[\"考勤查询\"]}}', '01', '请求参数', '2020-01-15 20:59:29');
INSERT INTO `sys_system_log` VALUES ('8fb93b57-e7ce-44d7-a6b3-248fcaf0864f', '127.0.0.1', '/api/current/logout', '{}', '02', '返回参数', '2020-01-15 21:11:27');
INSERT INTO `sys_system_log` VALUES ('8fe6e3f1-35a7-4989-ab80-79f972edd955', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090448554\"]}}', '02', '返回参数', '2020-01-15 20:14:09');
INSERT INTO `sys_system_log` VALUES ('908067f4-1043-475c-96ed-e6a564b818c0', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:56:04');
INSERT INTO `sys_system_log` VALUES ('916c86d9-544e-4ca4-9069-bf704f7b6a11', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354391\"]}}', '02', '返回参数', '2020-01-15 21:03:44');
INSERT INTO `sys_system_log` VALUES ('91ced1c1-8c99-47aa-9328-bcf4c33877de', '127.0.0.1', '/api/dev/excel/output/a2ecbb1f-831a-4e84-b450-bd5227bce98d', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092979697\"]}}', '02', '返回参数', '2020-01-15 20:56:20');
INSERT INTO `sys_system_log` VALUES ('928c75b6-6696-4933-9816-7c99844d3092', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-09\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-09\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999086\"]}}', '01', '请求参数', '2020-01-15 21:13:55');
INSERT INTO `sys_system_log` VALUES ('92e8ae47-84f0-44f2-aefa-755f802dd4e7', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2020-01-15\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2020-01-15\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354382\"]}}', '01', '请求参数', '2020-01-15 21:02:36');
INSERT INTO `sys_system_log` VALUES ('933d1745-75ea-48c8-9b2f-2dedd3f08f9e', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093354381\"]}}', '01', '请求参数', '2020-01-15 21:02:34');
INSERT INTO `sys_system_log` VALUES ('93a3a827-f9a6-4fdc-a9c7-0664c0e0684e', '127.0.0.1', '/api/dev/excel/output/exceldownload', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"code\":[\"att_excel\"],\"paramJson\":[\"{date:\\\"2019/12/02\\\"}\"],\"sheetName\":[\"考勤查询\"],\"titleName\":[\"考勤查询\"],\"ExcelName\":[\"考勤查询\"]}}', '02', '返回参数', '2020-01-15 21:07:46');
INSERT INTO `sys_system_log` VALUES ('93d6b56f-704c-46a2-8637-b65da13f6e70', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093721584\"]}}', '02', '返回参数', '2020-01-15 21:08:42');
INSERT INTO `sys_system_log` VALUES ('97d6711b-0fbb-49d6-8569-9027127d17be', '127.0.0.1', '/api/sys/menu/546c6069-5e0f-4700-bd3e-05d8c8fd9a00', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092843315\"]}}', '01', '请求参数', '2020-01-15 20:54:24');
INSERT INTO `sys_system_log` VALUES ('99d7df37-52a2-4608-b163-d6c3a22e2f72', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092896244\"]}}', '02', '返回参数', '2020-01-15 20:54:56');
INSERT INTO `sys_system_log` VALUES ('9b09aee2-c3d0-4042-aa92-a7e925312fe1', '127.0.0.1', '/api/sys/system/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093184179\"]}}', '02', '返回参数', '2020-01-15 20:59:44');
INSERT INTO `sys_system_log` VALUES ('9c5d41ee-4744-499d-834b-f459fa918502', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091153021\"]}}', '01', '请求参数', '2020-01-15 20:25:53');
INSERT INTO `sys_system_log` VALUES ('9c86099f-2998-438b-a830-d31c72eeed08', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579090970875\"]}}', '02', '返回参数', '2020-01-15 20:24:45');
INSERT INTO `sys_system_log` VALUES ('9d849a11-4b6e-463a-859f-696b8234e3c2', '127.0.0.1', '/api/sys/dict/item/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"MENU_SYSTEM\"],\"_\":[\"1579092843314\"]}}', '01', '请求参数', '2020-01-15 20:54:04');
INSERT INTO `sys_system_log` VALUES ('9eb6695a-b8a1-4b6e-8ca4-60b782e0e55d', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093189517\"]}}', '01', '请求参数', '2020-01-15 20:59:50');
INSERT INTO `sys_system_log` VALUES ('9f072ff2-7ca5-4d80-9651-7f1cc380272a', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 21:07:07');
INSERT INTO `sys_system_log` VALUES ('9fc16040-959f-49fd-b1db-b234eebdc228', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579094006839\"]}}', '01', '请求参数', '2020-01-15 21:13:27');
INSERT INTO `sys_system_log` VALUES ('a0df11f9-9229-4ce4-9fba-2a65aa6bb1d6', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093351501\"]}}', '02', '返回参数', '2020-01-15 21:02:32');
INSERT INTO `sys_system_log` VALUES ('a101d649-3340-4e88-949c-e4f2a7514683', '127.0.0.1', '/api/sys/department/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093717130\"]}}', '02', '返回参数', '2020-01-15 21:08:37');
INSERT INTO `sys_system_log` VALUES ('a23393d0-1914-4046-befc-c4b37513c75b', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092975335\"]}}', '01', '请求参数', '2020-01-15 20:56:15');
INSERT INTO `sys_system_log` VALUES ('a2814f62-a7ed-4bf6-a3c8-c0efada0e450', '127.0.0.1', '/api/dev/excel/output', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:58:28');
INSERT INTO `sys_system_log` VALUES ('a2d4a5cb-1f6a-4e65-8ec1-104e50540d63', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090223808\"]}}', '01', '请求参数', '2020-01-15 20:10:34');
INSERT INTO `sys_system_log` VALUES ('a43c1f06-62d2-46fc-a4ea-81dfde5964df', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092955330\"]}}', '02', '返回参数', '2020-01-15 20:56:04');
INSERT INTO `sys_system_log` VALUES ('a4913c75-0c58-43a9-b8bd-8cef0417517f', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090460620\"]}}', '02', '返回参数', '2020-01-15 20:14:21');
INSERT INTO `sys_system_log` VALUES ('a4f189ae-99e9-401c-82b8-786a084aec82', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006842\"]}}', '02', '返回参数', '2020-01-15 21:13:32');
INSERT INTO `sys_system_log` VALUES ('a4f322cf-97b6-48f5-8dd0-4bfc1b59ffeb', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354387\"]}}', '02', '返回参数', '2020-01-15 21:02:46');
INSERT INTO `sys_system_log` VALUES ('a88f3a1b-34c5-45fd-8564-e2a8aec2fbbd', '127.0.0.1', '/api/dev/excel/import/config/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093339264\"]}}', '02', '返回参数', '2020-01-15 21:02:19');
INSERT INTO `sys_system_log` VALUES ('a98e0c55-c895-4bff-bc95-8fe2f7dc0c6d', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092955330\"]}}', '01', '请求参数', '2020-01-15 20:56:04');
INSERT INTO `sys_system_log` VALUES ('a9bac08b-ef8e-4b00-8845-ba65148548ef', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091185482\"]}}', '01', '请求参数', '2020-01-15 20:26:26');
INSERT INTO `sys_system_log` VALUES ('aaf6ff56-b379-4f5b-8f17-67f5d5143a97', '127.0.0.1', '/api/dev/excel/output/exceldownload', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"code\":[\"att_excel\"],\"paramJson\":[\"{startTime:\\\"\\\",endTime:\\\"\\\"}\"],\"sheetName\":[\"考勤查询\"],\"titleName\":[\"考勤查询\"],\"ExcelName\":[\"考勤查询\"]}}', '01', '请求参数', '2020-01-15 21:05:37');
INSERT INTO `sys_system_log` VALUES ('aba25d15-6650-4e59-b4d2-4b5d637e7022', '127.0.0.1', '/api/sys/dict/item/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"DEPT_TYPE\"],\"_\":[\"1579093717129\"]}}', '01', '请求参数', '2020-01-15 21:08:37');
INSERT INTO `sys_system_log` VALUES ('ad5ea053-3c30-4a73-bdde-169a9acace55', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:57:23');
INSERT INTO `sys_system_log` VALUES ('b0bec355-738e-47c6-b2c5-13028c43108c', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354390\"]}}', '02', '返回参数', '2020-01-15 21:03:05');
INSERT INTO `sys_system_log` VALUES ('b1c1fbbe-273b-4de3-9083-5e760d9cdde7', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093723310\"]}}', '02', '返回参数', '2020-01-15 21:08:43');
INSERT INTO `sys_system_log` VALUES ('b2504839-a718-450e-a52e-eb22b1a03b32', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092955329\"]}}', '01', '请求参数', '2020-01-15 20:55:55');
INSERT INTO `sys_system_log` VALUES ('b35968ec-0320-4f40-b82c-89ac9ce8d77c', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093723310\"]}}', '01', '请求参数', '2020-01-15 21:08:43');
INSERT INTO `sys_system_log` VALUES ('b48376d7-ba73-47e2-9d46-0381e527d695', '127.0.0.1', '/api/sys/menu', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:54:50');
INSERT INTO `sys_system_log` VALUES ('b523663b-a1a9-431b-9bc1-87832394ae53', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093634182\"]}}', '02', '返回参数', '2020-01-15 21:07:14');
INSERT INTO `sys_system_log` VALUES ('b5d9ad90-29f5-420e-96c8-c66cb2f1a55d', '127.0.0.1', '/api/dev/custom/module/9a45dba5-3924-4355-9da8-34aa49eb9fad', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091121030\"]}}', '02', '返回参数', '2020-01-15 20:25:21');
INSERT INTO `sys_system_log` VALUES ('b6076698-de74-41f9-9f9b-62f8a0493d83', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091153022\"]}}', '02', '返回参数', '2020-01-15 20:25:55');
INSERT INTO `sys_system_log` VALUES ('b688445d-bbfd-4ea7-a0d1-025466438d9d', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579090448074\"]}}', '02', '返回参数', '2020-01-15 20:14:08');
INSERT INTO `sys_system_log` VALUES ('b7fb055c-f010-441b-8f41-6ce81829170b', '127.0.0.1', '/api/sys/dict/item/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"MENU_SYSTEM\"],\"_\":[\"1579090974804\"]}}', '02', '返回参数', '2020-01-15 20:22:55');
INSERT INTO `sys_system_log` VALUES ('b97a2df0-e8de-4c58-b9d2-73343d1ee045', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093022545\"]}}', '01', '请求参数', '2020-01-15 20:57:03');
INSERT INTO `sys_system_log` VALUES ('ba2888be-e2e2-413d-9a10-c9ca3e3ab576', '127.0.0.1', '/api/dev/excel/import/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093339263\"]}}', '01', '请求参数', '2020-01-15 21:02:19');
INSERT INTO `sys_system_log` VALUES ('bb18b771-0666-44d6-abbe-d91a6fda305e', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093174837\"]}}', '01', '请求参数', '2020-01-15 20:59:35');
INSERT INTO `sys_system_log` VALUES ('bc06c744-ddae-4982-9352-58e5dcb16206', '127.0.0.1', '/api/job/cron/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093859307\"]}}', '01', '请求参数', '2020-01-15 21:11:00');
INSERT INTO `sys_system_log` VALUES ('bcad953c-63e6-4481-a7d7-1c5fa2e7af2f', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092967793\"]}}', '02', '返回参数', '2020-01-15 20:56:08');
INSERT INTO `sys_system_log` VALUES ('bd3eb6cc-81e9-4c8d-97d8-d8232892419c', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579092896275\"]}}', '02', '返回参数', '2020-01-15 20:54:56');
INSERT INTO `sys_system_log` VALUES ('bdc0ce48-19fe-4d6f-9854-2f4321aa596f', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-09\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-09\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999087\"]}}', '01', '请求参数', '2020-01-15 21:13:56');
INSERT INTO `sys_system_log` VALUES ('be8c7854-e91a-4b8e-b723-e5d6a245c2c8', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091119268\"]}}', '01', '请求参数', '2020-01-15 20:25:33');
INSERT INTO `sys_system_log` VALUES ('c1000ba1-edd4-4819-ac39-daf87cc818bd', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093630900\"]}}', '01', '请求参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('c2d29c71-85b9-4ad1-b38a-517236b43f56', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579091091788\"]}}', '02', '返回参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('c38a5a41-f72b-4dd0-953d-51b9e3cd53e2', '127.0.0.1', '/api/dev/excel/import/config/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093980305\"]}}', '02', '返回参数', '2020-01-15 21:13:01');
INSERT INTO `sys_system_log` VALUES ('c3963274-2867-4eee-b062-e49bc43320dd', '127.0.0.1', '/api/sys/user/password', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"id\":[\"893050be-18eb-4682-9921-1109c10235a4\"],\"oldPwd\":[\"zysoft\"],\"newPwd\":[\"123456\"],\"confirmPwd\":[\"123456\"]}}', '02', '返回参数', '2020-01-15 21:11:16');
INSERT INTO `sys_system_log` VALUES ('c6baee95-7c00-4b12-bde0-b6e38ecd2c3a', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093131493\"]}}', '02', '返回参数', '2020-01-15 20:58:52');
INSERT INTO `sys_system_log` VALUES ('c7c8dfbe-75ba-445b-a9c3-4cefd0f4361b', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092832629\"]}}', '01', '请求参数', '2020-01-15 20:53:58');
INSERT INTO `sys_system_log` VALUES ('c856f064-2d3f-4819-858d-89205da0a060', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091181765\"]}}', '01', '请求参数', '2020-01-15 20:26:22');
INSERT INTO `sys_system_log` VALUES ('c864eecd-ce54-4be8-9602-b2c68a91e974', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579090970874\"]}}', '02', '返回参数', '2020-01-15 20:22:51');
INSERT INTO `sys_system_log` VALUES ('c91a602d-072f-46de-9bf5-cee9c57e434a', '127.0.0.1', '/api/sys/tree/dept', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"state\":[\"1\"],\"code\":[\"01\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579091119266\"]}}', '02', '返回参数', '2020-01-15 20:25:19');
INSERT INTO `sys_system_log` VALUES ('c96cc4e8-14da-49ff-afea-b52530fbccdd', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354385\"]}}', '02', '返回参数', '2020-01-15 21:02:45');
INSERT INTO `sys_system_log` VALUES ('c9fad0eb-1fa2-4634-9bb1-6fe61de56cef', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092977057\"]}}', '01', '请求参数', '2020-01-15 20:56:17');
INSERT INTO `sys_system_log` VALUES ('ca0898e9-e5b5-4027-80b1-6befd5ab1609', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093865283\"]}}', '01', '请求参数', '2020-01-15 21:11:05');
INSERT INTO `sys_system_log` VALUES ('ca608b56-cc3c-476a-a242-e7906e34ec37', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091136504\"]}}', '02', '返回参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('ca6978c5-ec5f-4760-8ffb-aefe6860fdc8', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093894747\"]}}', '01', '请求参数', '2020-01-15 21:11:35');
INSERT INTO `sys_system_log` VALUES ('cb61de71-4c5e-4470-a45c-e1a92c51c0b4', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579091121031\"]}}', '02', '返回参数', '2020-01-15 20:25:21');
INSERT INTO `sys_system_log` VALUES ('cc84f104-cdc9-4525-8ec3-a45af22b9d06', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2020-01-15\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2020-01-15\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093135979\"]}}', '01', '请求参数', '2020-01-15 20:59:25');
INSERT INTO `sys_system_log` VALUES ('cf7a5100-0779-4807-a608-e20ce8d06144', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 21:07:07');
INSERT INTO `sys_system_log` VALUES ('cf918cb9-bc99-4d63-b792-516addee5193', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354390\"]}}', '01', '请求参数', '2020-01-15 21:03:05');
INSERT INTO `sys_system_log` VALUES ('d0795830-c01d-45fc-a56d-6010893fc231', '127.0.0.1', '/api/dev/excel/output/exceldownload', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"code\":[\"att_excel\"],\"paramJson\":[\"{startTime:\\\"\\\",endTime:\\\"\\\"}\"],\"sheetName\":[\"考勤查询\"],\"titleName\":[\"考勤查询\"],\"ExcelName\":[\"考勤查询\"]}}', '01', '请求参数', '2020-01-15 21:05:44');
INSERT INTO `sys_system_log` VALUES ('d1f99fe2-2507-4dee-a666-3c857fee69c9', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-01\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-01\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354383\"]}}', '01', '请求参数', '2020-01-15 21:02:39');
INSERT INTO `sys_system_log` VALUES ('d24ed033-8cc3-4a34-b6d4-7f6381db1f0a', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093351504\"]}}', '01', '请求参数', '2020-01-15 21:02:50');
INSERT INTO `sys_system_log` VALUES ('d26f23e3-92fc-4716-89df-c86d9e5b89c3', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354388\"]}}', '01', '请求参数', '2020-01-15 21:03:04');
INSERT INTO `sys_system_log` VALUES ('d3d9c43a-a4f4-4355-ab12-6e697b857ac5', '127.0.0.1', '/api/dev/excel/output/a2ecbb1f-831a-4e84-b450-bd5227bce98d', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093046536\"]}}', '02', '返回参数', '2020-01-15 20:57:27');
INSERT INTO `sys_system_log` VALUES ('d53ed352-a121-46bf-8f42-2172a462f352', '127.0.0.1', '/api/dev/param/category/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091169243\"]}}', '01', '请求参数', '2020-01-15 20:26:09');
INSERT INTO `sys_system_log` VALUES ('d54592f5-c32d-4a7c-b769-011c67c62879', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091136459\"]}}', '02', '返回参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('d5da4c1a-4426-4c81-8e5e-3fa043eafa1b', '127.0.0.1', '/api/dev/custom/module/code/ATT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091094271\"]}}', '01', '请求参数', '2020-01-15 20:24:54');
INSERT INTO `sys_system_log` VALUES ('d62290d8-1a6d-42c4-b094-16e51a5c7016', '127.0.0.1', '/api/dev/excel/output/exceldownload', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"code\":[\"att_excel\"],\"paramJson\":[\"{startTime:\\\"\\\",endTime:\\\"\\\"}\"],\"sheetName\":[\"考勤查询\"],\"titleName\":[\"考勤查询\"],\"ExcelName\":[\"考勤查询\"]}}', '01', '请求参数', '2020-01-15 21:04:57');
INSERT INTO `sys_system_log` VALUES ('d67af965-1544-4f81-810a-8db5aec379d9', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579093131493\"]}}', '01', '请求参数', '2020-01-15 20:58:52');
INSERT INTO `sys_system_log` VALUES ('d67cf32b-80f6-45e5-91a1-cb956343f33a', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091141014\"]}}', '01', '请求参数', '2020-01-15 20:25:43');
INSERT INTO `sys_system_log` VALUES ('d74f2160-26ea-4758-9117-f5a5d7dd3712', '127.0.0.1', '/api/job/task/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091152028\"]}}', '02', '返回参数', '2020-01-15 20:25:52');
INSERT INTO `sys_system_log` VALUES ('d7779578-db1d-45f9-ade0-524e99c7c5f9', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092896620\"]}}', '01', '请求参数', '2020-01-15 20:54:57');
INSERT INTO `sys_system_log` VALUES ('d799937c-325f-4e25-8722-75a15481e982', '127.0.0.1', '/api/dev/excel/import/data/ATT_MANAGE', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 21:02:27');
INSERT INTO `sys_system_log` VALUES ('d862db6a-e396-497a-a64d-08248fecb51e', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2020-01-15\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2020-01-15\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093135979\"]}}', '02', '返回参数', '2020-01-15 20:59:26');
INSERT INTO `sys_system_log` VALUES ('d8c3cbe5-5c6f-486f-87cb-24486b3e2dc7', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092975336\"]}}', '01', '请求参数', '2020-01-15 20:57:23');
INSERT INTO `sys_system_log` VALUES ('d8f54e04-6094-42da-acd7-446fa573b9a1', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092832628\"]}}', '02', '返回参数', '2020-01-15 20:53:53');
INSERT INTO `sys_system_log` VALUES ('d9b166c0-36f5-49ba-9397-5d498dbd7485', '127.0.0.1', '/api/dev/excel/output/exceldownload', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"code\":[\"att_excel\"],\"paramJson\":[\"{date:\\\"2019/12/02\\\"}\"],\"sheetName\":[\"考勤查询\"],\"titleName\":[\"考勤查询\"],\"ExcelName\":[\"考勤查询\"]}}', '01', '请求参数', '2020-01-15 21:07:46');
INSERT INTO `sys_system_log` VALUES ('da44c197-c4bf-47b5-a1c1-51b9eea82d62', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019/12/02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019/12/02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354392\"]}}', '01', '请求参数', '2020-01-15 21:04:54');
INSERT INTO `sys_system_log` VALUES ('da4b1d42-bc58-49e9-b022-647e381d7a3e', '127.0.0.1', '/api/sys/menu/7a689910-9265-4fd3-b45f-5ba908364c6c', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092843313\"]}}', '02', '返回参数', '2020-01-15 20:54:03');
INSERT INTO `sys_system_log` VALUES ('dbac8928-cb62-4180-ae8d-17ff945d2680', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093715154\"]}}', '01', '请求参数', '2020-01-15 21:08:35');
INSERT INTO `sys_system_log` VALUES ('dbb60afc-a977-4032-af97-cacbbee89895', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092977058\"]}}', '01', '请求参数', '2020-01-15 20:57:00');
INSERT INTO `sys_system_log` VALUES ('dcf74bb1-1b1d-4874-be7b-8e9d1becd645', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579091121031\"]}}', '01', '请求参数', '2020-01-15 20:25:21');
INSERT INTO `sys_system_log` VALUES ('dd203390-6716-4401-ad64-53b83ba6a7cd', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093630899\"]}}', '01', '请求参数', '2020-01-15 21:07:11');
INSERT INTO `sys_system_log` VALUES ('ddc37100-7f50-4c45-824e-862bbf4a37e5', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091141015\"]}}', '01', '请求参数', '2020-01-15 20:25:43');
INSERT INTO `sys_system_log` VALUES ('de04c4cd-2f47-47a6-83ab-94a90379aee7', '127.0.0.1', '/api/dev/excel/output', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '02', '返回参数', '2020-01-15 20:57:00');
INSERT INTO `sys_system_log` VALUES ('df3f8cff-5b57-4fe4-81f0-504131140f1e', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"fkDictCategoryCode\":[\"JOB_LOG\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091153020\"]}}', '02', '返回参数', '2020-01-15 20:25:53');
INSERT INTO `sys_system_log` VALUES ('e15bf138-f168-4235-b6e9-8cf88574e877', '127.0.0.1', '/api/sys/system/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093184179\"]}}', '01', '请求参数', '2020-01-15 20:59:44');
INSERT INTO `sys_system_log` VALUES ('e176afc0-c4a0-4cfc-bddf-2b81907a4a9d', '127.0.0.1', '/api/sys/table/bak/tab/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093731795\"]}}', '01', '请求参数', '2020-01-15 21:08:52');
INSERT INTO `sys_system_log` VALUES ('e1f70fed-80c1-410e-a36e-357c8b15ed3a', '127.0.0.1', '/api/dev/report/config/717aed28-702a-4fdf-8741-c455bbac9467', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093621215\"]}}', '02', '返回参数', '2020-01-15 21:07:02');
INSERT INTO `sys_system_log` VALUES ('e2d8cb88-273b-44d6-bd3c-d07f78610cba', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092900595\"]}}', '02', '返回参数', '2020-01-15 20:55:01');
INSERT INTO `sys_system_log` VALUES ('e335129f-373f-43e4-ad57-5f09d1e27356', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091136794\"]}}', '02', '返回参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('e378a2b8-3596-42a5-bdb3-9f5ff7a1c62f', '127.0.0.1', '/api/sys/table/bak/tab/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093731795\"]}}', '02', '返回参数', '2020-01-15 21:08:52');
INSERT INTO `sys_system_log` VALUES ('e49352a4-4357-414d-b5a7-63f8f9435667', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090448556\"]}}', '01', '请求参数', '2020-01-15 20:21:03');
INSERT INTO `sys_system_log` VALUES ('e6665b8a-f0ab-47c9-a668-95ad661e45da', '127.0.0.1', '/api/dev/excel/import/config/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579093980306\"]}}', '02', '返回参数', '2020-01-15 21:13:01');
INSERT INTO `sys_system_log` VALUES ('e6967181-d458-4e21-ba8f-40bcba29c8bc', '127.0.0.1', '/api/dev/excel/output', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:56:59');
INSERT INTO `sys_system_log` VALUES ('e6abfa84-cab0-4a70-ac42-7ba2e368b780', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090223806\"]}}', '01', '请求参数', '2020-01-15 20:10:24');
INSERT INTO `sys_system_log` VALUES ('e6dbf8bc-73a5-4c9a-87a1-6500ea253a1d', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092857585\"]}}', '02', '返回参数', '2020-01-15 20:54:18');
INSERT INTO `sys_system_log` VALUES ('e700854f-85aa-4965-b904-152352cb60d2', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"789c2078-83b1-458b-a1fd-dc8d395c8e2c\"],\"_\":[\"1579093881838\"]}}', '02', '返回参数', '2020-01-15 21:11:22');
INSERT INTO `sys_system_log` VALUES ('e7b319e8-0e67-4446-99a3-36c17362f94e', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091086785\"]}}', '01', '请求参数', '2020-01-15 20:24:47');
INSERT INTO `sys_system_log` VALUES ('e87bf925-402e-4d3c-ba4f-b5ee2d8c0224', '127.0.0.1', '/api/job/log/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkJobLogCode\":[\"01\"],\"startTime\":[\"2019-12-16\"],\"endTime\":[\"2020-01-16\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093189517\"]}}', '02', '返回参数', '2020-01-15 20:59:50');
INSERT INTO `sys_system_log` VALUES ('e89657d2-09a6-45d6-9207-3bd83fca376d', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-04\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-04\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093354386\"]}}', '01', '请求参数', '2020-01-15 21:02:45');
INSERT INTO `sys_system_log` VALUES ('e8f16765-524c-467d-8f41-cdd75ebf907a', '127.0.0.1', '/api/dev/param/group/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkParamCategoryCode\":[\"SYS_PARAM\"],\"_\":[\"1579091169244\"]}}', '02', '返回参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('ea390614-206b-4f5c-88dd-e33de4ed0491', '127.0.0.1', '/api/current/user/info', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092896244\"]}}', '01', '请求参数', '2020-01-15 20:54:56');
INSERT INTO `sys_system_log` VALUES ('ed31a56c-3842-44c9-8980-6bffd7eb4ebc', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091094272\"]}}', '01', '请求参数', '2020-01-15 20:24:55');
INSERT INTO `sys_system_log` VALUES ('eeeabc74-cac5-42fd-ad1a-210472ac52c1', '127.0.0.1', '/api/dev/excel/output/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092977057\"]}}', '02', '返回参数', '2020-01-15 20:56:17');
INSERT INTO `sys_system_log` VALUES ('ef323c81-846b-4889-804e-64bfac9b9b03', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090448556\"]}}', '02', '返回参数', '2020-01-15 20:21:03');
INSERT INTO `sys_system_log` VALUES ('ef53ca0b-fefa-48b9-b66c-4d36f2dc1e26', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999083\"]}}', '02', '返回参数', '2020-01-15 21:13:25');
INSERT INTO `sys_system_log` VALUES ('ef99c160-c646-4eef-9101-4d3337187647', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579092846607\"]}}', '02', '返回参数', '2020-01-15 20:54:07');
INSERT INTO `sys_system_log` VALUES ('f1b318ca-9ded-4a1d-b753-0007314fc0b8', '127.0.0.1', '/api/dev/report/config/code/ATT_REPORT', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"_\":[\"1579093999082\"]}}', '01', '请求参数', '2020-01-15 21:13:19');
INSERT INTO `sys_system_log` VALUES ('f1f10f12-f6ba-483e-8e24-b7ea3ae29947', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579092955329\"]}}', '02', '返回参数', '2020-01-15 20:55:56');
INSERT INTO `sys_system_log` VALUES ('f350610a-4d2e-4f1f-b0a9-1bef0188ce95', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-01\"],\"endTime\":[\"2020-01-22\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579090223807\"]}}', '01', '请求参数', '2020-01-15 20:10:30');
INSERT INTO `sys_system_log` VALUES ('f3c7aae5-84c7-4180-887e-1eac007a505f', '127.0.0.1', '/api/dev/custom/module/9a45dba5-3924-4355-9da8-34aa49eb9fad', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579090865677\"]}}', '01', '请求参数', '2020-01-15 20:21:06');
INSERT INTO `sys_system_log` VALUES ('f451dcce-fb13-4bdb-837a-6996b29d2a38', '127.0.0.1', '/api/sys/dict/item/page/list', '{{\"fkDictCategoryCode\":[\"JOB_LOG\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579093189516\"]}}', '02', '返回参数', '2020-01-15 20:59:50');
INSERT INTO `sys_system_log` VALUES ('f54d4ed2-570f-4dc5-b4b1-f24cfaaad9c9', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019/12/02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019/12/02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093634183\"]}}', '02', '返回参数', '2020-01-15 21:07:32');
INSERT INTO `sys_system_log` VALUES ('f5a4474e-9330-4fa0-80a9-9cec48b3e007', '127.0.0.1', '/api/sys/menu/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"_\":[\"1579090970875\"]}}', '01', '请求参数', '2020-01-15 20:24:45');
INSERT INTO `sys_system_log` VALUES ('f5b42b7a-148d-455a-9fdf-e4961c770633', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091183877\"]}}', '02', '返回参数', '2020-01-15 20:26:24');
INSERT INTO `sys_system_log` VALUES ('f61b57f9-e066-448c-afe6-4bc933294b89', '127.0.0.1', '/api/sys/dict/item/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDictCategoryCode\":[\"MENU_SYSTEM\"],\"_\":[\"1579090974804\"]}}', '01', '请求参数', '2020-01-15 20:22:55');
INSERT INTO `sys_system_log` VALUES ('f6318a49-475d-4051-bbcd-8412feea5c19', '127.0.0.1', '/api/sys/user/password', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"id\":[\"893050be-18eb-4682-9921-1109c10235a4\"],\"oldPwd\":[\"zysoft\"],\"newPwd\":[\"123456\"],\"confirmPwd\":[\"123456\"]}}', '01', '请求参数', '2020-01-15 21:11:15');
INSERT INTO `sys_system_log` VALUES ('f6364e6d-877f-4eaa-bfd1-a22f48ad97ab', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"startTime\":[\"2019-12-01\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579094006843\"]}}', '01', '请求参数', '2020-01-15 21:13:33');
INSERT INTO `sys_system_log` VALUES ('f644eadb-cfb7-4767-a8df-27c471903128', '127.0.0.1', '/api/dev/report/config/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093619286\"]}}', '02', '返回参数', '2020-01-15 21:07:07');
INSERT INTO `sys_system_log` VALUES ('f72633b3-aaac-40b1-88c9-946693b9ef42', '127.0.0.1', '/api/dev/report/config/apply', '{{\"code\":[\"ATT_REPORT\"],\"access_token\":[\"fabfd6cf-1546-44d5-829e-b2e147f90ae9\"],\"paramJson\":[\"{\\\"date\\\":\\\"2019-12-02\\\",\\\"keyword\\\":\\\"\\\"}\"],\"date\":[\"2019-12-02\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093999084\"]}}', '02', '返回参数', '2020-01-15 21:13:50');
INSERT INTO `sys_system_log` VALUES ('f7b838c1-1f33-4993-9e87-adab0a20c185', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091091368\"]}}', '02', '返回参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('f89855e2-4ff4-4873-9e31-1528baeba5ae', '127.0.0.1', '/api/job/cron/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093859307\"]}}', '02', '返回参数', '2020-01-15 21:11:00');
INSERT INTO `sys_system_log` VALUES ('f976a8fd-8755-4b3b-9635-ecf6d02671cc', '127.0.0.1', '/api/sys/dict/category/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579091169707\"]}}', '02', '返回参数', '2020-01-15 20:26:10');
INSERT INTO `sys_system_log` VALUES ('fb918cea-ec00-45b4-b081-710b7cb05c78', '127.0.0.1', '/api/dev/custom/module', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:25:33');
INSERT INTO `sys_system_log` VALUES ('fc88ead0-af05-4485-ac44-d80dfe830d99', '127.0.0.1', '/api/sys/user/menu/favorites/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091136504\"]}}', '01', '请求参数', '2020-01-15 20:25:37');
INSERT INTO `sys_system_log` VALUES ('fcf0a469-d7d2-4c6c-a135-225eeb9146cc', '127.0.0.1', '/api/dev/report/config', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"]}}', '01', '请求参数', '2020-01-15 20:57:23');
INSERT INTO `sys_system_log` VALUES ('feb580f9-1c97-4c38-9ccb-7e4544ea03c9', '127.0.0.1', '/api/dev/custom/module/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptCode\":[\"001\"],\"keyword\":[\"\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579091119268\"]}}', '02', '返回参数', '2020-01-15 20:25:33');
INSERT INTO `sys_system_log` VALUES ('feb65bd6-aa58-4724-8c69-002505447db5', '127.0.0.1', '/api/sys/menu/tree', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkSystem\":[\"001\"],\"_\":[\"1579091091368\"]}}', '01', '请求参数', '2020-01-15 20:24:52');
INSERT INTO `sys_system_log` VALUES ('feb8406a-b7a9-496b-a248-6b5722c88844', '127.0.0.1', '/api/sys/menu/546c6069-5e0f-4700-bd3e-05d8c8fd9a00', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"_\":[\"1579092843315\"]}}', '02', '返回参数', '2020-01-15 20:54:25');
INSERT INTO `sys_system_log` VALUES ('ff2e018b-7d95-446a-b2b3-a3e057fa9c0e', '127.0.0.1', '/api/att/manage/page/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"startTime\":[\"2020-01-15\"],\"endTime\":[\"2020-01-22\"],\"pageNum\":[\"1\"],\"pageSize\":[\"20\"],\"_\":[\"1579093723311\"]}}', '01', '请求参数', '2020-01-15 21:08:44');
INSERT INTO `sys_system_log` VALUES ('ffecbc96-4d35-49fa-ada8-d586f3465820', '127.0.0.1', '/api/sys/department/list', '{{\"access_token\":[\"e3431d62-576b-4dec-96aa-51034f91575d\"],\"fkDeptTypeCode\":[\"01\"],\"_\":[\"1579090240689\"]}}', '02', '返回参数', '2020-01-15 20:10:41');

-- ----------------------------
-- Table structure for sys_table_bak_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_bak_job`;
CREATE TABLE `sys_table_bak_job` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(64) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `TABLE_SCHEMA` varchar(64) DEFAULT NULL COMMENT '库名',
  `TABLE_NAME` varchar(128) DEFAULT NULL COMMENT '表名',
  `CONDITIONS` varchar(512) DEFAULT NULL COMMENT '条件',
  `STATE` tinyint(4) DEFAULT NULL COMMENT '状态(0不可用，1可用)',
  `IS_CLEAR_TABLE_DATA` tinyint(4) DEFAULT NULL COMMENT '是否清除原表查询数据',
  `IS_CLEAR_BAK_DATA` tinyint(4) DEFAULT NULL COMMENT '是否清空备份表数据',
  `IS_EXECUTED` tinyint(4) DEFAULT '1' COMMENT '是否执行过(0未执行，1已执行)',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据库表备份清理任务表';

-- ----------------------------
-- Records of sys_table_bak_job
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `NAME` varchar(100) NOT NULL COMMENT '用户姓名',
  `LOGIN_CODE` varchar(50) NOT NULL COMMENT '登录账号',
  `PASSWORD` varchar(1024) NOT NULL COMMENT '用户密码',
  `EMAIL` varchar(128) DEFAULT NULL COMMENT '用户邮箱',
  `PHONE` varchar(50) NOT NULL COMMENT '手机号码',
  `SEX` int(11) DEFAULT NULL COMMENT '性别',
  `STATE` int(11) DEFAULT '1' COMMENT '用户状态',
  `FK_DEPARTMENT_CODE` varchar(64) DEFAULT NULL COMMENT '科室编码',
  `PY_CODE` varchar(50) DEFAULT NULL COMMENT '拼音首码',
  `WB_CODE` varchar(50) DEFAULT NULL COMMENT '五笔码',
  `ADD_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `ADD_USER_ID` varchar(36) DEFAULT NULL COMMENT '添加用户时间',
  `MODIFY_TIME` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `MODIFY_USER_ID` varchar(36) DEFAULT NULL COMMENT '修改人',
  `FK_JOB_CODE` varchar(64) DEFAULT NULL COMMENT '岗位名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('893050be-18eb-4682-9921-1109c10235a4', '超级管理员', 'admin', '$2a$10$nTsj4fiwon9Ab6rsfxapsu6UlgrbtJ6JHg.JhISCkyzNQ0usOuTke', '864994956@qq.com', '15206035962', '1', '1', '001', '', '', '2019-03-25 09:41:00', null, '2020-01-03 20:55:55', null, '001');

-- ----------------------------
-- Table structure for sys_user_menu_favorites
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_menu_favorites`;
CREATE TABLE `sys_user_menu_favorites` (
  `ID` varchar(36) NOT NULL COMMENT '主键',
  `FK_USER_ID` varchar(36) NOT NULL COMMENT '用户Id',
  `FK_MENU_ID` varchar(36) NOT NULL COMMENT '菜单Id',
  `SORT` int(11) DEFAULT '1',
  `FK_SYSTEM` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户常用菜单设置';

-- ----------------------------
-- Records of sys_user_menu_favorites
-- ----------------------------
INSERT INTO `sys_user_menu_favorites` VALUES ('753d2e4a-1dad-46b3-b104-132bbab613e5', '893050be-18eb-4682-9921-1109c10235a4', 'aa0c976c-4e75-4b7b-b42b-7971f726e384', '1', '001');
