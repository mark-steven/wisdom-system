# wisdom-system

#### 介绍
智慧CRM初心是做一款企业级的集OA、财务、客户管理等系统。
#### 代码生成
项目采用代码生成，减轻开发周期，剩下的多表关联自行扩展，项目会持续不断开发（业余时间），代码生成地址详见：
https://gitee.com/mark-steven/wisdom-system/blob/master/doc/%E4%BD%BF%E7%94%A8%E6%B5%81%E7%A8%8B.md
#### 集成第三方登录+oauth2.0样式（暂时只写这几种第三方登录，还有很多没在前端页面体现，但是后端支持，可自行开发）
第三方登录参考：https://gitee.com/yadong.zhang/JustAuth
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/155619_535aaaf3_1690182.png "微信截图_20200217155416.png")
#### 软件架构

1.后端：SpringBoot2.2.2 + Spring + SpringMvc + Mybatis（tk.mybatis）

2.安全框架：SpringSecurity + oauth2.0

3.前端框架：dhtmlx + LayUi（dhtmlx组件是由位于俄罗斯圣彼得堡的DHTMLX公司开发的，适用于B/S模式的Web应用开发）

4.持久层：Mysql、Redis

5.前端参考：dhtmlxhttps://docs.dhtmlx.com/layout__patterns.html

6.前端页面布局

![输入图片说明](https://images.gitee.com/uploads/images/2020/0116/144306_71b73450_1690182.png "微信截图_20200116144236.png")

#### 启动教程
注意：1、2步骤必须需要启动，3、4步骤看个人需求启动

1.ApiApplication 先启动api接口

2.ResourceApplication 在启动静态资源

3.ManageApplication 启动前端页面（管理平台）

4.登录账户：admin 密码：123456（这边需要注意如果使用该账号报密码错误，一般是你没有装redis需要配置redis后才能登录）

#### 部署流程（后续出docker部署流程，暂时可以简便部署和平时发布一样）

1.先根据自己磁盘定义打包路径（图示）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/194749_84376738_1690182.png "微信截图_20200217194309.png")

2.打包路径下将包发布的服务器（api.jar是接口，manage.war是前端界面），建议使用docker部署（图示）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/194923_9afde866_1690182.png "微信截图_20200217194349.png")

3.静态资源单独部署，但部署前应该将前端wisdom-system-all-client下的wisdom-system-resource包下的resource中的static单独发布即可不需要打包直接将static扔上去（记住这边一定要先修改wisdom-system-all-client下的wisdom-system-manage包下的resource下的application-dev.yml的静态资源引用地址）。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/195432_f29425b4_1690182.png "微信截图_20200217194543.png")

4.结合第三点修改，因为是前后端分离（图示）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/195617_352da191_1690182.png "微信截图_20200217195555.png")
#### 特色功能介绍（以下配置在下载项目后运行可以看到）

1.展示配置：不需要在项目中写前端、后端代码，直接在开发配置中的展示配置配置代码和SQL就可以,界面生成需要在项目中doc下有个tojson.html需要使用这个将界面的代码转成json格式。

2.导出配置：个人数据需要导出数据时在上线后也不需要写代码，直接在这个功能下配置Excel即可，
这个可能和展示配置是搭档，需要一起用的。

3.界面配置：此功能是配置列和搜索框可以自定义，需要哪些列和搜索栏都可以自定义配置。

4.Excel导入配置：Excel导入配置可以先定义好模板在执行批量导入。

5.系统中以考勤为例导入数据，已经配置好和附带考勤的Excel文档，其中考勤也有配置好的，直接参考学习。

6.剩余的是权限功能、调度模板相关，下载代码跑起来可以看到效果。

7.系统包含限流切面、日志切面、后续考虑加入中间件MQ。

8.系统是通过Oauth2.0 密码授权方式做登录认证。

9.新增第三方授权方式Github、Gitee、微博、钉钉、百度、Coding、腾讯云开发者平台、OSChina、支付宝、QQ、微信、淘宝、Google、Facebook、抖音、领英、小米、微软、今日头条、Teambition、StackOverflow、Pinterest、人人、华为、企业微信、酷家乐、Gitlab、美团、饿了么和推特等第三方平台的授权登录。

